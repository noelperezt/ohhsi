/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : proyectos

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-01-09 17:06:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for app_historico
-- ----------------------------
DROP TABLE IF EXISTS `app_historico`;
CREATE TABLE `app_historico` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tabla` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `concepto` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `idregistro` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `usuario` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5908 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of app_historico
-- ----------------------------
INSERT INTO `app_historico` VALUES ('1', 'app_perfil', 'creado', '1', 'Invitado', '2017-01-09 10:50:51', '2017-01-09 10:50:51');
INSERT INTO `app_historico` VALUES ('2', 'app_perfil', 'creado', '2', 'Invitado', '2017-01-09 10:50:51', '2017-01-09 10:50:51');
INSERT INTO `app_historico` VALUES ('3', 'app_perfil', 'creado', '3', 'Invitado', '2017-01-09 10:50:51', '2017-01-09 10:50:51');
INSERT INTO `app_historico` VALUES ('4', 'app_perfil', 'creado', '4', 'Invitado', '2017-01-09 10:50:51', '2017-01-09 10:50:51');
INSERT INTO `app_historico` VALUES ('5', 'app_perfil', 'creado', '5', 'Invitado', '2017-01-09 10:50:51', '2017-01-09 10:50:51');
INSERT INTO `app_historico` VALUES ('6', 'app_perfil', 'creado', '6', 'Invitado', '2017-01-09 10:50:51', '2017-01-09 10:50:51');
INSERT INTO `app_historico` VALUES ('7', 'app_usuario', 'creado', '1', 'Invitado', '2017-01-09 10:50:51', '2017-01-09 10:50:51');
INSERT INTO `app_historico` VALUES ('8', 'autenticacion', 'autenticacion', '', 'admin', '2017-01-09 10:51:20', '2017-01-09 10:51:20');
INSERT INTO `app_historico` VALUES ('9', 'autenticacion', 'autenticacion', '', 'admin', '2017-01-09 15:06:15', '2017-01-09 15:06:15');
INSERT INTO `app_historico` VALUES ('10', 'autenticacion', 'autenticacion', '', 'admin', '2017-01-09 16:13:18', '2017-01-09 16:13:18');
INSERT INTO `app_historico` VALUES ('3017', 'proyectos', 'creado', '3004', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3018', 'proyectos', 'creado', '3005', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3019', 'proyectos', 'creado', '3006', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3020', 'proyectos', 'creado', '3007', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3021', 'proyectos', 'creado', '3008', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3022', 'proyectos', 'creado', '3009', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3023', 'proyectos', 'creado', '3010', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3024', 'proyectos', 'creado', '3011', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3025', 'proyectos', 'creado', '3012', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3026', 'proyectos', 'creado', '3013', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3027', 'proyectos', 'creado', '3014', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3028', 'proyectos', 'creado', '3015', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3029', 'proyectos', 'creado', '3016', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3030', 'proyectos', 'creado', '3017', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3031', 'proyectos', 'creado', '3018', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3032', 'proyectos', 'creado', '3019', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3033', 'proyectos', 'creado', '3020', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3034', 'proyectos', 'creado', '3021', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3035', 'proyectos', 'creado', '3022', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3036', 'proyectos', 'creado', '3023', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3037', 'proyectos', 'creado', '3024', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3038', 'proyectos', 'creado', '3025', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3039', 'proyectos', 'creado', '3026', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3040', 'proyectos', 'creado', '3027', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3041', 'proyectos', 'creado', '3028', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3042', 'proyectos', 'creado', '3029', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3043', 'proyectos', 'creado', '3030', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3044', 'proyectos', 'creado', '3031', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3045', 'proyectos', 'creado', '3032', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3046', 'proyectos', 'creado', '3033', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3047', 'proyectos', 'creado', '3034', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3048', 'proyectos', 'creado', '3035', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3049', 'proyectos', 'creado', '3036', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3050', 'proyectos', 'creado', '3037', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3051', 'proyectos', 'creado', '3038', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3052', 'proyectos', 'creado', '3039', 'admin', '2017-01-09 16:46:55', '2017-01-09 16:46:55');
INSERT INTO `app_historico` VALUES ('3053', 'proyectos', 'creado', '3040', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3054', 'proyectos', 'creado', '3041', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3055', 'proyectos', 'creado', '3042', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3056', 'proyectos', 'creado', '3043', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3057', 'proyectos', 'creado', '3044', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3058', 'proyectos', 'creado', '3045', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3059', 'proyectos', 'creado', '3046', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3060', 'proyectos', 'creado', '3047', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3061', 'proyectos', 'creado', '3048', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3062', 'proyectos', 'creado', '3049', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3063', 'proyectos', 'creado', '3050', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3064', 'proyectos', 'creado', '3051', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3065', 'proyectos', 'creado', '3052', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3066', 'proyectos', 'creado', '3053', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3067', 'proyectos', 'creado', '3054', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3068', 'proyectos', 'creado', '3055', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3069', 'proyectos', 'creado', '3056', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3070', 'proyectos', 'creado', '3057', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3071', 'proyectos', 'creado', '3058', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3072', 'proyectos', 'creado', '3059', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3073', 'proyectos', 'creado', '3060', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3074', 'proyectos', 'creado', '3061', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3075', 'proyectos', 'creado', '3062', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3076', 'proyectos', 'creado', '3063', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3077', 'proyectos', 'creado', '3064', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3078', 'proyectos', 'creado', '3065', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3079', 'proyectos', 'creado', '3066', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3080', 'proyectos', 'creado', '3067', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3081', 'proyectos', 'creado', '3068', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3082', 'proyectos', 'creado', '3069', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3083', 'proyectos', 'creado', '3070', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3084', 'proyectos', 'creado', '3071', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3085', 'proyectos', 'creado', '3072', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3086', 'proyectos', 'creado', '3073', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3087', 'proyectos', 'creado', '3074', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3088', 'proyectos', 'creado', '3075', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3089', 'proyectos', 'creado', '3076', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3090', 'proyectos', 'creado', '3077', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3091', 'proyectos', 'creado', '3078', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3092', 'proyectos', 'creado', '3079', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3093', 'proyectos', 'creado', '3080', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3094', 'proyectos', 'creado', '3081', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3095', 'proyectos', 'creado', '3082', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3096', 'proyectos', 'creado', '3083', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3097', 'proyectos', 'creado', '3084', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3098', 'proyectos', 'creado', '3085', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3099', 'proyectos', 'creado', '3086', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3100', 'proyectos', 'creado', '3087', 'admin', '2017-01-09 16:46:56', '2017-01-09 16:46:56');
INSERT INTO `app_historico` VALUES ('3101', 'proyectos', 'creado', '3088', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3102', 'proyectos', 'creado', '3089', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3103', 'proyectos', 'creado', '3090', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3104', 'proyectos', 'creado', '3091', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3105', 'proyectos', 'creado', '3092', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3106', 'proyectos', 'creado', '3093', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3107', 'proyectos', 'creado', '3094', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3108', 'proyectos', 'creado', '3095', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3109', 'proyectos', 'creado', '3096', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3110', 'proyectos', 'creado', '3097', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3111', 'proyectos', 'creado', '3098', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3112', 'proyectos', 'creado', '3099', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3113', 'proyectos', 'creado', '3100', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3114', 'proyectos', 'creado', '3101', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3115', 'proyectos', 'creado', '3102', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3116', 'proyectos', 'creado', '3103', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3117', 'proyectos', 'creado', '3104', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3118', 'proyectos', 'creado', '3105', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3119', 'proyectos', 'creado', '3106', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3120', 'proyectos', 'creado', '3107', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3121', 'proyectos', 'creado', '3108', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3122', 'proyectos', 'creado', '3109', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3123', 'proyectos', 'creado', '3110', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3124', 'proyectos', 'creado', '3111', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3125', 'proyectos', 'creado', '3112', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3126', 'proyectos', 'creado', '3113', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3127', 'proyectos', 'creado', '3114', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3128', 'proyectos', 'creado', '3115', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3129', 'proyectos', 'creado', '3116', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3130', 'proyectos', 'creado', '3117', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3131', 'proyectos', 'creado', '3118', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3132', 'proyectos', 'creado', '3119', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3133', 'proyectos', 'creado', '3120', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3134', 'proyectos', 'creado', '3121', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3135', 'proyectos', 'creado', '3122', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3136', 'proyectos', 'creado', '3123', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3137', 'proyectos', 'creado', '3124', 'admin', '2017-01-09 16:46:57', '2017-01-09 16:46:57');
INSERT INTO `app_historico` VALUES ('3138', 'proyectos', 'creado', '3125', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3139', 'proyectos', 'creado', '3126', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3140', 'proyectos', 'creado', '3127', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3141', 'proyectos', 'creado', '3128', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3142', 'proyectos', 'creado', '3129', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3143', 'proyectos', 'creado', '3130', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3144', 'proyectos', 'creado', '3131', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3145', 'proyectos', 'creado', '3132', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3146', 'proyectos', 'creado', '3133', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3147', 'proyectos', 'creado', '3134', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3148', 'proyectos', 'creado', '3135', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3149', 'proyectos', 'creado', '3136', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3150', 'proyectos', 'creado', '3137', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3151', 'proyectos', 'creado', '3138', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3152', 'proyectos', 'creado', '3139', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3153', 'proyectos', 'creado', '3140', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3154', 'proyectos', 'creado', '3141', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3155', 'proyectos', 'creado', '3142', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3156', 'proyectos', 'creado', '3143', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3157', 'proyectos', 'creado', '3144', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3158', 'proyectos', 'creado', '3145', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3159', 'proyectos', 'creado', '3146', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3160', 'proyectos', 'creado', '3147', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3161', 'proyectos', 'creado', '3148', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3162', 'proyectos', 'creado', '3149', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3163', 'proyectos', 'creado', '3150', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3164', 'proyectos', 'creado', '3151', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3165', 'proyectos', 'creado', '3152', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3166', 'proyectos', 'creado', '3153', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3167', 'proyectos', 'creado', '3154', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3168', 'proyectos', 'creado', '3155', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3169', 'proyectos', 'creado', '3156', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3170', 'proyectos', 'creado', '3157', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3171', 'proyectos', 'creado', '3158', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3172', 'proyectos', 'creado', '3159', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3173', 'proyectos', 'creado', '3160', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3174', 'proyectos', 'creado', '3161', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3175', 'proyectos', 'creado', '3162', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3176', 'proyectos', 'creado', '3163', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3177', 'proyectos', 'creado', '3164', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3178', 'proyectos', 'creado', '3165', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3179', 'proyectos', 'creado', '3166', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3180', 'proyectos', 'creado', '3167', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3181', 'proyectos', 'creado', '3168', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3182', 'proyectos', 'creado', '3169', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3183', 'proyectos', 'creado', '3170', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3184', 'proyectos', 'creado', '3171', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3185', 'proyectos', 'creado', '3172', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3186', 'proyectos', 'creado', '3173', 'admin', '2017-01-09 16:46:58', '2017-01-09 16:46:58');
INSERT INTO `app_historico` VALUES ('3187', 'proyectos', 'creado', '3174', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3188', 'proyectos', 'creado', '3175', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3189', 'proyectos', 'creado', '3176', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3190', 'proyectos', 'creado', '3177', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3191', 'proyectos', 'creado', '3178', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3192', 'proyectos', 'creado', '3179', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3193', 'proyectos', 'creado', '3180', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3194', 'proyectos', 'creado', '3181', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3195', 'proyectos', 'creado', '3182', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3196', 'proyectos', 'creado', '3183', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3197', 'proyectos', 'creado', '3184', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3198', 'proyectos', 'creado', '3185', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3199', 'proyectos', 'creado', '3186', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3200', 'proyectos', 'creado', '3187', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3201', 'proyectos', 'creado', '3188', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3202', 'proyectos', 'creado', '3189', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3203', 'proyectos', 'creado', '3190', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3204', 'proyectos', 'creado', '3191', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3205', 'proyectos', 'creado', '3192', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3206', 'proyectos', 'creado', '3193', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3207', 'proyectos', 'creado', '3194', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3208', 'proyectos', 'creado', '3195', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3209', 'proyectos', 'creado', '3196', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3210', 'proyectos', 'creado', '3197', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3211', 'proyectos', 'creado', '3198', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3212', 'proyectos', 'creado', '3199', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3213', 'proyectos', 'creado', '3200', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3214', 'proyectos', 'creado', '3201', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3215', 'proyectos', 'creado', '3202', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3216', 'proyectos', 'creado', '3203', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3217', 'proyectos', 'creado', '3204', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3218', 'proyectos', 'creado', '3205', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3219', 'proyectos', 'creado', '3206', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3220', 'proyectos', 'creado', '3207', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3221', 'proyectos', 'creado', '3208', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3222', 'proyectos', 'creado', '3209', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3223', 'proyectos', 'creado', '3210', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3224', 'proyectos', 'creado', '3211', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3225', 'proyectos', 'creado', '3212', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3226', 'proyectos', 'creado', '3213', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3227', 'proyectos', 'creado', '3214', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3228', 'proyectos', 'creado', '3215', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3229', 'proyectos', 'creado', '3216', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3230', 'proyectos', 'creado', '3217', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3231', 'proyectos', 'creado', '3218', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3232', 'proyectos', 'creado', '3219', 'admin', '2017-01-09 16:46:59', '2017-01-09 16:46:59');
INSERT INTO `app_historico` VALUES ('3233', 'proyectos', 'creado', '3220', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3234', 'proyectos', 'creado', '3221', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3235', 'proyectos', 'creado', '3222', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3236', 'proyectos', 'creado', '3223', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3237', 'proyectos', 'creado', '3224', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3238', 'proyectos', 'creado', '3225', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3239', 'proyectos', 'creado', '3226', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3240', 'proyectos', 'creado', '3227', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3241', 'proyectos', 'creado', '3228', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3242', 'proyectos', 'creado', '3229', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3243', 'proyectos', 'creado', '3230', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3244', 'proyectos', 'creado', '3231', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3245', 'proyectos', 'creado', '3232', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3246', 'proyectos', 'creado', '3233', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3247', 'proyectos', 'creado', '3234', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3248', 'proyectos', 'creado', '3235', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3249', 'proyectos', 'creado', '3236', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3250', 'proyectos', 'creado', '3237', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3251', 'proyectos', 'creado', '3238', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3252', 'proyectos', 'creado', '3239', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3253', 'proyectos', 'creado', '3240', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3254', 'proyectos', 'creado', '3241', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3255', 'proyectos', 'creado', '3242', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3256', 'proyectos', 'creado', '3243', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3257', 'proyectos', 'creado', '3244', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3258', 'proyectos', 'creado', '3245', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3259', 'proyectos', 'creado', '3246', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3260', 'proyectos', 'creado', '3247', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3261', 'proyectos', 'creado', '3248', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3262', 'proyectos', 'creado', '3249', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3263', 'proyectos', 'creado', '3250', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3264', 'proyectos', 'creado', '3251', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3265', 'proyectos', 'creado', '3252', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3266', 'proyectos', 'creado', '3253', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3267', 'proyectos', 'creado', '3254', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3268', 'proyectos', 'creado', '3255', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3269', 'proyectos', 'creado', '3256', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3270', 'proyectos', 'creado', '3257', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3271', 'proyectos', 'creado', '3258', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3272', 'proyectos', 'creado', '3259', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3273', 'proyectos', 'creado', '3260', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3274', 'proyectos', 'creado', '3261', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3275', 'proyectos', 'creado', '3262', 'admin', '2017-01-09 16:47:00', '2017-01-09 16:47:00');
INSERT INTO `app_historico` VALUES ('3276', 'proyectos', 'creado', '3263', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3277', 'proyectos', 'creado', '3264', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3278', 'proyectos', 'creado', '3265', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3279', 'proyectos', 'creado', '3266', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3280', 'proyectos', 'creado', '3267', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3281', 'proyectos', 'creado', '3268', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3282', 'proyectos', 'creado', '3269', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3283', 'proyectos', 'creado', '3270', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3284', 'proyectos', 'creado', '3271', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3285', 'proyectos', 'creado', '3272', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3286', 'proyectos', 'creado', '3273', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3287', 'proyectos', 'creado', '3274', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3288', 'proyectos', 'creado', '3275', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3289', 'proyectos', 'creado', '3276', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3290', 'proyectos', 'creado', '3277', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3291', 'proyectos', 'creado', '3278', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3292', 'proyectos', 'creado', '3279', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3293', 'proyectos', 'creado', '3280', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3294', 'proyectos', 'creado', '3281', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3295', 'proyectos', 'creado', '3282', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3296', 'proyectos', 'creado', '3283', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3297', 'proyectos', 'creado', '3284', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3298', 'proyectos', 'creado', '3285', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3299', 'proyectos', 'creado', '3286', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3300', 'proyectos', 'creado', '3287', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3301', 'proyectos', 'creado', '3288', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3302', 'proyectos', 'creado', '3289', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3303', 'proyectos', 'creado', '3290', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3304', 'proyectos', 'creado', '3291', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3305', 'proyectos', 'creado', '3292', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3306', 'proyectos', 'creado', '3293', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3307', 'proyectos', 'creado', '3294', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3308', 'proyectos', 'creado', '3295', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3309', 'proyectos', 'creado', '3296', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3310', 'proyectos', 'creado', '3297', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3311', 'proyectos', 'creado', '3298', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3312', 'proyectos', 'creado', '3299', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3313', 'proyectos', 'creado', '3300', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3314', 'proyectos', 'creado', '3301', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3315', 'proyectos', 'creado', '3302', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3316', 'proyectos', 'creado', '3303', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3317', 'proyectos', 'creado', '3304', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3318', 'proyectos', 'creado', '3305', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3319', 'proyectos', 'creado', '3306', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3320', 'proyectos', 'creado', '3307', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3321', 'proyectos', 'creado', '3308', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3322', 'proyectos', 'creado', '3309', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3323', 'proyectos', 'creado', '3310', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3324', 'proyectos', 'creado', '3311', 'admin', '2017-01-09 16:47:01', '2017-01-09 16:47:01');
INSERT INTO `app_historico` VALUES ('3325', 'proyectos', 'creado', '3312', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3326', 'proyectos', 'creado', '3313', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3327', 'proyectos', 'creado', '3314', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3328', 'proyectos', 'creado', '3315', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3329', 'proyectos', 'creado', '3316', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3330', 'proyectos', 'creado', '3317', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3331', 'proyectos', 'creado', '3318', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3332', 'proyectos', 'creado', '3319', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3333', 'proyectos', 'creado', '3320', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3334', 'proyectos', 'creado', '3321', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3335', 'proyectos', 'creado', '3322', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3336', 'proyectos', 'creado', '3323', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3337', 'proyectos', 'creado', '3324', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3338', 'proyectos', 'creado', '3325', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3339', 'proyectos', 'creado', '3326', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3340', 'proyectos', 'creado', '3327', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3341', 'proyectos', 'creado', '3328', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3342', 'proyectos', 'creado', '3329', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3343', 'proyectos', 'creado', '3330', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3344', 'proyectos', 'creado', '3331', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3345', 'proyectos', 'creado', '3332', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3346', 'proyectos', 'creado', '3333', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3347', 'proyectos', 'creado', '3334', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3348', 'proyectos', 'creado', '3335', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3349', 'proyectos', 'creado', '3336', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3350', 'proyectos', 'creado', '3337', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3351', 'proyectos', 'creado', '3338', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3352', 'proyectos', 'creado', '3339', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3353', 'proyectos', 'creado', '3340', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3354', 'proyectos', 'creado', '3341', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3355', 'proyectos', 'creado', '3342', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3356', 'proyectos', 'creado', '3343', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3357', 'proyectos', 'creado', '3344', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3358', 'proyectos', 'creado', '3345', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3359', 'proyectos', 'creado', '3346', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3360', 'proyectos', 'creado', '3347', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3361', 'proyectos', 'creado', '3348', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3362', 'proyectos', 'creado', '3349', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3363', 'proyectos', 'creado', '3350', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3364', 'proyectos', 'creado', '3351', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3365', 'proyectos', 'creado', '3352', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3366', 'proyectos', 'creado', '3353', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3367', 'proyectos', 'creado', '3354', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3368', 'proyectos', 'creado', '3355', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3369', 'proyectos', 'creado', '3356', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3370', 'proyectos', 'creado', '3357', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3371', 'proyectos', 'creado', '3358', 'admin', '2017-01-09 16:47:02', '2017-01-09 16:47:02');
INSERT INTO `app_historico` VALUES ('3372', 'proyectos', 'creado', '3359', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3373', 'proyectos', 'creado', '3360', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3374', 'proyectos', 'creado', '3361', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3375', 'proyectos', 'creado', '3362', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3376', 'proyectos', 'creado', '3363', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3377', 'proyectos', 'creado', '3364', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3378', 'proyectos', 'creado', '3365', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3379', 'proyectos', 'creado', '3366', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3380', 'proyectos', 'creado', '3367', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3381', 'proyectos', 'creado', '3368', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3382', 'proyectos', 'creado', '3369', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3383', 'proyectos', 'creado', '3370', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3384', 'proyectos', 'creado', '3371', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3385', 'proyectos', 'creado', '3372', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3386', 'proyectos', 'creado', '3373', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3387', 'proyectos', 'creado', '3374', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3388', 'proyectos', 'creado', '3375', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3389', 'proyectos', 'creado', '3376', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3390', 'proyectos', 'creado', '3377', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3391', 'proyectos', 'creado', '3378', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3392', 'proyectos', 'creado', '3379', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3393', 'proyectos', 'creado', '3380', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3394', 'proyectos', 'creado', '3381', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3395', 'proyectos', 'creado', '3382', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3396', 'proyectos', 'creado', '3383', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3397', 'proyectos', 'creado', '3384', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3398', 'proyectos', 'creado', '3385', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3399', 'proyectos', 'creado', '3386', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3400', 'proyectos', 'creado', '3387', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3401', 'proyectos', 'creado', '3388', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3402', 'proyectos', 'creado', '3389', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3403', 'proyectos', 'creado', '3390', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3404', 'proyectos', 'creado', '3391', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3405', 'proyectos', 'creado', '3392', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3406', 'proyectos', 'creado', '3393', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3407', 'proyectos', 'creado', '3394', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3408', 'proyectos', 'creado', '3395', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3409', 'proyectos', 'creado', '3396', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3410', 'proyectos', 'creado', '3397', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3411', 'proyectos', 'creado', '3398', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3412', 'proyectos', 'creado', '3399', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3413', 'proyectos', 'creado', '3400', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3414', 'proyectos', 'creado', '3401', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3415', 'proyectos', 'creado', '3402', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3416', 'proyectos', 'creado', '3403', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3417', 'proyectos', 'creado', '3404', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3418', 'proyectos', 'creado', '3405', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3419', 'proyectos', 'creado', '3406', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3420', 'proyectos', 'creado', '3407', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3421', 'proyectos', 'creado', '3408', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3422', 'proyectos', 'creado', '3409', 'admin', '2017-01-09 16:47:03', '2017-01-09 16:47:03');
INSERT INTO `app_historico` VALUES ('3423', 'proyectos', 'creado', '3410', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3424', 'proyectos', 'creado', '3411', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3425', 'proyectos', 'creado', '3412', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3426', 'proyectos', 'creado', '3413', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3427', 'proyectos', 'creado', '3414', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3428', 'proyectos', 'creado', '3415', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3429', 'proyectos', 'creado', '3416', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3430', 'proyectos', 'creado', '3417', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3431', 'proyectos', 'creado', '3418', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3432', 'proyectos', 'creado', '3419', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3433', 'proyectos', 'creado', '3420', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3434', 'proyectos', 'creado', '3421', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3435', 'proyectos', 'creado', '3422', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3436', 'proyectos', 'creado', '3423', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3437', 'proyectos', 'creado', '3424', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3438', 'proyectos', 'creado', '3425', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3439', 'proyectos', 'creado', '3426', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3440', 'proyectos', 'creado', '3427', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3441', 'proyectos', 'creado', '3428', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3442', 'proyectos', 'creado', '3429', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3443', 'proyectos', 'creado', '3430', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3444', 'proyectos', 'creado', '3431', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3445', 'proyectos', 'creado', '3432', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3446', 'proyectos', 'creado', '3433', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3447', 'proyectos', 'creado', '3434', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3448', 'proyectos', 'creado', '3435', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3449', 'proyectos', 'creado', '3436', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3450', 'proyectos', 'creado', '3437', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3451', 'proyectos', 'creado', '3438', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3452', 'proyectos', 'creado', '3439', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3453', 'proyectos', 'creado', '3440', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3454', 'proyectos', 'creado', '3441', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3455', 'proyectos', 'creado', '3442', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3456', 'proyectos', 'creado', '3443', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3457', 'proyectos', 'creado', '3444', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3458', 'proyectos', 'creado', '3445', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3459', 'proyectos', 'creado', '3446', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3460', 'proyectos', 'creado', '3447', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3461', 'proyectos', 'creado', '3448', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3462', 'proyectos', 'creado', '3449', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3463', 'proyectos', 'creado', '3450', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3464', 'proyectos', 'creado', '3451', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3465', 'proyectos', 'creado', '3452', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3466', 'proyectos', 'creado', '3453', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3467', 'proyectos', 'creado', '3454', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3468', 'proyectos', 'creado', '3455', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3469', 'proyectos', 'creado', '3456', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3470', 'proyectos', 'creado', '3457', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3471', 'proyectos', 'creado', '3458', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3472', 'proyectos', 'creado', '3459', 'admin', '2017-01-09 16:47:04', '2017-01-09 16:47:04');
INSERT INTO `app_historico` VALUES ('3473', 'proyectos', 'creado', '3460', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3474', 'proyectos', 'creado', '3461', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3475', 'proyectos', 'creado', '3462', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3476', 'proyectos', 'creado', '3463', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3477', 'proyectos', 'creado', '3464', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3478', 'proyectos', 'creado', '3465', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3479', 'proyectos', 'creado', '3466', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3480', 'proyectos', 'creado', '3467', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3481', 'proyectos', 'creado', '3468', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3482', 'proyectos', 'creado', '3469', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3483', 'proyectos', 'creado', '3470', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3484', 'proyectos', 'creado', '3471', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3485', 'proyectos', 'creado', '3472', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3486', 'proyectos', 'creado', '3473', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3487', 'proyectos', 'creado', '3474', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3488', 'proyectos', 'creado', '3475', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3489', 'proyectos', 'creado', '3476', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3490', 'proyectos', 'creado', '3477', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3491', 'proyectos', 'creado', '3478', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3492', 'proyectos', 'creado', '3479', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3493', 'proyectos', 'creado', '3480', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3494', 'proyectos', 'creado', '3481', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3495', 'proyectos', 'creado', '3482', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3496', 'proyectos', 'creado', '3483', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3497', 'proyectos', 'creado', '3484', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3498', 'proyectos', 'creado', '3485', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3499', 'proyectos', 'creado', '3486', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3500', 'proyectos', 'creado', '3487', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3501', 'proyectos', 'creado', '3488', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3502', 'proyectos', 'creado', '3489', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3503', 'proyectos', 'creado', '3490', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3504', 'proyectos', 'creado', '3491', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3505', 'proyectos', 'creado', '3492', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3506', 'proyectos', 'creado', '3493', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3507', 'proyectos', 'creado', '3494', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3508', 'proyectos', 'creado', '3495', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3509', 'proyectos', 'creado', '3496', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3510', 'proyectos', 'creado', '3497', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3511', 'proyectos', 'creado', '3498', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3512', 'proyectos', 'creado', '3499', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3513', 'proyectos', 'creado', '3500', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3514', 'proyectos', 'creado', '3501', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3515', 'proyectos', 'creado', '3502', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3516', 'proyectos', 'creado', '3503', 'admin', '2017-01-09 16:47:05', '2017-01-09 16:47:05');
INSERT INTO `app_historico` VALUES ('3517', 'proyectos', 'creado', '3504', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3518', 'proyectos', 'creado', '3505', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3519', 'proyectos', 'creado', '3506', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3520', 'proyectos', 'creado', '3507', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3521', 'proyectos', 'creado', '3508', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3522', 'proyectos', 'creado', '3509', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3523', 'proyectos', 'creado', '3510', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3524', 'proyectos', 'creado', '3511', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3525', 'proyectos', 'creado', '3512', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3526', 'proyectos', 'creado', '3513', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3527', 'proyectos', 'creado', '3514', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3528', 'proyectos', 'creado', '3515', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3529', 'proyectos', 'creado', '3516', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3530', 'proyectos', 'creado', '3517', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3531', 'proyectos', 'creado', '3518', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3532', 'proyectos', 'creado', '3519', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3533', 'proyectos', 'creado', '3520', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3534', 'proyectos', 'creado', '3521', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3535', 'proyectos', 'creado', '3522', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3536', 'proyectos', 'creado', '3523', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3537', 'proyectos', 'creado', '3524', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3538', 'proyectos', 'creado', '3525', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3539', 'proyectos', 'creado', '3526', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3540', 'proyectos', 'creado', '3527', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3541', 'proyectos', 'creado', '3528', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3542', 'proyectos', 'creado', '3529', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3543', 'proyectos', 'creado', '3530', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3544', 'proyectos', 'creado', '3531', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3545', 'proyectos', 'creado', '3532', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3546', 'proyectos', 'creado', '3533', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3547', 'proyectos', 'creado', '3534', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3548', 'proyectos', 'creado', '3535', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3549', 'proyectos', 'creado', '3536', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3550', 'proyectos', 'creado', '3537', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3551', 'proyectos', 'creado', '3538', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3552', 'proyectos', 'creado', '3539', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3553', 'proyectos', 'creado', '3540', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3554', 'proyectos', 'creado', '3541', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3555', 'proyectos', 'creado', '3542', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3556', 'proyectos', 'creado', '3543', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3557', 'proyectos', 'creado', '3544', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3558', 'proyectos', 'creado', '3545', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3559', 'proyectos', 'creado', '3546', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3560', 'proyectos', 'creado', '3547', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3561', 'proyectos', 'creado', '3548', 'admin', '2017-01-09 16:47:06', '2017-01-09 16:47:06');
INSERT INTO `app_historico` VALUES ('3562', 'proyectos', 'creado', '3549', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3563', 'proyectos', 'creado', '3550', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3564', 'proyectos', 'creado', '3551', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3565', 'proyectos', 'creado', '3552', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3566', 'proyectos', 'creado', '3553', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3567', 'proyectos', 'creado', '3554', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3568', 'proyectos', 'creado', '3555', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3569', 'proyectos', 'creado', '3556', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3570', 'proyectos', 'creado', '3557', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3571', 'proyectos', 'creado', '3558', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3572', 'proyectos', 'creado', '3559', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3573', 'proyectos', 'creado', '3560', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3574', 'proyectos', 'creado', '3561', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3575', 'proyectos', 'creado', '3562', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3576', 'proyectos', 'creado', '3563', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3577', 'proyectos', 'creado', '3564', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3578', 'proyectos', 'creado', '3565', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3579', 'proyectos', 'creado', '3566', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3580', 'proyectos', 'creado', '3567', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3581', 'proyectos', 'creado', '3568', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3582', 'proyectos', 'creado', '3569', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3583', 'proyectos', 'creado', '3570', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3584', 'proyectos', 'creado', '3571', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3585', 'proyectos', 'creado', '3572', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3586', 'proyectos', 'creado', '3573', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3587', 'proyectos', 'creado', '3574', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3588', 'proyectos', 'creado', '3575', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3589', 'proyectos', 'creado', '3576', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3590', 'proyectos', 'creado', '3577', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3591', 'proyectos', 'creado', '3578', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3592', 'proyectos', 'creado', '3579', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3593', 'proyectos', 'creado', '3580', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3594', 'proyectos', 'creado', '3581', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3595', 'proyectos', 'creado', '3582', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3596', 'proyectos', 'creado', '3583', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3597', 'proyectos', 'creado', '3584', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3598', 'proyectos', 'creado', '3585', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3599', 'proyectos', 'creado', '3586', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3600', 'proyectos', 'creado', '3587', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3601', 'proyectos', 'creado', '3588', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3602', 'proyectos', 'creado', '3589', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3603', 'proyectos', 'creado', '3590', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3604', 'proyectos', 'creado', '3591', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3605', 'proyectos', 'creado', '3592', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3606', 'proyectos', 'creado', '3593', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3607', 'proyectos', 'creado', '3594', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3608', 'proyectos', 'creado', '3595', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3609', 'proyectos', 'creado', '3596', 'admin', '2017-01-09 16:47:07', '2017-01-09 16:47:07');
INSERT INTO `app_historico` VALUES ('3610', 'proyectos', 'creado', '3597', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3611', 'proyectos', 'creado', '3598', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3612', 'proyectos', 'creado', '3599', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3613', 'proyectos', 'creado', '3600', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3614', 'proyectos', 'creado', '3601', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3615', 'proyectos', 'creado', '3602', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3616', 'proyectos', 'creado', '3603', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3617', 'proyectos', 'creado', '3604', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3618', 'proyectos', 'creado', '3605', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3619', 'proyectos', 'creado', '3606', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3620', 'proyectos', 'creado', '3607', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3621', 'proyectos', 'creado', '3608', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3622', 'proyectos', 'creado', '3609', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3623', 'proyectos', 'creado', '3610', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3624', 'proyectos', 'creado', '3611', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3625', 'proyectos', 'creado', '3612', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3626', 'proyectos', 'creado', '3613', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3627', 'proyectos', 'creado', '3614', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3628', 'proyectos', 'creado', '3615', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3629', 'proyectos', 'creado', '3616', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3630', 'proyectos', 'creado', '3617', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3631', 'proyectos', 'creado', '3618', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3632', 'proyectos', 'creado', '3619', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3633', 'proyectos', 'creado', '3620', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3634', 'proyectos', 'creado', '3621', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3635', 'proyectos', 'creado', '3622', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3636', 'proyectos', 'creado', '3623', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3637', 'proyectos', 'creado', '3624', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3638', 'proyectos', 'creado', '3625', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3639', 'proyectos', 'creado', '3626', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3640', 'proyectos', 'creado', '3627', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3641', 'proyectos', 'creado', '3628', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3642', 'proyectos', 'creado', '3629', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3643', 'proyectos', 'creado', '3630', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3644', 'proyectos', 'creado', '3631', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3645', 'proyectos', 'creado', '3632', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3646', 'proyectos', 'creado', '3633', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3647', 'proyectos', 'creado', '3634', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3648', 'proyectos', 'creado', '3635', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3649', 'proyectos', 'creado', '3636', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3650', 'proyectos', 'creado', '3637', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3651', 'proyectos', 'creado', '3638', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3652', 'proyectos', 'creado', '3639', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3653', 'proyectos', 'creado', '3640', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3654', 'proyectos', 'creado', '3641', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3655', 'proyectos', 'creado', '3642', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3656', 'proyectos', 'creado', '3643', 'admin', '2017-01-09 16:47:08', '2017-01-09 16:47:08');
INSERT INTO `app_historico` VALUES ('3657', 'proyectos', 'creado', '3644', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3658', 'proyectos', 'creado', '3645', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3659', 'proyectos', 'creado', '3646', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3660', 'proyectos', 'creado', '3647', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3661', 'proyectos', 'creado', '3648', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3662', 'proyectos', 'creado', '3649', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3663', 'proyectos', 'creado', '3650', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3664', 'proyectos', 'creado', '3651', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3665', 'proyectos', 'creado', '3652', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3666', 'proyectos', 'creado', '3653', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3667', 'proyectos', 'creado', '3654', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3668', 'proyectos', 'creado', '3655', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3669', 'proyectos', 'creado', '3656', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3670', 'proyectos', 'creado', '3657', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3671', 'proyectos', 'creado', '3658', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3672', 'proyectos', 'creado', '3659', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3673', 'proyectos', 'creado', '3660', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3674', 'proyectos', 'creado', '3661', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3675', 'proyectos', 'creado', '3662', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3676', 'proyectos', 'creado', '3663', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3677', 'proyectos', 'creado', '3664', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3678', 'proyectos', 'creado', '3665', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3679', 'proyectos', 'creado', '3666', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3680', 'proyectos', 'creado', '3667', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3681', 'proyectos', 'creado', '3668', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3682', 'proyectos', 'creado', '3669', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3683', 'proyectos', 'creado', '3670', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3684', 'proyectos', 'creado', '3671', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3685', 'proyectos', 'creado', '3672', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3686', 'proyectos', 'creado', '3673', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3687', 'proyectos', 'creado', '3674', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3688', 'proyectos', 'creado', '3675', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3689', 'proyectos', 'creado', '3676', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3690', 'proyectos', 'creado', '3677', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3691', 'proyectos', 'creado', '3678', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3692', 'proyectos', 'creado', '3679', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3693', 'proyectos', 'creado', '3680', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3694', 'proyectos', 'creado', '3681', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3695', 'proyectos', 'creado', '3682', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3696', 'proyectos', 'creado', '3683', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3697', 'proyectos', 'creado', '3684', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3698', 'proyectos', 'creado', '3685', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3699', 'proyectos', 'creado', '3686', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3700', 'proyectos', 'creado', '3687', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3701', 'proyectos', 'creado', '3688', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3702', 'proyectos', 'creado', '3689', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3703', 'proyectos', 'creado', '3690', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3704', 'proyectos', 'creado', '3691', 'admin', '2017-01-09 16:47:09', '2017-01-09 16:47:09');
INSERT INTO `app_historico` VALUES ('3705', 'proyectos', 'creado', '3692', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3706', 'proyectos', 'creado', '3693', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3707', 'proyectos', 'creado', '3694', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3708', 'proyectos', 'creado', '3695', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3709', 'proyectos', 'creado', '3696', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3710', 'proyectos', 'creado', '3697', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3711', 'proyectos', 'creado', '3698', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3712', 'proyectos', 'creado', '3699', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3713', 'proyectos', 'creado', '3700', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3714', 'proyectos', 'creado', '3701', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3715', 'proyectos', 'creado', '3702', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3716', 'proyectos', 'creado', '3703', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3717', 'proyectos', 'creado', '3704', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3718', 'proyectos', 'creado', '3705', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3719', 'proyectos', 'creado', '3706', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3720', 'proyectos', 'actualizado', '3669', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3721', 'proyectos', 'creado', '3707', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3722', 'proyectos', 'creado', '3708', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3723', 'proyectos', 'creado', '3709', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3724', 'proyectos', 'creado', '3710', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3725', 'proyectos', 'creado', '3711', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3726', 'proyectos', 'creado', '3712', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3727', 'proyectos', 'creado', '3713', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3728', 'proyectos', 'creado', '3714', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3729', 'proyectos', 'creado', '3715', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3730', 'proyectos', 'creado', '3716', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3731', 'proyectos', 'creado', '3717', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3732', 'proyectos', 'creado', '3718', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3733', 'proyectos', 'creado', '3719', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3734', 'proyectos', 'creado', '3720', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3735', 'proyectos', 'creado', '3721', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3736', 'proyectos', 'creado', '3722', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3737', 'proyectos', 'creado', '3723', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3738', 'proyectos', 'creado', '3724', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3739', 'proyectos', 'creado', '3725', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3740', 'proyectos', 'creado', '3726', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3741', 'proyectos', 'creado', '3727', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3742', 'proyectos', 'creado', '3728', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3743', 'proyectos', 'creado', '3729', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3744', 'proyectos', 'creado', '3730', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3745', 'proyectos', 'creado', '3731', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3746', 'proyectos', 'creado', '3732', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3747', 'proyectos', 'creado', '3733', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3748', 'proyectos', 'creado', '3734', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3749', 'proyectos', 'creado', '3735', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3750', 'proyectos', 'creado', '3736', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3751', 'proyectos', 'creado', '3737', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3752', 'proyectos', 'creado', '3738', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3753', 'proyectos', 'creado', '3739', 'admin', '2017-01-09 16:47:10', '2017-01-09 16:47:10');
INSERT INTO `app_historico` VALUES ('3754', 'proyectos', 'creado', '3740', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3755', 'proyectos', 'creado', '3741', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3756', 'proyectos', 'creado', '3742', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3757', 'proyectos', 'creado', '3743', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3758', 'proyectos', 'creado', '3744', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3759', 'proyectos', 'creado', '3745', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3760', 'proyectos', 'creado', '3746', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3761', 'proyectos', 'creado', '3747', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3762', 'proyectos', 'creado', '3748', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3763', 'proyectos', 'creado', '3749', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3764', 'proyectos', 'creado', '3750', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3765', 'proyectos', 'creado', '3751', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3766', 'proyectos', 'creado', '3752', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3767', 'proyectos', 'creado', '3753', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3768', 'proyectos', 'creado', '3754', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3769', 'proyectos', 'creado', '3755', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3770', 'proyectos', 'creado', '3756', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3771', 'proyectos', 'creado', '3757', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3772', 'proyectos', 'creado', '3758', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3773', 'proyectos', 'creado', '3759', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3774', 'proyectos', 'creado', '3760', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3775', 'proyectos', 'creado', '3761', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3776', 'proyectos', 'creado', '3762', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3777', 'proyectos', 'creado', '3763', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3778', 'proyectos', 'creado', '3764', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3779', 'proyectos', 'creado', '3765', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3780', 'proyectos', 'creado', '3766', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3781', 'proyectos', 'creado', '3767', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3782', 'proyectos', 'creado', '3768', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3783', 'proyectos', 'creado', '3769', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3784', 'proyectos', 'creado', '3770', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3785', 'proyectos', 'creado', '3771', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3786', 'proyectos', 'creado', '3772', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3787', 'proyectos', 'creado', '3773', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3788', 'proyectos', 'creado', '3774', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3789', 'proyectos', 'creado', '3775', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3790', 'proyectos', 'creado', '3776', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3791', 'proyectos', 'creado', '3777', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3792', 'proyectos', 'creado', '3778', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3793', 'proyectos', 'creado', '3779', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3794', 'proyectos', 'creado', '3780', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3795', 'proyectos', 'creado', '3781', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3796', 'proyectos', 'creado', '3782', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3797', 'proyectos', 'creado', '3783', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3798', 'proyectos', 'creado', '3784', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3799', 'proyectos', 'creado', '3785', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3800', 'proyectos', 'creado', '3786', 'admin', '2017-01-09 16:47:11', '2017-01-09 16:47:11');
INSERT INTO `app_historico` VALUES ('3801', 'proyectos', 'creado', '3787', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3802', 'proyectos', 'creado', '3788', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3803', 'proyectos', 'creado', '3789', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3804', 'proyectos', 'creado', '3790', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3805', 'proyectos', 'creado', '3791', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3806', 'proyectos', 'creado', '3792', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3807', 'proyectos', 'creado', '3793', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3808', 'proyectos', 'creado', '3794', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3809', 'proyectos', 'creado', '3795', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3810', 'proyectos', 'creado', '3796', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3811', 'proyectos', 'creado', '3797', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3812', 'proyectos', 'creado', '3798', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3813', 'proyectos', 'creado', '3799', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3814', 'proyectos', 'creado', '3800', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3815', 'proyectos', 'creado', '3801', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3816', 'proyectos', 'creado', '3802', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3817', 'proyectos', 'creado', '3803', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3818', 'proyectos', 'creado', '3804', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3819', 'proyectos', 'creado', '3805', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3820', 'proyectos', 'creado', '3806', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3821', 'proyectos', 'creado', '3807', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3822', 'proyectos', 'creado', '3808', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3823', 'proyectos', 'creado', '3809', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3824', 'proyectos', 'creado', '3810', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3825', 'proyectos', 'creado', '3811', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3826', 'proyectos', 'creado', '3812', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3827', 'proyectos', 'creado', '3813', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3828', 'proyectos', 'creado', '3814', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3829', 'proyectos', 'creado', '3815', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3830', 'proyectos', 'creado', '3816', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3831', 'proyectos', 'creado', '3817', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3832', 'proyectos', 'creado', '3818', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3833', 'proyectos', 'creado', '3819', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3834', 'proyectos', 'creado', '3820', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3835', 'proyectos', 'creado', '3821', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3836', 'proyectos', 'creado', '3822', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3837', 'proyectos', 'creado', '3823', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3838', 'proyectos', 'creado', '3824', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3839', 'proyectos', 'creado', '3825', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3840', 'proyectos', 'creado', '3826', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3841', 'proyectos', 'creado', '3827', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3842', 'proyectos', 'creado', '3828', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3843', 'proyectos', 'creado', '3829', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3844', 'proyectos', 'creado', '3830', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3845', 'proyectos', 'creado', '3831', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3846', 'proyectos', 'creado', '3832', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3847', 'proyectos', 'creado', '3833', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3848', 'proyectos', 'creado', '3834', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3849', 'proyectos', 'creado', '3835', 'admin', '2017-01-09 16:47:12', '2017-01-09 16:47:12');
INSERT INTO `app_historico` VALUES ('3850', 'proyectos', 'creado', '3836', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3851', 'proyectos', 'creado', '3837', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3852', 'proyectos', 'creado', '3838', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3853', 'proyectos', 'creado', '3839', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3854', 'proyectos', 'creado', '3840', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3855', 'proyectos', 'creado', '3841', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3856', 'proyectos', 'creado', '3842', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3857', 'proyectos', 'creado', '3843', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3858', 'proyectos', 'creado', '3844', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3859', 'proyectos', 'creado', '3845', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3860', 'proyectos', 'creado', '3846', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3861', 'proyectos', 'creado', '3847', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3862', 'proyectos', 'creado', '3848', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3863', 'proyectos', 'creado', '3849', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3864', 'proyectos', 'creado', '3850', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3865', 'proyectos', 'creado', '3851', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3866', 'proyectos', 'creado', '3852', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3867', 'proyectos', 'creado', '3853', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3868', 'proyectos', 'creado', '3854', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3869', 'proyectos', 'creado', '3855', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3870', 'proyectos', 'creado', '3856', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3871', 'proyectos', 'creado', '3857', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3872', 'proyectos', 'creado', '3858', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3873', 'proyectos', 'creado', '3859', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3874', 'proyectos', 'creado', '3860', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3875', 'proyectos', 'creado', '3861', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3876', 'proyectos', 'creado', '3862', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3877', 'proyectos', 'creado', '3863', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3878', 'proyectos', 'creado', '3864', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3879', 'proyectos', 'creado', '3865', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3880', 'proyectos', 'creado', '3866', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3881', 'proyectos', 'creado', '3867', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3882', 'proyectos', 'creado', '3868', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3883', 'proyectos', 'creado', '3869', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3884', 'proyectos', 'creado', '3870', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3885', 'proyectos', 'creado', '3871', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3886', 'proyectos', 'creado', '3872', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3887', 'proyectos', 'creado', '3873', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3888', 'proyectos', 'creado', '3874', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3889', 'proyectos', 'creado', '3875', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3890', 'proyectos', 'creado', '3876', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3891', 'proyectos', 'creado', '3877', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3892', 'proyectos', 'creado', '3878', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3893', 'proyectos', 'creado', '3879', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3894', 'proyectos', 'creado', '3880', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3895', 'proyectos', 'creado', '3881', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3896', 'proyectos', 'creado', '3882', 'admin', '2017-01-09 16:47:13', '2017-01-09 16:47:13');
INSERT INTO `app_historico` VALUES ('3897', 'proyectos', 'creado', '3883', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3898', 'proyectos', 'creado', '3884', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3899', 'proyectos', 'creado', '3885', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3900', 'proyectos', 'creado', '3886', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3901', 'proyectos', 'creado', '3887', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3902', 'proyectos', 'creado', '3888', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3903', 'proyectos', 'creado', '3889', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3904', 'proyectos', 'creado', '3890', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3905', 'proyectos', 'creado', '3891', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3906', 'proyectos', 'creado', '3892', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3907', 'proyectos', 'creado', '3893', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3908', 'proyectos', 'creado', '3894', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3909', 'proyectos', 'creado', '3895', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3910', 'proyectos', 'creado', '3896', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3911', 'proyectos', 'creado', '3897', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3912', 'proyectos', 'creado', '3898', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3913', 'proyectos', 'creado', '3899', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3914', 'proyectos', 'creado', '3900', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3915', 'proyectos', 'creado', '3901', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3916', 'proyectos', 'creado', '3902', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3917', 'proyectos', 'creado', '3903', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3918', 'proyectos', 'creado', '3904', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3919', 'proyectos', 'creado', '3905', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3920', 'proyectos', 'creado', '3906', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3921', 'proyectos', 'creado', '3907', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3922', 'proyectos', 'creado', '3908', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3923', 'proyectos', 'creado', '3909', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3924', 'proyectos', 'creado', '3910', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3925', 'proyectos', 'creado', '3911', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3926', 'proyectos', 'creado', '3912', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3927', 'proyectos', 'creado', '3913', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3928', 'proyectos', 'creado', '3914', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3929', 'proyectos', 'creado', '3915', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3930', 'proyectos', 'creado', '3916', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3931', 'proyectos', 'creado', '3917', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3932', 'proyectos', 'creado', '3918', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3933', 'proyectos', 'creado', '3919', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3934', 'proyectos', 'creado', '3920', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3935', 'proyectos', 'creado', '3921', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3936', 'proyectos', 'creado', '3922', 'admin', '2017-01-09 16:47:14', '2017-01-09 16:47:14');
INSERT INTO `app_historico` VALUES ('3937', 'proyectos', 'creado', '3923', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3938', 'proyectos', 'creado', '3924', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3939', 'proyectos', 'creado', '3925', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3940', 'proyectos', 'creado', '3926', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3941', 'proyectos', 'creado', '3927', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3942', 'proyectos', 'creado', '3928', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3943', 'proyectos', 'creado', '3929', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3944', 'proyectos', 'creado', '3930', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3945', 'proyectos', 'creado', '3931', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3946', 'proyectos', 'creado', '3932', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3947', 'proyectos', 'creado', '3933', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3948', 'proyectos', 'creado', '3934', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3949', 'proyectos', 'creado', '3935', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3950', 'proyectos', 'creado', '3936', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3951', 'proyectos', 'creado', '3937', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3952', 'proyectos', 'creado', '3938', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3953', 'proyectos', 'creado', '3939', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3954', 'proyectos', 'creado', '3940', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3955', 'proyectos', 'creado', '3941', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3956', 'proyectos', 'creado', '3942', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3957', 'proyectos', 'creado', '3943', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3958', 'proyectos', 'creado', '3944', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3959', 'proyectos', 'creado', '3945', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3960', 'proyectos', 'creado', '3946', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3961', 'proyectos', 'creado', '3947', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3962', 'proyectos', 'creado', '3948', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3963', 'proyectos', 'creado', '3949', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3964', 'proyectos', 'creado', '3950', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3965', 'proyectos', 'creado', '3951', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3966', 'proyectos', 'creado', '3952', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3967', 'proyectos', 'creado', '3953', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3968', 'proyectos', 'creado', '3954', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3969', 'proyectos', 'creado', '3955', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3970', 'proyectos', 'creado', '3956', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3971', 'proyectos', 'creado', '3957', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3972', 'proyectos', 'creado', '3958', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3973', 'proyectos', 'creado', '3959', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3974', 'proyectos', 'creado', '3960', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3975', 'proyectos', 'creado', '3961', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3976', 'proyectos', 'creado', '3962', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3977', 'proyectos', 'creado', '3963', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3978', 'proyectos', 'creado', '3964', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3979', 'proyectos', 'creado', '3965', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3980', 'proyectos', 'creado', '3966', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3981', 'proyectos', 'creado', '3967', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3982', 'proyectos', 'creado', '3968', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3983', 'proyectos', 'creado', '3969', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3984', 'proyectos', 'creado', '3970', 'admin', '2017-01-09 16:47:15', '2017-01-09 16:47:15');
INSERT INTO `app_historico` VALUES ('3985', 'proyectos', 'creado', '3971', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('3986', 'proyectos', 'creado', '3972', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('3987', 'proyectos', 'creado', '3973', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('3988', 'proyectos', 'creado', '3974', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('3989', 'proyectos', 'creado', '3975', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('3990', 'proyectos', 'creado', '3976', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('3991', 'proyectos', 'creado', '3977', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('3992', 'proyectos', 'creado', '3978', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('3993', 'proyectos', 'creado', '3979', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('3994', 'proyectos', 'creado', '3980', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('3995', 'proyectos', 'creado', '3981', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('3996', 'proyectos', 'creado', '3982', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('3997', 'proyectos', 'creado', '3983', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('3998', 'proyectos', 'creado', '3984', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('3999', 'proyectos', 'creado', '3985', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4000', 'proyectos', 'creado', '3986', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4001', 'proyectos', 'creado', '3987', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4002', 'proyectos', 'creado', '3988', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4003', 'proyectos', 'creado', '3989', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4004', 'proyectos', 'creado', '3990', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4005', 'proyectos', 'creado', '3991', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4006', 'proyectos', 'creado', '3992', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4007', 'proyectos', 'creado', '3993', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4008', 'proyectos', 'creado', '3994', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4009', 'proyectos', 'creado', '3995', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4010', 'proyectos', 'creado', '3996', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4011', 'proyectos', 'creado', '3997', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4012', 'proyectos', 'creado', '3998', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4013', 'proyectos', 'creado', '3999', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4014', 'proyectos', 'creado', '4000', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4015', 'proyectos', 'creado', '4001', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4016', 'proyectos', 'creado', '4002', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4017', 'proyectos', 'creado', '4003', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4018', 'proyectos', 'creado', '4004', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4019', 'proyectos', 'creado', '4005', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4020', 'proyectos', 'creado', '4006', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4021', 'proyectos', 'creado', '4007', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4022', 'proyectos', 'creado', '4008', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4023', 'proyectos', 'creado', '4009', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4024', 'proyectos', 'creado', '4010', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4025', 'proyectos', 'creado', '4011', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4026', 'proyectos', 'creado', '4012', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4027', 'proyectos', 'creado', '4013', 'admin', '2017-01-09 16:47:16', '2017-01-09 16:47:16');
INSERT INTO `app_historico` VALUES ('4028', 'proyectos', 'creado', '4014', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4029', 'proyectos', 'creado', '4015', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4030', 'proyectos', 'creado', '4016', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4031', 'proyectos', 'creado', '4017', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4032', 'proyectos', 'creado', '4018', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4033', 'proyectos', 'creado', '4019', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4034', 'proyectos', 'creado', '4020', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4035', 'proyectos', 'creado', '4021', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4036', 'proyectos', 'creado', '4022', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4037', 'proyectos', 'creado', '4023', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4038', 'proyectos', 'creado', '4024', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4039', 'proyectos', 'creado', '4025', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4040', 'proyectos', 'creado', '4026', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4041', 'proyectos', 'creado', '4027', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4042', 'proyectos', 'creado', '4028', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4043', 'proyectos', 'creado', '4029', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4044', 'proyectos', 'creado', '4030', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4045', 'proyectos', 'creado', '4031', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4046', 'proyectos', 'creado', '4032', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4047', 'proyectos', 'creado', '4033', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4048', 'proyectos', 'creado', '4034', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4049', 'proyectos', 'creado', '4035', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4050', 'proyectos', 'creado', '4036', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4051', 'proyectos', 'creado', '4037', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4052', 'proyectos', 'creado', '4038', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4053', 'proyectos', 'creado', '4039', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4054', 'proyectos', 'creado', '4040', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4055', 'proyectos', 'creado', '4041', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4056', 'proyectos', 'creado', '4042', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4057', 'proyectos', 'creado', '4043', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4058', 'proyectos', 'creado', '4044', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4059', 'proyectos', 'creado', '4045', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4060', 'proyectos', 'creado', '4046', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4061', 'proyectos', 'creado', '4047', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4062', 'proyectos', 'creado', '4048', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4063', 'proyectos', 'creado', '4049', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4064', 'proyectos', 'creado', '4050', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4065', 'proyectos', 'creado', '4051', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4066', 'proyectos', 'creado', '4052', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4067', 'proyectos', 'creado', '4053', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4068', 'proyectos', 'creado', '4054', 'admin', '2017-01-09 16:47:17', '2017-01-09 16:47:17');
INSERT INTO `app_historico` VALUES ('4069', 'proyectos', 'creado', '4055', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4070', 'proyectos', 'creado', '4056', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4071', 'proyectos', 'creado', '4057', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4072', 'proyectos', 'creado', '4058', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4073', 'proyectos', 'creado', '4059', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4074', 'proyectos', 'creado', '4060', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4075', 'proyectos', 'creado', '4061', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4076', 'proyectos', 'creado', '4062', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4077', 'proyectos', 'creado', '4063', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4078', 'proyectos', 'creado', '4064', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4079', 'proyectos', 'creado', '4065', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4080', 'proyectos', 'creado', '4066', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4081', 'proyectos', 'creado', '4067', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4082', 'proyectos', 'creado', '4068', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4083', 'proyectos', 'creado', '4069', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4084', 'proyectos', 'creado', '4070', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4085', 'proyectos', 'creado', '4071', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4086', 'proyectos', 'creado', '4072', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4087', 'proyectos', 'creado', '4073', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4088', 'proyectos', 'creado', '4074', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4089', 'proyectos', 'creado', '4075', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4090', 'proyectos', 'creado', '4076', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4091', 'proyectos', 'creado', '4077', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4092', 'proyectos', 'creado', '4078', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4093', 'proyectos', 'creado', '4079', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4094', 'proyectos', 'creado', '4080', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4095', 'proyectos', 'creado', '4081', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4096', 'proyectos', 'creado', '4082', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4097', 'proyectos', 'creado', '4083', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4098', 'proyectos', 'creado', '4084', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4099', 'proyectos', 'creado', '4085', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4100', 'proyectos', 'creado', '4086', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4101', 'proyectos', 'creado', '4087', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4102', 'proyectos', 'creado', '4088', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4103', 'proyectos', 'creado', '4089', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4104', 'proyectos', 'creado', '4090', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4105', 'proyectos', 'creado', '4091', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4106', 'proyectos', 'creado', '4092', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4107', 'proyectos', 'creado', '4093', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4108', 'proyectos', 'creado', '4094', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4109', 'proyectos', 'creado', '4095', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4110', 'proyectos', 'creado', '4096', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4111', 'proyectos', 'creado', '4097', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4112', 'proyectos', 'creado', '4098', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4113', 'proyectos', 'creado', '4099', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4114', 'proyectos', 'creado', '4100', 'admin', '2017-01-09 16:47:18', '2017-01-09 16:47:18');
INSERT INTO `app_historico` VALUES ('4115', 'proyectos', 'creado', '4101', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4116', 'proyectos', 'creado', '4102', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4117', 'proyectos', 'creado', '4103', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4118', 'proyectos', 'creado', '4104', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4119', 'proyectos', 'creado', '4105', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4120', 'proyectos', 'creado', '4106', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4121', 'proyectos', 'creado', '4107', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4122', 'proyectos', 'creado', '4108', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4123', 'proyectos', 'creado', '4109', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4124', 'proyectos', 'creado', '4110', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4125', 'proyectos', 'creado', '4111', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4126', 'proyectos', 'creado', '4112', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4127', 'proyectos', 'creado', '4113', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4128', 'proyectos', 'creado', '4114', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4129', 'proyectos', 'creado', '4115', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4130', 'proyectos', 'creado', '4116', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4131', 'proyectos', 'creado', '4117', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4132', 'proyectos', 'creado', '4118', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4133', 'proyectos', 'creado', '4119', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4134', 'proyectos', 'creado', '4120', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4135', 'proyectos', 'creado', '4121', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4136', 'proyectos', 'creado', '4122', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4137', 'proyectos', 'creado', '4123', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4138', 'proyectos', 'creado', '4124', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4139', 'proyectos', 'creado', '4125', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4140', 'proyectos', 'creado', '4126', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4141', 'proyectos', 'creado', '4127', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4142', 'proyectos', 'creado', '4128', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4143', 'proyectos', 'creado', '4129', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4144', 'proyectos', 'creado', '4130', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4145', 'proyectos', 'creado', '4131', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4146', 'proyectos', 'creado', '4132', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4147', 'proyectos', 'creado', '4133', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4148', 'proyectos', 'creado', '4134', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4149', 'proyectos', 'creado', '4135', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4150', 'proyectos', 'creado', '4136', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4151', 'proyectos', 'creado', '4137', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4152', 'proyectos', 'creado', '4138', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4153', 'proyectos', 'creado', '4139', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4154', 'proyectos', 'creado', '4140', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4155', 'proyectos', 'creado', '4141', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4156', 'proyectos', 'creado', '4142', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4157', 'proyectos', 'creado', '4143', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4158', 'proyectos', 'creado', '4144', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4159', 'proyectos', 'creado', '4145', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4160', 'proyectos', 'creado', '4146', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4161', 'proyectos', 'creado', '4147', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4162', 'proyectos', 'creado', '4148', 'admin', '2017-01-09 16:47:19', '2017-01-09 16:47:19');
INSERT INTO `app_historico` VALUES ('4163', 'proyectos', 'creado', '4149', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4164', 'proyectos', 'creado', '4150', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4165', 'proyectos', 'creado', '4151', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4166', 'proyectos', 'creado', '4152', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4167', 'proyectos', 'creado', '4153', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4168', 'proyectos', 'creado', '4154', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4169', 'proyectos', 'creado', '4155', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4170', 'proyectos', 'creado', '4156', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4171', 'proyectos', 'creado', '4157', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4172', 'proyectos', 'creado', '4158', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4173', 'proyectos', 'creado', '4159', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4174', 'proyectos', 'creado', '4160', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4175', 'proyectos', 'creado', '4161', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4176', 'proyectos', 'creado', '4162', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4177', 'proyectos', 'creado', '4163', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4178', 'proyectos', 'creado', '4164', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4179', 'proyectos', 'creado', '4165', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4180', 'proyectos', 'creado', '4166', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4181', 'proyectos', 'creado', '4167', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4182', 'proyectos', 'creado', '4168', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4183', 'proyectos', 'creado', '4169', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4184', 'proyectos', 'creado', '4170', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4185', 'proyectos', 'creado', '4171', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4186', 'proyectos', 'creado', '4172', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4187', 'proyectos', 'creado', '4173', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4188', 'proyectos', 'creado', '4174', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4189', 'proyectos', 'creado', '4175', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4190', 'proyectos', 'creado', '4176', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4191', 'proyectos', 'creado', '4177', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4192', 'proyectos', 'creado', '4178', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4193', 'proyectos', 'creado', '4179', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4194', 'proyectos', 'creado', '4180', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4195', 'proyectos', 'creado', '4181', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4196', 'proyectos', 'creado', '4182', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4197', 'proyectos', 'creado', '4183', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4198', 'proyectos', 'creado', '4184', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4199', 'proyectos', 'creado', '4185', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4200', 'proyectos', 'creado', '4186', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4201', 'proyectos', 'creado', '4187', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4202', 'proyectos', 'creado', '4188', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4203', 'proyectos', 'creado', '4189', 'admin', '2017-01-09 16:47:20', '2017-01-09 16:47:20');
INSERT INTO `app_historico` VALUES ('4204', 'proyectos', 'creado', '4190', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4205', 'proyectos', 'creado', '4191', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4206', 'proyectos', 'creado', '4192', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4207', 'proyectos', 'creado', '4193', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4208', 'proyectos', 'creado', '4194', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4209', 'proyectos', 'creado', '4195', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4210', 'proyectos', 'creado', '4196', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4211', 'proyectos', 'creado', '4197', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4212', 'proyectos', 'creado', '4198', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4213', 'proyectos', 'creado', '4199', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4214', 'proyectos', 'creado', '4200', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4215', 'proyectos', 'creado', '4201', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4216', 'proyectos', 'creado', '4202', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4217', 'proyectos', 'creado', '4203', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4218', 'proyectos', 'creado', '4204', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4219', 'proyectos', 'creado', '4205', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4220', 'proyectos', 'creado', '4206', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4221', 'proyectos', 'creado', '4207', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4222', 'proyectos', 'creado', '4208', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4223', 'proyectos', 'creado', '4209', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4224', 'proyectos', 'creado', '4210', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4225', 'proyectos', 'creado', '4211', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4226', 'proyectos', 'creado', '4212', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4227', 'proyectos', 'creado', '4213', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4228', 'proyectos', 'creado', '4214', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4229', 'proyectos', 'creado', '4215', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4230', 'proyectos', 'creado', '4216', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4231', 'proyectos', 'creado', '4217', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4232', 'proyectos', 'creado', '4218', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4233', 'proyectos', 'creado', '4219', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4234', 'proyectos', 'creado', '4220', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4235', 'proyectos', 'creado', '4221', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4236', 'proyectos', 'creado', '4222', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4237', 'proyectos', 'creado', '4223', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4238', 'proyectos', 'creado', '4224', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4239', 'proyectos', 'creado', '4225', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4240', 'proyectos', 'creado', '4226', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4241', 'proyectos', 'creado', '4227', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4242', 'proyectos', 'creado', '4228', 'admin', '2017-01-09 16:47:21', '2017-01-09 16:47:21');
INSERT INTO `app_historico` VALUES ('4243', 'proyectos', 'creado', '4229', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4244', 'proyectos', 'creado', '4230', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4245', 'proyectos', 'creado', '4231', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4246', 'proyectos', 'creado', '4232', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4247', 'proyectos', 'creado', '4233', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4248', 'proyectos', 'creado', '4234', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4249', 'proyectos', 'creado', '4235', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4250', 'proyectos', 'creado', '4236', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4251', 'proyectos', 'creado', '4237', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4252', 'proyectos', 'creado', '4238', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4253', 'proyectos', 'creado', '4239', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4254', 'proyectos', 'creado', '4240', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4255', 'proyectos', 'creado', '4241', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4256', 'proyectos', 'creado', '4242', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4257', 'proyectos', 'creado', '4243', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4258', 'proyectos', 'creado', '4244', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4259', 'proyectos', 'creado', '4245', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4260', 'proyectos', 'creado', '4246', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4261', 'proyectos', 'creado', '4247', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4262', 'proyectos', 'creado', '4248', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4263', 'proyectos', 'creado', '4249', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4264', 'proyectos', 'creado', '4250', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4265', 'proyectos', 'creado', '4251', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4266', 'proyectos', 'creado', '4252', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4267', 'proyectos', 'creado', '4253', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4268', 'proyectos', 'creado', '4254', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4269', 'proyectos', 'creado', '4255', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4270', 'proyectos', 'creado', '4256', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4271', 'proyectos', 'creado', '4257', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4272', 'proyectos', 'creado', '4258', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4273', 'proyectos', 'creado', '4259', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4274', 'proyectos', 'creado', '4260', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4275', 'proyectos', 'creado', '4261', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4276', 'proyectos', 'creado', '4262', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4277', 'proyectos', 'creado', '4263', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4278', 'proyectos', 'creado', '4264', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4279', 'proyectos', 'creado', '4265', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4280', 'proyectos', 'creado', '4266', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4281', 'proyectos', 'creado', '4267', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4282', 'proyectos', 'creado', '4268', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4283', 'proyectos', 'creado', '4269', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4284', 'proyectos', 'creado', '4270', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4285', 'proyectos', 'creado', '4271', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4286', 'proyectos', 'creado', '4272', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4287', 'proyectos', 'creado', '4273', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4288', 'proyectos', 'creado', '4274', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4289', 'proyectos', 'creado', '4275', 'admin', '2017-01-09 16:47:22', '2017-01-09 16:47:22');
INSERT INTO `app_historico` VALUES ('4290', 'proyectos', 'creado', '4276', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4291', 'proyectos', 'creado', '4277', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4292', 'proyectos', 'creado', '4278', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4293', 'proyectos', 'creado', '4279', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4294', 'proyectos', 'creado', '4280', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4295', 'proyectos', 'creado', '4281', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4296', 'proyectos', 'creado', '4282', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4297', 'proyectos', 'creado', '4283', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4298', 'proyectos', 'creado', '4284', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4299', 'proyectos', 'creado', '4285', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4300', 'proyectos', 'creado', '4286', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4301', 'proyectos', 'creado', '4287', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4302', 'proyectos', 'creado', '4288', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4303', 'proyectos', 'creado', '4289', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4304', 'proyectos', 'creado', '4290', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4305', 'proyectos', 'creado', '4291', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4306', 'proyectos', 'creado', '4292', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4307', 'proyectos', 'creado', '4293', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4308', 'proyectos', 'creado', '4294', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4309', 'proyectos', 'creado', '4295', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4310', 'proyectos', 'creado', '4296', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4311', 'proyectos', 'creado', '4297', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4312', 'proyectos', 'creado', '4298', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4313', 'proyectos', 'creado', '4299', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4314', 'proyectos', 'creado', '4300', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4315', 'proyectos', 'creado', '4301', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4316', 'proyectos', 'creado', '4302', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4317', 'proyectos', 'creado', '4303', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4318', 'proyectos', 'creado', '4304', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4319', 'proyectos', 'creado', '4305', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4320', 'proyectos', 'creado', '4306', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4321', 'proyectos', 'creado', '4307', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4322', 'proyectos', 'creado', '4308', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4323', 'proyectos', 'creado', '4309', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4324', 'proyectos', 'creado', '4310', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4325', 'proyectos', 'creado', '4311', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4326', 'proyectos', 'creado', '4312', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4327', 'proyectos', 'creado', '4313', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4328', 'proyectos', 'creado', '4314', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4329', 'proyectos', 'creado', '4315', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4330', 'proyectos', 'creado', '4316', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4331', 'proyectos', 'creado', '4317', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4332', 'proyectos', 'creado', '4318', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4333', 'proyectos', 'creado', '4319', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4334', 'proyectos', 'creado', '4320', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4335', 'proyectos', 'creado', '4321', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4336', 'proyectos', 'creado', '4322', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4337', 'proyectos', 'creado', '4323', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4338', 'proyectos', 'creado', '4324', 'admin', '2017-01-09 16:47:23', '2017-01-09 16:47:23');
INSERT INTO `app_historico` VALUES ('4339', 'proyectos', 'creado', '4325', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4340', 'proyectos', 'creado', '4326', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4341', 'proyectos', 'creado', '4327', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4342', 'proyectos', 'creado', '4328', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4343', 'proyectos', 'creado', '4329', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4344', 'proyectos', 'creado', '4330', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4345', 'proyectos', 'creado', '4331', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4346', 'proyectos', 'creado', '4332', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4347', 'proyectos', 'creado', '4333', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4348', 'proyectos', 'creado', '4334', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4349', 'proyectos', 'creado', '4335', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4350', 'proyectos', 'creado', '4336', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4351', 'proyectos', 'creado', '4337', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4352', 'proyectos', 'creado', '4338', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4353', 'proyectos', 'creado', '4339', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4354', 'proyectos', 'creado', '4340', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4355', 'proyectos', 'creado', '4341', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4356', 'proyectos', 'creado', '4342', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4357', 'proyectos', 'creado', '4343', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4358', 'proyectos', 'creado', '4344', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4359', 'proyectos', 'creado', '4345', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4360', 'proyectos', 'creado', '4346', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4361', 'proyectos', 'creado', '4347', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4362', 'proyectos', 'creado', '4348', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4363', 'proyectos', 'creado', '4349', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4364', 'proyectos', 'creado', '4350', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4365', 'proyectos', 'creado', '4351', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4366', 'proyectos', 'creado', '4352', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4367', 'proyectos', 'creado', '4353', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4368', 'proyectos', 'creado', '4354', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4369', 'proyectos', 'creado', '4355', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4370', 'proyectos', 'creado', '4356', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4371', 'proyectos', 'creado', '4357', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4372', 'proyectos', 'creado', '4358', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4373', 'proyectos', 'creado', '4359', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4374', 'proyectos', 'creado', '4360', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4375', 'proyectos', 'creado', '4361', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4376', 'proyectos', 'creado', '4362', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4377', 'proyectos', 'creado', '4363', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4378', 'proyectos', 'creado', '4364', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4379', 'proyectos', 'creado', '4365', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4380', 'proyectos', 'creado', '4366', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4381', 'proyectos', 'creado', '4367', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4382', 'proyectos', 'creado', '4368', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4383', 'proyectos', 'creado', '4369', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4384', 'proyectos', 'creado', '4370', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4385', 'proyectos', 'creado', '4371', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4386', 'proyectos', 'creado', '4372', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4387', 'proyectos', 'creado', '4373', 'admin', '2017-01-09 16:47:24', '2017-01-09 16:47:24');
INSERT INTO `app_historico` VALUES ('4388', 'proyectos', 'creado', '4374', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4389', 'proyectos', 'creado', '4375', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4390', 'proyectos', 'creado', '4376', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4391', 'proyectos', 'creado', '4377', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4392', 'proyectos', 'creado', '4378', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4393', 'proyectos', 'creado', '4379', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4394', 'proyectos', 'creado', '4380', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4395', 'proyectos', 'creado', '4381', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4396', 'proyectos', 'creado', '4382', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4397', 'proyectos', 'creado', '4383', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4398', 'proyectos', 'creado', '4384', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4399', 'proyectos', 'creado', '4385', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4400', 'proyectos', 'creado', '4386', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4401', 'proyectos', 'creado', '4387', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4402', 'proyectos', 'creado', '4388', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4403', 'proyectos', 'creado', '4389', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4404', 'proyectos', 'creado', '4390', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4405', 'proyectos', 'creado', '4391', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4406', 'proyectos', 'creado', '4392', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4407', 'proyectos', 'creado', '4393', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4408', 'proyectos', 'creado', '4394', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4409', 'proyectos', 'creado', '4395', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4410', 'proyectos', 'creado', '4396', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4411', 'proyectos', 'creado', '4397', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4412', 'proyectos', 'creado', '4398', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4413', 'proyectos', 'creado', '4399', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4414', 'proyectos', 'creado', '4400', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4415', 'proyectos', 'creado', '4401', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4416', 'proyectos', 'creado', '4402', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4417', 'proyectos', 'creado', '4403', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4418', 'proyectos', 'creado', '4404', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4419', 'proyectos', 'creado', '4405', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4420', 'proyectos', 'creado', '4406', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4421', 'proyectos', 'creado', '4407', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4422', 'proyectos', 'creado', '4408', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4423', 'proyectos', 'creado', '4409', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4424', 'proyectos', 'creado', '4410', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4425', 'proyectos', 'creado', '4411', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4426', 'proyectos', 'creado', '4412', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4427', 'proyectos', 'creado', '4413', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4428', 'proyectos', 'creado', '4414', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4429', 'proyectos', 'creado', '4415', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4430', 'proyectos', 'creado', '4416', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4431', 'proyectos', 'creado', '4417', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4432', 'proyectos', 'creado', '4418', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4433', 'proyectos', 'creado', '4419', 'admin', '2017-01-09 16:47:25', '2017-01-09 16:47:25');
INSERT INTO `app_historico` VALUES ('4434', 'proyectos', 'creado', '4420', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4435', 'proyectos', 'creado', '4421', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4436', 'proyectos', 'creado', '4422', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4437', 'proyectos', 'creado', '4423', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4438', 'proyectos', 'creado', '4424', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4439', 'proyectos', 'creado', '4425', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4440', 'proyectos', 'creado', '4426', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4441', 'proyectos', 'creado', '4427', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4442', 'proyectos', 'creado', '4428', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4443', 'proyectos', 'creado', '4429', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4444', 'proyectos', 'creado', '4430', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4445', 'proyectos', 'creado', '4431', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4446', 'proyectos', 'creado', '4432', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4447', 'proyectos', 'creado', '4433', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4448', 'proyectos', 'creado', '4434', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4449', 'proyectos', 'creado', '4435', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4450', 'proyectos', 'creado', '4436', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4451', 'proyectos', 'creado', '4437', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4452', 'proyectos', 'creado', '4438', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4453', 'proyectos', 'creado', '4439', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4454', 'proyectos', 'creado', '4440', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4455', 'proyectos', 'creado', '4441', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4456', 'proyectos', 'creado', '4442', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4457', 'proyectos', 'creado', '4443', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4458', 'proyectos', 'creado', '4444', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4459', 'proyectos', 'creado', '4445', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4460', 'proyectos', 'creado', '4446', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4461', 'proyectos', 'creado', '4447', 'admin', '2017-01-09 16:47:26', '2017-01-09 16:47:26');
INSERT INTO `app_historico` VALUES ('4462', 'proyectos', 'creado', '1', 'admin', '2017-01-09 16:48:17', '2017-01-09 16:48:17');
INSERT INTO `app_historico` VALUES ('4463', 'proyectos', 'creado', '2', 'admin', '2017-01-09 16:48:17', '2017-01-09 16:48:17');
INSERT INTO `app_historico` VALUES ('4464', 'proyectos', 'creado', '3', 'admin', '2017-01-09 16:48:17', '2017-01-09 16:48:17');
INSERT INTO `app_historico` VALUES ('4465', 'proyectos', 'creado', '4', 'admin', '2017-01-09 16:48:17', '2017-01-09 16:48:17');
INSERT INTO `app_historico` VALUES ('4466', 'proyectos', 'creado', '5', 'admin', '2017-01-09 16:48:17', '2017-01-09 16:48:17');
INSERT INTO `app_historico` VALUES ('4467', 'proyectos', 'creado', '6', 'admin', '2017-01-09 16:48:17', '2017-01-09 16:48:17');
INSERT INTO `app_historico` VALUES ('4468', 'proyectos', 'creado', '7', 'admin', '2017-01-09 16:48:17', '2017-01-09 16:48:17');
INSERT INTO `app_historico` VALUES ('4469', 'proyectos', 'creado', '8', 'admin', '2017-01-09 16:48:17', '2017-01-09 16:48:17');
INSERT INTO `app_historico` VALUES ('4470', 'proyectos', 'creado', '9', 'admin', '2017-01-09 16:48:17', '2017-01-09 16:48:17');
INSERT INTO `app_historico` VALUES ('4471', 'proyectos', 'creado', '10', 'admin', '2017-01-09 16:48:17', '2017-01-09 16:48:17');
INSERT INTO `app_historico` VALUES ('4472', 'proyectos', 'creado', '11', 'admin', '2017-01-09 16:48:17', '2017-01-09 16:48:17');
INSERT INTO `app_historico` VALUES ('4473', 'proyectos', 'creado', '12', 'admin', '2017-01-09 16:48:17', '2017-01-09 16:48:17');
INSERT INTO `app_historico` VALUES ('4474', 'proyectos', 'creado', '13', 'admin', '2017-01-09 16:48:17', '2017-01-09 16:48:17');
INSERT INTO `app_historico` VALUES ('4475', 'proyectos', 'creado', '14', 'admin', '2017-01-09 16:48:17', '2017-01-09 16:48:17');
INSERT INTO `app_historico` VALUES ('4476', 'proyectos', 'creado', '15', 'admin', '2017-01-09 16:48:17', '2017-01-09 16:48:17');
INSERT INTO `app_historico` VALUES ('4477', 'proyectos', 'creado', '16', 'admin', '2017-01-09 16:48:17', '2017-01-09 16:48:17');
INSERT INTO `app_historico` VALUES ('4478', 'proyectos', 'creado', '17', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4479', 'proyectos', 'creado', '18', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4480', 'proyectos', 'creado', '19', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4481', 'proyectos', 'creado', '20', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4482', 'proyectos', 'creado', '21', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4483', 'proyectos', 'creado', '22', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4484', 'proyectos', 'creado', '23', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4485', 'proyectos', 'creado', '24', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4486', 'proyectos', 'creado', '25', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4487', 'proyectos', 'creado', '26', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4488', 'proyectos', 'creado', '27', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4489', 'proyectos', 'creado', '28', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4490', 'proyectos', 'creado', '29', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4491', 'proyectos', 'creado', '30', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4492', 'proyectos', 'creado', '31', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4493', 'proyectos', 'creado', '32', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4494', 'proyectos', 'creado', '33', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4495', 'proyectos', 'creado', '34', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4496', 'proyectos', 'creado', '35', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4497', 'proyectos', 'creado', '36', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4498', 'proyectos', 'creado', '37', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4499', 'proyectos', 'creado', '38', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4500', 'proyectos', 'creado', '39', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4501', 'proyectos', 'creado', '40', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4502', 'proyectos', 'creado', '41', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4503', 'proyectos', 'creado', '42', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4504', 'proyectos', 'creado', '43', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4505', 'proyectos', 'creado', '44', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4506', 'proyectos', 'creado', '45', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4507', 'proyectos', 'creado', '46', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4508', 'proyectos', 'creado', '47', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4509', 'proyectos', 'creado', '48', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4510', 'proyectos', 'creado', '49', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4511', 'proyectos', 'creado', '50', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4512', 'proyectos', 'creado', '51', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4513', 'proyectos', 'creado', '52', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4514', 'proyectos', 'creado', '53', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4515', 'proyectos', 'creado', '54', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4516', 'proyectos', 'creado', '55', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4517', 'proyectos', 'creado', '56', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4518', 'proyectos', 'creado', '57', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4519', 'proyectos', 'creado', '58', 'admin', '2017-01-09 16:48:18', '2017-01-09 16:48:18');
INSERT INTO `app_historico` VALUES ('4520', 'proyectos', 'creado', '59', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4521', 'proyectos', 'creado', '60', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4522', 'proyectos', 'creado', '61', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4523', 'proyectos', 'creado', '62', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4524', 'proyectos', 'creado', '63', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4525', 'proyectos', 'creado', '64', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4526', 'proyectos', 'creado', '65', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4527', 'proyectos', 'creado', '66', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4528', 'proyectos', 'creado', '67', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4529', 'proyectos', 'creado', '68', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4530', 'proyectos', 'creado', '69', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4531', 'proyectos', 'creado', '70', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4532', 'proyectos', 'creado', '71', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4533', 'proyectos', 'creado', '72', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4534', 'proyectos', 'creado', '73', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4535', 'proyectos', 'creado', '74', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4536', 'proyectos', 'creado', '75', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4537', 'proyectos', 'creado', '76', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4538', 'proyectos', 'creado', '77', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4539', 'proyectos', 'creado', '78', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4540', 'proyectos', 'creado', '79', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4541', 'proyectos', 'creado', '80', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4542', 'proyectos', 'creado', '81', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4543', 'proyectos', 'creado', '82', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4544', 'proyectos', 'creado', '83', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4545', 'proyectos', 'creado', '84', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4546', 'proyectos', 'creado', '85', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4547', 'proyectos', 'creado', '86', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4548', 'proyectos', 'creado', '87', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4549', 'proyectos', 'creado', '88', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4550', 'proyectos', 'creado', '89', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4551', 'proyectos', 'creado', '90', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4552', 'proyectos', 'creado', '91', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4553', 'proyectos', 'creado', '92', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4554', 'proyectos', 'creado', '93', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4555', 'proyectos', 'creado', '94', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4556', 'proyectos', 'creado', '95', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4557', 'proyectos', 'creado', '96', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4558', 'proyectos', 'creado', '97', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4559', 'proyectos', 'creado', '98', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4560', 'proyectos', 'creado', '99', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4561', 'proyectos', 'creado', '100', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4562', 'proyectos', 'creado', '101', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4563', 'proyectos', 'creado', '102', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4564', 'proyectos', 'creado', '103', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4565', 'proyectos', 'creado', '104', 'admin', '2017-01-09 16:48:19', '2017-01-09 16:48:19');
INSERT INTO `app_historico` VALUES ('4566', 'proyectos', 'creado', '105', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4567', 'proyectos', 'creado', '106', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4568', 'proyectos', 'creado', '107', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4569', 'proyectos', 'creado', '108', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4570', 'proyectos', 'creado', '109', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4571', 'proyectos', 'creado', '110', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4572', 'proyectos', 'creado', '111', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4573', 'proyectos', 'creado', '112', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4574', 'proyectos', 'creado', '113', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4575', 'proyectos', 'creado', '114', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4576', 'proyectos', 'creado', '115', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4577', 'proyectos', 'creado', '116', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4578', 'proyectos', 'creado', '117', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4579', 'proyectos', 'creado', '118', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4580', 'proyectos', 'creado', '119', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4581', 'proyectos', 'creado', '120', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4582', 'proyectos', 'creado', '121', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4583', 'proyectos', 'creado', '122', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4584', 'proyectos', 'creado', '123', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4585', 'proyectos', 'creado', '124', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4586', 'proyectos', 'creado', '125', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4587', 'proyectos', 'creado', '126', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4588', 'proyectos', 'creado', '127', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4589', 'proyectos', 'creado', '128', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4590', 'proyectos', 'creado', '129', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4591', 'proyectos', 'creado', '130', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4592', 'proyectos', 'creado', '131', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4593', 'proyectos', 'creado', '132', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4594', 'proyectos', 'creado', '133', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4595', 'proyectos', 'creado', '134', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4596', 'proyectos', 'creado', '135', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4597', 'proyectos', 'creado', '136', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4598', 'proyectos', 'creado', '137', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4599', 'proyectos', 'creado', '138', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4600', 'proyectos', 'creado', '139', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4601', 'proyectos', 'creado', '140', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4602', 'proyectos', 'creado', '141', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4603', 'proyectos', 'creado', '142', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4604', 'proyectos', 'creado', '143', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4605', 'proyectos', 'creado', '144', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4606', 'proyectos', 'creado', '145', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4607', 'proyectos', 'creado', '146', 'admin', '2017-01-09 16:48:20', '2017-01-09 16:48:20');
INSERT INTO `app_historico` VALUES ('4608', 'proyectos', 'creado', '147', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4609', 'proyectos', 'creado', '148', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4610', 'proyectos', 'creado', '149', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4611', 'proyectos', 'creado', '150', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4612', 'proyectos', 'creado', '151', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4613', 'proyectos', 'creado', '152', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4614', 'proyectos', 'creado', '153', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4615', 'proyectos', 'creado', '154', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4616', 'proyectos', 'creado', '155', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4617', 'proyectos', 'creado', '156', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4618', 'proyectos', 'creado', '157', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4619', 'proyectos', 'creado', '158', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4620', 'proyectos', 'creado', '159', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4621', 'proyectos', 'creado', '160', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4622', 'proyectos', 'creado', '161', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4623', 'proyectos', 'creado', '162', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4624', 'proyectos', 'creado', '163', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4625', 'proyectos', 'creado', '164', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4626', 'proyectos', 'creado', '165', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4627', 'proyectos', 'creado', '166', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4628', 'proyectos', 'creado', '167', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4629', 'proyectos', 'creado', '168', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4630', 'proyectos', 'creado', '169', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4631', 'proyectos', 'creado', '170', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4632', 'proyectos', 'creado', '171', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4633', 'proyectos', 'creado', '172', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4634', 'proyectos', 'creado', '173', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4635', 'proyectos', 'creado', '174', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4636', 'proyectos', 'creado', '175', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4637', 'proyectos', 'creado', '176', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4638', 'proyectos', 'creado', '177', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4639', 'proyectos', 'creado', '178', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4640', 'proyectos', 'creado', '179', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4641', 'proyectos', 'creado', '180', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4642', 'proyectos', 'creado', '181', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4643', 'proyectos', 'creado', '182', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4644', 'proyectos', 'creado', '183', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4645', 'proyectos', 'creado', '184', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4646', 'proyectos', 'creado', '185', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4647', 'proyectos', 'creado', '186', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4648', 'proyectos', 'creado', '187', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4649', 'proyectos', 'creado', '188', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4650', 'proyectos', 'creado', '189', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4651', 'proyectos', 'creado', '190', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4652', 'proyectos', 'creado', '191', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4653', 'proyectos', 'creado', '192', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4654', 'proyectos', 'creado', '193', 'admin', '2017-01-09 16:48:21', '2017-01-09 16:48:21');
INSERT INTO `app_historico` VALUES ('4655', 'proyectos', 'creado', '194', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4656', 'proyectos', 'actualizado', '176', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4657', 'proyectos', 'creado', '195', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4658', 'proyectos', 'creado', '196', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4659', 'proyectos', 'creado', '197', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4660', 'proyectos', 'creado', '198', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4661', 'proyectos', 'creado', '199', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4662', 'proyectos', 'creado', '200', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4663', 'proyectos', 'creado', '201', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4664', 'proyectos', 'creado', '202', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4665', 'proyectos', 'creado', '203', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4666', 'proyectos', 'creado', '204', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4667', 'proyectos', 'creado', '205', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4668', 'proyectos', 'creado', '206', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4669', 'proyectos', 'creado', '207', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4670', 'proyectos', 'creado', '208', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4671', 'proyectos', 'creado', '209', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4672', 'proyectos', 'creado', '210', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4673', 'proyectos', 'creado', '211', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4674', 'proyectos', 'creado', '212', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4675', 'proyectos', 'creado', '213', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4676', 'proyectos', 'creado', '214', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4677', 'proyectos', 'creado', '215', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4678', 'proyectos', 'creado', '216', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4679', 'proyectos', 'creado', '217', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4680', 'proyectos', 'creado', '218', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4681', 'proyectos', 'creado', '219', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4682', 'proyectos', 'creado', '220', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4683', 'proyectos', 'creado', '221', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4684', 'proyectos', 'creado', '222', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4685', 'proyectos', 'creado', '223', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4686', 'proyectos', 'creado', '224', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4687', 'proyectos', 'creado', '225', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4688', 'proyectos', 'creado', '226', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4689', 'proyectos', 'creado', '227', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4690', 'proyectos', 'creado', '228', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4691', 'proyectos', 'creado', '229', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4692', 'proyectos', 'creado', '230', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4693', 'proyectos', 'creado', '231', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4694', 'proyectos', 'creado', '232', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4695', 'proyectos', 'creado', '233', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4696', 'proyectos', 'creado', '234', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4697', 'proyectos', 'creado', '235', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4698', 'proyectos', 'creado', '236', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4699', 'proyectos', 'creado', '237', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4700', 'proyectos', 'creado', '238', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4701', 'proyectos', 'creado', '239', 'admin', '2017-01-09 16:48:22', '2017-01-09 16:48:22');
INSERT INTO `app_historico` VALUES ('4702', 'proyectos', 'creado', '240', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4703', 'proyectos', 'creado', '241', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4704', 'proyectos', 'creado', '242', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4705', 'proyectos', 'creado', '243', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4706', 'proyectos', 'creado', '244', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4707', 'proyectos', 'creado', '245', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4708', 'proyectos', 'creado', '246', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4709', 'proyectos', 'creado', '247', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4710', 'proyectos', 'creado', '248', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4711', 'proyectos', 'creado', '249', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4712', 'proyectos', 'creado', '250', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4713', 'proyectos', 'creado', '251', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4714', 'proyectos', 'creado', '252', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4715', 'proyectos', 'creado', '253', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4716', 'proyectos', 'creado', '254', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4717', 'proyectos', 'creado', '255', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4718', 'proyectos', 'creado', '256', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4719', 'proyectos', 'creado', '257', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4720', 'proyectos', 'creado', '258', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4721', 'proyectos', 'creado', '259', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4722', 'proyectos', 'creado', '260', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4723', 'proyectos', 'creado', '261', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4724', 'proyectos', 'creado', '262', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4725', 'proyectos', 'creado', '263', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4726', 'proyectos', 'creado', '264', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4727', 'proyectos', 'creado', '265', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4728', 'proyectos', 'creado', '266', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4729', 'proyectos', 'creado', '267', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4730', 'proyectos', 'creado', '268', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4731', 'proyectos', 'creado', '269', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4732', 'proyectos', 'creado', '270', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4733', 'proyectos', 'creado', '271', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4734', 'proyectos', 'creado', '272', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4735', 'proyectos', 'creado', '273', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4736', 'proyectos', 'creado', '274', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4737', 'proyectos', 'creado', '275', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4738', 'proyectos', 'creado', '276', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4739', 'proyectos', 'creado', '277', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4740', 'proyectos', 'creado', '278', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4741', 'proyectos', 'creado', '279', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4742', 'proyectos', 'creado', '280', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4743', 'proyectos', 'creado', '281', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4744', 'proyectos', 'creado', '282', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4745', 'proyectos', 'creado', '283', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4746', 'proyectos', 'creado', '284', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4747', 'proyectos', 'creado', '285', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4748', 'proyectos', 'creado', '286', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4749', 'proyectos', 'creado', '287', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4750', 'proyectos', 'creado', '288', 'admin', '2017-01-09 16:48:23', '2017-01-09 16:48:23');
INSERT INTO `app_historico` VALUES ('4751', 'proyectos', 'creado', '289', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4752', 'proyectos', 'creado', '290', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4753', 'proyectos', 'creado', '291', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4754', 'proyectos', 'creado', '292', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4755', 'proyectos', 'creado', '293', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4756', 'proyectos', 'creado', '294', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4757', 'proyectos', 'creado', '295', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4758', 'proyectos', 'creado', '296', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4759', 'proyectos', 'creado', '297', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4760', 'proyectos', 'creado', '298', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4761', 'proyectos', 'creado', '299', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4762', 'proyectos', 'creado', '300', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4763', 'proyectos', 'creado', '301', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4764', 'proyectos', 'creado', '302', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4765', 'proyectos', 'creado', '303', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4766', 'proyectos', 'creado', '304', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4767', 'proyectos', 'creado', '305', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4768', 'proyectos', 'creado', '306', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4769', 'proyectos', 'creado', '307', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4770', 'proyectos', 'creado', '308', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4771', 'proyectos', 'creado', '309', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4772', 'proyectos', 'creado', '310', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4773', 'proyectos', 'creado', '311', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4774', 'proyectos', 'creado', '312', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4775', 'proyectos', 'creado', '313', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4776', 'proyectos', 'creado', '314', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4777', 'proyectos', 'creado', '315', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4778', 'proyectos', 'creado', '316', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4779', 'proyectos', 'creado', '317', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4780', 'proyectos', 'creado', '318', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4781', 'proyectos', 'creado', '319', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4782', 'proyectos', 'creado', '320', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4783', 'proyectos', 'creado', '321', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4784', 'proyectos', 'creado', '322', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4785', 'proyectos', 'creado', '323', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4786', 'proyectos', 'creado', '324', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4787', 'proyectos', 'creado', '325', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4788', 'proyectos', 'creado', '326', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4789', 'proyectos', 'creado', '327', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4790', 'proyectos', 'creado', '328', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4791', 'proyectos', 'creado', '329', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4792', 'proyectos', 'creado', '330', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4793', 'proyectos', 'creado', '331', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4794', 'proyectos', 'creado', '332', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4795', 'proyectos', 'creado', '333', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4796', 'proyectos', 'creado', '334', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4797', 'proyectos', 'creado', '335', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4798', 'proyectos', 'creado', '336', 'admin', '2017-01-09 16:48:24', '2017-01-09 16:48:24');
INSERT INTO `app_historico` VALUES ('4799', 'proyectos', 'creado', '337', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4800', 'proyectos', 'creado', '338', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4801', 'proyectos', 'creado', '339', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4802', 'proyectos', 'creado', '340', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4803', 'proyectos', 'creado', '341', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4804', 'proyectos', 'creado', '342', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4805', 'proyectos', 'creado', '343', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4806', 'proyectos', 'creado', '344', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4807', 'proyectos', 'creado', '345', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4808', 'proyectos', 'creado', '346', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4809', 'proyectos', 'creado', '347', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4810', 'proyectos', 'creado', '348', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4811', 'proyectos', 'creado', '349', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4812', 'proyectos', 'creado', '350', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4813', 'proyectos', 'creado', '351', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4814', 'proyectos', 'creado', '352', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4815', 'proyectos', 'creado', '353', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4816', 'proyectos', 'creado', '354', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4817', 'proyectos', 'creado', '355', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4818', 'proyectos', 'creado', '356', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4819', 'proyectos', 'creado', '357', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4820', 'proyectos', 'creado', '358', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4821', 'proyectos', 'creado', '359', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4822', 'proyectos', 'creado', '360', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4823', 'proyectos', 'creado', '361', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4824', 'proyectos', 'creado', '362', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4825', 'proyectos', 'creado', '363', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4826', 'proyectos', 'creado', '364', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4827', 'proyectos', 'creado', '365', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4828', 'proyectos', 'creado', '366', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4829', 'proyectos', 'creado', '367', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4830', 'proyectos', 'creado', '368', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4831', 'proyectos', 'creado', '369', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4832', 'proyectos', 'creado', '370', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4833', 'proyectos', 'creado', '371', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4834', 'proyectos', 'creado', '372', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4835', 'proyectos', 'creado', '373', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4836', 'proyectos', 'creado', '374', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4837', 'proyectos', 'creado', '375', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4838', 'proyectos', 'creado', '376', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4839', 'proyectos', 'creado', '377', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4840', 'proyectos', 'creado', '378', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4841', 'proyectos', 'creado', '379', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4842', 'proyectos', 'creado', '380', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4843', 'proyectos', 'creado', '381', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4844', 'proyectos', 'creado', '382', 'admin', '2017-01-09 16:48:25', '2017-01-09 16:48:25');
INSERT INTO `app_historico` VALUES ('4845', 'proyectos', 'creado', '383', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4846', 'proyectos', 'creado', '384', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4847', 'proyectos', 'creado', '385', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4848', 'proyectos', 'creado', '386', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4849', 'proyectos', 'creado', '387', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4850', 'proyectos', 'creado', '388', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4851', 'proyectos', 'creado', '389', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4852', 'proyectos', 'creado', '390', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4853', 'proyectos', 'creado', '391', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4854', 'proyectos', 'creado', '392', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4855', 'proyectos', 'creado', '393', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4856', 'proyectos', 'creado', '394', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4857', 'proyectos', 'creado', '395', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4858', 'proyectos', 'creado', '396', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4859', 'proyectos', 'creado', '397', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4860', 'proyectos', 'creado', '398', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4861', 'proyectos', 'creado', '399', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4862', 'proyectos', 'creado', '400', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4863', 'proyectos', 'creado', '401', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4864', 'proyectos', 'creado', '402', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4865', 'proyectos', 'creado', '403', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4866', 'proyectos', 'creado', '404', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4867', 'proyectos', 'creado', '405', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4868', 'proyectos', 'creado', '406', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4869', 'proyectos', 'creado', '407', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4870', 'proyectos', 'creado', '408', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4871', 'proyectos', 'creado', '409', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4872', 'proyectos', 'creado', '410', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4873', 'proyectos', 'creado', '411', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4874', 'proyectos', 'creado', '412', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4875', 'proyectos', 'creado', '413', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4876', 'proyectos', 'creado', '414', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4877', 'proyectos', 'creado', '415', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4878', 'proyectos', 'creado', '416', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4879', 'proyectos', 'creado', '417', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4880', 'proyectos', 'creado', '418', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4881', 'proyectos', 'creado', '419', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4882', 'proyectos', 'creado', '420', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4883', 'proyectos', 'creado', '421', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4884', 'proyectos', 'creado', '422', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4885', 'proyectos', 'creado', '423', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4886', 'proyectos', 'creado', '424', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4887', 'proyectos', 'creado', '425', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4888', 'proyectos', 'creado', '426', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4889', 'proyectos', 'creado', '427', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4890', 'proyectos', 'creado', '428', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4891', 'proyectos', 'creado', '429', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4892', 'proyectos', 'creado', '430', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4893', 'proyectos', 'creado', '431', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4894', 'proyectos', 'creado', '432', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4895', 'proyectos', 'creado', '433', 'admin', '2017-01-09 16:48:26', '2017-01-09 16:48:26');
INSERT INTO `app_historico` VALUES ('4896', 'proyectos', 'creado', '434', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4897', 'proyectos', 'creado', '435', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4898', 'proyectos', 'creado', '436', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4899', 'proyectos', 'creado', '437', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4900', 'proyectos', 'creado', '438', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4901', 'proyectos', 'creado', '439', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4902', 'proyectos', 'creado', '440', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4903', 'proyectos', 'creado', '441', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4904', 'proyectos', 'creado', '442', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4905', 'proyectos', 'creado', '443', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4906', 'proyectos', 'creado', '444', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4907', 'proyectos', 'creado', '445', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4908', 'proyectos', 'creado', '446', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4909', 'proyectos', 'creado', '447', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4910', 'proyectos', 'creado', '448', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4911', 'proyectos', 'creado', '449', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4912', 'proyectos', 'creado', '450', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4913', 'proyectos', 'creado', '451', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4914', 'proyectos', 'creado', '452', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4915', 'proyectos', 'creado', '453', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4916', 'proyectos', 'creado', '454', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4917', 'proyectos', 'creado', '455', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4918', 'proyectos', 'creado', '456', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4919', 'proyectos', 'creado', '457', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4920', 'proyectos', 'creado', '458', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4921', 'proyectos', 'creado', '459', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4922', 'proyectos', 'creado', '460', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4923', 'proyectos', 'creado', '461', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4924', 'proyectos', 'creado', '462', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4925', 'proyectos', 'creado', '463', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4926', 'proyectos', 'creado', '464', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4927', 'proyectos', 'creado', '465', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4928', 'proyectos', 'creado', '466', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4929', 'proyectos', 'creado', '467', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4930', 'proyectos', 'creado', '468', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4931', 'proyectos', 'creado', '469', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4932', 'proyectos', 'creado', '470', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4933', 'proyectos', 'creado', '471', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4934', 'proyectos', 'creado', '472', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4935', 'proyectos', 'creado', '473', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4936', 'proyectos', 'creado', '474', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4937', 'proyectos', 'creado', '475', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4938', 'proyectos', 'creado', '476', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4939', 'proyectos', 'creado', '477', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4940', 'proyectos', 'creado', '478', 'admin', '2017-01-09 16:48:27', '2017-01-09 16:48:27');
INSERT INTO `app_historico` VALUES ('4941', 'proyectos', 'creado', '479', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4942', 'proyectos', 'creado', '480', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4943', 'proyectos', 'creado', '481', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4944', 'proyectos', 'creado', '482', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4945', 'proyectos', 'creado', '483', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4946', 'proyectos', 'creado', '484', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4947', 'proyectos', 'creado', '485', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4948', 'proyectos', 'creado', '486', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4949', 'proyectos', 'creado', '487', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4950', 'proyectos', 'creado', '488', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4951', 'proyectos', 'creado', '489', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4952', 'proyectos', 'creado', '490', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4953', 'proyectos', 'creado', '491', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4954', 'proyectos', 'creado', '492', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4955', 'proyectos', 'creado', '493', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4956', 'proyectos', 'creado', '494', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4957', 'proyectos', 'creado', '495', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4958', 'proyectos', 'creado', '496', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4959', 'proyectos', 'creado', '497', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4960', 'proyectos', 'creado', '498', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4961', 'proyectos', 'creado', '499', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4962', 'proyectos', 'creado', '500', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4963', 'proyectos', 'creado', '501', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4964', 'proyectos', 'creado', '502', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4965', 'proyectos', 'creado', '503', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4966', 'proyectos', 'creado', '504', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4967', 'proyectos', 'creado', '505', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4968', 'proyectos', 'creado', '506', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4969', 'proyectos', 'creado', '507', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4970', 'proyectos', 'creado', '508', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4971', 'proyectos', 'creado', '509', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4972', 'proyectos', 'creado', '510', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4973', 'proyectos', 'creado', '511', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4974', 'proyectos', 'creado', '512', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4975', 'proyectos', 'creado', '513', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4976', 'proyectos', 'creado', '514', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4977', 'proyectos', 'creado', '515', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4978', 'proyectos', 'creado', '516', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4979', 'proyectos', 'creado', '517', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4980', 'proyectos', 'creado', '518', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4981', 'proyectos', 'creado', '519', 'admin', '2017-01-09 16:48:28', '2017-01-09 16:48:28');
INSERT INTO `app_historico` VALUES ('4982', 'proyectos', 'creado', '520', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('4983', 'proyectos', 'creado', '521', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('4984', 'proyectos', 'creado', '522', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('4985', 'proyectos', 'creado', '523', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('4986', 'proyectos', 'creado', '524', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('4987', 'proyectos', 'creado', '525', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('4988', 'proyectos', 'creado', '526', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('4989', 'proyectos', 'creado', '527', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('4990', 'proyectos', 'creado', '528', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('4991', 'proyectos', 'creado', '529', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('4992', 'proyectos', 'creado', '530', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('4993', 'proyectos', 'creado', '531', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('4994', 'proyectos', 'creado', '532', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('4995', 'proyectos', 'creado', '533', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('4996', 'proyectos', 'creado', '534', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('4997', 'proyectos', 'creado', '535', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('4998', 'proyectos', 'creado', '536', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('4999', 'proyectos', 'creado', '537', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5000', 'proyectos', 'creado', '538', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5001', 'proyectos', 'creado', '539', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5002', 'proyectos', 'creado', '540', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5003', 'proyectos', 'creado', '541', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5004', 'proyectos', 'creado', '542', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5005', 'proyectos', 'creado', '543', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5006', 'proyectos', 'creado', '544', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5007', 'proyectos', 'creado', '545', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5008', 'proyectos', 'creado', '546', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5009', 'proyectos', 'creado', '547', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5010', 'proyectos', 'creado', '548', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5011', 'proyectos', 'creado', '549', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5012', 'proyectos', 'creado', '550', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5013', 'proyectos', 'creado', '551', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5014', 'proyectos', 'creado', '552', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5015', 'proyectos', 'creado', '553', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5016', 'proyectos', 'creado', '554', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5017', 'proyectos', 'creado', '555', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5018', 'proyectos', 'creado', '556', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5019', 'proyectos', 'creado', '557', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5020', 'proyectos', 'creado', '558', 'admin', '2017-01-09 16:48:29', '2017-01-09 16:48:29');
INSERT INTO `app_historico` VALUES ('5021', 'proyectos', 'creado', '559', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5022', 'proyectos', 'creado', '560', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5023', 'proyectos', 'creado', '561', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5024', 'proyectos', 'creado', '562', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5025', 'proyectos', 'creado', '563', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5026', 'proyectos', 'creado', '564', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5027', 'proyectos', 'creado', '565', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5028', 'proyectos', 'creado', '566', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5029', 'proyectos', 'creado', '567', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5030', 'proyectos', 'creado', '568', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5031', 'proyectos', 'creado', '569', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5032', 'proyectos', 'creado', '570', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5033', 'proyectos', 'creado', '571', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5034', 'proyectos', 'creado', '572', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5035', 'proyectos', 'creado', '573', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5036', 'proyectos', 'creado', '574', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5037', 'proyectos', 'creado', '575', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5038', 'proyectos', 'creado', '576', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5039', 'proyectos', 'creado', '577', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5040', 'proyectos', 'creado', '578', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5041', 'proyectos', 'creado', '579', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5042', 'proyectos', 'creado', '580', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5043', 'proyectos', 'creado', '581', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5044', 'proyectos', 'creado', '582', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5045', 'proyectos', 'creado', '583', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5046', 'proyectos', 'creado', '584', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5047', 'proyectos', 'creado', '585', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5048', 'proyectos', 'creado', '586', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5049', 'proyectos', 'creado', '587', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5050', 'proyectos', 'creado', '588', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5051', 'proyectos', 'creado', '589', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5052', 'proyectos', 'creado', '590', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5053', 'proyectos', 'creado', '591', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5054', 'proyectos', 'creado', '592', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5055', 'proyectos', 'creado', '593', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5056', 'proyectos', 'creado', '594', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5057', 'proyectos', 'creado', '595', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5058', 'proyectos', 'creado', '596', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5059', 'proyectos', 'creado', '597', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5060', 'proyectos', 'creado', '598', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5061', 'proyectos', 'creado', '599', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5062', 'proyectos', 'creado', '600', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5063', 'proyectos', 'creado', '601', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5064', 'proyectos', 'creado', '602', 'admin', '2017-01-09 16:48:30', '2017-01-09 16:48:30');
INSERT INTO `app_historico` VALUES ('5065', 'proyectos', 'creado', '603', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5066', 'proyectos', 'creado', '604', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5067', 'proyectos', 'creado', '605', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5068', 'proyectos', 'creado', '606', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5069', 'proyectos', 'creado', '607', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5070', 'proyectos', 'creado', '608', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5071', 'proyectos', 'creado', '609', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5072', 'proyectos', 'creado', '610', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5073', 'proyectos', 'creado', '611', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5074', 'proyectos', 'creado', '612', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5075', 'proyectos', 'creado', '613', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5076', 'proyectos', 'creado', '614', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5077', 'proyectos', 'creado', '615', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5078', 'proyectos', 'creado', '616', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5079', 'proyectos', 'creado', '617', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5080', 'proyectos', 'creado', '618', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5081', 'proyectos', 'creado', '619', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5082', 'proyectos', 'creado', '620', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5083', 'proyectos', 'creado', '621', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5084', 'proyectos', 'creado', '622', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5085', 'proyectos', 'creado', '623', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5086', 'proyectos', 'creado', '624', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5087', 'proyectos', 'creado', '625', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5088', 'proyectos', 'creado', '626', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5089', 'proyectos', 'creado', '627', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5090', 'proyectos', 'creado', '628', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5091', 'proyectos', 'creado', '629', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5092', 'proyectos', 'creado', '630', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5093', 'proyectos', 'creado', '631', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5094', 'proyectos', 'creado', '632', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5095', 'proyectos', 'creado', '633', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5096', 'proyectos', 'creado', '634', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5097', 'proyectos', 'creado', '635', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5098', 'proyectos', 'creado', '636', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5099', 'proyectos', 'creado', '637', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5100', 'proyectos', 'creado', '638', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5101', 'proyectos', 'creado', '639', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5102', 'proyectos', 'creado', '640', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5103', 'proyectos', 'creado', '641', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5104', 'proyectos', 'creado', '642', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5105', 'proyectos', 'creado', '643', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5106', 'proyectos', 'creado', '644', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5107', 'proyectos', 'creado', '645', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5108', 'proyectos', 'creado', '646', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5109', 'proyectos', 'creado', '647', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5110', 'proyectos', 'creado', '648', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5111', 'proyectos', 'creado', '649', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5112', 'proyectos', 'creado', '650', 'admin', '2017-01-09 16:48:31', '2017-01-09 16:48:31');
INSERT INTO `app_historico` VALUES ('5113', 'proyectos', 'creado', '651', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5114', 'proyectos', 'creado', '652', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5115', 'proyectos', 'creado', '653', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5116', 'proyectos', 'creado', '654', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5117', 'proyectos', 'creado', '655', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5118', 'proyectos', 'creado', '656', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5119', 'proyectos', 'creado', '657', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5120', 'proyectos', 'creado', '658', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5121', 'proyectos', 'creado', '659', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5122', 'proyectos', 'creado', '660', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5123', 'proyectos', 'creado', '661', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5124', 'proyectos', 'creado', '662', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5125', 'proyectos', 'creado', '663', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5126', 'proyectos', 'creado', '664', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5127', 'proyectos', 'creado', '665', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5128', 'proyectos', 'creado', '666', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5129', 'proyectos', 'creado', '667', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5130', 'proyectos', 'creado', '668', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5131', 'proyectos', 'creado', '669', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5132', 'proyectos', 'creado', '670', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5133', 'proyectos', 'creado', '671', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5134', 'proyectos', 'creado', '672', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5135', 'proyectos', 'creado', '673', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5136', 'proyectos', 'creado', '674', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5137', 'proyectos', 'creado', '675', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5138', 'proyectos', 'creado', '676', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5139', 'proyectos', 'creado', '677', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5140', 'proyectos', 'creado', '678', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5141', 'proyectos', 'creado', '679', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5142', 'proyectos', 'creado', '680', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5143', 'proyectos', 'creado', '681', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5144', 'proyectos', 'creado', '682', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5145', 'proyectos', 'creado', '683', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5146', 'proyectos', 'creado', '684', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5147', 'proyectos', 'creado', '685', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5148', 'proyectos', 'creado', '686', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5149', 'proyectos', 'creado', '687', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5150', 'proyectos', 'creado', '688', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5151', 'proyectos', 'creado', '689', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5152', 'proyectos', 'creado', '690', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5153', 'proyectos', 'creado', '691', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5154', 'proyectos', 'creado', '692', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5155', 'proyectos', 'creado', '693', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5156', 'proyectos', 'creado', '694', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5157', 'proyectos', 'creado', '695', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5158', 'proyectos', 'creado', '696', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5159', 'proyectos', 'creado', '697', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5160', 'proyectos', 'creado', '698', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5161', 'proyectos', 'creado', '699', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5162', 'proyectos', 'creado', '700', 'admin', '2017-01-09 16:48:32', '2017-01-09 16:48:32');
INSERT INTO `app_historico` VALUES ('5163', 'proyectos', 'creado', '701', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5164', 'proyectos', 'creado', '702', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5165', 'proyectos', 'creado', '703', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5166', 'proyectos', 'actualizado', '666', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5167', 'proyectos', 'creado', '704', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5168', 'proyectos', 'creado', '705', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5169', 'proyectos', 'creado', '706', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5170', 'proyectos', 'creado', '707', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5171', 'proyectos', 'creado', '708', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5172', 'proyectos', 'creado', '709', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5173', 'proyectos', 'creado', '710', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5174', 'proyectos', 'creado', '711', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5175', 'proyectos', 'creado', '712', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5176', 'proyectos', 'creado', '713', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5177', 'proyectos', 'creado', '714', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5178', 'proyectos', 'creado', '715', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5179', 'proyectos', 'creado', '716', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5180', 'proyectos', 'creado', '717', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5181', 'proyectos', 'creado', '718', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5182', 'proyectos', 'creado', '719', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5183', 'proyectos', 'creado', '720', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5184', 'proyectos', 'creado', '721', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5185', 'proyectos', 'creado', '722', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5186', 'proyectos', 'creado', '723', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5187', 'proyectos', 'creado', '724', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5188', 'proyectos', 'creado', '725', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5189', 'proyectos', 'creado', '726', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5190', 'proyectos', 'creado', '727', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5191', 'proyectos', 'creado', '728', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5192', 'proyectos', 'creado', '729', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5193', 'proyectos', 'creado', '730', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5194', 'proyectos', 'creado', '731', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5195', 'proyectos', 'creado', '732', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5196', 'proyectos', 'creado', '733', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5197', 'proyectos', 'creado', '734', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5198', 'proyectos', 'creado', '735', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5199', 'proyectos', 'creado', '736', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5200', 'proyectos', 'creado', '737', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5201', 'proyectos', 'creado', '738', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5202', 'proyectos', 'creado', '739', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5203', 'proyectos', 'creado', '740', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5204', 'proyectos', 'creado', '741', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5205', 'proyectos', 'creado', '742', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5206', 'proyectos', 'creado', '743', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5207', 'proyectos', 'creado', '744', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5208', 'proyectos', 'creado', '745', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5209', 'proyectos', 'creado', '746', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5210', 'proyectos', 'creado', '747', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5211', 'proyectos', 'creado', '748', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5212', 'proyectos', 'creado', '749', 'admin', '2017-01-09 16:48:33', '2017-01-09 16:48:33');
INSERT INTO `app_historico` VALUES ('5213', 'proyectos', 'creado', '750', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5214', 'proyectos', 'creado', '751', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5215', 'proyectos', 'creado', '752', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5216', 'proyectos', 'creado', '753', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5217', 'proyectos', 'creado', '754', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5218', 'proyectos', 'creado', '755', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5219', 'proyectos', 'creado', '756', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5220', 'proyectos', 'creado', '757', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5221', 'proyectos', 'creado', '758', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5222', 'proyectos', 'creado', '759', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5223', 'proyectos', 'creado', '760', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5224', 'proyectos', 'creado', '761', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5225', 'proyectos', 'creado', '762', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5226', 'proyectos', 'creado', '763', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5227', 'proyectos', 'creado', '764', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5228', 'proyectos', 'creado', '765', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5229', 'proyectos', 'creado', '766', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5230', 'proyectos', 'creado', '767', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5231', 'proyectos', 'creado', '768', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5232', 'proyectos', 'creado', '769', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5233', 'proyectos', 'creado', '770', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5234', 'proyectos', 'creado', '771', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5235', 'proyectos', 'creado', '772', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5236', 'proyectos', 'creado', '773', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5237', 'proyectos', 'creado', '774', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5238', 'proyectos', 'creado', '775', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5239', 'proyectos', 'creado', '776', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5240', 'proyectos', 'creado', '777', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5241', 'proyectos', 'creado', '778', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5242', 'proyectos', 'creado', '779', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5243', 'proyectos', 'creado', '780', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5244', 'proyectos', 'creado', '781', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5245', 'proyectos', 'creado', '782', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5246', 'proyectos', 'creado', '783', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5247', 'proyectos', 'creado', '784', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5248', 'proyectos', 'creado', '785', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5249', 'proyectos', 'creado', '786', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5250', 'proyectos', 'creado', '787', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5251', 'proyectos', 'creado', '788', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5252', 'proyectos', 'creado', '789', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5253', 'proyectos', 'creado', '790', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5254', 'proyectos', 'creado', '791', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5255', 'proyectos', 'creado', '792', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5256', 'proyectos', 'creado', '793', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5257', 'proyectos', 'creado', '794', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5258', 'proyectos', 'creado', '795', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5259', 'proyectos', 'creado', '796', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5260', 'proyectos', 'creado', '797', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5261', 'proyectos', 'creado', '798', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5262', 'proyectos', 'creado', '799', 'admin', '2017-01-09 16:48:34', '2017-01-09 16:48:34');
INSERT INTO `app_historico` VALUES ('5263', 'proyectos', 'creado', '800', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5264', 'proyectos', 'creado', '801', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5265', 'proyectos', 'creado', '802', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5266', 'proyectos', 'creado', '803', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5267', 'proyectos', 'creado', '804', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5268', 'proyectos', 'creado', '805', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5269', 'proyectos', 'creado', '806', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5270', 'proyectos', 'creado', '807', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5271', 'proyectos', 'creado', '808', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5272', 'proyectos', 'creado', '809', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5273', 'proyectos', 'creado', '810', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5274', 'proyectos', 'creado', '811', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5275', 'proyectos', 'creado', '812', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5276', 'proyectos', 'creado', '813', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5277', 'proyectos', 'creado', '814', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5278', 'proyectos', 'creado', '815', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5279', 'proyectos', 'creado', '816', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5280', 'proyectos', 'creado', '817', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5281', 'proyectos', 'creado', '818', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5282', 'proyectos', 'creado', '819', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5283', 'proyectos', 'creado', '820', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5284', 'proyectos', 'creado', '821', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5285', 'proyectos', 'creado', '822', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5286', 'proyectos', 'creado', '823', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5287', 'proyectos', 'creado', '824', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5288', 'proyectos', 'creado', '825', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5289', 'proyectos', 'creado', '826', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5290', 'proyectos', 'creado', '827', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5291', 'proyectos', 'creado', '828', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5292', 'proyectos', 'creado', '829', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5293', 'proyectos', 'creado', '830', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5294', 'proyectos', 'creado', '831', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5295', 'proyectos', 'creado', '832', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5296', 'proyectos', 'creado', '833', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5297', 'proyectos', 'creado', '834', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5298', 'proyectos', 'creado', '835', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5299', 'proyectos', 'creado', '836', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5300', 'proyectos', 'creado', '837', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5301', 'proyectos', 'creado', '838', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5302', 'proyectos', 'creado', '839', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5303', 'proyectos', 'creado', '840', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5304', 'proyectos', 'creado', '841', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5305', 'proyectos', 'creado', '842', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5306', 'proyectos', 'creado', '843', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5307', 'proyectos', 'creado', '844', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5308', 'proyectos', 'creado', '845', 'admin', '2017-01-09 16:48:35', '2017-01-09 16:48:35');
INSERT INTO `app_historico` VALUES ('5309', 'proyectos', 'creado', '846', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5310', 'proyectos', 'creado', '847', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5311', 'proyectos', 'creado', '848', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5312', 'proyectos', 'creado', '849', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5313', 'proyectos', 'creado', '850', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5314', 'proyectos', 'creado', '851', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5315', 'proyectos', 'creado', '852', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5316', 'proyectos', 'creado', '853', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5317', 'proyectos', 'creado', '854', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5318', 'proyectos', 'creado', '855', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5319', 'proyectos', 'creado', '856', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5320', 'proyectos', 'creado', '857', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5321', 'proyectos', 'creado', '858', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5322', 'proyectos', 'creado', '859', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5323', 'proyectos', 'creado', '860', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5324', 'proyectos', 'creado', '861', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5325', 'proyectos', 'creado', '862', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5326', 'proyectos', 'creado', '863', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5327', 'proyectos', 'creado', '864', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5328', 'proyectos', 'creado', '865', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5329', 'proyectos', 'creado', '866', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5330', 'proyectos', 'creado', '867', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5331', 'proyectos', 'creado', '868', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5332', 'proyectos', 'creado', '869', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5333', 'proyectos', 'creado', '870', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5334', 'proyectos', 'creado', '871', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5335', 'proyectos', 'creado', '872', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5336', 'proyectos', 'creado', '873', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5337', 'proyectos', 'creado', '874', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5338', 'proyectos', 'creado', '875', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5339', 'proyectos', 'creado', '876', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5340', 'proyectos', 'creado', '877', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5341', 'proyectos', 'creado', '878', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5342', 'proyectos', 'creado', '879', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5343', 'proyectos', 'creado', '880', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5344', 'proyectos', 'creado', '881', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5345', 'proyectos', 'creado', '882', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5346', 'proyectos', 'creado', '883', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5347', 'proyectos', 'creado', '884', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5348', 'proyectos', 'creado', '885', 'admin', '2017-01-09 16:48:36', '2017-01-09 16:48:36');
INSERT INTO `app_historico` VALUES ('5349', 'proyectos', 'creado', '886', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5350', 'proyectos', 'creado', '887', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5351', 'proyectos', 'creado', '888', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5352', 'proyectos', 'creado', '889', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5353', 'proyectos', 'creado', '890', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5354', 'proyectos', 'creado', '891', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5355', 'proyectos', 'creado', '892', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5356', 'proyectos', 'creado', '893', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5357', 'proyectos', 'creado', '894', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5358', 'proyectos', 'creado', '895', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5359', 'proyectos', 'creado', '896', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5360', 'proyectos', 'creado', '897', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5361', 'proyectos', 'creado', '898', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5362', 'proyectos', 'creado', '899', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5363', 'proyectos', 'creado', '900', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5364', 'proyectos', 'creado', '901', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5365', 'proyectos', 'creado', '902', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5366', 'proyectos', 'creado', '903', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5367', 'proyectos', 'creado', '904', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5368', 'proyectos', 'creado', '905', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5369', 'proyectos', 'creado', '906', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5370', 'proyectos', 'creado', '907', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5371', 'proyectos', 'creado', '908', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5372', 'proyectos', 'creado', '909', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5373', 'proyectos', 'creado', '910', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5374', 'proyectos', 'creado', '911', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5375', 'proyectos', 'creado', '912', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5376', 'proyectos', 'creado', '913', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5377', 'proyectos', 'creado', '914', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5378', 'proyectos', 'creado', '915', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5379', 'proyectos', 'creado', '916', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5380', 'proyectos', 'creado', '917', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5381', 'proyectos', 'creado', '918', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5382', 'proyectos', 'creado', '919', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5383', 'proyectos', 'creado', '920', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5384', 'proyectos', 'creado', '921', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5385', 'proyectos', 'creado', '922', 'admin', '2017-01-09 16:48:37', '2017-01-09 16:48:37');
INSERT INTO `app_historico` VALUES ('5386', 'proyectos', 'creado', '923', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5387', 'proyectos', 'creado', '924', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5388', 'proyectos', 'creado', '925', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5389', 'proyectos', 'creado', '926', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5390', 'proyectos', 'creado', '927', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5391', 'proyectos', 'creado', '928', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5392', 'proyectos', 'creado', '929', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5393', 'proyectos', 'creado', '930', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5394', 'proyectos', 'creado', '931', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5395', 'proyectos', 'creado', '932', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5396', 'proyectos', 'creado', '933', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5397', 'proyectos', 'creado', '934', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5398', 'proyectos', 'creado', '935', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5399', 'proyectos', 'creado', '936', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5400', 'proyectos', 'creado', '937', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5401', 'proyectos', 'creado', '938', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5402', 'proyectos', 'creado', '939', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5403', 'proyectos', 'creado', '940', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5404', 'proyectos', 'creado', '941', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5405', 'proyectos', 'creado', '942', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5406', 'proyectos', 'creado', '943', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5407', 'proyectos', 'creado', '944', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5408', 'proyectos', 'creado', '945', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5409', 'proyectos', 'creado', '946', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5410', 'proyectos', 'creado', '947', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5411', 'proyectos', 'creado', '948', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5412', 'proyectos', 'creado', '949', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5413', 'proyectos', 'creado', '950', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5414', 'proyectos', 'creado', '951', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5415', 'proyectos', 'creado', '952', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5416', 'proyectos', 'creado', '953', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5417', 'proyectos', 'creado', '954', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5418', 'proyectos', 'creado', '955', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5419', 'proyectos', 'creado', '956', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5420', 'proyectos', 'creado', '957', 'admin', '2017-01-09 16:48:38', '2017-01-09 16:48:38');
INSERT INTO `app_historico` VALUES ('5421', 'proyectos', 'creado', '958', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5422', 'proyectos', 'creado', '959', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5423', 'proyectos', 'creado', '960', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5424', 'proyectos', 'creado', '961', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5425', 'proyectos', 'creado', '962', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5426', 'proyectos', 'creado', '963', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5427', 'proyectos', 'creado', '964', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5428', 'proyectos', 'creado', '965', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5429', 'proyectos', 'creado', '966', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5430', 'proyectos', 'creado', '967', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5431', 'proyectos', 'creado', '968', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5432', 'proyectos', 'creado', '969', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5433', 'proyectos', 'creado', '970', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5434', 'proyectos', 'creado', '971', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5435', 'proyectos', 'creado', '972', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5436', 'proyectos', 'creado', '973', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5437', 'proyectos', 'creado', '974', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5438', 'proyectos', 'creado', '975', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5439', 'proyectos', 'creado', '976', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5440', 'proyectos', 'creado', '977', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5441', 'proyectos', 'creado', '978', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5442', 'proyectos', 'creado', '979', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5443', 'proyectos', 'creado', '980', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5444', 'proyectos', 'creado', '981', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5445', 'proyectos', 'creado', '982', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5446', 'proyectos', 'creado', '983', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5447', 'proyectos', 'creado', '984', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5448', 'proyectos', 'creado', '985', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5449', 'proyectos', 'creado', '986', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5450', 'proyectos', 'creado', '987', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5451', 'proyectos', 'creado', '988', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5452', 'proyectos', 'creado', '989', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5453', 'proyectos', 'creado', '990', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5454', 'proyectos', 'creado', '991', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5455', 'proyectos', 'creado', '992', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5456', 'proyectos', 'creado', '993', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5457', 'proyectos', 'creado', '994', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5458', 'proyectos', 'creado', '995', 'admin', '2017-01-09 16:48:39', '2017-01-09 16:48:39');
INSERT INTO `app_historico` VALUES ('5459', 'proyectos', 'creado', '996', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5460', 'proyectos', 'creado', '997', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5461', 'proyectos', 'creado', '998', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5462', 'proyectos', 'creado', '999', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5463', 'proyectos', 'creado', '1000', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5464', 'proyectos', 'creado', '1001', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5465', 'proyectos', 'creado', '1002', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5466', 'proyectos', 'creado', '1003', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5467', 'proyectos', 'creado', '1004', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5468', 'proyectos', 'creado', '1005', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5469', 'proyectos', 'creado', '1006', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5470', 'proyectos', 'creado', '1007', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5471', 'proyectos', 'creado', '1008', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5472', 'proyectos', 'creado', '1009', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5473', 'proyectos', 'creado', '1010', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5474', 'proyectos', 'creado', '1011', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5475', 'proyectos', 'creado', '1012', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5476', 'proyectos', 'creado', '1013', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5477', 'proyectos', 'creado', '1014', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5478', 'proyectos', 'creado', '1015', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5479', 'proyectos', 'creado', '1016', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5480', 'proyectos', 'creado', '1017', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5481', 'proyectos', 'creado', '1018', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5482', 'proyectos', 'creado', '1019', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5483', 'proyectos', 'creado', '1020', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5484', 'proyectos', 'creado', '1021', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5485', 'proyectos', 'creado', '1022', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5486', 'proyectos', 'creado', '1023', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5487', 'proyectos', 'creado', '1024', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5488', 'proyectos', 'creado', '1025', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5489', 'proyectos', 'creado', '1026', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5490', 'proyectos', 'creado', '1027', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5491', 'proyectos', 'creado', '1028', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5492', 'proyectos', 'creado', '1029', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5493', 'proyectos', 'creado', '1030', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5494', 'proyectos', 'creado', '1031', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5495', 'proyectos', 'creado', '1032', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5496', 'proyectos', 'creado', '1033', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5497', 'proyectos', 'creado', '1034', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5498', 'proyectos', 'creado', '1035', 'admin', '2017-01-09 16:48:40', '2017-01-09 16:48:40');
INSERT INTO `app_historico` VALUES ('5499', 'proyectos', 'creado', '1036', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5500', 'proyectos', 'creado', '1037', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5501', 'proyectos', 'creado', '1038', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5502', 'proyectos', 'creado', '1039', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5503', 'proyectos', 'creado', '1040', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5504', 'proyectos', 'creado', '1041', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5505', 'proyectos', 'creado', '1042', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5506', 'proyectos', 'creado', '1043', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5507', 'proyectos', 'creado', '1044', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5508', 'proyectos', 'creado', '1045', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5509', 'proyectos', 'creado', '1046', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5510', 'proyectos', 'creado', '1047', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5511', 'proyectos', 'creado', '1048', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5512', 'proyectos', 'creado', '1049', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5513', 'proyectos', 'creado', '1050', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5514', 'proyectos', 'creado', '1051', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5515', 'proyectos', 'creado', '1052', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5516', 'proyectos', 'creado', '1053', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5517', 'proyectos', 'creado', '1054', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5518', 'proyectos', 'creado', '1055', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5519', 'proyectos', 'creado', '1056', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5520', 'proyectos', 'creado', '1057', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5521', 'proyectos', 'creado', '1058', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5522', 'proyectos', 'creado', '1059', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5523', 'proyectos', 'creado', '1060', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5524', 'proyectos', 'creado', '1061', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5525', 'proyectos', 'creado', '1062', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5526', 'proyectos', 'creado', '1063', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5527', 'proyectos', 'creado', '1064', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5528', 'proyectos', 'creado', '1065', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5529', 'proyectos', 'creado', '1066', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5530', 'proyectos', 'creado', '1067', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5531', 'proyectos', 'creado', '1068', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5532', 'proyectos', 'creado', '1069', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5533', 'proyectos', 'creado', '1070', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5534', 'proyectos', 'creado', '1071', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5535', 'proyectos', 'creado', '1072', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5536', 'proyectos', 'creado', '1073', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5537', 'proyectos', 'creado', '1074', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5538', 'proyectos', 'creado', '1075', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5539', 'proyectos', 'creado', '1076', 'admin', '2017-01-09 16:48:41', '2017-01-09 16:48:41');
INSERT INTO `app_historico` VALUES ('5540', 'proyectos', 'creado', '1077', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5541', 'proyectos', 'creado', '1078', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5542', 'proyectos', 'creado', '1079', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5543', 'proyectos', 'creado', '1080', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5544', 'proyectos', 'creado', '1081', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5545', 'proyectos', 'creado', '1082', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5546', 'proyectos', 'creado', '1083', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5547', 'proyectos', 'creado', '1084', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5548', 'proyectos', 'creado', '1085', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5549', 'proyectos', 'creado', '1086', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5550', 'proyectos', 'creado', '1087', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5551', 'proyectos', 'creado', '1088', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5552', 'proyectos', 'creado', '1089', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5553', 'proyectos', 'creado', '1090', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5554', 'proyectos', 'creado', '1091', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5555', 'proyectos', 'creado', '1092', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5556', 'proyectos', 'creado', '1093', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5557', 'proyectos', 'creado', '1094', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5558', 'proyectos', 'creado', '1095', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5559', 'proyectos', 'creado', '1096', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5560', 'proyectos', 'creado', '1097', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5561', 'proyectos', 'creado', '1098', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5562', 'proyectos', 'creado', '1099', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5563', 'proyectos', 'creado', '1100', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5564', 'proyectos', 'creado', '1101', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5565', 'proyectos', 'creado', '1102', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5566', 'proyectos', 'creado', '1103', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5567', 'proyectos', 'creado', '1104', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5568', 'proyectos', 'creado', '1105', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5569', 'proyectos', 'creado', '1106', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5570', 'proyectos', 'creado', '1107', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5571', 'proyectos', 'creado', '1108', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5572', 'proyectos', 'creado', '1109', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5573', 'proyectos', 'creado', '1110', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5574', 'proyectos', 'creado', '1111', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5575', 'proyectos', 'creado', '1112', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5576', 'proyectos', 'creado', '1113', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5577', 'proyectos', 'creado', '1114', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5578', 'proyectos', 'creado', '1115', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5579', 'proyectos', 'creado', '1116', 'admin', '2017-01-09 16:48:42', '2017-01-09 16:48:42');
INSERT INTO `app_historico` VALUES ('5580', 'proyectos', 'creado', '1117', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5581', 'proyectos', 'creado', '1118', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5582', 'proyectos', 'creado', '1119', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5583', 'proyectos', 'creado', '1120', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5584', 'proyectos', 'creado', '1121', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5585', 'proyectos', 'creado', '1122', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5586', 'proyectos', 'creado', '1123', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5587', 'proyectos', 'creado', '1124', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5588', 'proyectos', 'creado', '1125', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5589', 'proyectos', 'creado', '1126', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5590', 'proyectos', 'creado', '1127', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5591', 'proyectos', 'creado', '1128', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5592', 'proyectos', 'creado', '1129', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5593', 'proyectos', 'creado', '1130', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5594', 'proyectos', 'creado', '1131', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5595', 'proyectos', 'creado', '1132', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5596', 'proyectos', 'creado', '1133', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5597', 'proyectos', 'creado', '1134', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5598', 'proyectos', 'creado', '1135', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5599', 'proyectos', 'creado', '1136', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5600', 'proyectos', 'creado', '1137', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5601', 'proyectos', 'creado', '1138', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5602', 'proyectos', 'creado', '1139', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5603', 'proyectos', 'creado', '1140', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5604', 'proyectos', 'creado', '1141', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5605', 'proyectos', 'creado', '1142', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5606', 'proyectos', 'creado', '1143', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5607', 'proyectos', 'creado', '1144', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5608', 'proyectos', 'creado', '1145', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5609', 'proyectos', 'creado', '1146', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5610', 'proyectos', 'creado', '1147', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5611', 'proyectos', 'creado', '1148', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5612', 'proyectos', 'creado', '1149', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5613', 'proyectos', 'creado', '1150', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5614', 'proyectos', 'creado', '1151', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5615', 'proyectos', 'creado', '1152', 'admin', '2017-01-09 16:48:43', '2017-01-09 16:48:43');
INSERT INTO `app_historico` VALUES ('5616', 'proyectos', 'creado', '1153', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5617', 'proyectos', 'creado', '1154', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5618', 'proyectos', 'creado', '1155', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5619', 'proyectos', 'creado', '1156', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5620', 'proyectos', 'creado', '1157', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5621', 'proyectos', 'creado', '1158', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5622', 'proyectos', 'creado', '1159', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5623', 'proyectos', 'creado', '1160', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5624', 'proyectos', 'creado', '1161', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5625', 'proyectos', 'creado', '1162', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5626', 'proyectos', 'creado', '1163', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5627', 'proyectos', 'creado', '1164', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5628', 'proyectos', 'creado', '1165', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5629', 'proyectos', 'creado', '1166', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5630', 'proyectos', 'creado', '1167', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5631', 'proyectos', 'creado', '1168', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5632', 'proyectos', 'creado', '1169', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5633', 'proyectos', 'creado', '1170', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5634', 'proyectos', 'creado', '1171', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5635', 'proyectos', 'creado', '1172', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5636', 'proyectos', 'creado', '1173', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5637', 'proyectos', 'creado', '1174', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5638', 'proyectos', 'creado', '1175', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5639', 'proyectos', 'creado', '1176', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5640', 'proyectos', 'creado', '1177', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5641', 'proyectos', 'creado', '1178', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5642', 'proyectos', 'creado', '1179', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5643', 'proyectos', 'creado', '1180', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5644', 'proyectos', 'creado', '1181', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5645', 'proyectos', 'creado', '1182', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5646', 'proyectos', 'creado', '1183', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5647', 'proyectos', 'creado', '1184', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5648', 'proyectos', 'creado', '1185', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5649', 'proyectos', 'creado', '1186', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5650', 'proyectos', 'creado', '1187', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5651', 'proyectos', 'creado', '1188', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5652', 'proyectos', 'creado', '1189', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5653', 'proyectos', 'creado', '1190', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5654', 'proyectos', 'creado', '1191', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5655', 'proyectos', 'creado', '1192', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5656', 'proyectos', 'creado', '1193', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5657', 'proyectos', 'creado', '1194', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5658', 'proyectos', 'creado', '1195', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5659', 'proyectos', 'creado', '1196', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5660', 'proyectos', 'creado', '1197', 'admin', '2017-01-09 16:48:44', '2017-01-09 16:48:44');
INSERT INTO `app_historico` VALUES ('5661', 'proyectos', 'creado', '1198', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5662', 'proyectos', 'creado', '1199', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5663', 'proyectos', 'creado', '1200', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5664', 'proyectos', 'creado', '1201', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5665', 'proyectos', 'creado', '1202', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5666', 'proyectos', 'creado', '1203', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5667', 'proyectos', 'creado', '1204', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5668', 'proyectos', 'creado', '1205', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5669', 'proyectos', 'creado', '1206', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5670', 'proyectos', 'creado', '1207', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5671', 'proyectos', 'creado', '1208', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5672', 'proyectos', 'creado', '1209', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5673', 'proyectos', 'creado', '1210', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5674', 'proyectos', 'creado', '1211', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5675', 'proyectos', 'creado', '1212', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5676', 'proyectos', 'creado', '1213', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5677', 'proyectos', 'creado', '1214', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5678', 'proyectos', 'creado', '1215', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5679', 'proyectos', 'creado', '1216', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5680', 'proyectos', 'creado', '1217', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5681', 'proyectos', 'creado', '1218', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5682', 'proyectos', 'creado', '1219', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5683', 'proyectos', 'creado', '1220', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5684', 'proyectos', 'creado', '1221', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5685', 'proyectos', 'creado', '1222', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5686', 'proyectos', 'creado', '1223', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5687', 'proyectos', 'creado', '1224', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5688', 'proyectos', 'creado', '1225', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5689', 'proyectos', 'creado', '1226', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5690', 'proyectos', 'creado', '1227', 'admin', '2017-01-09 16:48:45', '2017-01-09 16:48:45');
INSERT INTO `app_historico` VALUES ('5691', 'proyectos', 'creado', '1228', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5692', 'proyectos', 'creado', '1229', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5693', 'proyectos', 'creado', '1230', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5694', 'proyectos', 'creado', '1231', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5695', 'proyectos', 'creado', '1232', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5696', 'proyectos', 'creado', '1233', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5697', 'proyectos', 'creado', '1234', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5698', 'proyectos', 'creado', '1235', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5699', 'proyectos', 'creado', '1236', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5700', 'proyectos', 'creado', '1237', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5701', 'proyectos', 'creado', '1238', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5702', 'proyectos', 'creado', '1239', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5703', 'proyectos', 'creado', '1240', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5704', 'proyectos', 'creado', '1241', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5705', 'proyectos', 'creado', '1242', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5706', 'proyectos', 'creado', '1243', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5707', 'proyectos', 'creado', '1244', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5708', 'proyectos', 'creado', '1245', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5709', 'proyectos', 'creado', '1246', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5710', 'proyectos', 'creado', '1247', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5711', 'proyectos', 'creado', '1248', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5712', 'proyectos', 'creado', '1249', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5713', 'proyectos', 'creado', '1250', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5714', 'proyectos', 'creado', '1251', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5715', 'proyectos', 'creado', '1252', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5716', 'proyectos', 'creado', '1253', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5717', 'proyectos', 'creado', '1254', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5718', 'proyectos', 'creado', '1255', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5719', 'proyectos', 'creado', '1256', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5720', 'proyectos', 'creado', '1257', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5721', 'proyectos', 'creado', '1258', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5722', 'proyectos', 'creado', '1259', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5723', 'proyectos', 'creado', '1260', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5724', 'proyectos', 'creado', '1261', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5725', 'proyectos', 'creado', '1262', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5726', 'proyectos', 'creado', '1263', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5727', 'proyectos', 'creado', '1264', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5728', 'proyectos', 'creado', '1265', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5729', 'proyectos', 'creado', '1266', 'admin', '2017-01-09 16:48:46', '2017-01-09 16:48:46');
INSERT INTO `app_historico` VALUES ('5730', 'proyectos', 'creado', '1267', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5731', 'proyectos', 'creado', '1268', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5732', 'proyectos', 'creado', '1269', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5733', 'proyectos', 'creado', '1270', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5734', 'proyectos', 'creado', '1271', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5735', 'proyectos', 'creado', '1272', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5736', 'proyectos', 'creado', '1273', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5737', 'proyectos', 'creado', '1274', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5738', 'proyectos', 'creado', '1275', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5739', 'proyectos', 'creado', '1276', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5740', 'proyectos', 'creado', '1277', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5741', 'proyectos', 'creado', '1278', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5742', 'proyectos', 'creado', '1279', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5743', 'proyectos', 'creado', '1280', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5744', 'proyectos', 'creado', '1281', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5745', 'proyectos', 'creado', '1282', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5746', 'proyectos', 'creado', '1283', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5747', 'proyectos', 'creado', '1284', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5748', 'proyectos', 'creado', '1285', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5749', 'proyectos', 'creado', '1286', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5750', 'proyectos', 'creado', '1287', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5751', 'proyectos', 'creado', '1288', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5752', 'proyectos', 'creado', '1289', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5753', 'proyectos', 'creado', '1290', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5754', 'proyectos', 'creado', '1291', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5755', 'proyectos', 'creado', '1292', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5756', 'proyectos', 'creado', '1293', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5757', 'proyectos', 'creado', '1294', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5758', 'proyectos', 'creado', '1295', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5759', 'proyectos', 'creado', '1296', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5760', 'proyectos', 'creado', '1297', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5761', 'proyectos', 'creado', '1298', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5762', 'proyectos', 'creado', '1299', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5763', 'proyectos', 'creado', '1300', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5764', 'proyectos', 'creado', '1301', 'admin', '2017-01-09 16:48:47', '2017-01-09 16:48:47');
INSERT INTO `app_historico` VALUES ('5765', 'proyectos', 'creado', '1302', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5766', 'proyectos', 'creado', '1303', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5767', 'proyectos', 'creado', '1304', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5768', 'proyectos', 'creado', '1305', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5769', 'proyectos', 'creado', '1306', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5770', 'proyectos', 'creado', '1307', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5771', 'proyectos', 'creado', '1308', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5772', 'proyectos', 'creado', '1309', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5773', 'proyectos', 'creado', '1310', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5774', 'proyectos', 'creado', '1311', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5775', 'proyectos', 'creado', '1312', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5776', 'proyectos', 'creado', '1313', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5777', 'proyectos', 'creado', '1314', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5778', 'proyectos', 'creado', '1315', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5779', 'proyectos', 'creado', '1316', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5780', 'proyectos', 'creado', '1317', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5781', 'proyectos', 'creado', '1318', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5782', 'proyectos', 'creado', '1319', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5783', 'proyectos', 'creado', '1320', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5784', 'proyectos', 'creado', '1321', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5785', 'proyectos', 'creado', '1322', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5786', 'proyectos', 'creado', '1323', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5787', 'proyectos', 'creado', '1324', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5788', 'proyectos', 'creado', '1325', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5789', 'proyectos', 'creado', '1326', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5790', 'proyectos', 'creado', '1327', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5791', 'proyectos', 'creado', '1328', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5792', 'proyectos', 'creado', '1329', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5793', 'proyectos', 'creado', '1330', 'admin', '2017-01-09 16:48:48', '2017-01-09 16:48:48');
INSERT INTO `app_historico` VALUES ('5794', 'proyectos', 'creado', '1331', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5795', 'proyectos', 'creado', '1332', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5796', 'proyectos', 'creado', '1333', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5797', 'proyectos', 'creado', '1334', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5798', 'proyectos', 'creado', '1335', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5799', 'proyectos', 'creado', '1336', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5800', 'proyectos', 'creado', '1337', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5801', 'proyectos', 'creado', '1338', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5802', 'proyectos', 'creado', '1339', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5803', 'proyectos', 'creado', '1340', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5804', 'proyectos', 'creado', '1341', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5805', 'proyectos', 'creado', '1342', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5806', 'proyectos', 'creado', '1343', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5807', 'proyectos', 'creado', '1344', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5808', 'proyectos', 'creado', '1345', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5809', 'proyectos', 'creado', '1346', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5810', 'proyectos', 'creado', '1347', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5811', 'proyectos', 'creado', '1348', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5812', 'proyectos', 'creado', '1349', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5813', 'proyectos', 'creado', '1350', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5814', 'proyectos', 'creado', '1351', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5815', 'proyectos', 'creado', '1352', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5816', 'proyectos', 'creado', '1353', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5817', 'proyectos', 'creado', '1354', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5818', 'proyectos', 'creado', '1355', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5819', 'proyectos', 'creado', '1356', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5820', 'proyectos', 'creado', '1357', 'admin', '2017-01-09 16:48:49', '2017-01-09 16:48:49');
INSERT INTO `app_historico` VALUES ('5821', 'proyectos', 'creado', '1358', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5822', 'proyectos', 'creado', '1359', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5823', 'proyectos', 'creado', '1360', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5824', 'proyectos', 'creado', '1361', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5825', 'proyectos', 'creado', '1362', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5826', 'proyectos', 'creado', '1363', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5827', 'proyectos', 'creado', '1364', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5828', 'proyectos', 'creado', '1365', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5829', 'proyectos', 'creado', '1366', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5830', 'proyectos', 'creado', '1367', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5831', 'proyectos', 'creado', '1368', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5832', 'proyectos', 'creado', '1369', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5833', 'proyectos', 'creado', '1370', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5834', 'proyectos', 'creado', '1371', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5835', 'proyectos', 'creado', '1372', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5836', 'proyectos', 'creado', '1373', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5837', 'proyectos', 'creado', '1374', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5838', 'proyectos', 'creado', '1375', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5839', 'proyectos', 'creado', '1376', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5840', 'proyectos', 'creado', '1377', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5841', 'proyectos', 'creado', '1378', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5842', 'proyectos', 'creado', '1379', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5843', 'proyectos', 'creado', '1380', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5844', 'proyectos', 'creado', '1381', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5845', 'proyectos', 'creado', '1382', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5846', 'proyectos', 'creado', '1383', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5847', 'proyectos', 'creado', '1384', 'admin', '2017-01-09 16:48:50', '2017-01-09 16:48:50');
INSERT INTO `app_historico` VALUES ('5848', 'proyectos', 'creado', '1385', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5849', 'proyectos', 'creado', '1386', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5850', 'proyectos', 'creado', '1387', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5851', 'proyectos', 'creado', '1388', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5852', 'proyectos', 'creado', '1389', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5853', 'proyectos', 'creado', '1390', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5854', 'proyectos', 'creado', '1391', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5855', 'proyectos', 'creado', '1392', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5856', 'proyectos', 'creado', '1393', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5857', 'proyectos', 'creado', '1394', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5858', 'proyectos', 'creado', '1395', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5859', 'proyectos', 'creado', '1396', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5860', 'proyectos', 'creado', '1397', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5861', 'proyectos', 'creado', '1398', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5862', 'proyectos', 'creado', '1399', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5863', 'proyectos', 'creado', '1400', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5864', 'proyectos', 'creado', '1401', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5865', 'proyectos', 'creado', '1402', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5866', 'proyectos', 'creado', '1403', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5867', 'proyectos', 'creado', '1404', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5868', 'proyectos', 'creado', '1405', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5869', 'proyectos', 'creado', '1406', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5870', 'proyectos', 'creado', '1407', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5871', 'proyectos', 'creado', '1408', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5872', 'proyectos', 'creado', '1409', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5873', 'proyectos', 'creado', '1410', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5874', 'proyectos', 'creado', '1411', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5875', 'proyectos', 'creado', '1412', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5876', 'proyectos', 'creado', '1413', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5877', 'proyectos', 'creado', '1414', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5878', 'proyectos', 'creado', '1415', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5879', 'proyectos', 'creado', '1416', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5880', 'proyectos', 'creado', '1417', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5881', 'proyectos', 'creado', '1418', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5882', 'proyectos', 'creado', '1419', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5883', 'proyectos', 'creado', '1420', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5884', 'proyectos', 'creado', '1421', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5885', 'proyectos', 'creado', '1422', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5886', 'proyectos', 'creado', '1423', 'admin', '2017-01-09 16:48:51', '2017-01-09 16:48:51');
INSERT INTO `app_historico` VALUES ('5887', 'proyectos', 'creado', '1424', 'admin', '2017-01-09 16:48:52', '2017-01-09 16:48:52');
INSERT INTO `app_historico` VALUES ('5888', 'proyectos', 'creado', '1425', 'admin', '2017-01-09 16:48:52', '2017-01-09 16:48:52');
INSERT INTO `app_historico` VALUES ('5889', 'proyectos', 'creado', '1426', 'admin', '2017-01-09 16:48:52', '2017-01-09 16:48:52');
INSERT INTO `app_historico` VALUES ('5890', 'proyectos', 'creado', '1427', 'admin', '2017-01-09 16:48:52', '2017-01-09 16:48:52');
INSERT INTO `app_historico` VALUES ('5891', 'proyectos', 'creado', '1428', 'admin', '2017-01-09 16:48:52', '2017-01-09 16:48:52');
INSERT INTO `app_historico` VALUES ('5892', 'proyectos', 'creado', '1429', 'admin', '2017-01-09 16:48:52', '2017-01-09 16:48:52');
INSERT INTO `app_historico` VALUES ('5893', 'proyectos', 'creado', '1430', 'admin', '2017-01-09 16:48:52', '2017-01-09 16:48:52');
INSERT INTO `app_historico` VALUES ('5894', 'proyectos', 'creado', '1431', 'admin', '2017-01-09 16:48:52', '2017-01-09 16:48:52');
INSERT INTO `app_historico` VALUES ('5895', 'proyectos', 'creado', '1432', 'admin', '2017-01-09 16:48:52', '2017-01-09 16:48:52');
INSERT INTO `app_historico` VALUES ('5896', 'proyectos', 'creado', '1433', 'admin', '2017-01-09 16:48:52', '2017-01-09 16:48:52');
INSERT INTO `app_historico` VALUES ('5897', 'proyectos', 'creado', '1434', 'admin', '2017-01-09 16:48:52', '2017-01-09 16:48:52');
INSERT INTO `app_historico` VALUES ('5898', 'proyectos', 'creado', '1435', 'admin', '2017-01-09 16:48:52', '2017-01-09 16:48:52');
INSERT INTO `app_historico` VALUES ('5899', 'proyectos', 'creado', '1436', 'admin', '2017-01-09 16:48:52', '2017-01-09 16:48:52');
INSERT INTO `app_historico` VALUES ('5900', 'proyectos', 'creado', '1437', 'admin', '2017-01-09 16:48:52', '2017-01-09 16:48:52');
INSERT INTO `app_historico` VALUES ('5901', 'proyectos', 'creado', '1438', 'admin', '2017-01-09 16:48:52', '2017-01-09 16:48:52');
INSERT INTO `app_historico` VALUES ('5902', 'proyectos', 'creado', '1439', 'admin', '2017-01-09 16:48:52', '2017-01-09 16:48:52');
INSERT INTO `app_historico` VALUES ('5903', 'proyectos', 'creado', '1440', 'admin', '2017-01-09 16:48:52', '2017-01-09 16:48:52');
INSERT INTO `app_historico` VALUES ('5904', 'proyectos', 'creado', '1441', 'admin', '2017-01-09 16:48:52', '2017-01-09 16:48:52');
INSERT INTO `app_historico` VALUES ('5905', 'proyectos', 'creado', '1442', 'admin', '2017-01-09 16:48:52', '2017-01-09 16:48:52');
INSERT INTO `app_historico` VALUES ('5906', 'proyectos', 'creado', '1443', 'admin', '2017-01-09 16:48:52', '2017-01-09 16:48:52');
INSERT INTO `app_historico` VALUES ('5907', 'proyectos', 'creado', '1444', 'admin', '2017-01-09 16:48:52', '2017-01-09 16:48:52');

-- ----------------------------
-- Table structure for app_perfil
-- ----------------------------
DROP TABLE IF EXISTS `app_perfil`;
CREATE TABLE `app_perfil` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_perfil_nombre_unique` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of app_perfil
-- ----------------------------
INSERT INTO `app_perfil` VALUES ('1', 'Desarrollador', '2017-01-09 10:50:51', '2017-01-09 10:50:51', null);
INSERT INTO `app_perfil` VALUES ('2', 'Administrador', '2017-01-09 10:50:51', '2017-01-09 10:50:51', null);
INSERT INTO `app_perfil` VALUES ('3', 'Tecnico', '2017-01-09 10:50:51', '2017-01-09 10:50:51', null);
INSERT INTO `app_perfil` VALUES ('4', 'Supervisor', '2017-01-09 10:50:51', '2017-01-09 10:50:51', null);
INSERT INTO `app_perfil` VALUES ('5', 'Asistente', '2017-01-09 10:50:51', '2017-01-09 10:50:51', null);
INSERT INTO `app_perfil` VALUES ('6', 'Secretaria', '2017-01-09 10:50:51', '2017-01-09 10:50:51', null);

-- ----------------------------
-- Table structure for app_perfiles_permisos
-- ----------------------------
DROP TABLE IF EXISTS `app_perfiles_permisos`;
CREATE TABLE `app_perfiles_permisos` (
  `perfil_id` int(10) unsigned NOT NULL,
  `ruta` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `app_perfiles_permisos_perfil_id_foreign` (`perfil_id`),
  CONSTRAINT `app_perfiles_permisos_perfil_id_foreign` FOREIGN KEY (`perfil_id`) REFERENCES `app_perfil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of app_perfiles_permisos
-- ----------------------------

-- ----------------------------
-- Table structure for app_usuario
-- ----------------------------
DROP TABLE IF EXISTS `app_usuario`;
CREATE TABLE `app_usuario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `dni` int(10) unsigned NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user.png',
  `perfil_id` int(10) unsigned DEFAULT NULL,
  `autenticacion` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'l',
  `super` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `sexo` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edo_civil` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_usuario_usuario_unique` (`usuario`),
  UNIQUE KEY `app_usuario_dni_unique` (`dni`),
  UNIQUE KEY `app_usuario_correo_unique` (`correo`),
  KEY `app_usuario_perfil_id_foreign` (`perfil_id`),
  CONSTRAINT `app_usuario_perfil_id_foreign` FOREIGN KEY (`perfil_id`) REFERENCES `app_perfil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of app_usuario
-- ----------------------------
INSERT INTO `app_usuario` VALUES ('1', 'admin', '$2y$10$3uNddBpHgiPhxo.LWeu3RuJNBYXpJYpTOZik7iRMJ4.qFyM1Atsla', '12345678', 'Administrador', '', 'admin@gmail.com', '0414-123-1234', 'user.png', '1', 'B', 's', 'm', 's', null, null, null, null, null, '2017-01-09 10:50:51', '2017-01-09 10:50:51', null);

-- ----------------------------
-- Table structure for app_usuario_permisos
-- ----------------------------
DROP TABLE IF EXISTS `app_usuario_permisos`;
CREATE TABLE `app_usuario_permisos` (
  `usuario_id` int(10) unsigned NOT NULL,
  `ruta` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `app_usuario_permisos_usuario_id_foreign` (`usuario_id`),
  CONSTRAINT `app_usuario_permisos_usuario_id_foreign` FOREIGN KEY (`usuario_id`) REFERENCES `app_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of app_usuario_permisos
-- ----------------------------

-- ----------------------------
-- Table structure for configuracion
-- ----------------------------
DROP TABLE IF EXISTS `configuracion`;
CREATE TABLE `configuracion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `propiedad` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `valor` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of configuracion
-- ----------------------------

-- ----------------------------
-- Table structure for ente_ejecutor
-- ----------------------------
DROP TABLE IF EXISTS `ente_ejecutor`;
CREATE TABLE `ente_ejecutor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ente_ejecutor
-- ----------------------------
INSERT INTO `ente_ejecutor` VALUES ('1', 'Hidrobolívar', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('2', 'Secretaría de Mantenimiento y Servicios Generales', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('3', 'Inviobras', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('4', 'Secretaría de Administración y Finanzas', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('5', 'Fondo Bolívar', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('6', 'Fundacite Guayana', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('7', 'Secretaría de Vivienda y Habitat', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('8', 'Construbolívar', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('9', 'Instituto Autónomo Minas Bolívar (IAMIB)', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('10', 'Fundación Social del Niño', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('11', 'Dirección de Patrimonio Cultural', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('12', 'UCER', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('13', 'FEDE - Gobernación', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('14', 'Servicio Autónomo de Aeropuertos Regionales del Estado Bolívar (SAAR)', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('15', 'Instituto de Salud Pública (ISP)', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('16', 'Instituto Regional de Tecnología Agropecuaria del Estado Bolívar (IRTAB)', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('17', 'Secretaría de Cultura', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('18', 'Servicio Autónomo Asfalto Bolívar (SAAB)', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('19', 'Dirección de Servicios Aéreos del Estado Bolívar', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('20', 'Servicio Autónomo Emergencia 1-7-1', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('21', 'Instituto de Deporte del Estado Bolívar', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('22', 'Secretaría de Turismo y Ambiente', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('23', 'Transbolívar', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('24', 'Granitos Bolívar', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('25', 'Secretaría General de Gobierno', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('26', 'Operadora Turística Bolívar, C.A.', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('27', 'Alimentos Bolívar, C.A.', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('28', 'Secretaría de Ambiente ', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('29', 'Agroinsumos Bolívar, C.A.', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('30', 'Agrobolívar, C.A.', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `ente_ejecutor` VALUES ('31', 'Servicio Autónomo Asfalto Bolívar', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);

-- ----------------------------
-- Table structure for estatus
-- ----------------------------
DROP TABLE IF EXISTS `estatus`;
CREATE TABLE `estatus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of estatus
-- ----------------------------
INSERT INTO `estatus` VALUES ('1', 'Concluida', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `estatus` VALUES ('2', 'En Ejecución', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `estatus` VALUES ('3', 'Por Iniciar', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);

-- ----------------------------
-- Table structure for fuente_financiamiento
-- ----------------------------
DROP TABLE IF EXISTS `fuente_financiamiento`;
CREATE TABLE `fuente_financiamiento` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of fuente_financiamiento
-- ----------------------------
INSERT INTO `fuente_financiamiento` VALUES ('1', 'Consejo Federal de Gobierno', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `fuente_financiamiento` VALUES ('2', 'Fides', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `fuente_financiamiento` VALUES ('3', 'Laee', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `fuente_financiamiento` VALUES ('4', 'CFG asignación Directa', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `fuente_financiamiento` VALUES ('5', 'Fondo Zamorano', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `fuente_financiamiento` VALUES ('6', 'Convenio CFG - Gobernación', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `fuente_financiamiento` VALUES ('7', 'Oficina Nacional del Tesoro (ONT)', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `fuente_financiamiento` VALUES ('8', 'HIDROVEN', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `fuente_financiamiento` VALUES ('9', 'MPPPTT', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `fuente_financiamiento` VALUES ('10', 'Consejo Federal de Gobierno (ATENCIÓN ESPECIAL)', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `fuente_financiamiento` VALUES ('11', 'Consejo Federal de Gobierno (CONVENIO Gobierno de Calle - Consejos Comunales)', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2016_10_04_092235_sessions', '1');
INSERT INTO `migrations` VALUES ('2', '2016_10_04_092243_password_resets', '1');
INSERT INTO `migrations` VALUES ('3', '2016_10_04_092257_app_perfil', '1');
INSERT INTO `migrations` VALUES ('4', '2016_10_04_092308_app_usuario', '1');
INSERT INTO `migrations` VALUES ('5', '2016_10_04_092315_app_perfiles_permisos', '1');
INSERT INTO `migrations` VALUES ('6', '2016_10_04_092321_app_usuario_permisos', '1');
INSERT INTO `migrations` VALUES ('7', '2016_10_04_092327_app_historico', '1');
INSERT INTO `migrations` VALUES ('8', '2016_11_11_092235_configuracion', '2');
INSERT INTO `migrations` VALUES ('9', '2016_12_28_084040_municipio', '3');
INSERT INTO `migrations` VALUES ('10', '2016_12_28_084109_parroquia', '3');
INSERT INTO `migrations` VALUES ('11', '2016_12_28_084128_sector_primario', '3');
INSERT INTO `migrations` VALUES ('12', '2016_12_28_084134_estatus', '3');
INSERT INTO `migrations` VALUES ('13', '2016_12_28_084147_fuente_financiamiento', '3');
INSERT INTO `migrations` VALUES ('14', '2016_12_28_084154_ente_ejecutor', '3');
INSERT INTO `migrations` VALUES ('15', '2016_12_28_090142_proyectos', '3');

-- ----------------------------
-- Table structure for municipio
-- ----------------------------
DROP TABLE IF EXISTS `municipio`;
CREATE TABLE `municipio` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of municipio
-- ----------------------------
INSERT INTO `municipio` VALUES ('1', 'Heres', '2016-07-04 17:42:34', '2016-07-04 17:42:34', null);
INSERT INTO `municipio` VALUES ('2', 'Caroni', '2016-07-04 17:42:34', '2016-07-04 17:42:34', null);
INSERT INTO `municipio` VALUES ('3', 'Piar', '2016-07-04 17:42:34', '2016-07-04 17:42:34', null);
INSERT INTO `municipio` VALUES ('4', 'Cedeño', '2016-07-04 17:42:34', '2016-07-04 17:42:34', null);
INSERT INTO `municipio` VALUES ('5', 'Sifontes', '2016-07-04 17:42:34', '2016-07-04 17:42:34', null);
INSERT INTO `municipio` VALUES ('6', 'Bolivariano Angostura', '2016-07-04 17:42:34', '2016-07-07 11:33:00', null);
INSERT INTO `municipio` VALUES ('7', 'Sucre', '2016-07-04 17:42:34', '2016-07-04 17:42:34', null);
INSERT INTO `municipio` VALUES ('8', 'Roscio', '2016-07-04 17:42:34', '2016-07-04 17:42:34', null);
INSERT INTO `municipio` VALUES ('9', 'Gran Sabana', '2016-07-04 17:42:34', '2016-07-04 17:42:34', null);
INSERT INTO `municipio` VALUES ('10', 'El Callao', '2016-07-04 17:42:34', '2016-07-04 17:42:34', null);
INSERT INTO `municipio` VALUES ('11', 'Padre Pedro Chien', '2016-07-04 17:42:34', '2016-07-13 17:42:34', null);
INSERT INTO `municipio` VALUES ('12', 'Estado', '2017-01-04 00:30:00', '2017-01-10 00:30:00', null);

-- ----------------------------
-- Table structure for parroquia
-- ----------------------------
DROP TABLE IF EXISTS `parroquia`;
CREATE TABLE `parroquia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `municipio_id` int(10) unsigned NOT NULL,
  `nombre` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parroquia_municipio_id_foreign` (`municipio_id`),
  CONSTRAINT `parroquia_municipio_id_foreign` FOREIGN KEY (`municipio_id`) REFERENCES `municipio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of parroquia
-- ----------------------------
INSERT INTO `parroquia` VALUES ('1', '1', 'Catedral', '2016-07-04 17:42:34', '2016-07-04 17:42:34', null);
INSERT INTO `parroquia` VALUES ('2', '1', 'Vista Hermosa', '2016-07-04 17:42:34', '2016-07-04 17:42:34', null);
INSERT INTO `parroquia` VALUES ('3', '1', 'Jose Antonio Paez', '2016-07-04 17:42:35', '2016-07-04 17:42:35', null);
INSERT INTO `parroquia` VALUES ('4', '1', 'Marhuanta', '2016-07-04 17:42:35', '2016-07-04 17:42:35', null);
INSERT INTO `parroquia` VALUES ('5', '1', 'Agua Salada', '2016-07-04 17:42:35', '2016-07-04 17:42:35', null);
INSERT INTO `parroquia` VALUES ('6', '1', 'La Sabanita', '2016-07-04 17:42:35', '2016-07-04 17:42:35', null);
INSERT INTO `parroquia` VALUES ('7', '1', 'Orinoco', '2016-07-04 17:42:35', '2016-07-04 17:42:35', null);
INSERT INTO `parroquia` VALUES ('8', '1', 'Panapana', '2016-07-04 17:42:35', '2016-07-04 17:42:35', null);
INSERT INTO `parroquia` VALUES ('9', '1', 'Zea', '2016-07-04 17:42:35', '2016-07-04 17:42:35', null);
INSERT INTO `parroquia` VALUES ('10', '2', 'Unare', '2016-07-04 17:42:35', '2016-07-04 17:42:35', null);
INSERT INTO `parroquia` VALUES ('11', '2', 'Cachamay', '2016-07-04 17:42:35', '2016-07-04 17:42:35', null);
INSERT INTO `parroquia` VALUES ('12', '2', 'Chirica', '2016-07-04 17:42:35', '2016-07-04 17:42:35', null);
INSERT INTO `parroquia` VALUES ('13', '2', 'Simon Bolivar', '2016-07-04 17:42:35', '2016-07-04 17:42:35', null);
INSERT INTO `parroquia` VALUES ('14', '2', 'Yocoima', '2016-07-04 17:42:35', '2016-07-04 17:42:35', null);
INSERT INTO `parroquia` VALUES ('15', '2', 'Dalla Costa', '2016-07-04 17:42:35', '2016-07-04 17:42:35', null);
INSERT INTO `parroquia` VALUES ('16', '2', 'Pozo Verde', '2016-07-04 17:42:35', '2016-07-04 17:42:35', null);
INSERT INTO `parroquia` VALUES ('17', '2', 'Vista al Sol', '2016-07-04 17:42:35', '2016-07-04 17:42:35', null);
INSERT INTO `parroquia` VALUES ('18', '2', 'Universidad', '2016-07-04 17:42:35', '2016-07-04 17:42:35', null);
INSERT INTO `parroquia` VALUES ('19', '2', 'Once de Abril', '2016-07-04 17:42:35', '2016-07-04 17:42:35', null);
INSERT INTO `parroquia` VALUES ('20', '2', '5 de Julio', '2016-07-04 17:42:35', '2016-07-04 17:42:35', null);
INSERT INTO `parroquia` VALUES ('21', '3', 'Upata', '2016-07-04 17:42:35', '2016-07-04 17:42:35', null);
INSERT INTO `parroquia` VALUES ('22', '3', 'Andres Eloy Blanco', '2016-07-04 17:42:36', '2016-07-04 17:42:36', null);
INSERT INTO `parroquia` VALUES ('23', '3', 'Pedro Cova', '2016-07-04 17:42:36', '2016-07-04 17:42:36', null);
INSERT INTO `parroquia` VALUES ('24', '4', 'Altagracia', '2016-07-04 17:42:36', '2016-07-04 17:42:36', null);
INSERT INTO `parroquia` VALUES ('25', '4', 'Pijiguaos', '2016-07-04 17:42:36', '2016-07-04 17:42:36', null);
INSERT INTO `parroquia` VALUES ('26', '4', 'Guaniamo', '2016-07-04 17:42:36', '2016-07-04 17:42:36', null);
INSERT INTO `parroquia` VALUES ('27', '4', 'Ascension Farreras', '2016-07-04 17:42:36', '2016-07-04 17:42:36', null);
INSERT INTO `parroquia` VALUES ('28', '4', 'Cedeño', '2016-07-04 17:42:36', '2016-07-04 17:42:36', null);
INSERT INTO `parroquia` VALUES ('29', '4', 'La Urbana', '2016-07-04 17:42:36', '2016-11-30 15:55:21', null);
INSERT INTO `parroquia` VALUES ('30', '4', 'Caicara del Orinoco', '2016-07-04 17:42:36', '2016-11-30 15:55:09', null);
INSERT INTO `parroquia` VALUES ('31', '5', 'Dalla Costa', '2016-07-04 17:42:36', '2016-07-04 17:42:36', null);
INSERT INTO `parroquia` VALUES ('32', '6', 'Raul Leoni', '2016-07-04 17:42:36', '2016-07-04 17:42:36', null);
INSERT INTO `parroquia` VALUES ('33', '5', 'El Dorado', '2016-07-04 17:42:36', '2016-07-04 17:42:36', null);
INSERT INTO `parroquia` VALUES ('34', '5', 'San Isidro', '2016-07-04 17:42:36', '2016-07-04 17:42:36', null);
INSERT INTO `parroquia` VALUES ('35', '6', 'San Francisco', '2016-07-04 17:42:36', '2016-07-04 17:42:36', null);
INSERT INTO `parroquia` VALUES ('36', '6', 'Barceloneta', '2016-07-04 17:42:36', '2016-07-04 17:42:36', null);
INSERT INTO `parroquia` VALUES ('37', '6', 'Ciudad Piar', '2016-07-04 17:42:36', '2016-07-04 17:42:36', null);
INSERT INTO `parroquia` VALUES ('38', '6', 'Santa Barbara', '2016-07-04 17:42:36', '2016-07-04 17:42:36', null);
INSERT INTO `parroquia` VALUES ('39', '7', 'Maripa', '2016-07-04 17:42:36', '2016-07-04 17:42:36', null);
INSERT INTO `parroquia` VALUES ('40', '7', 'Guarataro', '2016-07-04 17:42:36', '2016-07-04 17:42:36', null);
INSERT INTO `parroquia` VALUES ('41', '7', 'Moitaco', '2016-07-04 17:42:37', '2016-07-04 17:42:37', null);
INSERT INTO `parroquia` VALUES ('42', '7', 'Aripao', '2016-07-04 17:42:37', '2016-07-04 17:42:37', null);
INSERT INTO `parroquia` VALUES ('43', '7', 'Las Majadas', '2016-07-04 17:42:37', '2016-07-04 17:42:37', null);
INSERT INTO `parroquia` VALUES ('44', '8', 'Guasipati', '2016-07-04 17:42:37', '2016-07-04 17:42:37', null);
INSERT INTO `parroquia` VALUES ('45', '8', 'Salom', '2016-07-04 17:42:37', '2016-07-04 17:42:37', null);
INSERT INTO `parroquia` VALUES ('46', '9', 'Gran Sabana', '2016-07-04 17:42:37', '2016-07-04 17:42:37', null);
INSERT INTO `parroquia` VALUES ('47', '9', 'Santa Elena de Uairen', '2016-07-04 17:42:37', '2016-07-04 17:42:37', null);
INSERT INTO `parroquia` VALUES ('48', '9', 'Ikabaru', '2016-07-04 17:42:37', '2016-07-04 17:42:37', null);
INSERT INTO `parroquia` VALUES ('49', '10', 'El Callao', '2016-07-04 17:42:37', '2016-07-04 17:42:37', null);
INSERT INTO `parroquia` VALUES ('50', '11', 'Padre Pedro Chien', '2016-07-04 17:42:37', '2016-07-04 17:42:37', null);
INSERT INTO `parroquia` VALUES ('51', '11', 'El Palmar', '2016-07-04 17:42:37', '2016-07-04 17:42:37', null);
INSERT INTO `parroquia` VALUES ('52', '5', 'Tumeremo', '2016-07-04 17:50:03', '2016-09-21 11:55:03', null);
INSERT INTO `parroquia` VALUES ('53', '12', 'Todas', '2017-01-02 00:30:00', '2017-01-02 00:30:00', null);
INSERT INTO `parroquia` VALUES ('54', '3', 'Sección Capital Upata', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `parroquia` VALUES ('55', '2', '11 de Abril', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `parroquia` VALUES ('56', '4', 'Los Pijiguaos', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `parroquia` VALUES ('57', '9', 'Capital Gran Sabana', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for proyectos
-- ----------------------------
DROP TABLE IF EXISTS `proyectos`;
CREATE TABLE `proyectos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `municipio_id` int(10) unsigned NOT NULL,
  `parroquia_id` int(10) unsigned NOT NULL,
  `inversion` decimal(15,2) NOT NULL,
  `sector_primario_id` int(10) unsigned NOT NULL,
  `estatus_id` int(10) unsigned NOT NULL,
  `fuente_financiamiento_id` int(10) unsigned NOT NULL,
  `avance` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ente_ejecutor_id` int(10) unsigned NOT NULL,
  `ano_ejecucion` smallint(3) unsigned NOT NULL,
  `numero_certificacion` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `proyectos_municipio_id_foreign` (`municipio_id`),
  KEY `proyectos_parroquia_id_foreign` (`parroquia_id`),
  KEY `proyectos_sector_primario_id_foreign` (`sector_primario_id`),
  KEY `proyectos_estatus_id_foreign` (`estatus_id`),
  KEY `proyectos_fuente_financiamiento_id_foreign` (`fuente_financiamiento_id`),
  KEY `proyectos_ente_ejecutor_id_foreign` (`ente_ejecutor_id`),
  CONSTRAINT `proyectos_ente_ejecutor_id_foreign` FOREIGN KEY (`ente_ejecutor_id`) REFERENCES `ente_ejecutor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `proyectos_estatus_id_foreign` FOREIGN KEY (`estatus_id`) REFERENCES `estatus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `proyectos_fuente_financiamiento_id_foreign` FOREIGN KEY (`fuente_financiamiento_id`) REFERENCES `fuente_financiamiento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `proyectos_municipio_id_foreign` FOREIGN KEY (`municipio_id`) REFERENCES `municipio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `proyectos_parroquia_id_foreign` FOREIGN KEY (`parroquia_id`) REFERENCES `parroquia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `proyectos_sector_primario_id_foreign` FOREIGN KEY (`sector_primario_id`) REFERENCES `sector_primario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1445 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of proyectos
-- ----------------------------
INSERT INTO `proyectos` VALUES ('1', 'Activación de Sistema de Rebombeo Chiripón, municipio Piar del estado Bolívar.', '3', '54', '55000000.00', '1', '1', '1', '100', '1', '2016', '2016-0037', '2017-01-09 16:48:17', '2017-01-09 16:48:17', null);
INSERT INTO `proyectos` VALUES ('2', 'Instalación y Puesta en Marcha de una Planta Embotelladora de Agua Potable y Fabricación de Envases en el estado Bolívar.', '12', '53', '10000000.00', '1', '1', '1', '100', '1', '2015', '2015-0050', '2017-01-09 16:48:17', '2017-01-09 16:48:17', null);
INSERT INTO `proyectos` VALUES ('3', 'Construcción de Red de Aguas en el sector Cristóbal Colón, San Félix, parroquia Vista Al Sol, municipio Caroní del estado Bolívar. (I Etapa)', '2', '17', '15000000.00', '1', '2', '1', '55', '1', '2016', '2016-0010', '2017-01-09 16:48:17', '2017-01-09 16:48:17', null);
INSERT INTO `proyectos` VALUES ('4', 'Rehabilitación y Mejoras de Vías de Accesos en la Carrera 4, calle El Parque y calle Bolívar de la parroquia Guarataro, municipio Sucre del estado Bolívar.', '7', '40', '6000000.00', '2', '1', '1', '100', '2', '2016', '2016-0016', '2017-01-09 16:48:17', '2017-01-09 16:48:17', null);
INSERT INTO `proyectos` VALUES ('5', 'Rehabilitación y Mejoras de Vías de Accesos en la calle 2 y calle Los Mangos del Barrio La Sabanita, parroquia Guarataro, municipio Sucre del estado Bolívar.', '7', '40', '9000000.00', '2', '1', '1', '100', '2', '2016', '2016-0017', '2017-01-09 16:48:17', '2017-01-09 16:48:17', null);
INSERT INTO `proyectos` VALUES ('6', 'Rehabilitación y Mejoras de Vías de Accesos de la calle Sucre y Carrera 1, parroquia Guarataro, municipio Sucre del estado Bolívar.', '7', '40', '4500000.00', '2', '1', '1', '100', '2', '2016', '2016-0018', '2017-01-09 16:48:17', '2017-01-09 16:48:17', null);
INSERT INTO `proyectos` VALUES ('7', 'Rehabilitación y Mejoras de Vías de Accesos de la calle 2 del Casco Histórico del sector Guarataro, municipio Sucre del estado Bolívar.', '7', '40', '4500000.00', '2', '1', '1', '100', '2', '2016', '2016-0019', '2017-01-09 16:48:17', '2017-01-09 16:48:17', null);
INSERT INTO `proyectos` VALUES ('8', 'Rehabilitación de Vías de Acceso en Calle José Antonio Páez, parroquia Vista Al Sol, municipio Caroní del estado Bolívar.', '2', '17', '6275430.00', '2', '1', '1', '100', '2', '2016', '2016-0048', '2017-01-09 16:48:17', '2017-01-09 16:48:17', null);
INSERT INTO `proyectos` VALUES ('9', 'Construcción de Liceo en Brisas del Este I, municipio Heres, estado Bolívar.', '1', '5', '15000000.00', '3', '1', '1', '100', '3', '2016', '2016-0072', '2017-01-09 16:48:17', '2017-01-09 16:48:17', null);
INSERT INTO `proyectos` VALUES ('10', 'Rehabilitación y mejoras de Ambulatorio Tipo III, Guri, municipio Bolivariano Angostura, estado Bolívar (II Etapa).', '6', '38', '15000000.00', '4', '2', '1', '78.1', '3', '2016', '2016-0075', '2017-01-09 16:48:17', '2017-01-09 16:48:17', null);
INSERT INTO `proyectos` VALUES ('11', 'Construcción de caseta de seguridad y cercado perimetral en el Centro de Operaciones y Mantenimiento de Transbolívar, municipio Caroní, estado Bolívar.', '2', '10', '12000000.00', '5', '2', '1', '51.5', '3', '2016', '2016-0094', '2017-01-09 16:48:17', '2017-01-09 16:48:17', null);
INSERT INTO `proyectos` VALUES ('12', 'Rehabilitación y mejoras del Hospital Ruíz y Páez, municipio Heres del estado Bolívar.', '1', '1', '45000000.00', '4', '1', '1', '100', '2', '2016', '2016-0103', '2017-01-09 16:48:17', '2017-01-09 16:48:17', null);
INSERT INTO `proyectos` VALUES ('13', 'Construcción de Liceo en Brisas del Este I, municipio Heres, estado Bolívar.', '1', '5', '20000000.00', '3', '1', '1', '100', '3', '2016', '2016-0114', '2017-01-09 16:48:17', '2017-01-09 16:48:17', null);
INSERT INTO `proyectos` VALUES ('14', 'Dotación, Equipos y Mobiliario Médico Complementarios Hospital Trauma Shock, San Félix, municipio Caroní.', '2', '31', '1873628.88', '4', '1', '2', '100', '4', '2004', '-', '2017-01-09 16:48:17', '2017-01-09 16:48:17', null);
INSERT INTO `proyectos` VALUES ('15', 'Construcción de una Cancha Deportiva de Usos Múltiples en el Liceo Ramón Antonio Pérez, Parroquia Simón Bolívar.', '2', '13', '91265.47', '3', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:17', '2017-01-09 16:48:17', null);
INSERT INTO `proyectos` VALUES ('16', 'Recuperación y Mejoras Integral el Zanjon en el Centro Histórico, Ciudad Bolívar, municipio Heres, estado Bolívar.', '1', '1', '495216.79', '5', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:17', '2017-01-09 16:48:17', null);
INSERT INTO `proyectos` VALUES ('17', 'Sistema de Recolección de Aguas Negras en el Sector Parques del Sur (electricidad, bombas y tableros de control), parroquia Sabanita, municipio Heres.', '1', '6', '116751.75', '1', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('18', 'Construcción de Red Eléctrica Asentamiento Campesino 24 de Julio Asociación de Productores, parroquia Marhuanta, municipio Heres.', '1', '4', '39663.87', '5', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('19', 'Construcción de Escuela El Corozo, sector El Corozo, vía El Pao, parroquia Andrés Eloy blanco.', '3', '22', '596698.10', '3', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('20', 'Construcción Preescolar Hexagonal La Floresta, municipio Piar, estado Bolívar.', '3', '54', '272984.06', '3', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('21', 'Creación y Fundación Escuela Luz del Mundo en la Ciudad de Upata.', '3', '54', '150000.00', '3', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('22', 'Construcción del Canal de Drenaje Superficial (aceras y brocales)  , Sector  San Lorenzo, Upata.', '3', '54', '120000.00', '5', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('23', 'Ampliación de Red de Cloacas en la Calle Principal del Corozo de Upata, municipio Piar, estado Bolívar.', '3', '54', '86197.54', '1', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('24', 'Construcción de Cancha de Futbol en el Barrio Alaska, Upata, municipio Piar, estado Bolívar.', '3', '54', '34111.83', '3', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('25', 'Pavimentación de la Vialidad Urbana en la Población de Guasipati. Municipio Autónomo Roscio.', '8', '44', '1718016.36', '2', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('26', 'Construcción de línea eléctrica para el asentamiento campesino santa catalina - los monos (sociedad civil) Municipio Angostura Bolivariana.', '6', '35', '282353.09', '5', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('27', 'Mejoras y Rehabilitación de Vialidad Agrícola en el sector Los Pepes - Los Monos (progresiva 7+000 a la 14+500) parroquia San Francisco.', '6', '35', '280292.65', '2', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('28', 'Mejoras y Rehabilitación de la Vialidad Agrícola en el sector Los Pepes - Los Monos, (progresiva 0+000 a la 7+000), parroquia San Francisco, municipio Bolivariano Angostura.', '6', '35', '279458.96', '2', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('29', 'Mejoras y Rehabilitación de Vialidad Agrícola en el sector La Rotunda - Los Lirios (progresiva 0+000 a la 4+500) parroquia San Francisco, municipio Bolívariano Angostura, estado Bolívar.', '6', '35', '235925.67', '2', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('30', 'Mejoras y Rehabilitación de Vialidad Agrícola en el sector La Rotunda - Los Lirios (progresiva 4+500 a la 9+500) parroquia San Francisco, municipio Bolivariano Angostura, estado Bolívar.', '6', '35', '235925.67', '2', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('31', 'Construcción de Línea Eléctrica para el Asentamiento Campesino Los Lirios - Santa Catalina - El Gallo, (sociedad civil), municipio Bolivariano Angostura.', '6', '53', '220000.00', '5', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('32', 'Construcción de Línea Eléctrica en Alta y Baja Tensión para el Asentamiento Campesino los Morocotos.', '6', '36', '89012.02', '5', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('33', 'Construcción de Escuela Santa Fe, parroquia Pozo Verde, sector Santa Fe, municipio Caroní.', '2', '16', '677974.82', '3', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('34', 'Mejoras en el Abastecimiento de Agua del Asentamiento Campesino la Ceiba de la parroquia Unare, municipio Caroní, estado Bolívar.', '2', '10', '180514.75', '1', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('35', 'Construcción de la vialidad Agrícola de la Comunidad Indígena de la Etnia Panare, Local Cinco – Guamalito - Temblador, municipio Cedeño del estado Bolívar', '4', '53', '1014943.54', '2', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('36', 'Construcción de Cancha Techada Deportiva de Usos Múltiples en la Universidad de Oriente en Caicara del Orinoco, municipio Cedeño, estado Bolívar.', '4', '30', '187938.32', '3', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('37', 'Consolidación y Mejoras de la Carretera Caicara del Orinoco - Pijiguao - Puerto Ayacucho, municipio Cedeño, estado Bolívar.', '4', '53', '2648006.70', '2', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('38', 'Construcción de Vía hacia el Asentamiento Campesino Indígena de Wairampay, municipio Gran Sabana, estado Bolívar.', '9', '48', '748489.19', '2', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('39', 'Ampliación de la Red de Distribución de Energía Eléctrica y Alumbrado Público de la Urbanización Akurima Santa Elena de Uairen Gran Sabana.', '9', '47', '313208.53', '5', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('40', 'Construcción de Aceras y Brocales en el Barrio José Antonio Páez, sector II, Ciudad Bolívar, municipio Heres.', '1', '3', '387312.07', '5', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('41', 'Construcción de Modulo para la Atención de la Salud y la Vida, municipio Heres, estado Bolívar.', '1', '53', '18127.15', '4', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('42', 'Dotación Equipos y Mobiliario Médico Complementario Hospital Materno Infantil, Ciudad Bolívar.', '1', '1', '2394593.48', '4', '1', '2', '100', '4', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('43', 'Construcción de Escuela los Chorros, sector Los Chorros I y II, parroquia Alaska, municipio Piar.', '3', '54', '617064.02', '3', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('44', 'Construcción de Escuela Bolivariana en el municipio Piar, sector Los Chivos.', '3', '54', '582555.28', '3', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('45', 'Construcción de Aceras y Brocales Canales de Drenajes y Escalinatas en la parroquia Andrés Eloy Blanco.', '3', '22', '175831.22', '5', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('46', 'Construcción del Canal de Drenaje Superficial y Cloacas enel Sector Libertador, Upata (sociedad civil).', '3', '54', '115031.94', '5', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('47', 'Adquisición de Autobuses para las Comunidades del municipio Bolivariano  Angostura, estado Bolívar.', '6', '53', '1479635.54', '6', '1', '2', '100', '4', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('48', 'Mejoras y Bacheo, carretera R-08, La Quina, San Francisco, II Etapa, municipio Bolivariano Angostura.', '6', '35', '523006.21', '2', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('49', 'Adquisición de Lanchas para el Transporte Fluvial de las Comunidades Indígenas del municipio Bolivariano  Angostura, estado Bolívar.', '6', '53', '512949.50', '7', '1', '2', '100', '4', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('50', 'Construcción Línea Eléctrica para el sector Los Guacos,  Municipio Bolivariano Angostura, estado Bolívar. Resolución nº 39-28', '6', '35', '282134.50', '5', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('51', 'Mejoras y Rehabilitación de Vialidad Agrícolas en el sector Salto Los Morocotos (progresiva 0+000 a la 7+000) parroquia San Francisco.', '6', '35', '280292.65', '2', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('52', '\"Mejoras y Rehabilitación de la Vialidad Agrícola en el sector El Gallo, (progresiva 6+600 a la 13+000) parroquia San Francisco, municipio Bolivariano Angostura\"  Resolución  nº 66-34', '6', '35', '279681.35', '2', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('53', 'Bacheo y Mejoras de la Vialidad Urbana San Francisco,  municipio  Bolivariano Angostura. (parroquia San Francisco)', '6', '35', '266200.95', '2', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('54', 'Unidades de Transporte para la Universidad Bolivariana de Venezuela - Sede Ciudad Bolívar, municipio Heres, estado Bolívar.', '1', '53', '2109106.70', '3', '1', '2', '100', '4', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('55', 'Construcción de Sistema de Acueducto en el Sector Rural La Vigía, municipio Piar, estado Bolívar.', '3', '53', '283469.98', '1', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('56', 'Construcción del Complejo Guasipati, municipio Roscio, estado Bolívar.', '8', '44', '1810562.22', '5', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('57', 'Construcción de Cajón de Tres (03) Celdas con Aletas de 45 Grados sobre el Rio Chimayco - Vías Las Majadas, Progresivas 10+600, Ensanche de la Vía, Modificación Rasante, Bacheo, Asfaltado y Señalización, municipio Sucre.', '7', '40', '2099822.39', '5', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('58', 'Implantación de un Infocentro en el sector Core 8, UD-337, parroquia Unare, municipio Caroní, estado Bolívar.', '2', '10', '72971.45', '3', '1', '2', '100', '4', '2004', '-', '2017-01-09 16:48:18', '2017-01-09 16:48:18', null);
INSERT INTO `proyectos` VALUES ('59', 'Implantación de un Infocentro en el sector Toro Muerto, parroquia Universidad, municipio Caroní, estado Bolívar.', '2', '18', '67481.62', '3', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('60', 'Implantación de un Infocentro en la Comunidad Indígena de Kamaracoto, Localidad de Kamarata, municipio Gran Sabana, estado Bolívar.', '9', '47', '73005.01', '3', '1', '2', '100', '4', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('61', 'Implantación de un Infocentro en la Alcaldía del municipio Heres, Ciudad Bolívar, estado Bolívar.', '1', '1', '72971.45', '3', '1', '2', '100', '4', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('62', 'Acondicionamiento de la Sala de Audiovisuales para el Instituto Universitario de Tecnología del estado Bolívar (IUTEB), municipio Heres, estado Bolívar', '1', '1', '51620.71', '3', '1', '2', '100', '4', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('63', 'Construcción del Sistema de Acueducto en el sector Rural Quebrada Juas Jual I, municipio Piar, estado Bolívar.', '3', '54', '283431.32', '1', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('64', 'Nuevo Diseño Arquitectónico de la Plaza Bolívar, Guasipati, municipio Roscio, estado Bolívar (I Etapa).', '8', '44', '656722.85', '5', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('65', 'Adquisición de Dos Tractores con sus Implementos y un Camión 4x4 F350, para la Prestación de Servicios Agrícolas.', '8', '53', '274440.71', '7', '1', '2', '100', '5', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('66', 'Implantación de un Infocentro en Maripa, municipio Sucre estado Bolívar (adquisición de equipos de computación y mobiliario).', '7', '39', '73005.01', '3', '1', '2', '100', '6', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('67', 'Implantación de un Infocentro en la Comunidad de San Pedro de Tauca, municipio Sucre, estado Bolívar (ciencia y tecnología).', '7', '39', '72971.45', '3', '1', '2', '100', '4', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('68', 'Adquisición de Vehículos para el Fortalecimiento Institucional de la Gobernación del estado Bolívar.', '12', '53', '2965481.70', '8', '1', '2', '100', '4', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('69', 'Bacheo y Mejoras de la Vialidad Urbana en la Población de la paragua municipio Bolivariano Angostura.', '6', '36', '626283.60', '2', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('70', 'Construcción de Acueducto en la parroquia San Francisco sector Carmen de Rodríguez, municipio Bolivariano Angostura, I Etapa (Los Guacos).', '6', '35', '486464.14', '1', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('71', 'Construcción de Línea Eléctrica en Alta y Baja Tensión para el sector Cerro Peldio municipio Bolivariano Angostura  (I Etapa).', '6', '35', '277120.16', '5', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('72', 'Construcción de Línea Eléctrica para el Asentamiento Campesino sector Los Monos – Guatista.', '6', '35', '252443.40', '5', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('73', 'Bacheos y Vialidad Urbana Santa Bárbara, Municipio Bolivariao Angostura, parroquia Santa Bárbara.', '6', '38', '246002.23', '2', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('74', 'Construcción de una Línea Eléctrica para el sector \"Las Colinas\".', '6', '36', '82749.08', '5', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('75', 'Adquisición de Cargadores Frontales para la Gobernación del estado Bolívar. ', '12', '53', '2206185.00', '5', '1', '2', '100', '4', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('76', 'Requerimiento de Unidades de Transporte para el Instituto Universitario de Tecnología del estado Bolívar.', '1', '1', '1550500.30', '3', '1', '2', '100', '4', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('77', 'Construcción de 14  Viviendas en diferentes sectores de la parroquia Vista Hermosa, municipio Heres.', '1', '2', '637471.91', '5', '1', '3', '100', '7', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('78', 'Construcción de Red Eléctrica del Barrio Santa Eduviges, parroquia Agua Salada, municipio Heres.', '1', '5', '129939.16', '5', '1', '3', '100', '3', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('79', 'Construcción del Centro Materno Infantil.', '1', '1', '3033417.83', '4', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('80', 'Culminación de la Construcción de la Sede de la Alcaldía Padre Pedro Chien, El Palmar (I Etapa).', '11', '51', '882791.74', '5', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('81', 'Adquisición de Dos (02) Autobuses con Capacidad de 32 Puestos, para el Fortalecimiento del Transporte Estudiantil, en el municipio Padre Pedro Chien, estado. Bolívar.', '11', '51', '400578.30', '6', '1', '2', '100', '4', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('82', 'Construcción del Sistema de Acueducto en el sector Rural San José de Hacha, municipio Piar, estado Bolívar.', '3', '23', '280966.70', '1', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('83', 'Construcción de Viviendas en El Manteco, municipio Piar del estado Bolívar.', '3', '53', '200000.00', '5', '1', '3', '100', '3', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('84', 'Construcción de un Módulo de Cuatro Aulas y Dos Sanitarios en la Unidad Educativa El Frio, municipio Autónomo Sifontes, estado Bolívar.', '5', '52', '275460.60', '3', '1', '2', '100', '3', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('85', 'Adquisición de Lanchas para el Transporte Fluvial para las Comunidades del municipio Sucre, edo. Bolívar (sociedad civil).', '7', '53', '512786.17', '7', '1', '2', '100', '4', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('86', 'Construcción del Sistema de Aguas Blancas en el Barrio Santa Eduviges, municipio Heres (sociedad civil).', '1', '5', '131669.11', '1', '1', '2', '100', '1', '2004', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('87', 'Mejoras y Rehabilitación de Vialidad Agrícola en el sector Mama Eulalia II (prog. 9+000 a la 18+000), parroquia Barceloneta.', '6', '36', '337331.88', '2', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('88', 'Mejoras y Rehabilitación de Vialidad Agrícola en el sector \"Mama Eulalia I\", parroquia Barceloneta, municipio Bolivariano Angostura, estado Bolívar.', '6', '36', '336102.09', '2', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('89', 'Mejoras y Rehabilitación de Vialidad Agrícola en sector los Guacos - Curichapo, parroquia Barceloneta, municipio Bolivariano Angostura, estado Bolívar.', '6', '36', '312992.35', '2', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('90', 'Mejoras y Rehabilitación de la Vialidad Agrícola en el sector \"El Chivo - Llano Alto\" Progresiva 0+000 a la 7+000 parroquia Barceloneta, municipio Bolivariano Angostura,  estado Bolívar.', '6', '36', '290645.23', '2', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('91', 'Mejoras y Rehabilitación de Vialidad Agrícola en el sector Cruce El Pao - Curichapo, parroquia Barceloneta, municipio Bolivariano Angostura,  estado Bolívar.', '6', '36', '234947.72', '2', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('92', 'Construcción, Ampliación Escuela Brisas del Orinoco, municipio Caroní.', '2', '55', '641313.36', '3', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('93', 'Consolidación del Barrio Trapichito II de San Félix, municipio Caroní.', '2', '17', '599791.20', '5', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('94', 'Cloacas Barrio La Laguna, sector II, UD-151, San Félix-estado Bolívar.', '2', '13', '500702.40', '1', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('95', 'Recolección Final de las Aguas Servidas del Barrio Canaima, UD-119, parroquia Simón Bolívar, San Félix.', '2', '13', '310571.22', '1', '1', '2', '100', '1', '2005', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('96', 'Recuperación de Canal Principal de Drenaje de Vista al Sol \"cárcava\", municipio Caroní, estado Bolívar.', '2', '17', '2936963.10', '5', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('97', 'Reacondicionamiento y Expansión  de la Capacidad de Toma de la Planta de Tratamiento de Agua Potable de Caicara del Orinoco, municipio Cedeño.', '4', '30', '2421841.63', '1', '1', '2', '100', '1', '2005', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('98', 'Laboratorio Pedagógico Telemático Universidad Bolivariana de Venezuela (UBV) Sede Ciudad Bolívar.', '12', '53', '382493.48', '3', '1', '2', '100', '4', '2005', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('99', 'Construcción de Comisaria en la parroquia Cachamay municipio Caroní del estado Bolívar (Complemento para la Culminación).', '2', '11', '2841361.89', '9', '1', '3', '100', '3', '2010', '2010-0045', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('100', 'Construcción y Dotación de la Comisaría Los Olivos municipio Caroní del estado Bolívar (Culminación).', '2', '18', '1537911.69', '9', '1', '3', '100', '3', '2010', '2010-0047', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('101', 'Construcción de un Comando de la Guardia Nacional en el Aeropuerto Manuel Carlos Piar, parroquia Unare, municipio Caroní, estado Bolívar. ', '2', '10', '350000.00', '9', '1', '3', '100', '8', '2010', '2010-0015', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('102', 'Rehabilitación de la Planta Potabilizadora de Santa Elena de Uairen, municipio Gran Sabana, estado Bolívar.', '9', '47', '1756301.88', '1', '1', '2', '100', '1', '2005', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('103', 'Recuperación Ambiental y Manejo Sostenible  de un Área Afectada por la Minería Artesanal de Oro en un Sector Adyacente a la Comunidad de Ikabaru, municipio Gran Sabana, estado Bolívar.', '9', '48', '1480985.57', '10', '1', '3', '100', '9', '2005', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('104', 'Recuperación Ambiental y Manejo Sustentable de unas Parcelas Afectadas por Trabajos de Pequeños Mineros de Oro y Diamante Ubicada en los Brasileros, municipio Gran Sabana, estado Bolívar.', '9', '48', '1329883.10', '10', '1', '3', '100', '9', '2005', '-', '2017-01-09 16:48:19', '2017-01-09 16:48:19', null);
INSERT INTO `proyectos` VALUES ('105', 'Ruta del Sororopan - Tu  Repon Tupu en el eje Luepa -Kanavayen y el Eje Fluvial Karuay del municipio Gran Sabana.', '9', '53', '1075005.10', '11', '1', '2', '100', '5', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('106', 'Recuperación Ambiental y Manejo Sostenible de una Parcela Trabajada por Pequeños Mineros de Diamante, Ubicada en la Comunidad Minera El Polaco, municipio Gran Sabana, estado Bolívar.', '9', '48', '1064717.65', '10', '1', '3', '100', '9', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('107', 'Ampliación y Mejoras de Líneas Eléctricas en Alta y Baja Tensión sector Kewey II.', '9', '47', '690165.15', '5', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('108', 'Ampliación y Mejoras de Líneas Eléctricas en Alta y Baja Tensión sector Kewey I.', '9', '47', '328516.65', '5', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('109', 'Construcción de Sistema de Acueductos de la Comunidad Indígena de Wara, municipio Gran Sabana.', '9', '47', '218606.52', '1', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('110', 'Ampliación y Mejoras de la Línea Eléctrica  en Alta y Baja Tensión de la Comunidad Indígena de Wara, municipio Gran Sabana - estado Bolívar.', '9', '47', '152443.89', '5', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('111', 'Construcción de Tendido Eléctrico de Alta y Baja Tensión para la Comunidad Indígena de Manakru, municipio Gran Sabana.', '9', '47', '138265.53', '5', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('112', 'Estabilización de Taludes de la Cárcava Ubicada en la Urbanización Bicentenario, Cuenca del Rio San Rafael, parroquia Vista Hermosa. ', '1', '2', '1202892.60', '1', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('113', 'Construcción de Aulas en la Unidad Educativa Nacional Las Beatrices, municipio Heres, estado Bolívar.', '1', '5', '217927.13', '3', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('114', 'Construcción de Alimentador de Agua Potable  al Estanque de la Red Baja y Matrices Zona Oeste parroquia Agua Salada, Acueducto de Ciudad Bolívar, municipio Heres.', '1', '5', '3826099.59', '1', '1', '2', '100', '1', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('115', 'Construcción de Planta Potabilizadora para El Palmar de 50 lps, municipio Pedro Padre Chien, estado Bolívar.', '11', '51', '2505972.25', '1', '1', '2', '100', '1', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('116', 'Remodelación, Ampliación y Mejoras de la Unidad Educativa Humberto Bartoli, Ubicada en el sector Las Malvinas, parroquia Upata, municipio Piar, estado Bolívar.', '3', '54', '408071.10', '3', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('117', 'Reparación y Mejoras de la Vialidad Agrícola, Parcelamiento Campesino Unión 2000.', '3', '54', '337058.18', '2', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('118', 'Construcción de Colector de Cloacas en el sector de la calle Piñerua de la Ciudad de Upata, municipio Piar.', '3', '54', '257631.68', '1', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('119', 'Construcción, Ampliación Mejoras de Redes Eléctricas y Alumbrado Público en los sectores Santa María, Matajey, Manganeso, Parcelamiento Unión 2000 y El  Buey.', '3', '54', '229620.20', '5', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('120', 'Embaulamiento de la Quebrada el Caballo entre las urbanizaciones Manuel Carlos Piar y Santo Domingo II, Upata, municipio Piar.', '3', '54', '220150.16', '1', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('121', 'Ampliación y Mejoras de Redes en Baja Tensión en el sector Bicentenario II, Upata, municipio Piar.', '3', '54', '133632.08', '5', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('122', 'Implantación de un Infocentro en El Manteco, municipio Piar.', '3', '53', '73005.01', '3', '1', '2', '100', '4', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('123', 'Construcción de Tendido Eléctrico a Nivel de 34,5 kv,  desde San Miguel de Betania - El Dorado, municipio Sifontes.', '5', '52', '4253376.95', '5', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('124', 'Adquisición e Instalacion de una Caldera de 150 hp para Sistema de Vapor del Hospital Ruíz y Páez de Ciudad Bolívar, Municipio Heres, estado Bolívar.', '1', '1', '229856.47', '4', '1', '3', '100', '4', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('125', 'Ampliación de la Unidad Educativa Nacional \"Creación La Victoria\" Ubicada en la parroquia Vista al Sol, sector 2, municipio Caroní, Edo. Bolívar.', '2', '17', '921153.25', '3', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('126', 'Remodelación, Ampliación y Mejoras de la Unidad Educativa Misiones del Caroní, Ubicada en el sector Mina Arriba, parroquia Pozo Verde, municipio Caroní, estado Bolívar.', '2', '16', '497606.00', '3', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('127', 'Remodelación, Ampliación y Mejoras de la Unidad Educativa Dr. José María Vargas, Ubicada en  el sector Cambalache, parroquia Unare, municipio Caroní, estado Bolívar.', '2', '10', '486406.66', '3', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('128', 'Remodelación, Ampliación y Mejoras de la Unidad Educativa Guaicaipuro, Ubicada en el Barrio Guaicaipuro, parroquia Chirica, municipio Caroní, estado Bolívar.', '2', '12', '435801.25', '3', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('129', 'Electrificación y Alumbrado Público del barrio San Ignacio del Cocuy (red en alta y baja tensión), parroquia San José de Chirica, municipio Caroní (comunidad organizada).', '2', '12', '332547.73', '5', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('130', 'Remodelación, Ampliación y Mejoras de la Unidad Educativa Las Malvinas, Ubicada en el Barrio Las Malvinas de la parroquia Dalla Costa, municipio Caroní, estado Bolívar.', '2', '31', '328838.86', '3', '1', '3', '100', '2', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('131', 'Remodelación, Ampliación y Mejoras del Pre escolar  Sara Marcano de Pino, Ubicado en la calle Independencia  del sector Roble por dentro de la parroquia Simón Bolívar, municipio Caroní, estado Bolívar.', '2', '13', '169638.01', '3', '1', '3', '100', '2', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('132', 'Reacondicionamiento del Instituto de Educación Especial Manuel Piar, municipio Caroní, estado Bolívar.', '2', '53', '98000.38', '3', '1', '3', '100', '2', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('133', 'Reacondicionamiento del Instituto de Educación Especial Taller Laboral, municipio Caroní, estado Bolívar.', '2', '53', '91725.40', '3', '1', '3', '100', '2', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('134', 'Reacondicionamiento del Instituto de Educación Especial Simón Bolívar, municipio Caroní, estado Bolívar.', '2', '13', '75137.47', '3', '1', '3', '100', '2', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('135', 'Fortalecimiento Tecnológico de la Fundación del Niño.', '1', '1', '493213.67', '6', '1', '3', '100', '10', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('136', 'Construcción de Acueducto en el Asentamiento Campesino de Pueblo Nuevo, parroquia Panapana, municipio Heres, estado Bolívar.', '1', '8', '334837.92', '1', '1', '3', '100', '2', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('137', 'Remodelación, Ampliación y Mejoras de la Unidad Educativa Brisas del Este II, Ubicada en el Barrio Brisas del Este II,  parroquia La Sabanita, municipio Heres, estado Bolívar.', '1', '6', '293977.32', '3', '1', '3', '100', '2', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('138', 'Construcción de Redes Eléctricas y Alumbrado Público en el sector Los Olivos II, parroquia José Antonio Páez, Ciudad Bolívar, estado Bolívar.', '1', '3', '180768.63', '5', '1', '3', '100', '2', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('139', 'Remodelación, Ampliación y Mejoras de la Unidad Educativa María Antonia Mejías, Ubicada en el Barrio Las Moreas, parroquia Catedral, municipio Heres, estado Bolívar.', '1', '1', '153879.03', '3', '1', '3', '100', '2', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('140', 'Reacondicionamiento del Instituto de Educación Especial Fragachan, municipio Heres, estado Bolívar.', '1', '3', '99869.67', '3', '1', '3', '100', '2', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('141', 'Remodelación, Ampliación y Mejoras de la Unidad Educativa J.M. Siso Martínez, Ubicada en  el Caserío Primera Agua de la parroquia Andrés Eloy Blanco, municipio Piar, estado Bolívar.', '3', '22', '400784.72', '3', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('142', 'Remodelación, Ampliación y Mejoras de la Unidad Educativa Estadal San Lorenzo, Ubicada en la calle Principal del Caserío Rural San Lorenzo, parroquia Upata, municipio Piar del estado Bolívar.', '3', '54', '342392.90', '3', '1', '3', '100', '2', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('143', 'Canal de Drenaje de Los Olivos, Puerto Ordaz, M.A. Caroní, estado Bolívar (I Etapa).', '2', '18', '803637.90', '5', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('144', 'Embaulamiento del Canal de Drenaje de Aguas Pluviales en el Barrio San Valentín, parroquia Marhuanta, municipio Heres.', '1', '4', '426378.26', '5', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('145', 'Asfaltado en la Urbanización La Llovizna, sector Agua Salada, Ciudad Bolívar, municipio Heres, estado Bolívar.', '1', '5', '154015.05', '2', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('146', 'Mejoras y Amplificación de la Planta Potabilizadora de la Paragua a 50 lps municipio Bolivariano Angostura, estado Bolívar.', '6', '36', '1082702.98', '1', '1', '3', '100', '1', '2005', '-', '2017-01-09 16:48:20', '2017-01-09 16:48:20', null);
INSERT INTO `proyectos` VALUES ('147', 'Mejoras y Rehabilitación de la Vialidad Agrícola en el sector \"Los Yopitos - Agua Verde\", municipio Bolivariano Angostura, estado Bolívar.', '6', '53', '333846.64', '2', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('148', 'Implantación de un Infocentro en el sector La Paragua, municipio Bolivariano Angostura, estado Bolívar.', '6', '36', '79880.66', '3', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('149', 'Consolidación y Pavimentación de 7 + 754 km de carretera en la Vía Agrícola Los Culíes, sector El Guamo, municipio Caroní.', '2', '14', '1324121.43', '2', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('150', 'Construcción de Vialidad Agrícola en el sector Campesino El Corocito, parroquia Chirica, municipio Caroní (comunidad organizada).', '2', '12', '1155500.00', '2', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('151', 'Construcción de Viviendas en el municipio Caroní del estado Bolívar.', '2', '53', '600000.00', '5', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('152', 'Electrificación y Alumbrado Público del barrio Nueva Jerusalén (red en alta y baja tensión) parroquia San José de Chirica, municipio Autónomo Caroní del estado Bolívar.', '2', '12', '313192.85', '5', '1', '3', '100', '2', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('153', 'Construcción de Acueducto para el Barrio Nueva Jerusalén, parroquia San José de Chirica, municipio Caroní.', '2', '12', '302727.17', '1', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('154', 'Reparación y Mejoras en la Escuela Dr. José María Vargas sector Cambalache, municipio Caroní, estado Bolívar (cocina de cocina tipo 2).', '2', '10', '196314.18', '3', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('155', 'Construcción Red Eléctrica del sector Los Lirios km 18, parroquia Yocoima, vía San Félix - Upata, municipio Caroní.', '2', '14', '132808.72', '5', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('156', 'Conclusión Construcción del Liceo La Romana, Caicara del Orinoco, municipio Cedeño.', '4', '30', '896315.44', '3', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('157', 'Adquisición de Camión Perforador de Pozos de Agua para la Gobernación del estado Bolívar (Etapa I).', '12', '53', '2046578.27', '8', '1', '2', '100', '4', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('158', 'Adquisición de Tres (03) Maquinas Motoniveladora para la Gobernación del estado Bolívar.', '12', '53', '2094750.00', '8', '1', '2', '100', '4', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('159', 'Adquisición de Asfaltadora para la Gobernación del estado Bolívar.', '12', '53', '2246831.70', '8', '1', '2', '100', '4', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('160', 'Adquisición de Aplanadora de Doble Rodillo para la Gobernación del estado Bolívar.', '12', '53', '1267875.00', '8', '1', '2', '100', '4', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('161', 'Adquisición de Low Boy con Camión para la Gobernación del estado Bolívar.', '12', '53', '784442.55', '8', '1', '2', '100', '4', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('162', 'Adquisición de Camión Vactor (adquisición de camión con equipo de saneamiento ambiental de limpieza y aspirado para la Gobernación  del estado Bolívar) (II Etapa).', '12', '53', '603750.00', '8', '1', '2', '100', '4', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('163', 'Dotación de Equipos de Computación para el Fortalecimiento de la Dirección de Proyectos de la Gobernación del estado Bolívar.', '12', '53', '14905.24', '8', '1', '2', '100', '4', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('164', 'Desarrollo Integral de las Comunidades Pesqueras de los municipios Cedeño, Sucre, Heres, Bolivariano Angostura y Caroní.', '12', '53', '6253030.37', '7', '1', '2', '100', '4', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('165', 'Mejoras y Acondicionamiento Acueductos Santa Elena de Uairen, municipio Gran Sabana, estado Bolívar.', '9', '47', '1756301.88', '1', '1', '2', '100', '1', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('166', 'Recuperación Ambiental de Áreas Intervenidas por Actividades Mineras en la Comunidad de Las Agallas, municipio Gran Sabana, estado Bolívar.', '9', '48', '1416749.90', '10', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('167', 'Recuperación Ambiental de una Parcela Afectada por Trabajo de Pequeños Mineros, Ubicada en Pamina, municipio Autónomo Gran Sabana, estado Bolívar.', '9', '48', '969241.87', '10', '1', '3', '100', '9', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('168', 'Construcción de Ambulatorio Tipo I en la Comunidad Indígena de Kamoiran, municipio Gran Sabana.', '9', '47', '360000.00', '4', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('169', 'Construcción de Acueducto Rural para la Población de El Pauji, municipio Gran Sabana, estado Bolívar.', '9', '48', '352193.53', '1', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('170', 'Adquisición  de Transporte de Uso Social para la Comunidad Indígena Waramasen, municipio Gran Sabana.', '9', '53', '175985.94', '6', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('171', 'Construcción de Escuela en la Comunidad Indígena de Santa Cruz de Mapauri, municipio Gran Sabana.', '9', '47', '110832.60', '3', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('172', 'Construcción de Canchas de Usos Múltiples en la Escuela de la Comunidad Indígena de San Antonio de Morichal, municipio Gran Sabana.', '9', '48', '93827.09', '3', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('173', 'Adquisición de Minibuses para el Fortalecimiento del Transporte Escolar en el municipio Heres, estado Bolívar.', '1', '53', '1673520.00', '3', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('174', 'Construcción de Pilotaje para Tanque de Concreto Postensado, Red Baja Agua, Agua Salada, Ciudad Bolívar municipio Heres, estado Bolívar.', '1', '5', '929000.00', '1', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('175', 'Mejoras del Sistema Eléctrico Riveras del Caura, sector Los Próceres, municipio Heres.', '1', '5', '865224.40', '5', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('176', 'Solución Hidráulica y Estabilización de Taludes en una Cárcava del Barrio Brisas del Este Cuenca del Rio Buena Vista, parroquia La Sabanita, municipio Heres, estado Bolívar.', '1', '6', '837477.73', '5', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('177', 'Remodelación, Ampliación y Mejoras de la Unidad Educativa Manuel Carlos Piar, Ubicada en  el Barrio Nueva Guayana, parroquia José Antonio Páez, municipio Heres, estado Bolívar.', '1', '3', '498532.89', '3', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('178', 'Rehabilitación de la Casa Colonial en el Casco Histórico de Ciudad Bolívar, municipio Heres.', '1', '1', '399796.24', '5', '1', '3', '100', '11', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('179', 'Consolidación de la Plataforma Tecnológica y de Conectividad de Administración y Finanzas de la Gobernación del estado Bolívar, municipio Heres, estado Bolívar.', '1', '1', '375071.58', '8', '1', '2', '100', '4', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('180', 'Construcción de Acueducto en la Avenida José Gregorio Hernández, Los Próceres, parroquia Agua Salada, Ciudad Bolívar, municipio Heres.', '1', '5', '265724.26', '1', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('181', 'Construcción de la Red de Cloacas sector Villa Central (parte baja) de Los Próceres, municipio Heres, estado Bolívar.', '1', '5', '235360.73', '1', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('182', 'Construcción de Red de Aguas Blancas de la Urbanización Villas del Orinoco, parroquia Agua Salada, municipio Heres.', '1', '5', '199982.97', '1', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('183', 'Remodelación, Ampliación y Mejoras de la Unidad Educativa Juan Bautista González, Ubicada en la calle Las Campiñas, sector II del Barrio Las Campiñas, parroquia La Sabanita, municipio Heres, estado Bolívar.', '1', '6', '96481.80', '3', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('184', 'Adquisición e Instalación de los Sistemas de Bombeo de Aguas Blancas del Hospital Ruiz y Páez de Ciudad Bolívar municipio Heres, estado Bolívar.', '1', '1', '64074.04', '4', '1', '3', '100', '4', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('185', 'Culminación de la Escuela Ampliación y Mejoras en la Unidad Educativa Escolar Tomas de Heres, Barrio Tomas de Heres (II Etapa), Cuidad Bolívar, municipio Heres.', '1', '1', '290000.00', '3', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('186', 'Ampliación y Acondicionamiento del Ambulatorio Urbano Tipo I, Urbanización La Floresta, Upata, municipio Piar.', '3', '54', '100759.88', '4', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('187', 'Culminación de la Planta Potabilizadora de 25lts/seg, para la Población del Dorado.', '5', '31', '300032.28', '1', '1', '2', '100', '1', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('188', 'Primera Etapa, Construcción de Aducción desde la Planta Potabilizadora hasta la Población de Tumeremo, municipio Sifontes, estado Bolívar.', '5', '52', '3970677.92', '1', '1', '2', '100', '1', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('189', 'Recuperación Ambiental del sector La Trinidad, Eje el dorado, km 88, Municipio Autónomo Sifontes, estado Bolívar.', '5', '34', '1226449.95', '10', '1', '3', '100', '9', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('190', 'Mejoras y Rehabilitación de Vialidad Agrícola en el sector Mama Eulalia II (Prog. 9+000 a la 18+000), parroquia Barceloneta.', '6', '36', '337331.88', '2', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('191', 'Mejoras y Rehabilitación de Vialidad Agrícola en el sector \"Mama Eulalia I\", parroquia Barceloneta, municipio Bolivariano Angostura, estado Bolívar.', '6', '36', '336102.09', '2', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('192', 'Mejoras y Rehabilitación de Vialidad Agrícola en sector Los Guacos - Curichapo, parroquia Barceloneta, municipio Bolivariano Angostura, estado Bolívar.', '6', '36', '312992.35', '2', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('193', 'Mejoras y Rehabilitación de Vialidad Agrícola en el sector Cruce El Pao - Curichapo, parroquia Barceloneta, municipio Bolivariano Angostura, estado Bolívar.', '6', '36', '234947.72', '2', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('194', 'Recuperación Ambiental y Manejo Sustentable de unas Parcelas Afectadas por Trabajos de Pequeños Mineros de Oro y Diamante Ubicada en Los Brasileros, municipio Gran Sabana, estado Bolívar.', '9', '48', '1329883.10', '10', '1', '3', '100', '9', '2005', '-', '2017-01-09 16:48:21', '2017-01-09 16:48:21', null);
INSERT INTO `proyectos` VALUES ('195', 'Acondicionamiento Hidráulico de la Línea de Aducción Tocomita, municipio Heres, estado Bolívar.', '1', '4', '577944.63', '1', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('196', 'Biblioteca Virtual Manuel Carlos Piar Fundapiar, Sede Ciudad Bolívar.', '1', '1', '468495.43', '3', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('197', 'Remodelación, Ampliación y Mejoras de la Unidad Educativa Brisas del Este II, Ubicada en el Barrio Brisas del Este II, parroquia La Sabanita, municipio Heres, estado Bolívar.', '1', '6', '293977.32', '3', '1', '3', '100', '2', '2005', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('198', 'Ampliación de la Red Eléctrica San Valentín, parroquia Marhuanta, municipio Heres.', '1', '4', '363870.92', '5', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('199', 'Fortalecimiento Tecnológico de la Dirección de Proyectos Especiales de la Gobernación del estado Bolívar.', '2', '18', '178305.82', '8', '1', '3', '100', '4', '2005', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('200', 'Ampliación y Reparación de la Unidad Educativa Los Pijiguaos, municipio Cedeño, estado Bolívar.', '4', '56', '202399.69', '3', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('201', 'Proyecto de Renovación y Ampliación de la Plataforma Tecnológica de Inviobras Bolívar e Hidrobolívar.', '12', '53', '1100060.00', '8', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('202', 'Planta de Tratamiento Toro Muerto, Puerto Ordaz municipio Caroní, estado Bolívar.', '2', '18', '5139113.66', '1', '1', '2', '100', '1', '2005', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('203', 'Construcción de Aducción desde la Salida de la Planta de Tratamiento hasta el Tanque de Monserrat (primera etapa).', '3', '54', '10868564.42', '1', '1', '3', '100', '1', '2005', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('204', 'Adquisición de Tractores de Oruga D8N para la Gobernación del estado Bolívar.', '12', '53', '3817290.00', '8', '1', '2', '100', '4', '2005', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('205', 'Remodelación, Ampliación y Mejoras de la Unidad Educativa José Antonio Páez, Ubicada en  el Barrio José Antonio Páez, parroquia José Antonio Páez, municipio Heres, estado Bolívar.', '1', '3', '411152.44', '3', '1', '3', '100', '12', '2005', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('206', 'Remodelación, Ampliación y Mejoras  del Instituto de Educación Especial La Sabanita, Ubicado en el Barrio La Sabanita, parroquia La Sabanita, Ciudad Bolívar, municipio Heres del estado Bolívar.', '1', '6', '124775.53', '3', '1', '3', '100', '12', '2005', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('207', 'Remodelación, Ampliación y Mejoras de la Unidad Educativa Simón Rodríguez, Ubicada en  el Sector  Sierra Tres, parroquia Upata, municipio Piar, estado Bolívar.', '3', '54', '247326.83', '3', '1', '3', '100', '12', '2005', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('208', 'Adquisición de Cargadores Frontales para la Gobernación del estado Bolívar. ', '12', '53', '2206185.00', '8', '1', '2', '100', '4', '2005', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('209', 'Adquisición de Camiones con Volteo para la Gobernación del estado Bolívar.', '12', '53', '1964040.00', '8', '1', '2', '100', '4', '2005', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('210', 'Construcción de las Edificaciones (estación de bomberos, vialidad y terraceo) del Terminal Aéreo de Santa Elena de Uairen, municipio Gran Sabana del estado Bolívar.', '9', '47', '3400000.00', '5', '1', '2', '100', '3', '2005', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('211', 'Remodelación, Ampliación y Mejoras de la Unidad Educativa Rafael Urdaneta, Ubicada en el barrio Guayana, parroquia Unare, municipio Caroní, Ciudad Guayana, estado Bolívar.', '2', '10', '186711.72', '3', '1', '3', '100', '12', '2005', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('212', 'II Etapa Estabilización de Taludes de la Cárcava Ubicada en la Urbanización Bicentenario, Cuenca del Río San Rafael, parroquia Vista Hermosa municipio Autónomo Heres, estado Bolívar.', '1', '2', '1617308.83', '1', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('213', 'Asfaltado del barrio 24 de Abril, sector Brisas del Orinoco, parroquia La Sabanita, municipio Heres,  estado Bolívar.', '1', '6', '699392.60', '2', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('214', 'Electrificación y Alumbrado Público del barrio 24 de Abril, sector Brisas del Orinoco, parroquia La Sabanita, municipio Heres, estado Bolívar.', '1', '6', '480676.09', '5', '1', '3', '100', '3', '2005', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('215', 'Remodelación, Ampliación y Mejoras de La Unidad Educativa Angostura, Ubicada en  el Barrio Angosturita II,  parroquia Vista Hermosa, municipio Heres, estado Bolívar.', '1', '2', '422860.74', '3', '1', '3', '100', '12', '2005', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('216', 'Construcción  de Viviendas  en el municipio Heres del estado Bolívar.', '1', '53', '7692611.38', '5', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('217', 'Construcción de Cloacas y Estación de Bombeo, sector Francisco de Miranda, UD-148, San Félix, municipio Caroní.', '2', '12', '729975.78', '1', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('218', 'Construcción de Centro Integral de Servicios a la Comunidad de la Victoria, parroquia Vista al Sol, municipio Caroní del estado Bolívar (I Etapa).', '2', '17', '1000000.00', '9', '1', '1', '100', '2', '2012', '2012-0075', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('219', 'Construcción de Centro de Atención Infantil (CAI) en Trapichito, (remodelación de pre-escolar maternal Trapichito) municipio Caroní, estado Bolívar.', '2', '17', '2395841.00', '3', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('220', 'Ampliación de la Unidad Educativa Quiriviti, municipio Cedeño, estado Bolívar.', '4', '30', '299992.62', '3', '1', '2', '100', '13', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('221', 'Ampliación de la Unidad Educativa Soraima, municipio Cedeño, estado Bolívar.', '4', '24', '299992.62', '3', '1', '2', '100', '13', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('222', 'Ampliación de la Unidad Educativa Santa Rosalía, sector Santa Rosalía, municipio Cedeño, estado Bolívar.', '4', '30', '299992.62', '3', '1', '2', '100', '13', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('223', 'Adquisición de  Transporte de Pescado Fresco (camiones cava) para los municipios  Sucre, Cedeño, Angostura Bolivariana, Heres y Caroní. (fortalecimiento de la actividad pesquera en las comunidades de los municipios Sucre, Cedeño, Heres, Angostura Bolivariana y Caroní)', '12', '53', '1900000.00', '7', '1', '2', '100', '5', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('224', 'Adquisición de  Transporte Terrestre de Pasajeros Urbanos y Extraurbanos (adquisición de minibuses para Municipios Caroní y Heres del  estado Bolívar).', '12', '53', '6600000.00', '6', '1', '2', '100', '5', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('225', 'Adquisición de Vehículos para la Fundación del Niño.', '1', '1', '408703.68', '6', '1', '2', '100', '10', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('226', 'Suministro e Instalación de Aires Acondicionados para el Liceo \"Lino Maradey Donato\" de Ciudad Piar, municipio Bolivariano Angostura, estado Bolívar.', '6', '36', '3402.00', '3', '1', '2', '100', '4', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('227', 'Centro de Educación Inicial (maternal y preescolar) Sede Emergencia Bolívar 171, municipio Caroní, estado Bolívar.', '2', '13', '2038545.05', '6', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('228', 'Construcción de Centro Integral de Servicios a la Comunidad de la Victoria, parroquia Vista al Sol, municipio Caroní, del estado Bolívar. (II Etapa)', '2', '17', '280000.00', '9', '1', '1', '100', '2', '2011', '2012-0142', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('229', 'Señalización Autopista municipios Heres - Caroní - Upata.', '12', '53', '758293.10', '2', '1', '2', '100', '2', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('230', 'Adquisición de Camión Vactor (adquisición de Camión con Equipo de Saneamiento Ambiental de Limpieza y Aspirado para la Gobernación  del estado Bolívar) (I Etapa).', '12', '53', '205650.00', '8', '1', '2', '100', '4', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('231', 'Adquisición de Camión Perforador de Pozos de Agua para la Gobernación del estado Bolívar.', '12', '53', '187485.23', '8', '1', '2', '100', '4', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('232', 'Construcción de Centro de Atención para  Niños Especiales para la Fundación del Niño, municipio Heres, estado Bolívar.', '1', '1', '4400000.00', '6', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('233', 'Construcción de Viviendas en el municipio Piar, estado Bolívar.', '3', '53', '2000000.00', '5', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('234', 'Adquisición de una Unidad de Transporte Público para la Población del Manteco, parroquia Pedro Cova, municipio Piar.', '3', '23', '36000.00', '6', '1', '2', '100', '4', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('235', 'Construcción Aceras y Brocales en las Comunidades del sector San Lorenzo de la Ciudad de Upata, municipio Piar (sociedad civil).', '3', '54', '32893.19', '5', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('236', 'Desarrollo Hortícola Coozsur, municipio Roscio, estado Bolívar. ', '8', '53', '79261.01', '7', '1', '2', '100', '5', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('237', 'Asfaltado del sector Core 8 del municipio Caroní del estado Bolívar.', '2', '10', '1700000.00', '2', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('238', 'Culminación de la Obra: Recuperación del Canal Principal de Drenaje de Vista al Sol \"Cárcava\", municipio Caroní, estado Bolívar.', '2', '17', '1500000.00', '5', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('239', 'Construcción de Centro de Alta Tecnología (cubo rojo), Unare II, Puerto Ordaz, municipio Caroní estado Bolívar.', '2', '10', '1047317.41', '4', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:22', '2017-01-09 16:48:22', null);
INSERT INTO `proyectos` VALUES ('240', 'Ampliación y Mejoras de Edificaciones para el Centro de Diagnóstico Integral (sin quirófano) de Castillito, Puerto Ordaz, Municipio Caroní.', '2', '11', '415000.00', '4', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('241', 'Construcción del Centro de Diagnóstico Integral Los Olivos, municipio Caroní, estado Bolívar.', '2', '18', '200000.00', '4', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('242', 'Ampliación y Mejoras de Edificios para Centro de Diagnóstico Integral Bella Vista UD-126, sector Bella Vista, San Félix, municipio Caroní del estado Bolívar', '2', '55', '130250.17', '4', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('243', 'Conclusión Construcción del Liceo La Romana, Caicara del Orinoco, municipio Cedeño.', '4', '30', '600000.00', '3', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('244', 'Centro de Rehabilitación Integral Arnoldo Gabaldón Caicara del Orinoco, municipio Cedeño del Estado Bolívar.', '4', '30', '550000.00', '4', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('245', 'Construcción de Centros de Servicios a la Comunidad en la parroquia Marhuanta, municipio Heres del estado Bolívar.', '1', '4', '2700000.00', '9', '1', '1', '100', '3', '2012', '2012-0119', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('246', 'Construcción del Centro de Coordinación Policial en el Retumbo, municipio Caroní, estado Bolívar. ', '2', '16', '1000000.00', '9', '1', '1', '100', '2', '2013', '2013-0107', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('247', 'Construcción de Centros de Servicios a la Comunidad en la parroquia Marhuanta municipio Heres del estado Bolívar (II Etapa). ', '1', '4', '1248655.45', '9', '1', '1', '100', '3', '2013', '2013-0109', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('248', 'Reparación de Comisarías y Subcomisarias en todos los  municipios del estado Bolívar.', '12', '53', '5000000.00', '9', '1', '2', '100', '2', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('249', 'Culminación de la Primera Etapa del Edificio Sede de la Fundación del Niño, municipio Heres, estado Bolívar.', '1', '1', '1700000.00', '6', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('250', 'Construcción de Churuata Indígena, municipio Heres, estado Bolívar (construcción de estructura, cubierta de techos y acabados).', '1', '3', '1200000.00', '5', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('251', 'Centro de Diagnóstico Integral con quirófano \"La Paragua\" Ciudad Bolívar al lado del INCE, municipio Heres, estado Bolívar.', '1', '2', '445131.94', '4', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('252', 'Centro de Alta Tecnología sin Quirófano, Vista Hermosa II, Ciudad Bolívar, municipio Heres, estado Bolívar. (Construcción).', '1', '2', '264496.57', '4', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('253', 'Ampliación y Mejoras de Edificaciones para el Centro De Diagnóstico Integral (CDI) Lino Maradey, Urbanización Banco Obrero, Ciudad Bolívar, municipio Heres, estado Bolívar.', '1', '2', '70000.00', '4', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('254', 'Ampliación y Mejoras de Edificios para Centro de Diagnóstico Integral Coviaguard, Upata, municipio Piar, estado Bolívar.', '3', '54', '406357.88', '4', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('255', 'Centro de Diagnóstico Integral \"Bicentenario\" calle Orinoco cruce con calle  Uonquen, municipio Piar, Upata, edo. Bolívar (Ampliación y Mejoras).', '3', '54', '889614.01', '4', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('256', 'Conclusión del Nuevo Diseño Arquitectónico de la Plaza Bolívar, Guasipati, municipio Roscio, estado Bolívar. ', '8', '44', '700000.00', '5', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('257', 'Ampliación y Mejoras de Edificio para Centro de Diagnóstico Integral Edwin Lailow, Guasipati, municipio Roscio, estado Bolívar.', '8', '44', '237846.75', '4', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('258', 'Ampliación y Mejoras de Edificios para Centro de Diagnóstico Integral El Dorado, municipio Sifontes, estado Bolívar.', '5', '31', '935751.85', '4', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('259', 'Ampliación y Mejoras de Edificios para Centro de Diagnóstico Integral Tumeremo, municipio Sifontes, estado Bolívar.', '5', '52', '735000.00', '4', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('260', 'Ampliación y Mejoras de Edificio para Centro de Diagnóstico Integral Guarataro, municipio Sucre.', '7', '40', '250000.00', '4', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('261', 'Adquisición de un Sistema de Abordaje/Puente Telescopio/Jet Way/Pass Way para el Aeropuerto Internacional Manuel Carlos Piar de Ciudad Guayana, municipio Caroní, estado Bolívar.', '2', '10', '2000000.00', '5', '1', '3', '100', '14', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('262', 'Nuevo Acueducto para el Asentamiento Campesino el Corocito, parroquia Chirica, municipio Caroní, estado Bolívar.', '2', '12', '650000.00', '1', '1', '3', '100', '1', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('263', 'Adquisición de un Sistema de Abordaje/Puente Telescopio/Jet Way/Pass Way para el Aeropuerto Internacional Manuel Carlos Piar de Ciudad Guayana, municipio Caroní, estado Bolívar (complemento).', '2', '10', '300000.00', '5', '1', '3', '100', '14', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('264', 'Adquisición de Vehículos para la Gobernación del estado Bolívar.', '12', '53', '1500000.00', '8', '1', '3', '100', '4', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('265', 'Adquisición de Planta Eléctrica para el Instituto de Salud Pública de estado Bolívar.', '12', '53', '300000.00', '4', '1', '3', '100', '4', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('266', 'Construcción y Dotación de la Sede del Palacio de Gobierno, Ciudad Bolívar, municipio Heres del estado Bolívar.', '1', '1', '2800000.00', '5', '1', '3', '100', '11', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('267', 'Ampliación de la U.E.N. Heriberto Vera Tirado, municipio Caroní, estado Bolívar.', '2', '14', '250087.89', '3', '1', '3', '100', '2', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('268', 'Mejoras y Rehabilitación de la Vialidad Agrícola, en el sector El Chivo - Llano Alto, II Etapa Progresiva 7+000 a la 10+500, parroquia Barceloneta.', '6', '36', '746061.39', '2', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('269', 'Construcción de Línea Eléctrica en Alta y Baja Tensión para el sector Cerro Peldio, municipio Bolivariano Angostura  (II Etapa).', '6', '35', '100310.48', '5', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('270', 'Construcción de Drenaje Transversal en la Carretera Troncal 16 Progresiva 161+800, municipio Bolivariano Angostura.', '6', '36', '100000.00', '2', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('271', 'Construcción de la Avenida Loefling, parroquia Universidad, municipio Caroní, estado Bolívar.', '2', '18', '3000000.00', '2', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('272', 'Culminación de Asfaltado a las calles de Acceso a la Urbanización Los Bucares, municipio Caroní, estado Bolívar.', '2', '10', '200000.00', '2', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('273', 'Construcción de Vivienda Unifamiliar Aislada, Modelo Tipo 2003-2005 Rural con Cubierta de Techo Tropical, Ubicado en sector San José de Cacahual, San Félix, municipio Caroní, estado Bolívar.', '2', '55', '48973.24', '5', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('274', 'Construcción de Cuatro Alcantarillas  en la Troncal 12 vía Guaniamo, municipio Cedeño.', '4', '53', '320000.00', '2', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('275', 'Asfaltado de calle Principal, urbanización La Romana, municipio Cedeño en el estado Bolívar.', '4', '30', '287935.95', '2', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('276', 'Construcción del Salón de Música y Servicios en la Casa del Niño Manuel Centurión, Caicara del Orinoco, municipio Cedeño en el estado Bolívar.', '4', '30', '217768.99', '6', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('277', 'Culminación de la Construcción del Pre - Escolar Yuruary, sector La Gasolina, El Callao, edo – Bolívar.', '10', '49', '386291.58', '3', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('278', 'Asfaltado Vialidad Urbana del estado Bolívar.', '12', '53', '2000000.00', '2', '1', '3', '100', '2', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('279', 'Adquisición  de  Vehículos para el Fortalecimiento de los Programas Sociales del estado Bolívar.', '12', '53', '1700000.00', '8', '1', '3', '100', '4', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('280', 'Construcción y Dotación de Posadas Turísticas.', '12', '53', '3500000.00', '7', '1', '3', '100', '5', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('281', 'Consolidación del Barrio 4 de Febrero en municipio Heres del estado Bolívar.', '1', '5', '591249.67', '5', '1', '3', '100', '2', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('282', 'Culminación de la Red de Cloacas de la Urbanización Villas del Orinoco, parroquia Agua Salada, Ciudad  Bolívar, municipio Heres.', '1', '5', '200000.00', '1', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('283', 'Adquisición de  Transporte Fluvial de Pasajeros (peñeros) (modernización del sistema de transporte fluvial de pasajeros en el municipio Heres) (Complemento).', '1', '1', '100000.00', '7', '1', '3', '100', '5', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('284', 'Abastecimiento de Agua Potable al Poblado de Las Galderas, en la parroquia Panapana, Jurisdicción del municipio Heres.', '1', '8', '27732.92', '1', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('285', 'Reparación Bote de Agua y Pavimentación en la calle Juan José Landaeta frente al Centro de Diagnóstico Integral “Los Próceres” parroquia Agua Salada, municipio Heres del estado Bolívar.', '1', '5', '26925.51', '1', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('286', 'Recuperación del Tramo Colapsado del Canal Drenaje  Principal de Vista al Sol (cárcava), Ubicado en la UD-136 del barrio Vista al Sol 2.', '2', '17', '2935630.68', '5', '1', '3', '100', '1', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('287', 'Canal de Drenaje de Los Olivos, Puerto Ordaz, M. A. Caroní, estado Bolívar (II Etapa).', '2', '18', '700000.00', '5', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('288', 'Centro de Diagnóstico Integral El Rosario, municipio Caroní, estado Bolívar.', '2', '12', '630810.49', '4', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:23', '2017-01-09 16:48:23', null);
INSERT INTO `proyectos` VALUES ('289', 'Sala de Rehabilitación Integral La Unidad UD-111, San Félix, municipio Caroní, estado Bolívar. (obras complementarias y obras exteriores)', '2', '13', '420000.00', '4', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('290', 'Sala de Rehabilitación Integral Los Olivos UD-235, municipio Caroní, estado Bolívar.', '2', '18', '320000.00', '4', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('291', 'Construcción del Centro de Rehabilitación Integral Nueva Chirica, San Félix, municipio Caroní, estado Bolívar.', '2', '12', '220000.00', '4', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('292', 'Ampliación y Mejoras de Edificios para Centros de Diagnóstico (Centro de Diagnóstico Integral La Llovizna UD-145, municipio Caroní, estado Bolívar.', '2', '31', '39265.30', '4', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('293', 'Construcción de Cloacas y Estación de Bombeo, sector Francisco de Miranda, UD-148, San Félix, municipio Caroní (II Etapa).', '2', '12', '347101.85', '1', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('294', 'Centro de Diagnóstico Integral \"Arnaldo  Gabaldon\" Caicara del Orinoco municipio Cedeño edo. Bolívar (Mejoras)', '4', '30', '969000.00', '4', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('295', 'Adquisición de Vehículos para el Fortalecimiento Institucional de la Gobernación del estado Bolívar.', '12', '53', '2295158.60', '8', '1', '2', '100', '4', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('296', 'Ampliación, Restructuración, Construcción de la Infraestructura de la Comandancia General, Comisarias, Subcomisarias, Puestos Policiales y Módulos Policiales.', '12', '53', '2000000.00', '9', '1', '2', '100', '2', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('297', 'Adquisición de Software para Digitalización y Automatización del Archivo Administrativo de la Gobernación del estado Bolívar.', '12', '53', '567720.00', '8', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('298', 'Centro Integral de Coordinación y Seguridad Ciudadana del estado Bolívar.', '12', '53', '1726007.50', '9', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('299', 'Abastecimiento de Agua Potable al Poblado de Las Galderas, en la parroquia Panapana, Jurisdicción del municipio Heres.', '1', '8', '113410.48', '1', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('300', 'Perforación de Pozo de Agua San José de Bongo, en la parroquia Pana-Pana, municipio Heres.', '1', '8', '26912.59', '1', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('301', 'Conclusión de la construcción del Centro Lácteo de Upata, Municipio Piar del estado Bolívar. ', '3', '54', '600000.00', '7', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('302', 'Nuevo Diseño Arquitectónico de la Plaza Bolívar, Guasipati, municipio Roscio, estado Bolívar (II Etapa).', '8', '44', '700000.00', '5', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('303', 'Construcción del Muro de Contención en el Campo de Beisbol del Polideportivo Vista al Sol, municipio Caroní, estado Bolívar.', '2', '17', '138000.00', '3', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('304', 'Reacondicionamiento de la E.B.N. Ramón Isidro Monte, municipio Caroní, estado Bolívar.', '2', '13', '284679.28', '3', '1', '3', '100', '2', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('305', 'Reacondicionamiento de la U.E.N. Unare III, municipio Caroní, edo. Bolívar. ', '2', '10', '349968.38', '3', '1', '3', '100', '2', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('306', 'Reacondicionamiento del Instituto de Educación Especial Puerto Ordaz, municipio Caroní, estado Bolívar.', '2', '13', '261260.35', '3', '1', '3', '100', '2', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('307', 'Culminación del Centro Materno Infantil, municipio Heres estado Bolívar.', '1', '1', '4000000.00', '4', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('308', 'Adquisición de Tres (03) Sistemas de Abordaje/Puente Telescópico/Jetway/Pass Way para el Aeropuerto  Internacional \"Manuel Carlos Piar\" de Ciudad Guayana, municipio Caroní del estado Bolívar.', '2', '10', '1610309.15', '5', '1', '2', '100', '4', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('309', 'Reacondicionamiento de la U.E.E. Nelly de Sofía, municipio Caroní, estado Bolívar.', '2', '12', '260361.12', '3', '1', '3', '100', '2', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('310', 'Ampliación de la Red Eléctrica del Barrio Villa Lola parroquia Marhuanta, municipio Heres.', '1', '4', '109085.03', '5', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('311', 'Culminación de la Construcción de la Sede de la Alcaldía Padre Pedro Chien, El Palmar (II Etapa).', '11', '51', '500000.00', '5', '1', '3', '100', '3', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('312', 'Culminación de la Construcción de la Sede de la Alcaldía Padre Pedro Chien, El Palmar (II Etapa).', '11', '51', '499999.96', '5', '1', '2', '100', '3', '2006', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('313', 'Equipamiento del Centro de Acopio Pesquero en el municipio Heres del estado Bolívar.', '1', '1', '1000000.00', '7', '1', '2', '100', '5', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('314', 'Construcción de la Avenida Loefling, parroquia Universidad, municipio Caroní, estado Bolívar. ', '2', '18', '6000000.00', '2', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('315', 'Ampliación de Escuela San José del Bongo (primera etapa).', '1', '8', '148636.56', '3', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('316', 'Construcción de Acueducto en la parroquia San Francisco sector Los Cerritos, municipio Bolivariano Angostura (Juas Jual I).', '6', '35', '505632.80', '1', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('317', 'Construcción de Acueducto en la parroquia San Francisco sector Aeropuerto, municipio  Bolivariano Angostura. (El Gallo).', '6', '35', '482170.87', '1', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('318', 'Mejoras y Bacheo, Carretera R-08, La Quina, San Francisco, II Etapa, municipio Bolivariano Angostura (Complemento).', '6', '35', '221672.52', '2', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('319', 'I Etapa de Consolidación del sector Cambalache, municipio Caroní, estado Bolívar.', '2', '10', '2000000.00', '5', '1', '3', '100', '2', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('320', 'Señalización de Autopista Ciudad Bolívar-Puerto Ordaz y Vías de Acceso al Centro Total de Entretenimiento Cachamay, estado Bolívar.', '2', '11', '2500000.00', '2', '1', '2', '100', '2', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('321', 'Modernización de la Flota de Vehículos Taxis en el municipio Caroní del estado Bolívar.', '2', '53', '3500000.00', '6', '1', '2', '100', '5', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('322', 'Centro de Atención Integral Mundo Sonrisa, municipio Caroní, estado Bolívar', '2', '18', '3689087.70', '6', '1', '3', '100', '3', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('323', 'Construcción de Muro de Contención y Cerca Perimetral de Concreto en el Centro de Total Entretenimiento Cachamay, municipio Caroní, estado Bolívar', '2', '11', '1999845.15', '5', '1', '3', '100', '3', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('324', 'Iluminación Áreas Adyacentes al CTE Cachamay y Distintos sectores de Ciudad Guayana, municipio Caroní del estado Bolívar.', '2', '11', '782440.27', '5', '1', '3', '100', '2', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('325', 'Rehabilitación de Canchas Deportivas en el municipio Caroní del estado Bolívar (I Etapa).', '2', '53', '700000.00', '3', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('326', 'Acondicionamiento de las Dependencias de la Secretaría de Participación Popular (sede los sabanales y la sabanita).', '2', '31', '389000.00', '5', '1', '2', '100', '2', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('327', 'Enrejado entre Tribuna Principal y Gradas del CTE Cachamay, municipio Caroní, estado Bolívar.', '2', '11', '260000.00', '5', '1', '2', '100', '2', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('328', 'Construcción del Sistema de Cloacas sector La Laguna Urb. Jardín Buena Vista, parroquia Dalla Costa, Mun. Caroní.', '2', '31', '255665.06', '1', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('329', 'Cloacas Barrio La Laguna, sector II, UD-151, San Félix-estado Bolívar.', '2', '13', '255665.06', '1', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('330', 'Rehabilitación de Canchas Deportivas en el municipio Caroní del estado Bolívar (I y II Etapa).', '2', '53', '200000.00', '3', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('331', 'Construcción Ampliación y Mejoras de Centros de Diagnóstico Integral (CDI), Centros de Alta Tecnología (CAT) y Salas de Rehabilitación Integral (SRI). (Construcción de Centro de Diagnóstico con Quirófano en La Victoria, municipio Caroní, estado Bolívar).', '2', '17', '106333.13', '4', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('332', 'Construcción de una Cancha Deportiva de Usos Múltiples en el Liceo Ramón Antonio Pérez, parroquia Simón Bolívar (Complemento).', '2', '13', '61162.78', '3', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('333', 'Construcción de Escuelas Bolivarianas en el estado Bolívar. (Construcción de Escuela Bolivariana en el Barrio Nueva Jerusalén, Fundo El Algarrobo, parroquia Chirica)', '2', '12', '3999990.80', '3', '1', '3', '100', '3', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('334', 'Señalización vial de la Autopista Manuel Carlos Piar desde el Peaje Matanzas hasta la Redoma Bauxilum, municipio Caroní del estado Bolívar.', '2', '10', '4000000.00', '2', '1', '2', '100', '2', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('335', 'Construcción y Mejoras de Vialidad Interna y Conexa, Estacionamiento y Obras Exteriores en el Polideportivo Cachamay, municipio Caroní del estado Bolívar.', '2', '11', '15000000.00', '2', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('336', 'Ampliación y Remodelación del Polideportivo Cachamay, Extensión y Vías de Acceso, municipio Caroní.', '2', '11', '15000000.00', '5', '1', '3', '100', '3', '2007', '-', '2017-01-09 16:48:24', '2017-01-09 16:48:24', null);
INSERT INTO `proyectos` VALUES ('337', 'Ampliación y Modernización del Aeropuerto Carlos Manuel Piar, municipio Caroní del estado Bolívar', '2', '10', '18000000.00', '5', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('338', 'Conclusión Construcción del Liceo La Romana, Caicara del Orinoco, municipio Cedeño.', '4', '30', '315000.00', '3', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('339', 'Ampliación de la E.T.A. Luis Morillo Colmenares, municipio El Callao, estado Bolívar (complemento).', '10', '49', '134553.45', '3', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('340', 'Centro Integral de Coordinación y Seguridad Ciudadana del estado Bolívar.', '12', '53', '6473992.50', '9', '1', '3', '100', '3', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('341', 'Adquisición de Maquinarias Pesadas para el Fortalecimiento de la Infraestructura Vial del estado Bolívar.', '12', '53', '2980525.07', '5', '1', '2', '100', '4', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('342', 'Dotación y Adecuación de las Instalaciones de la Comisaria Nº 13, parroquia Cachamay, estado Bolívar.', '2', '11', '247623.04', '9', '1', '2', '100', '4', '2009', '2009-0034', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('343', 'Construcción y Dotación de la Comisaria Los Olivos, municipio Caroní del estado Bolívar.  ', '2', '18', '7000000.00', '9', '1', '3', '100', '3', '2009', '2009-0066', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('344', 'Construcción de Sede para la Imprenta y Depósitos de la Gobernación y Fundación del Niño del estado Bolívar.', '12', '53', '1614838.11', '6', '1', '2', '100', '2', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('345', 'Fortalecimiento de la Plataforma Futbolística del estado Bolívar.', '12', '53', '1500000.00', '3', '1', '2', '100', '4', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('346', 'Dotación de Equipos de Computación para el Fortalecimiento Educativo del estado Bolívar.', '12', '53', '1365357.50', '3', '1', '3', '100', '4', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('347', 'Fortalecimiento Tecnológico para el Instituto Salud Pública del estado Bolívar.', '12', '53', '500000.00', '4', '1', '2', '100', '4', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('348', 'Adecuación y Mejoras de la Comisaria Nº 20, parroquia Agua Salada, municipio Heres, estado Bolívar. ', '1', '5', '350000.00', '9', '1', '3', '100', '2', '2010', '2010-0011', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('349', 'Adquisición de Mobiliario y Equipos para el Fortalecimiento del Instituto Deportivo del estado Bolívar (IDEBOL).', '12', '53', '175000.00', '8', '1', '2', '100', '4', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('350', 'Fortalecimiento Tecnológico para la Secretaría de Participación Popular y Desarrollo Social del estado Bolívar.', '12', '53', '100000.00', '8', '1', '2', '100', '4', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('351', 'Adecuación y Mejoras del Comedor de la Subcomisaria Maipure, municipio Heres, estado Bolívar.', '1', '4', '260000.00', '9', '1', '3', '100', '2', '2010', '2010-0010', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('352', 'Adquisición de Vehículos para el Fortalecimiento del sector Educativo y de Participación Popular del estado Bolívar.', '12', '53', '7000000.00', '3', '1', '2', '100', '4', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('353', 'Plan de Asfaltado en los Diferentes municipios del estado Bolívar.', '12', '53', '10000000.00', '2', '1', '3', '100', '2', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('354', 'Transporte Terrestre de Pasajeros.', '12', '53', '13200000.00', '6', '1', '2', '100', '5', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('355', 'Adquisición de Autobuses como Parte del Sistema Integral de Rutas Extraurbanas en el estado Bolívar.', '12', '53', '15000000.00', '6', '1', '2', '100', '5', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('356', 'Adquisición de Doscientas (200) Unidades Autobuseras para el Fortalecimiento del Transporte Público en el estado Bolívar.', '12', '53', '26400000.00', '6', '1', '2', '100', '5', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('357', 'Culminación del Aeropuerto de Santa Elena de Uairen en municipio Gran Sabana del estado Bolívar. (Complemento)', '9', '47', '3100000.00', '5', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('358', 'Culminación del Aeropuerto de Santa Elena de Uairen en municipio Gran Sabana del estado Bolívar.', '9', '47', '7000000.00', '5', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('359', 'Consolidación del Barrio 4 de Febrero en municipio Heres del estado Bolívar.', '1', '5', '2500000.00', '5', '1', '3', '100', '2', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('360', 'Ampliación y Mejoras de la Unidad Educativa Integral Bolivariana Mayagua, municipio Heres del estado Bolívar.', '1', '7', '800000.00', '3', '1', '3', '100', '2', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('361', 'Mejoras y Equipamiento de Restaurantes y Aulas académicas del jardín botánico del Orinoco para el Desarrollo del Proyecto Socio-Productivo, municipio Heres, estado Bolívar.', '1', '1', '400000.00', '11', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('362', 'Construcción de Escuela Rinconote, Vía  Ciudad Piar, municipio Heres, estado Bolívar.', '1', '3', '150000.00', '3', '1', '2', '100', '2', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('363', 'Recuperación de Paseo Octava Estrella (alumbrado, pintura de aceras y brocales, alcantarillado plaza la estrella), parroquia Catedral, municipio Heres (ejecución en 2009, redistribución de recurso 2007).', '1', '1', '700000.00', '5', '1', '2', '100', '2', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('364', 'Construcción de Sistema de Drenaje y Recolección de Aguas de Lluvia y Asfaltado del sector Palo Grande, parroquia Vista Hermosa, municipio Heres (5 calles, 300mts, 1300 ml ) (ejecución en 2009, redistribución de recurso 2007).', '1', '2', '600000.00', '5', '1', '2', '100', '2', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('365', 'Recuperación del Parque Ruiz Pineda, municipio Heres, estado Bolívar (ejecución en 2009, redistribución de recurso 2007).', '1', '2', '500000.00', '5', '1', '2', '100', '2', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('366', 'Construcción de Escuela Bolivariana en el municipio Heres, estado Bolívar. (Construcción de Escuela Bolivariana en el barrio comunidad democrática,  Parroquia Vista Hermosa, Ciudad Bolívar).', '1', '2', '3999990.86', '3', '1', '3', '100', '3', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('367', 'Construcción de Escuelas Bolivarianas sector Santo Domingo en el municipio Piar del estado Bolívar.', '3', '54', '3922719.08', '3', '1', '3', '100', '3', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('368', 'Ampliación y Mejoras de la Red Eléctrica del sector Los Chorros, Upata, municipio Piar.', '3', '54', '200000.00', '5', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('369', 'Ampliación de la Escuela Rural El Troncón, parroquia Moitaco, Jurisdicción del municipio Sucre, estado Bolívar.', '7', '41', '494800.48', '3', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('370', 'Construcción de la Avenida Loefling, parroquia Universidad, municipio Caroní, estado Bolívar. ', '2', '18', '4555499.80', '2', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('371', 'Adquisición de  Transporte Fluvial de Pasajeros (peñeros) (modernización del sistema de transporte fluvial de pasajeros) en el Municipio Heres.', '1', '1', '1000000.00', '7', '1', '2', '100', '5', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('372', 'Matriz de Servicio para Abastecimiento de Agua del Barrio Nueva República, Parroquia La Sabanita, Ciudad Bolívar, municipio Heres.', '1', '6', '334837.53', '1', '1', '3', '100', '3', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('373', 'Ampliación de la E.T.A. Luis Morillo Colmenares, municipio El Callao, estado Bolívar.', '10', '49', '275460.60', '3', '1', '2', '100', '3', '2007', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('374', 'Culminación del Liceo Bolivariano Las Amazonas, municipio Caroní, edo. Bolívar. ', '2', '10', '2109432.67', '3', '1', '3', '100', '3', '2008', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('375', 'Construcción de Comisaria en la parroquia Cachamay, municipio Caroní  del estado Bolívar.', '2', '11', '2999471.49', '5', '1', '3', '100', '3', '2008', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('376', 'Construcción de Liceo y Cancha de Usos Múltiples en  Fronteras de Guaiparo parroquia Dalla Costa, municipio Caroní del estado Bolívar.', '2', '31', '3500000.00', '3', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('377', 'Construcción de  Viviendas en la UD-128 y Villa Jardín del municipio Caroní, estado Bolívar.', '2', '55', '1650119.27', '5', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('378', 'Construcción de la Segunda Etapa de la Escuela Técnica Agropecuaria y Forestal Ezequiel Zamora, sector Las Amazonas, parroquia Unare, municipio Caroní, estado Bolívar.', '2', '10', '1200000.00', '3', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('379', 'Mantenimiento, Conservación y Mejoras en las Áreas Adyacentes al CTE Cachamay, municipio Caroní, estado Bolívar.', '2', '11', '1100000.00', '5', '1', '3', '100', '2', '2008', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('380', 'Reacondicionamiento y Mejoras de Unidades Educativas en el municipio Caroní del estado Bolívar.', '2', '53', '899912.14', '5', '1', '3', '100', '2', '2008', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('381', 'Rehabilitación de los Puentes de Abordaje (Pass-Way) del Aeropuerto Internacional \"Manuel Carlos Piar\", Ciudad Guayana, municipio Caroní del estado Bolívar.', '2', '10', '738538.19', '5', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('382', 'Rehabilitación y Mejoras de la E.B.N 25 de Marzo, parroquia 11 de Abril, municipio Caroní, estado Bolívar.', '2', '55', '600000.00', '3', '1', '3', '100', '2', '2008', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('383', 'Reacondicionamiento de la U.E.B. Gral. Francisco Conde, municipio Caroní, estado Bolívar.', '2', '55', '568891.93', '3', '1', '3', '100', '2', '2008', '-', '2017-01-09 16:48:25', '2017-01-09 16:48:25', null);
INSERT INTO `proyectos` VALUES ('384', 'Rehabilitación y Mejoras de la Unidad Educativa Bella Vista, parroquia 11 de Abril, municipio Caroní, estado Bolívar.', '2', '55', '500000.00', '3', '1', '3', '100', '2', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('385', 'Electrificación de la manzana 3 de la UD-128, parroquia Once de Abril, municipio Caroní, estado Bolívar.', '2', '55', '468737.87', '5', '1', '3', '100', '2', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('386', 'Rehabilitación de la Plataforma Comercial del Aeropuerto Internacional \"Manuel Carlos Piar\", Ciudad Guayana, municipio Caroní del estado Bolívar.', '2', '10', '453698.59', '5', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('387', 'Remodelación C.E.I. Los Arenales, sector Los Arenales, San Félix, estado Bolívar.', '2', '31', '439886.57', '3', '1', '3', '100', '3', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('388', 'Ampliación de Unidad Educativa San José De Cacahual, parroquia 11 de Abril, municipio Caroní. ', '2', '55', '400000.00', '3', '1', '3', '100', '2', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('389', 'Construcción del Tendido Eléctrico en el sector 23 de Enero parroquia Chirica, municipio Caroní, estado Bolívar.', '2', '12', '400000.00', '5', '1', '2', '100', '2', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('390', 'Ampliación y Mejoras de la Red Eléctrica de las manzanas 13 y 18 del sector UD-128, San Félix, municipio Caroní, estado Bolívar.', '2', '55', '339764.14', '5', '1', '3', '100', '2', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('391', 'Ampliación y Mejoras del Servicio de Energía Eléctrica en la manzana 20 de la UD-128, parroquia 11 de Abril, municipio Caroní, estado Bolívar. ', '2', '55', '270000.00', '5', '1', '2', '100', '2', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('392', 'Ampliación y Mejoras del Servicio de Energía Eléctrica en la manzana 26 de la UD-128, parroquia 11 de Abril, municipio Caroní, estado Bolívar. ', '2', '55', '230000.00', '5', '1', '2', '100', '2', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('393', 'Rehabilitación del Taxi Way del Aeropuerto Internacional \"Manuel Carlos Piar\", Ciudad Guayana, municipio Caroní del estado Bolívar.', '2', '10', '228536.47', '5', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('394', 'Rehabilitación  de la U.E.N. Lloyd Petterson, parroquia 11 de Abril, municipio Caroní, estado Bolívar.', '2', '55', '218389.09', '3', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('395', 'Mejoras y Ampliación de Red de Distribución Eléctrica en Baja y Alta Tensión en el sector El Porvenir de la UD-140 parroquia Once de Abril municipio Caroní.', '2', '55', '103887.59', '5', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('396', 'Iluminación a Comunidades del  municipio Caroní del estado Bolívar.', '2', '55', '99987.86', '5', '1', '3', '100', '2', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('397', 'Equipamiento Tecnológico de la Dirección de Proyecto de la Gobernación del estado Bolívar.', '2', '18', '60000.00', '8', '1', '3', '100', '4', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('398', 'Mejoramiento del Servicio Eléctrico de las manzanas 25 y 39 de la UD-128, parroquia 11 de Abril, del municipio Caroní, estado Bolívar.', '2', '55', '30514.00', '5', '1', '3', '100', '2', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('399', 'Reparación y Mejoras en la Escuela Dr. José María Vargas sector Cambalache, municipio Caroní, estado Bolívar (cocina de cocina tipo II).', '2', '10', '200000.00', '3', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('400', 'Bacheo y Mejoras de la Vialidad del Barrio Guaiparito, parroquia Dalla Costa, Ciudad Guayana, municipio Caroní, estado Bolívar.', '2', '31', '40931.04', '2', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('401', 'Instalación de Sistema de Abordaje del Aeropuerto Manuel Carlos Piar, Ciudad Guayana, municipio Caroní, estado Bolívar.', '2', '10', '5825354.80', '5', '1', '3', '100', '3', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('402', 'Rehabilitación de la E.B.E. Simón Rodríguez, Ubicada en el Asentamiento Campesino 19 de Abril, parroquia Pozo Verde, municipio Caroní, estado Bolívar. ', '2', '16', '550000.00', '3', '1', '2', '100', '2', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('403', 'Construcción de Sede Mundo Sonrisa, ubicado en la UD-245 municipio Caroní, estado Bolívar.', '2', '18', '10933303.35', '6', '1', '3', '100', '3', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('404', 'Construcción de 120 Viviendas de 70 m2 en las manzanas 05, 12, 25 y 32 de la UD-128, parroquia 11 de Abril, San Félix, municipio Caroní, estado Bolívar.', '2', '55', '13979711.87', '5', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('405', 'Remodelación de la Sede de la UNEG en Caicara del Orinoco, municipio Cedeño, estado Bolívar.', '4', '30', '467724.58', '3', '1', '3', '100', '2', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('406', 'Acondicionamiento y Mejoras de la Comisaria No. 22, parroquia Simón Bolívar, municipio Caroní del estado Bolívar.', '2', '13', '850000.00', '9', '1', '2', '100', '2', '2010', '2010-0054', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('407', 'Dotación de Escuelas Bolivarianas en el estado Bolívar.', '12', '53', '2700000.00', '3', '1', '3', '100', '4', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('408', 'Rehabilitación de Sede CICPC Caroní, Impermeabilización de Techo, municipio Caroní, estado Bolívar.', '2', '31', '400000.00', '9', '1', '2', '100', '2', '2010', '2010-0095', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('409', 'Continuación de la Rehabilitación de la Troncal 19 del estado Bolívar.', '12', '53', '4050361.34', '2', '1', '2', '100', '2', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('410', 'Continuación de la Rehabilitación de la Troncal 16 del estado Bolívar.', '12', '53', '4050361.34', '2', '1', '2', '100', '2', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('411', 'Planta de Asfalto en el municipio Caroní del estado Bolívar.', '2', '53', '4800000.00', '2', '1', '2', '100', '4', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('412', 'Construcción de Viviendas SUVI.  ', '12', '53', '4946103.38', '5', '1', '3', '100', '3', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('413', 'Plan de Asfalto en Distintos municipios del estado Bolívar.', '12', '53', '5000000.00', '2', '1', '3', '100', '2', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('414', 'Recuperación de Canchas y Parques en el estado Bolívar.', '12', '53', '1200000.00', '5', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('415', 'Adquisición de Teatro Móvil para el estado Bolívar.', '12', '53', '1000000.00', '3', '1', '3', '100', '4', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('416', 'Rehabilitación de las Instalaciones del Internado “Agua Salada”, Ubicado en Ciudad Bolívar, municipio Heres del estado Bolívar.', '1', '5', '85000.00', '9', '1', '1', '100', '2', '2011', '2012-0002', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('417', 'Dotación de Escuelas en el estado Bolívar.', '12', '53', '654012.53', '5', '1', '2', '100', '4', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('418', 'Dotación de Escuelas en el estado Bolívar.', '12', '53', '645987.47', '5', '1', '3', '100', '4', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('419', 'Centro de Atención Integral Mundo de Sonrisas (dotación).', '12', '53', '573443.55', '3', '1', '3', '100', '3', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('420', 'Adquisición de Dos (02) Autobuses para Traslados de Discapacitados. Fundación del Niño.', '12', '53', '400000.00', '6', '1', '3', '100', '4', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('421', 'Adquisición de Un (01) Camiones 350.', '12', '53', '170000.00', '5', '1', '2', '100', '4', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('422', 'Adquisición de Vans para el Fortalecimiento de la Misión Sucre.', '12', '53', '150000.00', '3', '1', '3', '100', '4', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('423', 'Adquisición de Low Boy con Camión para la Gobernación del estado Bolívar.', '12', '53', '79590.38', '5', '1', '2', '100', '4', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('424', 'Acondicionamiento de las Dependencias de la Secretaría de Participación Popular (sede los sabanales y la sabanita).', '12', '53', '41000.00', '5', '1', '2', '100', '2', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('425', 'Rehabilitación de Centros de Coordinación Policial del estado Bolívar.', '12', '53', '2000000.00', '9', '1', '1', '100', '2', '2013', '2013-0102', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('426', 'Rehabilitación de Centros de Coordinación Policial Del Estado Bolívar.', '12', '53', '4000000.00', '9', '1', '1', '100', '2', '2014', '2014-0002', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('427', 'Asfaltado en los municipios Piar, Heres, Caroní y Angostura Bolivariana del estado Bolívar.', '12', '53', '15606251.58', '2', '1', '2', '100', '2', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('428', 'Adquisición de  Doscientos (200) autobuses.', '12', '53', '45000000.00', '6', '1', '2', '100', '4', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('429', 'Conclusión de la Construcción del Terminal Aéreo de Santa Elena de Uairen, municipio Gran Sabana del estado Bolívar.', '9', '47', '5127924.56', '5', '1', '3', '100', '3', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('430', 'Mejoramiento de la Vía a Maurak en Santa Elena de Uairen, municipio Gran Sabana, estado Bolívar', '9', '47', '272717.68', '2', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('431', 'Conformación, Relleno, Acondicionamiento de Parcela y Construcción de Vivienda Unifamiliar en la Comunidad de Wara, Santa Elena de Uairen, municipio Gran Sabana del estado Bolívar.', '9', '47', '100042.48', '5', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('432', 'Inspección Técnica de la Obra Construcción de Urbanismo y Viviendas de 70m2  en los sectores: Los Pinos (60 viviendas) y la Calle Constituyente (20 viviendas) de Santa Elena de Uairen, municipio Gran Sabana, estado Bolívar.', '9', '47', '374142.78', '5', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('433', 'Rehabilitación y Mejoras del Casco Histórico del estado Bolívar.', '1', '1', '2000000.00', '5', '1', '2', '100', '11', '2008', '-', '2017-01-09 16:48:26', '2017-01-09 16:48:26', null);
INSERT INTO `proyectos` VALUES ('434', 'Dotación del Hospital Ruiz y Páez, Ciudad Bolívar municipio Heres del estado Bolívar.', '1', '1', '3000000.00', '4', '1', '2', '100', '4', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('435', 'Construcción de la Segunda Etapa  del Centro de Atención Integral Mundo De Sonrisas, Municipio Heres, estado Bolívar.', '1', '1', '4601128.00', '6', '1', '3', '100', '3', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('436', 'Culminación del Centro de Alta Tecnología Vista Hermosa - Ciudad Bolívar.', '1', '2', '1200000.00', '4', '1', '3', '100', '3', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('437', 'Adquisición de Tres (03) Unidades de Transporte Universitario para la Universidad Nacional Experimental Simón Rodríguez Núcleo, Ciudad Bolívar, estado Bolívar', '1', '5', '900000.00', '3', '1', '3', '100', '4', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('438', 'Culminación del Urbanismo para el Desarrollo Habitacional Ciudad Angostura, sector Los Próceres municipio Heres, estado Bolívar.', '1', '5', '686000.00', '5', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('439', 'Adquisición de un Equipo de Ecocardiograma para el Servicio de Hemodinamia del Hospital Ruiz y Páez.', '1', '1', '300000.00', '4', '1', '3', '100', '4', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('440', 'Continuación de la Consolidación del barrio 4 de Febrero municipio Heres del estado Bolívar. ', '1', '5', '200000.00', '5', '1', '3', '100', '2', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('441', 'Adquisición   del Banco de Transformadores  para el Jardín Botánico del municipio Heres y Materiales Eléctricos  para varios sectores del Municipio Caroní, estado Bolívar.', '1', '1', '170000.00', '5', '1', '3', '100', '4', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('442', 'Adquisición e Instalación de una Caldera de 150 hp para Sistema de Vapor del Hospital Ruiz y Páez, municipio Heres, estado Bolívar.', '1', '1', '29856.58', '4', '1', '3', '100', '4', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('443', 'Consolidación del barrio 4 de Febrero en municipio Heres del estado Bolívar.', '1', '5', '5562000.00', '5', '1', '3', '100', '2', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('444', 'Construcción de una Vivienda en el municipio Padre Pedro Chien, estado Bolívar.', '11', '51', '110196.09', '5', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('445', 'Culminación de la Construcción de Urbanismo para 30 Viviendas y Construcción de 30 Viviendas de 62 m2 para el Desarrollo Habitacional El Manteco, municipio Piar del estado Bolívar.', '3', '54', '950000.00', '5', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('446', 'Construcción de Escuela El Corozo, sector El Corozo, Vía El Pao, parroquia Andrés Eloy Blanco, municipio Piar, estado Bolívar (complemento).', '3', '22', '448139.77', '3', '1', '3', '100', '3', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('447', 'Culminación de la Construcción de Once Viviendas Unifamiliares en el municipio Piar del estado Bolívar.', '3', '54', '89397.65', '5', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('448', 'Dotación del Centro Lácteo de Upata, municipio Piar, estado Bolívar.', '3', '54', '1800000.00', '7', '1', '3', '100', '4', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('449', 'Construcción del Complejo Guasipati, II Etapa, municipio Roscio, estado Bolívar.', '8', '44', '400000.00', '5', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('450', 'Conclusión de la Construcción del Tendido Eléctrico en San Miguel de Betania, municipio Gran Sabana, estado Bolívar.', '9', '48', '2000000.00', '5', '1', '3', '100', '3', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('451', 'Adquisición de Dos Camiones Compactadores de Basura y un Camión de Plataforma para el municipio Sifontes del estado Bolívar.', '5', '53', '1009000.00', '8', '1', '3', '100', '4', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('452', 'Culminación de Construcción de 27 Viviendas en el municipio Autónomo Sifontes, estado Bolívar.', '5', '53', '630000.00', '5', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('453', 'Culminación de la Construcción de Urbanismo y 40 Viviendas de 62 m2, para el Desarrollo Habitacional Guarataro, municipio Autónomo Sucre, estado Bolívar.', '7', '40', '1023000.00', '5', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('454', 'Culminación de la Construcción para 40 Viviendas de 62 m2 para el Desarrollo Habitacional Guarataro, municipio Autónomo Sucre, estado Bolívar.', '7', '40', '500000.00', '5', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('455', 'Rehabilitación y Mejoras de Centros Educativos del municipio Caroní estado Bolívar.', '2', '16', '400000.00', '3', '1', '2', '100', '2', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('456', 'Iluminación Áreas Adyacentes al CTE Cachamay  y Distintos sectores de Ciudad Guayana, municipio Caroní del estado Bolívar.', '2', '11', '17559.73', '5', '1', '2', '100', '2', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('457', 'Asfaltado del Barrio 24 de Abril, sector Brisas del Orinoco, parroquia La Sabanita, municipio Heres,  estado Bolívar.', '1', '6', '699392.60', '2', '1', '3', '100', '3', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('458', 'Electrificación y Alumbrado Público del Barrio 24 de Abril, sector Brisas del Orinoco, parroquia La Sabanita, municipio Heres, estado Bolívar.', '1', '6', '480676.09', '5', '1', '3', '100', '3', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('459', 'Rehabilitación de Infraestructuras de Centros de Coordinación Policial del estado Bolívar.', '12', '53', '10000000.00', '9', '1', '1', '100', '2', '2015', '2015-0004', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('460', 'Centro de Atención Integral para Discapacitados con Problemas Visuales, municipio Caroní, estado Bolívar.', '2', '13', '1194626.94', '6', '1', '3', '100', '3', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('461', 'Adquisición de Ambulancias Tipo II para el Servicio Autónomo de Emergencias Bolívar 171 en Apoyo a Diferentes municipios del estado Bolívar.', '12', '53', '2826127.79', '9', '1', '2', '100', '15', '2005', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('462', 'Construcción Red Eléctrica La Galdera, parroquia Panapana, municipio Heres, estado Bolívar.', '1', '8', '727190.76', '5', '1', '2', '100', '3', '2008', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('463', 'Rehabilitación de Alcantarillas en el estado Bolívar.', '12', '53', '10000000.00', '5', '1', '2', '100', '3', '2009', '2009-0018', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('464', 'Adquisición de Cuatro (4) Retroexcavadoras, Cuatro (4) Camiones 350 y Un (01) Camión con Brazo Pigman.', '12', '53', '2400000.00', '5', '1', '2', '100', '4', '2009', '2009-0016', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('465', 'Construcción de Red de Aguas Negras para las Calles 5, 6, 13, 19, 20 y 21 en el Tramo que se encuentra por debajo del Colector Principal del sector 4 de Febrero municipio Heres, Ciudad Bolívar, estado Bolívar.', '1', '5', '1487123.25', '1', '1', '3', '100', '2', '2009', '2009-0021', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('466', 'Limpieza Mecánica de la Aducción Tocomita - Ciudad Bolívar, municipio Heres, estado Bolívar.', '1', '4', '1142407.64', '1', '1', '2', '100', '1', '2009', '2009-0014', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('467', 'Construcción de Isla Central en Avenida Bolívar del sector 4 de Febrero, municipio Heres, Ciudad Bolívar, estado Bolívar.', '1', '5', '837675.41', '5', '1', '3', '100', '2', '2009', '2009-0019', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('468', 'Prolongación de Red de Aguas Negras de 4 de Febrero, municipio Heres, Ciudad Bolívar, estado Bolívar.', '1', '5', '333350.00', '5', '1', '3', '100', '2', '2009', '2009-0020', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('469', 'Construcción de Canal de Aguas de Lluvias Ubicado entre las Calles 2 y 3 del sector 4 de Febrero municipio Heres, Ciudad Bolívar, estado Bolívar.', '1', '5', '99000.00', '5', '1', '3', '100', '2', '2009', '2009-0022', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('470', 'Continuación de Asfaltado para Todos los municipios del estado Bolívar.', '12', '53', '35000000.00', '2', '1', '3', '100', '2', '2009', '2009-0044', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('471', 'Adquisición de Unidades de Ambulancia para Fortalecer el Sistema de Salud Integral del estado Bolívar.', '12', '53', '10000000.00', '4', '1', '2', '100', '4', '2009', '2009-0045', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('472', 'Dotación de Unidades Vehiculares para el Instituto Autónomo Policía del Estado.', '12', '53', '4577655.15', '9', '1', '2', '100', '4', '2005', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('473', 'Construcción de las Matrices de Distribución del Sistema de Acueducto del sector Sur Oeste de Puerto Ordaz – estado Bolívar (I Etapa).', '2', '10', '7000000.00', '1', '1', '2', '100', '3', '2009', '2009-0033', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('474', 'Dotación de Equipos de Radiocomunicaciones Banda Ancha vhf para el Instituto Autónomo de Policía del estado Bolívar.', '12', '53', '4924950.08', '9', '1', '2', '100', '4', '2005', '-', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('475', 'Rehabilitación de La Villa Olímpica La Paragua.', '1', '2', '5000000.00', '5', '1', '2', '100', '2', '2009', '2009-0050', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('476', 'Instalación de Redes en el Acueducto Guarataro, municipio Sucre, estado Bolívar (Culminación del Nuevo Acueducto Guarataro).', '7', '40', '4000000.00', '1', '1', '3', '100', '3', '2009', '2010-0035', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('477', 'Desarrollo Piscícola en los municipios Heres y Caroní.', '12', '53', '3675121.00', '7', '1', '3', '100', '16', '2009', '2009-0046', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('478', 'Culminación de la Rehabilitación de la Planta Potabilizadora Tocomita (2.100 LPS), municipio Heres, estado Bolívar.', '1', '4', '3558000.00', '1', '1', '2', '100', '3', '2009', '2009-0032', '2017-01-09 16:48:27', '2017-01-09 16:48:27', null);
INSERT INTO `proyectos` VALUES ('479', 'Dotación del Parque Automotor para Mejorar el Sistema de la Dirección del Servicio Autónomo de Emergencias Bolívar 171 en el estado Bolívar.', '12', '53', '2381699.90', '9', '1', '2', '100', '4', '2006', '-', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('480', 'Adquisición de Maquinaria Pesada para el municipio Cedeño.', '4', '30', '3000000.00', '5', '1', '2', '100', '4', '2009', '2009-0038', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('481', 'Adquisición de 10 Camiones Volteo.  ', '12', '53', '3000000.00', '5', '1', '3', '100', '4', '2009', '2009-0043', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('482', 'Culminación del Centro de Refugio y Pernocta para Gandoleros del Peaje Matanza del estado Bolívar.', '2', '10', '2000000.00', '7', '1', '2', '100', '2', '2009', '2009-0051', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('483', 'Fortalecimiento de la Unidad de Protección Vial del estado Bolívar (tránsito terrestre).', '12', '53', '2957133.62', '9', '1', '2', '100', '4', '2006', '-', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('484', 'Ampliación y Mejoras de la U.E. Bolivariana Las Bonitas, sector Las Bonitas, parroquia Altagracia, municipio Cedeño, estado Bolívar.', '4', '24', '1000000.00', '3', '1', '2', '100', '2', '2009', '2009-0039-2', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('485', 'Unidad de Producción  Agrícola (Invernadero).', '1', '1', '1000000.00', '7', '1', '2', '100', '4', '2009', '2009-0041', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('486', 'Rehabilitación  de Infraestructura y Reparaciones Eléctricas en la parroquia Altagracia, municipio Cedeño, estado Bolívar.', '4', '24', '828462.33', '2', '1', '2', '100', '4', '2009', '2010-0073', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('487', 'Complemento para la Adecuación del Centro Piloto de Producción Agrícola Bajo Ambiente Controlado, Ubicado en el Jardín Botánico en Cdad. Bolívar, municipio Heres, estado Bolívar.', '1', '1', '95537.67', '7', '1', '2', '100', '2', '2009', '2010-0074 ', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('488', 'Mejoras del Sistema de Distribución de Energía Eléctrica de la Calle 4 de Febrero del sector 3 de Villa del Sur (Villa Bahía), parroquia Unare, municipio Caroní, estado Bolívar.', '2', '10', '57724.91', '5', '1', '2', '100', '2', '2009', '2010-0075', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('489', 'Ampliación del Liceo Bolivariano Juan Germán Roscio, municipio Roscio, Estado Bolívar.', '8', '44', '700000.00', '3', '1', '2', '100', '2', '2009', '2009-0056', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('490', 'Sistema de Ultrasonido de Alta Definición para Imágenes Cardiovasculares.', '1', '53', '632000.00', '4', '1', '2', '100', '4', '2009', '2009-0053', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('491', 'Complemento para la Construcción de 1.400mts para Agua Potable, parroquia Marhuanta, municipio Heres, estado Bolívar.', '1', '4', '600762.44', '1', '1', '3', '100', '1', '2009', '2010-0071', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('492', 'Dotación de Comisarías y Subcomisarias en todos los  municipios del estado Bolívar.', '12', '53', '2000000.00', '9', '1', '2', '100', '2', '2006', '-', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('493', 'Dotación de Uniforme para el Personal de la Comandancia General de la Policía del estado Bolívar.', '12', '53', '3111469.26', '9', '1', '2', '100', '4', '2006', '-', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('494', 'Adquisición de 50 Autobuses para el estado Bolívar.', '12', '53', '13500000.00', '6', '1', '2', '100', '4', '2009', '2009-0095', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('495', 'Recuperación y Rehabilitación de Alcantarillas en el estado Bolívar. ', '12', '53', '10000000.00', '5', '1', '3', '100', '3', '2009', '2009-0090', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('496', 'Plan de Asfaltado para el estado Bolívar.  ', '12', '53', '10000000.00', '2', '1', '2', '100', '2', '2009', '2009-0096', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('497', 'Dotación de Unidades Vehiculares para la Policía del estado Bolívar.', '12', '53', '7048674.83', '9', '1', '2', '100', '4', '2006', '-', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('498', 'Adquisición de Vehículos para la Gobernación del estado Bolívar. ', '12', '53', '4000000.00', '8', '1', '2', '100', '4', '2009', '2009-0094', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('499', 'Adquisición de Maquinarias  y Equipos para Construcción de Viviendas e Infraestructuras  del estado Bolívar. ', '12', '53', '3500000.00', '5', '1', '2', '100', '4', '2009', '2009-0092', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('500', 'Dotación e Instalación de Aire Acondicionado para la Universidad  Bolivariana de Venezuela en el municipio Heres.', '1', '1', '3000000.00', '3', '1', '3', '100', '4', '2009', '2009-0062', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('501', 'Adquisición de Transformadores, Materiales Eléctricos y Partes, para Cubrir Distintas Necesidades en el estado Bolívar.  ', '12', '53', '3000000.00', '5', '1', '2', '100', '2', '2009', '2009-0087', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('502', 'Adquisición de Unidades de Transporte para Fortalecer el Sistema de Suministro de Alimentos Mercal a las Comunidades del estado Bolívar.  ', '12', '53', '3000000.00', '8', '1', '3', '100', '4', '2009', '2009-0093', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('503', 'Recuperación del Casco Histórico, Ciudad Bolívar, municipio Heres.', '1', '1', '2000000.00', '5', '1', '2', '100', '11', '2009', '2009-0063', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('504', 'Rehabilitación de Escuelas en los municipios del estado Bolívar.', '12', '53', '1500000.00', '3', '1', '2', '100', '2', '2009', '2009-0110', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('505', 'Proyecto de Adquisición de Unidades Vehiculares para Protección Civil del estado Bolívar.', '12', '53', '2697623.92', '9', '1', '2', '100', '4', '2006', '-', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('506', 'Adquisición de Maquinarias (Payloader, Retroexcavadora y Camión Volteo) para el municipio Roscio.', '8', '44', '1383320.00', '2', '1', '2', '100', '4', '2009', '2009-0075', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('507', 'Tuberías de Aducción de Agua Potable para la Población de Tocomita - municipio Bolivariano Angostura del estado Bolívar.', '6', '36', '1200000.00', '1', '1', '3', '100', '1', '2009', '2009-0072', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('508', 'II Etapa y Consolidación del Servicio de Energía Eléctrica y Alumbrado Público, de las Mz. 13 y 18, de la UD-128 parroquia 11 de Abril, municipio Caroní, estado Bolívar. ', '2', '55', '1048785.94', '5', '1', '2', '100', '2', '2009', '2009-0065', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('509', 'Culminación de las Unidades Educativas Lucila Palacios, Jusepa, Simón Bolívar, Rómulo Gallegos, municipio Cedeño.', '4', '30', '1000000.00', '3', '1', '3', '100', '3', '2009', '2009-0078', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('510', 'Rehabilitación Anfiteatro Parque Bicentenario, Upata, municipio Piar - estado Bolívar.', '3', '54', '800000.00', '6', '1', '2', '100', '2', '2009', '2009-0071', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('511', 'Construcción de Muro de Contención de la Unidad Educativa Nacional Horacio Cabrera, municipio Sifontes, estado Bolívar.', '5', '52', '800000.00', '3', '1', '3', '100', '2', '2009', '2009-0101', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('512', 'Ampliación y Mejoras del Liceo Monseñor Zabaleta, municipio Sifontes, estado Bolívar.', '5', '52', '800000.00', '3', '1', '3', '100', '2', '2009', '2009-0102', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('513', 'Rehabilitación de Casa en el Casco Histórico Ubicado en la calle Maracay, municipio Heres, estado Bolívar.', '1', '1', '800000.00', '5', '1', '2', '100', '11', '2009', '2009-0103', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('514', 'Ampliación de la U.E.B. Brisas del Orinoco, UD-128, parroquia 11 de abril, municipio Caroní del estado Bolívar. ', '2', '55', '751330.35', '3', '1', '3', '100', '2', '2009', '2009-0064', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('515', 'Complemento para la Construcción de 1.400 mts de para Agua Potable, parroquia Marhuanta, municipio Heres. ', '1', '4', '700000.00', '1', '1', '3', '100', '1', '2009', '2010-0072', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('516', 'Ampliación de la U.E.B. Imataca, sector la Paragua, municipio Bolívariano Angostura, estado Bolívar. ', '6', '36', '699518.72', '3', '1', '3', '100', '2', '2009', '2009-0073', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('517', 'Construcción de Planta Compacta para Potabilización de Agua, con Capacidad de 40 LPS, sector UD-128, municipio Caroní, estado Bolívar.', '2', '55', '690198.95', '1', '1', '3', '100', '3', '2009', '2010-0034', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('518', 'Rehabilitación del Sistema Eléctrico de Santa Elena de Uairen, municipio Gran Sabana.', '9', '47', '680493.18', '5', '1', '2', '100', '2', '2009', '2009-0086', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('519', 'Adquisición de Planta Diesel para Generación Eléctrica para la Planta Potabilizadora de Agua en la Paragua, municipio Bolivariano Angostura. ', '6', '35', '600000.00', '1', '1', '3', '100', '1', '2009', '2009-0074', '2017-01-09 16:48:28', '2017-01-09 16:48:28', null);
INSERT INTO `proyectos` VALUES ('520', 'Unidades de Transporte para el Personal de la Policía del estado Bolívar.', '12', '53', '294000.00', '9', '1', '2', '100', '4', '2006', '-', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('521', 'Fabricación del Sistema Integral de Potabilización Bolivariano, S.I.P.B., municipio Heres, estado Bolívar.', '1', '53', '509801.05', '1', '1', '3', '100', '1', '2009', '2010-0033', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('522', 'Consolidación de la II Etapa del Sistema de Aguas Servidas en la Avenida José Gregorio Hernández, sector Los Próceres, municipio Heres.', '1', '2', '495185.02', '1', '1', '3', '100', '2', '2009', '2012-0139', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('523', 'Rehabilitación (Baños y Electricidad) de la U.E.N. Antonio de Berrios, municipio Caroní.', '2', '13', '500000.00', '3', '1', '3', '100', '2', '2009', '2009-0067', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('524', 'Rehabilitación y Mejoras (Techo y Cercado Perimetral) de la Escuela Integral Bolivariana Hipódromo Viejo, Barrio Shell, parroquia Catedral, municipio Heres. ', '1', '1', '496648.86', '3', '1', '3', '100', '2', '2009', '2009-0061', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('525', 'Ampliación del C.E.I.B. Guaricongo, parroquia José Antonio Páez, municipio Heres del Estado Bolívar', '1', '3', '482242.11', '3', '1', '2', '100', '2', '2009', '2009-0106', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('526', 'Adquisición de un Camión Cisterna para el municipio El Callao.', '10', '49', '477400.00', '8', '1', '2', '100', '4', '2009', '2009-0105', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('527', 'Dotación de Uniformes para  2600 Funcionarios Policiales del estado Bolívar.', '12', '53', '3170334.63', '9', '1', '2', '100', '4', '2007', '-', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('528', 'Mejoras en el \"Hospital Gervasio Vera Custodio\" Ubicado en la Ciudad de Upata, municipio Piar - estado Bolívar.', '3', '54', '400000.00', '4', '1', '2', '100', '2', '2009', '2009-0069', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('529', 'Adquisición de una Mini Mack Compactador de Basura para el municipio El Callao.', '10', '49', '385000.00', '8', '1', '2', '100', '4', '2009', '2009-0082', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('530', 'Adquisición de Maquinaria (Retroexcavadora) para el municipio El Callao.', '10', '49', '359800.00', '8', '1', '2', '100', '4', '2009', '2009-0081', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('531', 'Adquisición de Dos Camiones con Plataforma para Santa Elena de Uairen, municipio Gran Sabana.', '9', '47', '288800.00', '5', '1', '2', '100', '4', '2009', '2009-0084', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('532', 'Adquisición de una Maquinaria Vibro Compactadora de Fabricación de Adoquines para Santa Elena de Uairen, municipio Gran Sabana.', '9', '47', '230706.82', '5', '1', '2', '100', '4', '2009', '2009-0085', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('533', 'Dotación e Instalación de Unidades de Aire Acondicionado para las Aulas y Oficinas en la Universidad Nacional Experimental Politécnica de la Fuerza Armada Bolivariana, municipio Heres.', '1', '1', '215292.47', '3', '1', '3', '100', '4', '2009', '2009-0057', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('534', 'Adquisición de Dos (2) Plantas Iluminarias para el estado Bolívar.', '12', '53', '210000.00', '5', '1', '2', '100', '4', '2009', '2009-0088', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('535', 'Construcción de Pantallas Separadoras en Floculador y Sedimentador de la Planta de Tratamiento Angostura, Ciudad Bolívar, municipio Heres.', '1', '6', '200000.00', '1', '1', '3', '100', '1', '2009', '2009-0060', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('536', 'Rehabilitación de Filtro de la Planta de Tratamiento de Agua, municipio Sifontes.', '5', '52', '120000.00', '1', '1', '3', '100', '1', '2009', '2009-0079', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('537', 'Recuperación de Alcantarillas en los municipios: Sucre: Sectores Moitaco, Camurica, El Troncón y Los Tres Moriches, Gran Sabana: sectores  Maurack y Sierra de Lema y Piar: sector Los Primos Troncal 10 en el estado Bolívar.', '12', '53', '16000000.00', '5', '1', '2', '100', '3', '2009', '2009-0131', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('538', 'Mejoras y Rehabilitación de la Vialidad en los municipios Cedeño, Sucre, Piar, Roscio, Heres y Caroní en el estado Bolívar.', '12', '53', '13850000.00', '2', '1', '2', '100', '2', '2009', '2009-0140', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('539', 'Asfaltado, Alumbrado, Demarcación y Señalización del Tramo desde la Redoma Ruiz Pineda hasta el Distribuidor La Paragua, municipio Heres del estado Bolívar.', '1', '2', '4476444.43', '2', '1', '3', '100', '2', '2009', '2009-0117', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('540', 'Culminación de la Sede Mundo de Sonrisa, municipio Caroní, estado Bolívar.', '2', '18', '3500000.00', '6', '1', '3', '100', '3', '2009', '2009-0112', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('541', 'Recuperación de las Edificaciones del Velódromo \"La Paragua\", parroquia Vista Hermosa, municipio Heres del estado Bolívar.', '1', '2', '3000000.00', '5', '1', '3', '100', '2', '2009', '2009-0133', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('542', 'Rehabilitación del Liceo Tomas de Heres, municipio Heres, estado Bolívar.', '1', '1', '2400000.00', '3', '1', '3', '100', '2', '2009', '2009-0111', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('543', 'Rehabilitación de la  Escuela Técnica Industrial Robinsoniana (ETIR) Ciudad Bolívar, municipio Heres del estado Bolívar.', '1', '2', '2200000.00', '3', '1', '2', '100', '2', '2009', '2009-0118', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('544', 'Adquisición de Tuberías de PVC para  Redes de Agua  Potable y Aguas Servidas para los municipios: Caroní: sectores Core 8, Las Amazonas, 11 de Abril, La Victoria, Las Malvinas y 25 de Marzo y Heres: sectores Los Próceres y La Orquídea en el estado Bolívar.', '12', '53', '1500000.00', '1', '1', '2', '100', '1', '2009', '2009-0138', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('545', 'Adquisición de Unidades Vehiculares para la Gobernación del estado Bolívar.', '12', '53', '1500000.00', '8', '1', '2', '100', '4', '2009', '2009-0150', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('546', 'Equipamiento de Cuatro Camiones DYNA para la Seguridad y Orden Público del estado Bolívar.', '12', '53', '400000.00', '9', '1', '2', '100', '4', '2007', '-', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('547', 'Adquisición de Camiones para el municipio Sucre del estado Bolívar (Modificación de Proyecto: Planta de Asfalto convenio con Gobernación, municipio Sucre, estado Bolívar).', '7', '53', '1200000.00', '2', '1', '2', '100', '4', '2009', '2009-0152', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('548', 'Culminación de Bacheo y Repavimentación en diversas Calles de la Población del Manteco, municipio Piar, estado Bolívar.', '3', '54', '889999.96', '2', '1', '3', '100', '3', '2009', '2009-0125', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('549', 'Rehabilitación de la Unidad Educación Nuevo Mundo, parroquia 5 de Julio, municipio Caroní, estado Bolívar.', '2', '20', '770000.00', '3', '1', '3', '100', '2', '2009', '2009-0128', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('550', 'Culminación de la Construcción del Centro de Diagnóstico (sin quirófano) Los Olivos, municipio Caroní, estado Bolívar. (Obras Complementarias, Instalaciones Eléctricas e Instalaciones Mecánicas).', '2', '18', '699054.07', '4', '1', '3', '100', '3', '2009', '2009-0126', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('551', 'Culminación de la Rehabilitación del Grupo Escolar Mérida, municipio Heres, estado Bolívar.', '1', '1', '600000.00', '3', '1', '2', '100', '2', '2009', '2009-0115', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('552', 'Culminación del Centro de Rehabilitación Integral La Unidad, UD-111, San Félix, municipio Caroní, estado Bolívar.', '2', '13', '600000.00', '4', '1', '3', '100', '3', '2009', '2009-0122', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('553', 'Culminación del Centro Materno Infantil, municipio Heres, estado Bolívar.', '1', '1', '572860.94', '4', '1', '3', '100', '3', '2009', '2009-0121', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('554', 'Construcción de Nueve (09) Pozos Profundos para los municipios: Caroní: sector Los Brincos, Heres: sectores Mayagua y Cardozo, Sifontes: sectores Los Arenales - Tumeremo y La Maravilla – Santa María y Piar: sectores La Masusa, Las Bombitas y Ojo de Agua en el estado Bolívar.', '12', '53', '560000.00', '1', '1', '2', '100', '1', '2009', '2009-0134', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('555', 'Acondicionamiento del Centro de Salud Virgen del Valle, Ciudad Bolívar, municipio Heres, estado Bolívar.', '1', '1', '527551.92', '4', '1', '3', '100', '3', '2009', '2009-0129', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('556', 'Continuación de Asfaltado, Alumbrado, Demarcación y Señalización del Tramo desde la Redoma Ruiz Pineda hasta el Distribuidor La Paragua, municipio Heres del estado Bolívar.', '1', '2', '523555.57', '2', '1', '2', '100', '2', '2009', '2009-0151', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('557', 'Dotación de Cincuenta (50) Motos DR-650 para el Cuerpo de Policía del estado Bolívar.', '12', '53', '2452500.00', '9', '1', '3', '100', '4', '2008', '-', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('558', 'Adquisición de Una (01) Mezcladora de Concreto (trompo).', '12', '53', '467641.00', '2', '1', '3', '100', '4', '2009', '2009-0139', '2017-01-09 16:48:29', '2017-01-09 16:48:29', null);
INSERT INTO `proyectos` VALUES ('559', 'Rehabilitación de la Sala de Obstetricia del Hospital Ruiz y Páez, municipio Heres, estado Bolívar.', '1', '1', '450000.00', '4', '1', '2', '100', '2', '2009', '2009-0113', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('560', 'Adquisición de Dos (02) Montacargas y Una (01) Transpaleta (zorra) de 2.500 Kg.', '12', '53', '445038.00', '5', '1', '3', '100', '4', '2009', '2010-0031', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('561', 'Rehabilitación y Mejoras de la Escuela Integral Bolivariana Hipódromo Viejo, Barrio Shell, parroquia Catedral, municipio Heres del estado Bolívar (II Etapa).', '1', '1', '400000.00', '3', '1', '2', '100', '2', '2009', '2009-0114', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('562', 'Culminación de la Construcción de Sistema Eléctrico a nivel 34,5 Kv desde San Miguel de Tetania El Dorado, municipio Sifontes, estado Bolívar.', '5', '52', '378275.00', '5', '1', '3', '100', '3', '2009', '2009-0124', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('563', 'Culminación de la Construcción del Centro de Rehabilitación Integral Los Olivos, municipio Caroní, estado Bolívar (obras complementarias, instalaciones eléctricas e instalaciones mecánicas).', '2', '18', '331949.59', '4', '1', '2', '100', '3', '2009', '2009-0120', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('564', 'Culminación de la Construcción de Áreas de Servidores en el Nivel 5, Edificio Sede de Inviobras Bolívar, municipio Caroní, estado Bolívar.', '2', '10', '319168.47', '8', '1', '3', '100', '3', '2009', '2009-0119', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('565', 'Culminación de la Construcción del Liceo Bolivariano en Ciudad Piar, municipio Bolivariano Angostura, estado Bolívar (II Etapa).', '6', '36', '300130.15', '3', '1', '3', '100', '3', '2009', '2009-0123', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('566', 'Adquisición de un (01) Mini Cargador (mini shower).', '12', '53', '238000.00', '5', '1', '3', '100', '4', '2009', '2009-0137', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('567', 'Construcción de Planta Compacta para Potabilización de Agua, Capacidad de 40 LPS, sector UD-128, municipio Caroní, estado Bolívar.', '2', '55', '1000000.00', '1', '1', '3', '100', '1', '2009', '2010-0036', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('568', 'Remodelación del Tramo de Línea de Media Tensión de 13,8 kv de los Circuitos Giraluna y Rio Grande, desde la Subestación Paragua hasta la Entrada de la Comunidad Virgen del Valle, parroquia Vista Hermosa, municipio Heres.', '1', '2', '872454.52', '5', '1', '2', '100', '3', '2009', '2009-0054', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('569', 'Recuperación de la Cancha Deportiva de Usos Múltiples Moreno de Mendoza en San Félix, parroquia Simón Bolívar, municipio Caroní, estado Bolívar.', '2', '13', '203182.36', '3', '1', '3', '100', '3', '2009', '2009-0130', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('570', 'Rehabilitación a Infraestructura ETA Monseñor García Mohedano, Upata, estado Bolívar.', '3', '54', '770000.00', '3', '1', '3', '100', '2', '2010', '2011-0068', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('571', 'Escuela Técnica Agropecuaria Río Grande, sector Río Grande El Palmar, municipio Padre Pedro Chien, estado Bolívar.', '11', '51', '700000.00', '3', '1', '3', '100', '2', '2010', '-', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('572', 'Rehabilitación de la Escuela María Teresa Carreño, parroquia 5 de Julio, municipio Caroní, estado Bolívar', '2', '20', '635298.76', '3', '1', '3', '100', '2', '2010', '-', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('573', 'Complemento Par la Adquisición de Tuberías de PVC para  Redes de Agua  Potable y Aguas Servidas para los municipios: Caroní: sectores Core 8, Las Amazonas, 11 de Abril, La Victoria, Las Malvinas y 25 de Marzo y Heres: sectores Los Próceres y La Orquídea en el estado Bolívar.', '2', '53', '1000000.00', '1', '1', '2', '100', '1', '2010', '2010-0039', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('574', 'Continuación del Plan de Asfaltado para todo el estado Bolívar.', '12', '53', '39983000.00', '2', '1', '2', '100', '2', '2010', '2012-0140', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('575', 'Construcción del Modulo “C” con capacidad para 1.200 Lts. x Seg. para la Planta Potabilizadora de los Alacranes, San Félix, estado Bolívar. ', '2', '13', '6345375.73', '1', '1', '2', '100', '1', '2010', '2010-0005', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('576', 'Dotación de Uniformes para el Cuerpo de Policía del estado Bolívar.', '12', '53', '3170334.63', '9', '1', '3', '100', '4', '2008', '-', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('577', 'Culminación de la sede Mundo de Sonrisa municipio Caroní del Estado Bolívar (Complemento).', '2', '18', '2718238.59', '6', '1', '3', '100', '3', '2010', '2010-0046', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('578', 'Culminación de la Construcción  del Comedor de la UNEG para Fortalecer el sector Universitario en el municipio Heres, estado Bolívar. ', '1', '1', '2500000.00', '3', '1', '2', '100', '2', '2010', '2010-0003', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('579', 'Acondicionamiento e Instalación del Sistema de Bombeo Auxiliar de la Toma Chiripón que Abastece a la Población de Upata, municipio Piar, estado Bolívar. ', '3', '54', '2500000.00', '1', '1', '2', '100', '1', '2010', '2010-0006', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('580', 'Adquisición de Bombas para Acueductos Rurales del estado Bolívar.', '12', '53', '2000000.00', '1', '1', '3', '100', '1', '2010', '2010-0027', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('581', 'Adecuación del Centro Piloto de Producción Agrícola Bajo Ambiente Controlado, Ubicado en el Jardín Botánico de Cdad. Bolívar, municipio Heres, estado Bolívar.', '1', '1', '1800000.00', '7', '1', '3', '100', '2', '2010', '2010-0009', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('582', 'Estación de Bombeo Aguas Servidas en UD-338 sector  Las Amazonas, Puerto Ordaz,  municipio Caroní, estado Bolívar. ', '2', '10', '1700000.00', '1', '1', '3', '100', '1', '2010', '2010-0017', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('583', 'Dotación de Unidades para Transporte de Personal de la Comandancia General de la Policía del estado Bolívar (2 unidades).', '12', '53', '814922.58', '9', '1', '3', '100', '4', '2008', '-', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('584', 'Remodelación de las Oficinas Administrativas de la Planta de Asfalto de Upata, municipio Piar, estado Bolívar', '3', '54', '1200000.00', '2', '1', '3', '100', '2', '2010', '2010-0024', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('585', 'Rehabilitación de Techo de la Villa Olímpica, La Paragua, Cdad. Bolívar, municipio Heres, estado Bolívar. ', '1', '2', '1000000.00', '5', '1', '3', '100', '2', '2010', '2010-0012', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('586', 'Mejoramiento y Ampliación de las Estaciones de Bombeo de Puerto Ordaz, municipio Caroní, estado Bolívar. ', '2', '18', '1000000.00', '1', '1', '3', '100', '1', '2010', '2010-0021', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('587', 'Adquisición de Equipos de Computación para la Gobernación del estado Bolívar.', '12', '53', '600219.20', '8', '1', '3', '100', '4', '2010', '2010-0028', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('588', 'Rehabilitación del Módulo de Asistencia Médica de Manoa, parroquia Simón Bolívar, municipio Caroní, estado Bolívar. ', '2', '13', '500000.00', '4', '1', '3', '100', '2', '2010', '2010-0022', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('589', 'Instalación de Planta Diesel para Generación Eléctrica de la Planta Potabilizadora de Agua La Paragua, municipio Bolivariano  Angostura, estado Bolívar.', '6', '36', '324397.64', '1', '1', '3', '100', '1', '2010', '2011-0077', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('590', 'Ampliación y Mejoras de la Red Eléctrica del sector 23 de Enero, San Félix, municipio Caroní, estado Bolívar. ', '2', '12', '446509.99', '5', '1', '3', '100', '2', '2010', '2010-0020', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('591', 'Equipamiento Integral del Cuerpo de Policía del estado Bolívar.', '12', '53', '5879394.98', '9', '1', '3', '100', '4', '2008', '-', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('592', 'Remodelación de Casa Telmo Almada en el Casco Histórico Ciudad Bolívar, municipio Heres, estado Bolívar.', '1', '1', '389965.19', '5', '1', '3', '100', '11', '2010', '2010-0008', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('593', 'Dotación de Cuarenta (40) Vehículos para el Cuerpo de Policía del estado Bolívar.', '12', '53', '7048992.09', '9', '1', '3', '100', '4', '2008', '-', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('594', 'Requerimiento de Unidades de Transporte para el traslado de la Comunidad Penitenciaria del estado Bolívar (requerimiento de unidad de transporte para el traslado de los niños del preescolar \"mi mundo azul\" donde estudian los hijos de los funcionarios adscritos al cuerpo de policía del estado Bolívar).', '12', '53', '321789.00', '9', '1', '2', '100', '4', '2008', '-', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('595', 'Complemento para la Rehabilitación de la C.E.I.B. Guaricongo, parroquia José Antonio Páez, municipio Heres, estado Bolívar. ', '1', '1', '300000.00', '3', '1', '3', '100', '2', '2010', '2010-0013', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('596', 'Adecuación de Aulas para la UNEG de Cdad. Bolívar, municipio Heres, estado Bolívar. ', '1', '3', '300000.00', '3', '1', '3', '100', '2', '2010', '2010-0014', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('597', 'Dotación de Unidades Policiales Motorizadas, de Diversas Cilindradas, para la Brigada Motorizada de la Policía del estado Bolívar. ', '12', '53', '2423410.08', '9', '1', '2', '100', '4', '2008', '-', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('598', 'Dotación de Unidades Vehiculares para el Cuerpo de Policía del estado Bolívar.', '12', '53', '8500000.00', '9', '1', '2', '100', '4', '2009', '2009-0035', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('599', 'Dotación de Uniformes para el Personal del Cuerpo de Policía del estado Bolívar.', '12', '53', '5000799.01', '9', '1', '2', '100', '4', '2009', '2009-0047', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('600', 'Dotación de Unidades Policiales Motorizadas para el Cuerpo de Policía del estado Bolívar.', '12', '53', '3500000.00', '9', '1', '2', '100', '4', '2009', '2009-0036', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('601', 'Complemento de la Adquisición de Planta Diesel para Generación Eléctrica para la Planta Potabilizadora de Agua en la Paragua, municipio Bolivariano Angostura, estado Bolívar.', '6', '35', '180000.00', '1', '1', '3', '100', '1', '2010', '2010-0025', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('602', 'Dotación de Vitrinas y Enfriadoras para Casas Alimentarias, municipio Piar, estado Bolívar.', '3', '54', '172260.00', '6', '1', '3', '100', '4', '2010', '2010-0023', '2017-01-09 16:48:30', '2017-01-09 16:48:30', null);
INSERT INTO `proyectos` VALUES ('603', 'Rehabilitación de Baño y Canalización de Techo de la U.E. Nicolás Antonio Farrera, municipio El Callao, estado Bolívar.', '10', '49', '147213.50', '3', '1', '3', '100', '2', '2010', '2012-0136', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('604', 'Rehabilitación de Riel de Carga y Descarga de Sulfato de Aluminio en El Almacén de la Planta de Tratamiento Macagua, San Félix, municipio Caroní, estado Bolívar.', '2', '31', '120000.00', '1', '1', '3', '100', '1', '2010', '2011-0073', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('605', 'Rehabilitación del Hospital Ruíz y Páez, municipio Heres del estado Bolívar.', '1', '1', '24800000.00', '4', '1', '2', '100', '4', '2010', '2010-0149', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('606', 'Acondicionamiento de la Unidad de Quemados del Hospital Ruíz y Páez, municipio Heres del estado Bolívar.', '1', '1', '5200000.00', '4', '1', '2', '100', '8', '2010', '2010-0150', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('607', 'Adecuación del Galpón para la Planta Procesadora de Granito (EPSI GRANITOS BOLÍVAR).', '12', '53', '4768652.00', '7', '1', '3', '100', '8', '2010', '2010-0084', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('608', 'Adquisición de Materiales Eléctricos y de Construcción para la Rehabilitación de Centro de Diagnóstico Integral (C.D.I.) y Apoyo a las Misiones en el estado Bolívar. ', '12', '53', '4000000.00', '6', '1', '3', '100', '2', '2010', '2010-0057', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('609', 'Dotación de Antenas, Baterías y Cargadores Múltiples para el Cuerpo de Policía del estado Bolívar.', '12', '53', '1499937.16', '9', '1', '2', '100', '4', '2009', '2009-0048', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('610', 'Adquisición de Tubería Pead para ser Instalada en las Redes de Distribución en Distintos Sectores del estado Bolívar.', '12', '53', '405619.20', '1', '1', '3', '100', '1', '2010', '2011-0075', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('611', 'Acondicionamiento y Dotación de Sillas para la Villa Olímpica La Paragua, municipio Heres del Estado Bolívar.', '1', '2', '1925000.00', '8', '1', '2', '100', '2', '2010', '2010-0064', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('612', 'Adquisición de Bombas para Acueductos Rurales del estado Bolívar.', '12', '53', '1500000.00', '1', '1', '3', '100', '1', '2010', '2010-0123', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('613', 'Culminación de la Unidad Educativa Francisco Javier  Yánez en la Población de La Paragua, municipio Bolivariano Angostura, Estado Bolívar.', '6', '36', '1500000.00', '3', '1', '2', '100', '2', '2010', '2010-0068', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('614', 'Adquisición de dos (02) Montacargas para EPSI Granitos Bolívar.', '12', '53', '1250000.00', '5', '1', '3', '100', '4', '2010', '2010-0087', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('615', 'Adquisición de Camión Cesta, para el estado Bolívar.', '12', '53', '1000000.00', '5', '1', '2', '100', '4', '2010', '2010-0066', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('616', 'Dotación de Equipos Tecnológicos para la División de Educación en Apoyo a la Difusión de las Jornadas Educativas para la Prevención del Delito.', '12', '53', '270257.43', '9', '1', '2', '100', '4', '2009', '2009-0037', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('617', 'Reparación y Mejora de la Casa Jara Colmenares, Ciudad Bolívar,  municipio Heres, estado Bolívar.', '1', '1', '733154.23', '5', '1', '2', '100', '2', '2010', '2010-0051', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('618', 'Complemento para la Culminación de la Escuela Técnica Industrial Robinsoniana (ETIR) Ciudad Bolívar, municipio Heres del estado Bolívar.', '1', '2', '650000.00', '3', '1', '2', '100', '2', '2010', '2010-0062', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('619', 'Ampliación del Sistema de Cámaras de Vigilancia (CIACS 1-7-1) del municipio Caroní.', '2', '53', '1453964.79', '9', '1', '2', '100', '4', '2009', '2009-0068', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('620', 'Adquisición de Tres (03) Camiones Ford 350 Tipo Chasis, para el estado Bolívar.', '12', '53', '580079.67', '5', '1', '3', '100', '4', '2010', '2010-0077', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('621', 'Reacondicionamiento de la Universidad Bolivariana de Venezuela (U.B.V.), municipio Heres, estado Bolívar.', '1', '1', '560000.00', '3', '1', '2', '100', '2', '2010', '2010-0063', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('622', 'Adquisición de Camioneta Pick Up y Camión 350 para EPSI Granitos Bolívar.', '12', '53', '540000.00', '5', '1', '3', '100', '4', '2010', '2010-0086', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('623', 'Construcción de un Galpón para Cría de Pollos en la ETA Monseñor García Mohedano, Upata, edo. Bolívar.', '3', '54', '530000.00', '3', '1', '3', '100', '2', '2010', '2011-0069', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('624', 'Dotación de Equipos de Alojamiento para el Cuerpo de Policía del estado Bolívar.', '12', '53', '524060.00', '9', '1', '2', '100', '4', '2009', '2009-0099', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('625', 'Consolidación del Servicio Eléctrico y Alumbrado Público Manzana 32 de la UD-128, parroquia 11 de Abril, municipio Caroní, estado Bolívar.', '2', '55', '455063.52', '5', '1', '3', '100', '2', '2010', '2010-0078', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('626', 'Construcción de Tendido Eléctrico en el Sector Las Mulas, parroquia Pozo Verde, municipio Caroní del estado Bolívar.', '2', '16', '386664.30', '5', '1', '2', '100', '2', '2010', '2010-0048', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('627', 'Complemento para la Planta de Tratamiento Toro Muerto (Ampliación y Mejoras), Puerto Ordaz, municipio Caroní, estado Bolívar.', '2', '10', '336000.00', '1', '1', '2', '100', '3', '2010', '2010-0059', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('628', 'Dotación de Equipos de Computación para el Cuerpo de Policía del estado Bolívar.', '12', '53', '420000.00', '9', '1', '2', '100', '4', '2009', '2009-0098', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('629', 'Complemento para la Construcción de Viviendas en alto riesgo, municipio Caroní del estado Bolívar', '2', '53', '190000.00', '5', '1', '3', '100', '2', '2010', '2012-0138', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('630', 'Culminación de la Consolidación de la II Etapa del Sistema de Aguas Servidas en la Av. José Gregorio Hernández, sector Los Próceres, municipio Heres del estado Bolívar.', '1', '2', '230000.00', '1', '1', '2', '100', '2', '2010', '2010-0069', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('631', 'Ampliación y Mejoras del Servicio de Energía Eléctrica y Alumbrado Público Manzana 27 de la UD-128, parroquia 11 de Abril, municipio Caroní, estado Bolívar', '2', '55', '220000.00', '5', '1', '3', '100', '2', '2010', '2010-0050', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('632', 'Dotación de Mobiliario para EPSI GRANITOS BOLÍVAR.', '12', '53', '200000.00', '7', '1', '3', '100', '4', '2010', '2010-0085', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('633', 'Complemento para Mejoras y Ampliación a 50 lts/seg de la Planta Potabilizadora La Paragua, municipio Bolivariano Angostura, estado Bolívar.', '6', '36', '197520.54', '1', '1', '3', '100', '3', '2010', '2010-0060', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('634', 'Adquisición de Equipos de Radiocomunicación para Fortalecer las Operaciones de Seguridad Ciudadana en el estado Bolívar.', '12', '53', '1369164.84', '9', '1', '2', '100', '4', '2009', '2009-0017', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('635', 'Ampliación y Mejoras del Servicio de Energía Eléctrica y Alumbrado Público Manzana 29 de la UD-128, parroquia 11 de Abril, municipio Caroní, estado Bolívar.', '2', '55', '169729.53', '5', '1', '3', '100', '2', '2010', '2010-0049', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('636', 'Complemento para la Adquisición de  una Mezcladora de Concreto (Trompo).', '12', '53', '77000.00', '5', '1', '3', '100', '4', '2010', '2010-0067', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('637', 'Complemento para la Adecuación del Galpón para la Planta Procesadora de Granito \"Fase I\" (EPSI Granitos Bolívar).', '12', '53', '1250961.62', '7', '1', '3', '100', '8', '2010', '2010-0112', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('638', 'Mejoras de la Sub-estación La Sabanita (Cambio de Conductor de 266 a 559,5 desde la S/E farallones hasta S/E Sabanita, Construcción de Tramo de Línea desde la S/E Sabanita hasta la Fábrica de Granito (EPSI Granito Bolívar).', '12', '53', '600000.00', '7', '1', '3', '100', '8', '2010', '2010-0113', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('639', 'Adecuación del Galpón para la Planta Procesadora de Granito \"Fase II\" EPSI Granitos Bolívar (Área Administrativa y de Mercadería, Instalaciones Mecánicas y Sistema Contra Incendios).', '12', '53', '2358404.19', '7', '1', '3', '100', '8', '2010', '2010-0114', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('640', 'Recuperación de la Troncal 10 del estado Bolívar.', '12', '53', '28000000.00', '2', '1', '2', '100', '3', '2010', '2010-0097', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('641', 'Adquisición de Maquinarias y Equipos de Infraestructura (Cargador frontal “Payloader”, motoniveladora “Patrol” y Jumbo Volvo).', '12', '53', '4052000.10', '2', '1', '2', '100', '4', '2010', '2010-0099', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('642', 'Rehabilitación y Mejoras de U.E.N. Morales Marcano, sector 19 de Abril, parroquia Upata, municipio Piar, estado Bolívar.', '3', '54', '3216821.28', '3', '1', '3', '100', '2', '2010', '2010-0106', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('643', 'Rehabilitación de Paredes Internas y Externas, Impermeabilización y Mejoras de Estanque “C”, Ubicado en la Avenida Libertador de Ciudad Bolívar, municipio Heres del estado Bolívar. ', '1', '2', '2141473.03', '5', '1', '2', '100', '1', '2010', '2010-0104', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('644', 'Culminación de Rehabilitación de Módulo de Manoa, Impermeabilización de Techo, Limpieza General, Pintura Interna, municipio Caroní, estado Bolívar.', '2', '13', '2000000.00', '4', '1', '3', '100', '2', '2010', '2010-0092', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('645', 'Adquisición de Stock de Repuestos, Herramientas e Insumos para las Tres Plantas de Asfalto Ubicadas en los municipios Heres, Caroní y Piar.', '12', '53', '1686043.72', '5', '1', '2', '100', '4', '2010', '2010-0100', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('646', 'Dotación de Chalecos Internos Antibalas Nivel III A, para el Cuerpo de Policía del estado Bolívar.', '12', '53', '500080.00', '9', '1', '2', '100', '4', '2009', '2009-0104', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('647', 'III Fase Plan de Asfaltado para el estado Bolívar.', '12', '53', '1172792.32', '2', '1', '2', '100', '2', '2010', '2010-0148', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('648', 'Construcción del Comedor Mundo de Sonrisas, UD-245, parroquia Universidad, municipio Caroní, estado Bolívar.', '2', '18', '620807.92', '6', '1', '3', '100', '3', '2010', '2010-0093', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('649', 'Complemento para la Adquisición de un Galpón para la Gobernación del estado Bolívar. ', '12', '53', '610073.85', '8', '1', '3', '100', '4', '2010', '2010-0090', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('650', 'Culminación de la Planta de Tratamiento Toro Muerto (Ampliación y Mejoras), Puerto Ordaz, municipio Caroní, estado Bolívar.', '2', '10', '505520.43', '1', '1', '2', '100', '3', '2010', '2010-0094', '2017-01-09 16:48:31', '2017-01-09 16:48:31', null);
INSERT INTO `proyectos` VALUES ('651', 'Adquisición de un Camión Vactor para el municipio Roscio del estado Bolívar', '8', '44', '500000.00', '8', '1', '2', '100', '4', '2010', '2010-0109', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('652', 'Adquisición de Brazo Pick-man Marca Ferrari Crane Modelo PL 7,2 sobre Camión Chevrolet Modelo FVR 33K.', '12', '53', '466071.43', '5', '1', '2', '100', '4', '2010', '2010-0103', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('653', 'Dotación de Equipos para Protección Civil del estado Bolívar.', '12', '53', '400000.00', '9', '1', '3', '100', '4', '2010', '2010-0030', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('654', 'Adquisición de Dos (02) Plantas Diesel para la Planta de Agua Potable de Guarataro, municipio Sucre del estado Bolívar.', '7', '40', '350000.00', '1', '1', '2', '100', '1', '2010', '2010-0111', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('655', 'Iluminación Externa del Complejo Villa Olímpica La Paragua, Incluyendo Velódromo.', '1', '2', '300000.00', '5', '1', '3', '100', '2', '2010', '2010-0105', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('656', 'Consolidación de Servicio Eléctrico de la Manzana 32 UD-128, parroquia 11 de Abril, municipio Caroní, estado Bolívar (Complemento para la Culminación).', '2', '55', '292000.00', '5', '1', '3', '100', '2', '2010', '2010-0110', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('657', 'Culminación de Muro de Contención de la U.E.N. Horacio Cabrera, municipio Sifontes, estado Bolívar.', '5', '52', '209553.00', '3', '1', '3', '100', '2', '2010', '2012-0137', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('658', 'Culminación de la Rehabilitación de Baño y Canalización de Techo de la U.E. Nicolás Antonio Farrera, municipio El Callao, estado Bolívar.  ', '10', '49', '40000.00', '3', '1', '3', '100', '2', '2010', '2010-0096', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('659', 'Recuperación de la Troncal 19 del estado Bolívar.', '12', '53', '12000000.00', '2', '1', '2', '100', '3', '2010', '2010-0117', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('660', 'III Fase Plan de Asfaltado para el estado Bolívar.', '12', '53', '36000000.00', '2', '1', '2', '100', '2', '2010', '2010-0118', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('661', 'Adquisición de Vehículos para el Fortalecimiento Institucional de la Gobernación del estado Bolívar.', '12', '53', '2000000.00', '8', '1', '2', '100', '4', '2010', '2010-0122', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('662', 'Rehabilitación y Mejoras de la U.E.N. Morales Marcano, sector  19 de Abril, parroquia Upata, municipio Piar, estado Bolívar (Complemento).', '3', '54', '1783178.72', '3', '1', '3', '100', '2', '2010', '2010-0121', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('663', 'Adecuación del Galpón  para la Planta  Procesadora de Granitos (Área de Maquinas), estado Bolívar.', '12', '53', '1146990.99', '7', '1', '3', '100', '8', '2010', '2010-0124', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('664', 'Adquisición de Vehículos para el Fortalecimiento Institucional de la Gobernación del estado Bolívar.', '12', '53', '3000000.00', '8', '1', '3', '100', '4', '2010', '2010-0125', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('665', 'Adquisición de Vehículos para el Fortalecimiento Institucional de la EPSI Granitos Bolívar.', '12', '53', '402000.00', '7', '1', '3', '100', '4', '2010', '2010-0126', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('666', 'Dotación de Mobiliario para EPSI GRANITOS BOLÍVAR (II Etapa).', '12', '53', '108000.00', '7', '1', '3', '100', '4', '2010', '2011-0087', '2017-01-09 16:48:32', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('667', 'Adquisición de Montacarga para EPSI Granitos Bolívar.', '12', '53', '1010969.20', '5', '1', '3', '100', '4', '2010', '2010-0152', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('668', 'Consolidación de Villa Olímpica, La Paragua, Cdad. Bolívar, municipio Heres, estado Bolívar. ', '1', '2', '2730000.00', '5', '1', '3', '100', '2', '2010', '2010-0129', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('669', 'Impermeabilización de Techos y Reparaciones Sanitarios de la ETIR Dalla Costa, parroquia Vista Hermosa, municipio Heres, estado Bolívar.', '1', '2', '1500000.00', '3', '1', '2', '100', '2', '2010', '2010-0136', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('670', 'Recuperación de las Edificaciones del Velódromo \"La Paragua\", parroquia Vista Hermosa, municipio Heres del estado Bolívar (Culminación).', '1', '2', '1400000.00', '5', '1', '3', '100', '2', '2010', '2010-0130', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('671', 'Suministro y Colocación de 450 mts de tubería de 12\". En la Avenida Principal Las Flores, parroquia Agua Salada, municipio Heres, estado Bolívar.', '1', '5', '242000.00', '1', '1', '2', '100', '1', '2010', '2010-0131', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('672', 'III Fase Plan de Asfaltado para el estado Bolívar.', '12', '53', '9397851.54', '2', '1', '3', '100', '2', '2010', '2010-0132', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('673', 'Recuperación de la Troncal 19 del estado Bolívar.', '12', '53', '2000000.00', '2', '1', '2', '100', '3', '2010', '2010-0133', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('674', 'Rehabilitación y Mejoras a Centros de Salud Misión Barrio Adentro II CDI – SRI – estado Bolívar.', '12', '53', '6000000.00', '4', '1', '2', '100', '3', '2010', '2010-0142', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('675', 'Suministro e Instalaciones de Equipos Tecnológicos de Abordaje del estado Bolívar.', '12', '53', '2994306.00', '5', '1', '2', '100', '4', '2010', '2010-0134', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('676', 'Adquisición de Vehículos para el Fortalecimiento Institucional de la Gobernación del estado Bolívar', '12', '53', '1000000.00', '8', '1', '3', '100', '4', '2010', '2010-0127', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('677', 'Automatización de la Planta de Casas Prefabricadas (Construbolívar).', '12', '53', '266392.00', '5', '1', '2', '100', '4', '2010', '2010-0135', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('678', 'Complemento para la Adquisición de Brazo Pick-man Marca Ferrari Crane Modelo PL 7,2 sobre camión Chevrolet Modelo FVR 33K.', '12', '53', '142428.57', '5', '1', '2', '100', '4', '2010', '2010-0120', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('679', 'Culminación  del Centro de Refugio y Pernocta para Gandoleros del Peaje Matanza del estado Bolívar (Complemento).', '2', '10', '1300000.00', '7', '1', '2', '100', '2', '2010', '2010-0137', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('680', 'Complemento para la Adquisición de Equipos de Radio Comunicación para Fortalecer la Operaciones de Seguridad Ciudadana en el estado Bolívar', '12', '53', '286337.34', '9', '1', '2', '100', '4', '2010', '2010-0044', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('681', 'Construcción de Cocina Tipo I Escuela Las Malvinas, parroquia Dalla Costa, municipio Caroní, estado Bolívar.', '2', '31', '273677.92', '3', '1', '2', '100', '3', '2010', '2010-0139', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('682', 'Rehabilitación de Sistema de Eléctrico de la Zona Comercial del CTE Cachamay.', '2', '11', '224757.18', '5', '1', '2', '100', '2', '2010', '2010-0145', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('683', 'Adquisición de Equipos de Computación para la Gobernación del estado Bolívar.', '12', '53', '112418.24', '8', '1', '2', '100', '4', '2010', '2010-0146', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('684', 'Adquisición de Equipos de Computación para la Gobernación del estado Bolívar.', '12', '53', '112264.27', '8', '1', '3', '100', '4', '2010', '2010-0147', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('685', 'Consolidación del Servicio Eléctrico y Alumbrado Público en el sector Rosa Inés, parroquia Simón Bolívar, municipio Caroní del estado Bolívar.', '2', '13', '162376.36', '5', '1', '2', '100', '2', '2010', '2010-0140', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('686', 'Rehabilitación del Alumbrado Público de la Cancha de Usos Múltiples, parroquia Dalla Costa, municipio Caroní del estado Bolívar.', '2', '31', '29551.98', '5', '1', '2', '100', '2', '2010', '2010-0143', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('687', 'Culminación de Ampliación de  U.E.B. Imataca, sector la Paragua, municipio Bolivariano Angostura, estado Bolívar. ', '6', '36', '550000.00', '3', '1', '2', '100', '2', '2010', '2010-0144', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('688', 'Construcción de Viviendas para el estado Bolívar.', '12', '53', '7000000.00', '5', '1', '3', '100', '8', '2010', '-', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('689', 'Construcción de dos (02) Estanques de Almacenamiento de Agua Potable, ubicado en el Cerro El Quemao, sector Oeste de Puerto Ordaz, municipio Caroní, estado Bolívar. ', '2', '10', '2642435.89', '1', '1', '2', '100', '1', '2010', '2010-0004', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('690', 'Construcción de la Estación de Bombeo Principal del Nuevo Acueducto para el sector Oeste de Puerto Ordaz, municipio Caroní, estado Bolívar. ', '2', '10', '2271240.00', '1', '1', '3', '100', '1', '2010', '2010-0016', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('691', 'Mejoras en el Colector Principal del sector Core 8 UD-337 II, parroquia Unare, municipio Caroní, estado Bolívar ', '2', '10', '155602.36', '1', '1', '3', '100', '1', '2010', '2011-0076', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('692', 'Construcción de Planta Compacta Para Potabilización de Agua, con Capacidad de 40 Lps, sector Ud-128, municipio Caroní, estado Bolívar (II Etapa).', '2', '55', '2094380.80', '1', '1', '3', '100', '1', '2010', '2011-0074', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('693', 'Complemento para el Proyecto de Ampliación y Optimización del Sistema de Potabilización de Agua de Puente Blanco, municipio El Callao, Estado Bolívar.', '10', '49', '312908.56', '1', '1', '3', '100', '3', '2010', '2010-0061', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('694', 'Complemento para la Tramitación y Puesta en Servicio del Acueducto Suroeste de Puerto Ordaz, municipio Caroní, estado Bolívar (Convenio suscrito con Hidroven y la C.A.F.)', '2', '10', '4368000.00', '1', '1', '2', '100', '1', '2010', '2010-0091', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('695', 'Rehabilitación del Complejo Cultural Manuel Piar, municipio Piar.', '3', '54', '1000000.00', '5', '1', '2', '100', '17', '2010', '2010-0107', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('696', 'Complemento para la Adquisición de Equipos del Centro Lácteo de Upata, municipio Piar, estado Bolívar.', '3', '54', '656564.07', '7', '1', '3', '100', '4', '2010', '2010-0088', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('697', 'Optimización y Acondicionamiento de las Instalaciones del Centro Lácteo de Upata, municipio Piar, estado Bolívar.', '3', '54', '305180.00', '7', '1', '3', '100', '16', '2010', '2010-0089', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('698', 'Complejo Cultural Manuel Piar, municipio Piar, estado Bolívar.', '3', '54', '3000000.00', '5', '1', '2', '100', '17', '2010', '2011-0071', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('699', 'Construcción de una Planta Piloto para la Adecuación y Utilización del Sulfato de Aluminio Residual, municipio Caroní, estado Bolívar.', '2', '10', '600000.00', '1', '1', '2', '100', '1', '2010', '2010-0138', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('700', 'Rehabilitación del Liceo Bolivariano Manuel Cedeño, municipio Cedeño del estado Bolívar.', '4', '30', '1000000.00', '3', '1', '2', '100', '2', '2010', '2010-0076', '2017-01-09 16:48:32', '2017-01-09 16:48:32', null);
INSERT INTO `proyectos` VALUES ('701', 'Construcción de Sanitarios en la Oficina de Dirección de Proyectos, Secretaría de Planificación y Desarrollo, Upata, municipio Piar, estado Bolívar.', '3', '54', '9740.73', '8', '1', '3', '100', '2', '2010', '2010-0119', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('702', 'Plan de Apoyo a Contingencia por Lluvias.', '12', '53', '7500000.00', '8', '1', '2', '100', '3', '2010', '-', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('703', 'Recuperación de Ambulancias del Parque Automotor 171.', '12', '53', '1015838.00', '8', '1', '3', '100', '4', '2010', '-', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('704', 'Sistema de Transporte Público.', '12', '53', '11137606.00', '6', '1', '2', '100', '4', '2011', '-', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('705', 'Plan  de  Pavimentación  Asfáltica  de Vías de Comunicación del estado Bolívar.', '12', '53', '40000000.00', '2', '1', '1', '100', '2', '2011', '2011-0035', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('706', 'Fortalecimiento del Proceso de Carga, Acarreo y Transporte de Materia Prima Requerida en las Plantas de Asfalto.', '12', '53', '8960021.11', '2', '1', '1', '100', '4', '2011', '2011-0004', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('707', 'Plan  de  Asfaltado  de El Callao, municipio El Callao.', '10', '49', '5000000.00', '2', '1', '1', '100', '2', '2011', '2011-0023', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('708', 'Plan de Asfaltado de Guasipati, municipio Roscio.', '8', '44', '5000000.00', '2', '1', '1', '100', '2', '2011', '2011-0025', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('709', 'Adquisición de Maquinarias para Fortalecimiento de la Secretaría de Mantenimiento y Servicios Generales.', '2', '53', '5000000.00', '5', '1', '1', '100', '4', '2011', '2011-0003', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('710', 'Rehabilitación Infraestructura ETIR El Cambao.', '1', '1', '4000000.00', '3', '1', '1', '100', '2', '2011', '2011-0014', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('711', 'Plan de Asfaltado El Dorado y sector km 88, municipio Sifontes.', '5', '34', '3000000.00', '2', '1', '1', '100', '2', '2011', '2011-0026', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('712', 'Mejoras al Proceso Productivo de las  Plantas de Asfaltos Utilizadas en los Proyectos de Rehabilitación de la Infraestructura Vial del estado Bolívar.', '12', '53', '1325643.82', '2', '1', '1', '100', '4', '2011', '2011-0028', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('713', 'Construcción de Cancha Deportiva Múltiple y Techado de Patio Central E.B.E. La Paragua.  ', '1', '2', '1100000.00', '3', '1', '1', '100', '2', '2011', '2011-0020', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('714', 'Planta de Tratamiento para Aguas Servidas CAT Tumeremo – municipio Sifontes / CAO. 25.000 lts.  ', '5', '52', '1018381.04', '5', '1', '1', '100', '3', '2011', '2011-0012', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('715', 'Construcción de Cocina U.E.E. Pedro Rafael Bucarito.', '1', '2', '1000000.00', '3', '1', '1', '100', '2', '2011', '2011-0021', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('716', 'Adquisición de Vibrocompactadora para el estado Bolívar.', '12', '53', '800000.00', '5', '1', '1', '100', '4', '2011', '2011-0002', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('717', 'Rehabilitación de Complejo Deportivo de Piscina Olímpica, parroquia Catedral, municipio Heres, estado Bolívar.', '1', '1', '600000.00', '3', '1', '1', '100', '2', '2011', '2011-0013', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('718', 'Electrificación para el sector El Puente en la comunidad de Santo Domingo II, sector III, Upata, municipio Piar, estado Bolívar.', '3', '54', '574375.82', '5', '1', '1', '100', '3', '2011', '2011-0011', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('719', 'Rehabilitación de Edificaciones Deportivas de Complejo Cívico  Parque Bolívar, municipio Heres, estado Bolívar.', '1', '2', '400000.00', '5', '1', '1', '100', '2', '2011', '2011-0022', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('720', 'Camión Cava Granja Socialista Avícola en municipio Cedeño.', '4', '53', '350000.00', '7', '1', '1', '100', '4', '2011', '2011-0005', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('721', 'Reparación y Mejora en C.E.I. Negra Isidora, Ubicada en la Sede de SAEB 171, San Félix, municipio Caroní, estado Bolívar.', '2', '31', '146348.63', '3', '1', '1', '100', '3', '2011', '2011-0010', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('722', 'Adquisición de 02 Barrenos.', '12', '53', '60000.00', '5', '1', '1', '100', '4', '2011', '2011-0001', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('723', 'Construcción de Centro de Salud en Los Pijigüaos,  municipio Cedeño.', '4', '56', '7490000.00', '4', '1', '1', '100', '3', '2011', '2012-0096', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('724', 'Centro Integral de Producción Agropecuaria Bolívar.', '12', '53', '7500000.00', '7', '1', '1', '100', '16', '2011', '2010-0009', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('725', 'Construcción de Taller de Mantenimiento y Terminal de Transporte de Transbolívar en Puerto Ordaz  - Transbolívar (I Etapa).', '2', '11', '6922717.29', '5', '1', '1', '100', '3', '2011', '2012-0098', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('726', 'Construcción Liceo Manuel Cedeño en Caicara del Orinoco.', '4', '30', '5397500.00', '3', '1', '1', '100', '3', '2011', '2012-0097', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('727', 'Plan de Asfaltado de Población de Guarataro, municipio Sucre.', '7', '40', '3000000.00', '2', '1', '1', '100', '2', '2011', '2011-0027', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('728', 'Adquisición de Motores y Bombas para Acueductos del estado Bolívar.', '12', '53', '3000000.00', '1', '1', '1', '100', '1', '2011', '2011-0006', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('729', 'Rehabilitación Infraestructura ETAR Luis Morillo Colmenares.  ', '10', '49', '2000000.00', '3', '1', '1', '100', '2', '2011', '2011-0017', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('730', 'Construcción  de  un Pozo Profundo  y Tanque Australiano para la Población del sector Km 88, municipio Sifontes del estado Bolívar.', '5', '52', '250000.00', '1', '1', '1', '100', '1', '2011', '2011-0007', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('731', 'Rehabilitación y Mejoras de la Avenida Expresa I, Angosturita, municipio Caroní.', '2', '10', '27637000.00', '2', '1', '1', '100', '3', '2011', '2011-0058', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('732', 'Rehabilitación y Mejoras de la Avenida Expresa I, Angosturita, municipio Caroní.', '2', '10', '21450000.00', '2', '1', '1', '100', '3', '2011', '2011-0043', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('733', 'Plan de Asfaltado para el edo. Bolívar (Complemento).', '12', '53', '13525329.00', '2', '1', '1', '100', '2', '2011', '2011-0066', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('734', 'Acueducto este de San Félix – municipio Caroní.', '2', '13', '8000000.00', '1', '1', '1', '100', '3', '2011', '2011-0042', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('735', 'Construcción de Viviendas, Manzana B, Villa Caruachi, sector las Amazonas, Parroquia Unare, municipio Caroní, edo. Bolívar (I Etapa).', '2', '10', '5912966.06', '5', '1', '1', '100', '8', '2011', '2011-0039', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('736', 'Plan de Asfaltado para el edo. Bolívar (Complemento).', '12', '53', '4000000.00', '2', '1', '1', '100', '2', '2011', '2011-0039', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('737', 'Construcción de Centro Integral de Servicios a la Comunidad, parroquia Vista Hermosa, municipio Heres, estado Bolívar.', '1', '2', '2500000.00', '5', '1', '1', '100', '2', '2011', '2011-0053', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('738', 'Construcción de Viviendas, sector la Shell, Ciudad Bolívar, municipio Heres, estado Bolívar.', '1', '1', '1787483.00', '5', '1', '1', '100', '8', '2011', '2011-0054', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('739', 'Rehabilitación del Hospital Gervasio Vera Custodio, municipio Piar, edo. Bolívar.', '3', '54', '1750000.00', '4', '1', '1', '100', '2', '2011', '2011-0063', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('740', 'Recuperación de la Pista del Velodromo Perteneciente al Complejo Deportivo Villa Olímpica Ricardo Tulio Maya, parroquia Vista Hermosa, Ciudad Bolívar, estado Bolívar.', '1', '2', '1600000.00', '3', '1', '1', '100', '2', '2011', '2011-0055', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('741', 'Rehabilitación y Mejoras del Ambulatorio de Vista al Sol, parroquia Vista al Sol,  municipio Caroní, estado Bolívar.', '2', '17', '1500000.00', '4', '1', '1', '100', '2', '2011', '2011-0048', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('742', 'Construcción de Centro Integral de Servicios a la Comunidad, municipio Caroní.', '2', '53', '1500000.00', '6', '1', '1', '100', '2', '2011', '2011-0060', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('743', 'Culminación de la Empresa de Producción Social Indirecta (EPSI) GRANITOS BOLIVAR.', '12', '53', '1474671.00', '8', '1', '1', '100', '8', '2011', '2011-0059', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('744', 'Fortalecimiento de la Producción de Material Integral de Minería no Metálica.', '12', '53', '1000000.00', '7', '1', '1', '100', '18', '2011', '2011-0044', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('745', 'Adquisición de Camiones 4x4 para las Plantas de Asfaltos del estado Bolívar.', '12', '53', '942000.00', '8', '1', '1', '100', '4', '2011', '2011-0046', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('746', 'Construcción de Viviendas, en El Palmar, municipio Padre Pedro Chien, estado Bolívar (Complemento).', '11', '51', '786000.00', '5', '1', '1', '100', '8', '2011', '2011-0064', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('747', 'Rehabilitación de Sala de Maquinarias de Complejo de Piscinas para Atletas de Alto Rendimiento, municipio Heres, estado Bolívar.', '1', '1', '728000.00', '3', '1', '1', '100', '2', '2011', '2011-0056', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('748', 'Construcción de By Pass Tanque C, Ciudad Bolívar, municipio Heres del edo. Bolívar.', '1', '2', '700000.00', '1', '1', '1', '100', '1', '2011', '2011-0038', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('749', 'Recuperación de Espacios Deportivos y Recreativos en E.B.E. La Paragua, municipio Heres, estado Bolívar.', '1', '2', '220000.00', '3', '1', '1', '100', '2', '2011', '2011-0057', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('750', 'Rehabilitación de la Edificación de la Casa de los Niños Libertador, municipio Piar, estado Bolívar.', '3', '54', '212800.25', '3', '1', '1', '100', '3', '2011', '2011-0070', '2017-01-09 16:48:33', '2017-01-09 16:48:33', null);
INSERT INTO `proyectos` VALUES ('751', 'Complemento para la Adquisición de 02 Barrenos, para el Edo. Bolívar.', '12', '53', '80000.00', '8', '1', '1', '100', '4', '2011', '2011-0065', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('752', 'Recuperación de la Troncal 19 del estado Bolívar (II Etapa).', '12', '53', '7000000.00', '2', '1', '1', '100', '3', '2011', '2011-0088', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('753', 'Plan de Asfaltado de la Vía Upata - El Pao, municipio Piar del estado Bolívar.', '3', '53', '4981357.80', '2', '1', '1', '100', '3', '2011', '2012-0095', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('754', 'Adquisición de Vehículos para el Desarrollo Socio -Comunitario de la Población del estado Bolívar.', '12', '53', '4779414.84', '8', '1', '1', '100', '4', '2011', '2012-0130', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('755', 'Plan de Asfaltado para el municipio Caroní del estado Bolívar.', '2', '53', '2325000.00', '2', '1', '1', '100', '2', '2011', '2011-0103', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('756', 'Complemento para la Ampliación del Sistema de Cámaras de Vigilancia (CIACS 1-7-1) del municipio Caroní ', '2', '53', '243692.79', '9', '1', '2', '100', '4', '2010', '2010-0042', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('757', 'Rehabilitación y Acondicionamiento de las Instalaciones de la Empresa de Producción Social Indirecta (EPSI) Granitos Bolívar. ', '12', '53', '2000000.00', '7', '1', '1', '100', '8', '2011', '2011-0091', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('758', 'Plan de Asfaltado para el municipio Heres del estado Bolívar.', '1', '53', '2000000.00', '2', '1', '1', '100', '2', '2011', '2011-0095', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('759', 'Rehabilitación Infraestructura  ETIR  El Cambao (II Etapa).', '1', '1', '1000000.00', '3', '1', '1', '100', '2', '2011', '2011-0096', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('760', 'Rehabilitación de la Escuela Técnica Comercial Robinsoniana Dalla Costa (ETCR),  Ciudad Bolívar, municipios Heres del estado Bolívar.', '1', '2', '1000000.00', '3', '1', '1', '100', '2', '2011', '2011-0097', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('761', 'Construcción de Centro Integral de Servicios a la Comunidad, parroquia Vista al Sol, municipio Caroní, estado Bolívar.', '2', '17', '1000000.00', '6', '1', '1', '100', '2', '2011', '2011-0104', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('762', 'Adquisición de Camiones para el Transporte de Materiales en Fortalecimiento a la Misión Vivienda Venezuela.', '12', '53', '800000.00', '8', '1', '1', '100', '4', '2011', '2011-0092', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('763', 'Culminación de las Obras en las Instalaciones del Complejo Deportivo Villa Olímpica Ricardo Tulio Maya, parroquia Vista Hermosa, Ciudad Bolívar, estado Bolívar.', '1', '2', '800000.00', '3', '1', '1', '100', '2', '2011', '2011-0098', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('764', 'Recuperación de la Pista del Velódromo Perteneciente al Complejo Deportivo Villa Olímpica Ricardo Tulio Maya, parroquia Vista Hermosa, Ciudad Bolívar, estado Bolívar (II Etapa).', '1', '2', '500000.00', '3', '1', '1', '100', '2', '2011', '2011-0099', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('765', 'Adquisición de Máquina Ensacadora de Cal para Tratamiento de Agua de los Acueductos del estado Bolívar. ', '12', '53', '436800.00', '1', '1', '1', '100', '1', '2011', '2011-0080', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('766', 'Construcción de By Pass Tanque C, Ciudad Bolívar, municipio Heres del edo. Bolívar (II Etapa).', '1', '2', '420519.27', '1', '1', '1', '100', '1', '2011', '2011-0084', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('767', 'Rehabilitación de Complejo Deportivo de Piscinas, parroquia Catedral, municipio Heres, estado Bolívar (II Etapa).', '1', '1', '400000.00', '3', '1', '1', '100', '2', '2011', '2011-0101', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('768', 'Construcción de Centro Integral de Servicios a la Comunidad, parroquia Catedral, municipio Heres, estado Bolívar.', '1', '1', '400000.00', '6', '1', '1', '100', '2', '2011', '2011-0102', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('769', 'Remoción y Colocación de Tubería de 10” de Agua Servidas Incluyendo Reparación de Varias Tuberías de Agua Potable de ¾ en el sector del Mercado Periférico, municipio Heres, estado Bolívar.', '1', '1', '327276.81', '1', '1', '1', '100', '1', '2011', '2011-0082', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('770', 'Rehabilitación de Falsos Fondos en la Planta Potabilizadora La Paragua, municipio Bolivariano Angostura, estado Bolívar.', '6', '35', '180000.00', '1', '1', '1', '100', '1', '2011', '2011-0085', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('771', 'Dotación e Instalación de Puentes Elevadores y Herramientas Especializadas para el Centro de Lubricación del Taller de la Policía del Estado.', '12', '53', '218036.00', '9', '1', '3', '100', '4', '2010', '2010-0029', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('772', 'Adquisición de Adaptador para Barrenos, edo. Bolívar.', '12', '53', '65000.00', '8', '1', '1', '100', '8', '2011', '2012-0001', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('773', 'Construcción de Nuevo Sistema de Recolección de Aguas Servidas para el Campo A-1 de Ferrominera Puerto Ordaz, parroquia Cachamay, municipio Caroní, estado Bolívar.', '2', '53', '99480.73', '1', '1', '1', '100', '1', '2011', '2011-0082', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('774', 'Mejoras en el Colector de la calle Merevari del sector Unare I, UD-291, parroquia Unare, municipio Caroní, estado Bolívar.', '2', '10', '45798.90', '1', '1', '1', '100', '1', '2011', '2011-0083', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('775', 'Dotación de Uniformes para el Personal Uniformado del Cuerpo de Policía del estado Bolívar.', '12', '53', '3500000.00', '9', '1', '3', '100', '4', '2010', '2010-0053', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('776', 'Rehabilitación y Mejoras de la Avenida Expresa I, Angosturita, municipio Caroní, (II Etapa).', '2', '10', '10000000.00', '2', '1', '1', '100', '3', '2011', '2011-0114', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('777', 'Plan de Asfaltado del  municipio El Callao, estado Bolívar.', '10', '49', '231684.00', '2', '1', '1', '100', '2', '2011', '2011-0115', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('778', 'Construcción Dique del Sistema Wara de Santa Elena de Uairen.', '9', '47', '6000000.00', '1', '1', '1', '100', '1', '2011', '2011-0041', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('779', 'Construcción de la II Etapa de la Sede de Mundo de Sonrisas (Atención a los Niños y Niñas con Alguna Discapacidad) Ubicada en la UD-245 parroquia Universidad, Mcpio. Caroní.', '2', '18', '4051899.13', '6', '1', '1', '100', '3', '2011', '2011-0040', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('780', 'Fábrica de Bloques de Arcilla, municipio Heres del edo. Bolívar.', '1', '3', '3834304.73', '7', '1', '1', '100', '8', '2011', '2011-0052', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('781', 'Construcción de Seis (06) Aulas de la Escuela Pueblo Guri, parroquia Santa Bárbara, municipio Bolivariano Angostura, estado Bolívar.', '6', '38', '2499067.09', '3', '1', '1', '100', '3', '2011', '2012-0094', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('782', 'Fortalecimiento de Sistema de Bombeo en Acueductos Urbanos, Industriales y Rurales del edo. Bolívar.', '12', '53', '1500000.00', '5', '1', '1', '100', '3', '2011', '2011-0047', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('783', 'Complemento Estación de Bombeo de Aguas Servidas en UD-338 sector Las Amazonas, Puerto Ordaz, municipio Caroní, estado Bolívar.', '2', '10', '700000.00', '1', '1', '1', '100', '1', '2011', '2011-0078', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('784', 'Adquisición de Sistema de Dosificación de Cloro para Acueductos del estado Bolívar.', '12', '53', '3563200.00', '1', '1', '1', '100', '1', '2011', '2011-0079', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('785', 'Fábrica de Bloques de Arcilla, municipio Heres del edo. Bolívar (II Etapa).', '1', '3', '3500000.00', '7', '1', '1', '100', '8', '2011', '2011-0094', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('786', 'Acueducto en el sector El Bongo, municipio Heres del estado Bolívar (Culminación).', '1', '8', '500000.00', '1', '1', '1', '100', '1', '2011', '2011-0100', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('787', 'Rehabilitación de Casa de la Cultura, Comunidad Manak-kru, parroquia Santa Elena de Uairen, municipio Gran Sabana, estado Bolívar.', '9', '47', '500000.00', '6', '1', '1', '100', '3', '2011', '2011-0106', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('788', 'Construcción de la Red de Aguas Servidas en la Urb. Los Peregrinos, UD-225 y el Colector Paralelo en la  calle Merecure, sector Unare, Puerto Ordaz, estado Bolívar.', '2', '10', '326924.29', '1', '1', '1', '100', '1', '2011', '2011-0081', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('789', 'Rehabilitación de Aéreo Ambulancias King Air C-90, Siglas YVO-136, estado Bolívar.', '12', '53', '4407500.00', '8', '1', '1', '100', '19', '2011', '2011-0107', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('790', 'Rehabilitación de Aéreo Ambulancia  Cessna Grand Caravan 208 b, Siglas YVO-147, estado Bolívar.', '12', '53', '1505000.00', '8', '1', '1', '100', '19', '2011', '2011-0108', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('791', 'Rehabilitación de Aéreo Ambulancia Cessna 206 H, Siglas YVO-135, estado Bolívar.', '12', '53', '2000000.00', '8', '1', '1', '100', '19', '2011', '2011-0109', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('792', 'Acondicionamiento de Ambulancias para el 1-7-1.', '12', '53', '650000.00', '9', '1', '3', '100', '20', '2010', '2010-0080', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('793', 'Acondicionamiento de Dos (02) Vehículos Toyota del Servicio de Auxilio Vial 1-7-1 para Maniobras de Rescate y Equipamiento.', '12', '53', '520324.02', '9', '1', '3', '100', '20', '2010', '2010-0081', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('794', 'Gimnasios Urbanos Comunitarios en Todo el estado Bolívar.', '12', '53', '2000000.00', '3', '1', '1', '100', '21', '2011', '2011-0111', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('795', 'Construcción de Aula Integral para Trabajadores de Inviobras Bolívar (Sede de Puerto Ordaz y Sede de Bolívar).', '12', '53', '209357.82', '5', '1', '1', '100', '3', '2011', '2012-0103', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('796', 'Remodelación y Dotación de Módulo de Emergencia 171 para Fortalecimiento de las Comunidades, parroquia Guarataro, municipio Sucre, estado Bolívar.', '12', '53', '5000000.00', '4', '1', '1', '100', '3', '2014', '2014-0021', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('797', 'Construcción de Dique del Sistema Wara de Santa Elena de Uairen, municipio Gran Sabana, estado Bolívar (II Etapa).', '9', '47', '1856645.00', '1', '1', '1', '100', '1', '2012', '2012-0132', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('798', 'Reparación de un Tramo de la Vía de Acceso a los Tanques de Cerro Quemao, Puerto Ordaz, municipio Caroní, estado Bolívar.', '2', '10', '545073.43', '1', '1', '1', '100', '1', '2012', '2012-0133', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('799', 'Sustitución de Tramo de Tuberías de Acero Ø8”, entre las Progresivas 0+900 Y 1+100 del Sistema Wara, San Rafael, Santa Elena de Uairen, municipio Gran Sabana, estado Bolívar.', '9', '47', '315934.00', '1', '1', '1', '100', '1', '2012', '2012-0134', '2017-01-09 16:48:34', '2017-01-09 16:48:34', null);
INSERT INTO `proyectos` VALUES ('800', 'Construcción de Soporte Especial, para Tubería de Acero de 52” Proveniente del Sistema Tocomita, Ciudad Bolívar, estado Bolívar.', '1', '4', '513494.06', '1', '1', '1', '100', '1', '2012', '2012-0135', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('801', 'Terminación de Tanque Postensado de Capacidad, Ubicado en el Cerro El Quemao, sector Oeste de Puerto Ordaz, estado Bolívar.', '2', '10', '2850453.51', '1', '1', '1', '100', '1', '2012', '2012-0091', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('802', 'Rehabilitación de Centro Integral de Servicios a la Comunidad de Santa Elena de Uairen, municipio Gran Sabana del estado Bolívar.', '9', '47', '1000000.00', '6', '1', '1', '100', '2', '2012', '2012-0043', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('803', 'Plan de Asfaltado de desde el Peaje Puerto Ordaz hasta Empalme con Av. Expresa I (Avenida Angosturita), municipio Caroní. ', '2', '10', '10000000.00', '2', '1', '1', '100', '2', '2012', '2012-0092', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('804', 'Fortalecimiento de la Economía Popular Comunitaria para Madres Trabajadoras en Situación de Vulnerabilidad Social del estado Bolívar.', '12', '53', '5000000.00', '7', '1', '1', '100', '5', '2012', '2012-0003', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('805', 'Proyecto de Alfarería para Producir Ladrillos de Arcillas para la Gran Misión Vivienda Venezuela y para la Comunidad en General, municipio Heres, estado Bolívar.', '1', '3', '3000000.00', '7', '1', '1', '100', '8', '2012', '2012-0004', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('806', 'Centro de Procesamiento de Producción Agroindustrial Bolívar - Modulo Socio Productivo de Hortalizas con 12 Casas de Cultivos Bajo Ambiente Controlado en el municipio Heres, estado Bolívar.', '1', '8', '2000000.00', '7', '1', '1', '100', '16', '2012', '2012-0005', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('807', 'Fortalecimiento de Agrotienda Socialista “Agroinsumos Bolívar” en el municipio Caroní del estado Bolívar.', '2', '53', '2000000.00', '7', '1', '1', '100', '16', '2012', '2012-0006', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('808', 'Construcción del Acueducto de San Félix, UD-128, parroquia 11 de Abril, municipio Caroní, estado Bolívar. ', '2', '55', '8803959.36', '1', '1', '1', '100', '1', '2012', '2012-0008', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('809', 'Construcción del Acueducto Este de San Félix - municipio Caroní, estado Bolívar (II Etapa).', '2', '13', '4000000.00', '1', '1', '1', '100', '1', '2012', '2012-0010', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('810', 'Construcción de la Estación de Bombeo de Aguas Servidas en UD-338 sector Las Amazonas, Puerto Ordaz, municipio Caroní, estado Bolívar. (III Etapa)', '2', '10', '3500000.00', '1', '1', '1', '100', '1', '2012', '2012-0011', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('811', 'Ampliación de Redes de Aguas Potable en los sectores: Core 8, Unare I y II, Las Minifincas, Los Monos, Cambalache, 25 de Marzo, UD- 128, La Victoria, Vista al Sol, Bella Vista y Las Américas en el municipio Caroní del estado Bolívar. ', '2', '53', '1000000.00', '1', '1', '1', '100', '1', '2012', '2012-0012', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('812', 'Complemento para el Acueducto en el sector El Bongo, parroquia Panapana, municipio Heres, estado Bolívar.  ', '1', '8', '1000000.00', '1', '1', '1', '100', '1', '2012', '2012-0089', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('813', 'Fortalecimiento del Sistema Comunitario del Saneamiento de Aguas Servidas para el municipio Caroní del estado Bolívar.', '2', '53', '2000000.00', '1', '1', '1', '100', '1', '2012', '2012-0014', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('814', 'Remodelación y Ampliación en la Coordinación de Educación Especial Centro de Atención Integral a las Personas con Autismo (CAIPA), Barrio Angostura, municipio Heres, estado Bolívar.', '1', '2', '1133000.00', '3', '1', '1', '100', '3', '2012', '2012-0024', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('815', 'Rehabilitación de la Infraestructura del Edificio Cristal para Sede de la UNEFA, San Félix, municipio Caroní, estado Bolívar.', '2', '11', '1000000.00', '3', '1', '1', '100', '3', '2012', '2012-0026', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('816', 'Saneamiento Ambiental del sector de Cambalache, Ubicado en la parroquia Unare, municipio Caroní, estado Bolívar.', '2', '10', '8000000.00', '5', '1', '1', '100', '4', '2012', '2012-0028', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('817', 'Implementación y Modernización del Sistema Fijo de Seguridad en las Planta de Asfaltos en el municipio Caroní del estado Bolívar.', '2', '53', '1000000.00', '5', '1', '1', '100', '4', '2012', '2012-0029', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('818', 'Fortalecimiento de Infraestructura para el Sistema de Deporte Comunitario en el estado Bolívar.', '12', '53', '4000000.00', '3', '1', '1', '100', '3', '2012', '2012-0035', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('819', 'Rehabilitación de Infraestructura del Sistema de Parques Metropolitano (LA LLOVIZNA, CACHAMAY, LOEFLING) en el municipio Caroní del estado Bolívar.', '2', '11', '2500000.00', '5', '1', '1', '100', '22', '2012', '2012-0039', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('820', 'Rehabilitación y Adecuación del Centro Multiétnico \"Las Churuatas\" para el Desarrollo Social para las Comunidades Indígenas Autóctonas del municipio Heres del estado Bolívar.', '1', '3', '1000000.00', '6', '1', '1', '100', '2', '2012', '2012-0040', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('821', 'Rehabilitación de Infraestructura del Sistema de Parque Metropolitano (RUÍZ PINEDA) en el municipio Heres del estado Bolívar.', '1', '2', '1000000.00', '5', '1', '1', '100', '22', '2012', '2012-0044', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('822', 'Rehabilitación de Centro Integral de Servicios a la Comunidad de Upata, municipio Piar del estado Bolívar.', '3', '54', '500000.00', '6', '1', '1', '100', '2', '2012', '2012-0045', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('823', 'Rehabilitación y Mejoras de la Avenida Expresa I, Angosturita, municipio Caroní, estado Bolívar (II Etapa).', '2', '10', '20000000.00', '2', '1', '1', '100', '3', '2012', '2012-0048', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('824', 'Fortalecimiento del Sistema de Demarcación de Vialidades en el municipio Caroní del estado Bolívar.', '2', '53', '500000.00', '2', '1', '1', '100', '4', '2012', '2012-0060', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('825', 'Fortalecimiento del Sistema de Demarcación de Vialidades en el municipio Heres del estado Bolívar.', '1', '53', '500000.00', '2', '1', '1', '100', '4', '2012', '2012-0061', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('826', 'Construcción de Urbanismo para  Viviendas Unifamiliares, sector 24 de Julio, parroquia Marhuanta, municipio Heres, estado Bolívar.', '1', '4', '1900000.00', '5', '1', '1', '100', '8', '2012', '2012-0062', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('827', 'Construcción de Urbanismo para Viviendas Unifamiliares, parroquia Vista Hermosa, municipio Heres, estado Bolívar.', '1', '2', '1350000.00', '5', '1', '1', '100', '8', '2012', '2012-0063', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('828', 'Fortalecimiento del Proceso de Carga y Acarreo de Materia Prima Requerida  en las Plantas de Asfaltos en el municipio Caroní, estado Bolívar.', '2', '53', '1937000.00', '2', '1', '1', '100', '4', '2012', '2012-0064', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('829', 'Ampliación de la Capacidad de Almacenamiento de Asfalto Liquido (A-30) en Plantas de Asfalto del municipio Piar, estado Bolívar.', '3', '54', '1500000.00', '2', '1', '1', '100', '18', '2012', '2012-0065', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('830', 'Ampliación de la Capacidad de Almacenamiento de Asfalto Liquido (A-30) en Plantas de Asfalto del municipio Heres, estado Bolívar.', '1', '3', '1500000.00', '2', '1', '1', '100', '18', '2012', '2012-0066', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('831', 'Fortalecimiento en el Sistema de Mantenimiento y Sustitución de Infraestructura de Planta de Producción de Asfalto en el municipio Caroní, estado Bolívar.', '2', '53', '1000000.00', '2', '1', '1', '100', '4', '2012', '2012-0067', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('832', 'Fortalecimiento en el Sistema de Mantenimiento y Sustitución de Infraestructura de Planta de Producción de Asfalto en el municipio Heres, estado Bolívar.', '1', '3', '500000.00', '2', '1', '1', '100', '4', '2012', '2012-0068', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('833', 'Fortalecimiento en el Sistema de Mantenimiento y Sustitución de Infraestructura de Planta de Producción de Asfalto en el municipio Piar, estado Bolívar.', '3', '54', '500000.00', '2', '1', '1', '100', '4', '2012', '2012-0069', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('834', 'Rehabilitación y Mejoras a Centros de Salud Misión Barrio Adentro II CDI - SRI, estado Bolívar.', '12', '53', '5000000.00', '4', '1', '1', '100', '3', '2012', '2012-0070', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('835', 'Fortalecimiento del Sistema de Salud Pública para Traslados de Emergencias en el estado Bolívar.', '12', '53', '4309331.57', '4', '1', '1', '100', '4', '2012', '2012-0071', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('836', 'Dotación de Mobiliario para la Comisaria Los Olivos, municipio Caroní, estado Bolívar.', '2', '18', '287199.01', '9', '1', '3', '100', '4', '2010', '2010-0052', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('837', 'Fortalecimiento del Sistema de Transporte Bolivariano para Renovar la Flota de la Ruta Urbana de TRANSBOLÍVAR en el municipio Caroní, estado Bolívar.', '2', '11', '7724571.27', '6', '1', '1', '100', '4', '2012', '2012-0076', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('838', 'Fortalecimiento del Sistema de Transporte Bolivariano para Completar la Flota de la Ruta Urbana de TRANSBOLÍVAR en el municipio Heres del estado Bolívar.', '1', '53', '7724329.39', '6', '1', '1', '100', '4', '2012', '2012-0077', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('839', 'Fortalecimiento del Sistema de Transporte Bolivariano para el Incremento de Frecuencia en Rutas Interurbanas del estado Bolívar.', '12', '53', '5758345.38', '6', '1', '1', '100', '4', '2012', '2012-0078', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('840', 'Fortalecimiento del Sistema de Transporte Bolivariano para Crear Rutas Interurbana Ciudad Bolívar - Caicara del Orinoco  del municipio Cedeño, estado Bolívar. ', '4', '30', '3023601.98', '6', '1', '1', '100', '4', '2012', '2012-0079', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('841', 'Fortalecimiento del Sistema de Transporte Bolivariano para Inaugurar la Ruta Estudiantil de Upata - Ciudad Guayana del municipio Piar, estado Bolívar.', '3', '54', '1902460.17', '6', '1', '1', '100', '4', '2012', '2012-0080', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('842', 'Fortalecimiento del Sistema de Transporte Bolivariano para Cubrir las Rutas Turística del estado Bolívar.              ', '12', '53', '1436320.22', '6', '1', '1', '100', '4', '2012', '2012-0081', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('843', 'Proyecto de Alfarería para Producir Ladrillos de Arcillas para la Gran Misión Vivienda Venezuela y para la Comunidad en General, municipio Heres, estado Bolívar (IV Etapa).', '1', '3', '2000000.00', '7', '1', '1', '100', '8', '2012', '2012-0141', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('844', 'Construcción e Instalación de la Unidad  Potabilizadora de Agua (UPA) del Sistema de Potabilización Bolivariano en La Urbana, municipio Cedeño, estado Bolívar.', '4', '53', '2000000.00', '1', '1', '1', '100', '1', '2012', '2012-0015', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('845', 'Ampliación de Redes de Colectores de Aguas Servidas en los sectores: Las Amazonas, Core 8, 4 de Febrero, Las Minifincas, Los Monos, Cambalache, 25 de Marzo, UD-128 en el municipio Caroní del estado Bolívar.', '2', '53', '1000000.00', '1', '1', '1', '100', '1', '2012', '2012-0016', '2017-01-09 16:48:35', '2017-01-09 16:48:35', null);
INSERT INTO `proyectos` VALUES ('846', 'Ampliación de Redes de Colectores de Aguas Servidas en los sectores: La Paragua, Divino Niño, Los Próceres, Calle Caracas, Av. Táchira, El Porvenir, Prolongación Paseo Gaspari, El Mangal, Agua Salada, Casco Histórico y Grupo Mérida en el municipio Heres del estado Bolívar.', '1', '53', '1000000.00', '1', '1', '1', '100', '1', '2012', '2012-0017', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('847', 'Construcción del Liceo Bolivariano Manuel Cedeño, municipio Cedeño del estado Bolívar (II Etapa).', '4', '30', '10000000.00', '3', '1', '1', '100', '3', '2012', '2012-0018', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('848', 'Recuperación y Mejoras de la Escuela Juan Bautista Farreras, parroquia Catedral, municipio Heres del estado Bolívar.', '1', '1', '3500000.00', '3', '1', '1', '100', '2', '2012', '2012-0019', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('849', 'Rehabilitación de la Unidad Educativa Bolivariana Nacional María Zuria de Morales, parroquia José Antonio Páez, municipio Heres, estado Bolívar.', '1', '3', '1500000.00', '3', '1', '1', '100', '2', '2012', '2012-0020', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('850', 'Rehabilitación de Infraestructura en los Laboratorios de la Escuela de Ciencias de la Tierra en la Universidad de Oriente, parroquia La Sabanita, municipio Heres, estado Bolívar.', '1', '6', '1500000.00', '3', '1', '1', '100', '2', '2012', '2012-0021', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('851', 'Construcción de Aulas y Mejoras de la Escuela “Provincia de Guayana”, parroquia Chirica, municipio Caroní, estado Bolívar.', '2', '12', '1200000.00', '3', '1', '1', '100', '3', '2012', '2012-0022', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('852', 'Rehabilitación de la U.E. Las Garzas, parroquia Marhuanta, municipio Heres del estado Bolívar.', '1', '4', '1200000.00', '3', '1', '1', '100', '2', '2012', '2012-0023', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('853', 'Rehabilitación de la Escuela Técnica Comercial Robinsoniana (ETCR) Dalla Costa,  Ciudad Bolívar, municipio Heres del estado Bolívar (II Etapa).', '1', '2', '1000000.00', '3', '1', '1', '100', '2', '2012', '2012-0025', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('854', 'Recuperación del Liceo Bolivariano U.E.N. José Eugenio Sánchez Afanador en el Palmar, municipio Padre Pedro Chien, estado Bolívar.', '11', '51', '972362.64', '3', '1', '1', '100', '3', '2012', '2012-0027', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('855', 'Complemento del Plan de Asfaltado de desde el Peaje Puerto Ordaz hasta Empalme con Av. Expresa I (Avenida Angosturita), municipio Caroní. ', '2', '10', '12000000.00', '2', '1', '1', '100', '3', '2012', '2012-0093', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('856', 'Acondicionamiento de Instalaciones del Gimnasio Hermanas González para Climatización de Ambiente, municipio Caroní, estado Bolívar', '2', '11', '3300000.00', '3', '1', '1', '100', '3', '2012', '2012-0104', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('857', 'Rehabilitación del Gimnasio Hermanas González, municipio Caroní, estado Bolívar.', '2', '11', '3700000.00', '3', '1', '1', '100', '3', '2012', '2012-0099', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('858', 'Rehabilitación y Mejoras de las Instalaciones del Gimnasio Boris Planchart, parroquia Catedral, municipio Heres del estado Bolívar.', '1', '1', '5000000.00', '3', '1', '1', '100', '2', '2012', '2012-0034', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('859', 'Rehabilitación y Mejoras de Infraestructura de los Gimnasios de Esgrima, Gimnasia y Judo para Fortalecer el Deporte Comunitario, parroquia Vista Hermosa, municipio Heres, estado Bolívar.', '1', '2', '2000000.00', '3', '1', '1', '100', '2', '2012', '2012-0036', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('860', 'Rehabilitación y Mejoras del Centro de Deporte Comunitario para Taekwondo y Cancha Múltiple, parroquia Vista Hermosa, municipio Heres, estado Bolívar.', '1', '2', '500000.00', '3', '1', '1', '100', '2', '2012', '2012-0037', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('861', 'Recuperación del Centro Cultural Bolívar, parroquia Vista Hermosa, municipio Heres, estado Bolívar.', '1', '2', '10000000.00', '6', '1', '1', '100', '2', '2012', '2012-0038', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('862', 'Rehabilitación del Centro Integral de Servicios a la Comunidad, parroquia Catedral, municipio Heres, estado Bolívar.', '1', '1', '1000000.00', '6', '1', '1', '100', '2', '2012', '2012-0041', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('863', 'Construcción de Centro de Servicios Generales para la Comunidad, parroquia Upata, municipio Piar, estado Bolívar.', '3', '54', '1000000.00', '6', '1', '1', '100', '2', '2012', '2012-0042', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('864', 'Plan de Asfaltado de Vialidades en el municipio Heres, estado Bolívar.', '1', '53', '28000000.00', '2', '1', '1', '100', '2', '2012', '2012-0046', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('865', 'Plan de Asfaltado de Vialidades en el municipio Caroní, estado Bolívar.', '2', '53', '28000000.00', '2', '1', '1', '100', '2', '2012', '2012-0047', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('866', 'Rehabilitación del Puente Kaballape Ubicado en el Km. 535, Troncal 10 (Callao - Tumeremo), municipio Sifontes del estado Bolívar.', '5', '52', '6337921.98', '5', '1', '1', '100', '3', '2012', '2012-0123', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('867', 'Recuperación de la Troncal 19 del estado Bolívar (III Etapa).', '12', '53', '13662078.02', '2', '1', '1', '100', '3', '2012', '2012-0124', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('868', 'Plan de Asfaltado de Vialidades en el municipio Piar, estado Bolívar.', '3', '53', '10000000.00', '2', '1', '1', '100', '2', '2012', '2012-0050', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('869', 'Plan de Asfaltado de Vialidades en el  municipio Cedeño, estado Bolívar.', '4', '53', '3000000.00', '2', '1', '1', '100', '2', '2012', '2012-0052', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('870', 'Plan de Asfaltado de Vialidades en el municipio Gran Sabana, estado Bolívar.', '9', '53', '3000000.00', '2', '1', '1', '100', '2', '2012', '2012-0053', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('871', 'Plan de Asfaltado de Vialidades en el municipio Padre Pedro Chien, estado Bolívar.', '11', '53', '3000000.00', '2', '1', '1', '100', '2', '2012', '2012-0054', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('872', 'Plan de Asfaltado de Vialidades en el municipio Bolivariano Angostura, estado Bolívar.', '6', '53', '3000000.00', '2', '1', '1', '100', '2', '2012', '2012-0055', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('873', 'Plan de Asfaltado de Vialidades en el municipio Sifontes, estado Bolívar.', '5', '53', '3000000.00', '2', '1', '1', '100', '2', '2012', '2012-0056', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('874', 'Plan de Asfaltado de Vialidades en el municipio El Callao, estado Bolívar (II Etapa).', '10', '49', '2000000.00', '2', '1', '1', '100', '2', '2012', '2012-0057', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('875', 'Plan de Asfaltado de Vialidades en el municipio Sucre, estado Bolívar.', '7', '53', '2000000.00', '2', '1', '1', '100', '2', '2012', '2012-0058', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('876', 'Plan de Asfaltado de Vialidades en el municipio Roscio, estado Bolívar.', '8', '53', '2000000.00', '2', '1', '1', '100', '2', '2012', '2012-0059', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('877', 'Rehabilitación Física del Ambulatorio Urbano Tipo III Manoa, Ubicado en  San Félix, municipio Caroní, estado Bolívar.', '2', '13', '2000000.00', '4', '1', '1', '100', '2', '2012', '2012-0072', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('878', 'Rehabilitación y Mejoras del Ambulatorio de Vista al Sol, parroquia Vista al Sol,  municipio Caroní, estado Bolívar. (II Etapa).', '2', '17', '1000000.00', '4', '1', '1', '100', '2', '2012', '2012-0073', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('879', 'Activación e Incorporación de Módulos de la Central Telefónica para Optimizar las bondades 1-7-1, municipio Caroní, estado Bolívar.', '2', '53', '179780.00', '9', '1', '3', '100', '20', '2010', '2010-0082', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('880', 'Fortalecimiento del Sistema de Transporte Bolivariano para el Incremento de Frecuencia en Rutas Interurbanas del estado Bolívar. (Complemento)', '12', '53', '421400.84', '6', '1', '1', '100', '4', '2012', '2012-0100', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('881', 'Fortalecimiento del Sistema de Transporte Bolivariano para Inaugurar la Ruta Estudiantil de Upata - Ciudad - Guayana del municipio Piar, estado Bolívar. (Complemento)', '3', '54', '186203.33', '6', '1', '1', '100', '4', '2012', '2012-0101', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('882', 'Fortalecimiento del Sistema de Salud Pública para Traslados de Emergencias en el estado Bolívar. (Complemento)', '12', '53', '175423.05', '4', '1', '1', '100', '4', '2012', '2012-0102', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('883', 'Rehabilitación de la Estación de Rebombeo de Puente Blanco, parroquia Guasipati, municipio Roscio, estado Bolívar.', '8', '44', '1300000.00', '1', '1', '1', '100', '1', '2012', '2012-0105', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('884', 'Rehabilitación de la Infraestructura del Edificio Cristal para Sede de la UNEFA, San Félix, municipio Caroní, estado Bolívar. (II Etapa) ', '2', '11', '2600000.00', '3', '1', '1', '100', '3', '2012', '2012-0106', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('885', 'Construcción de Aulas y Mejoras de la Escuela “Provincia de Guayana”, parroquia Chirica, municipio Caroní, estado Bolívar. (II Etapa)', '2', '12', '1808528.92', '3', '1', '1', '100', '3', '2012', '2012-0107', '2017-01-09 16:48:36', '2017-01-09 16:48:36', null);
INSERT INTO `proyectos` VALUES ('886', 'Consolidación del Servicio Eléctrico para el sector Francisca Duarte, parroquia Chirica, municipio Caroní, estado Bolívar.', '2', '12', '3000000.00', '5', '1', '1', '100', '3', '2012', '2012-0108', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('887', 'Fortalecimiento del Hábitat Integral Comunitario para Fortalecer las Comunidades del municipio Caroní, estado Bolívar.', '2', '53', '1600000.00', '8', '1', '1', '100', '3', '2012', '2012-0109', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('888', 'Complemento del Plan de Asfaltado desde el Peaje Puerto Ordaz hasta Empalme con Av. Expresa I (Avenida Angosturita), municipio Caroní.', '2', '10', '3000000.00', '2', '1', '1', '100', '3', '2012', '2012-0127', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('889', 'Rehabilitación de Centro Integral de Servicios a la Comunidad de Santa Elena de Uairen, municipio Gran Sabana del estado Bolívar. (II Etapa)', '9', '47', '650000.00', '6', '1', '1', '100', '2', '2012', '2012-0126', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('890', 'Reparación y Mejoras del Simoncito en el sector Manak-Kru, parroquia Santa Elena de Uairen, municipio Gran Sabana, estado Bolívar.', '9', '47', '61557.27', '3', '1', '1', '100', '3', '2012', '2012-0128', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('891', 'Construcción de Tanque Australiano en la parroquia Pozo Verde, municipio Caroní, estado Bolívar.', '2', '16', '58442.73', '5', '1', '1', '100', '3', '2012', '2012-0129', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('892', 'Rehabilitación de Casa de la Cultura, Comunidad Manak-kru, parroquia Santa Elena de Uairen, municipio Gran Sabana, estado Bolívar. (II Etapa)', '9', '47', '103641.00', '6', '1', '1', '100', '3', '2012', '2012-0111', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('893', 'Plan de Asfaltado de la Vía Upata - El Pao, municipio Piar del estado Bolívar. (II Etapa)', '3', '54', '5000000.00', '2', '1', '1', '100', '3', '2012', '2012-0112', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('894', 'Rehabilitación y Mejoras (Drenaje - Alcantarillas) en la vía Upata, El Manteco, Km. 24, sector Mata Negra, municipio Piar, Edo. Bolívar.', '3', '54', '1105621.44', '2', '1', '1', '100', '3', '2012', '2012-0113', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('895', 'Complemento para Construcción de Viviendas, sector la Shell, Ciudad Bolívar, municipio Heres, estado Bolívar.', '1', '1', '391283.64', '5', '1', '1', '100', '8', '2012', '2012-0114', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('896', 'Complemento para la Ampliación de la Capacidad de Almacenamiento de Asfalto Liquido (A-30) en Plantas de Asfalto del municipio Heres, estado Bolívar. ', '1', '3', '441228.00', '2', '1', '1', '100', '18', '2012', '2012-0115', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('897', 'Complemento para la Ampliación de la Capacidad de Almacenamiento de Asfalto Liquido (A-30) en Plantas de Asfalto del municipio Piar, estado Bolívar. ', '3', '54', '441228.00', '2', '1', '1', '100', '18', '2012', '2012-0116', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('898', 'Dotación de Uniformes para el Personal de la Policía del estado Bolívar.', '12', '53', '1600058.88', '9', '1', '2', '100', '4', '2010', '2010-0101', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('899', 'Rehabilitación y Mejoras del Centro de Educación Inicial (CEI) Vista Hermosa, ubicado en la parroquia Vista Hermosa, municipio Heres del estado Bolívar.', '1', '2', '169241.99', '3', '1', '1', '100', '2', '2011', '2012-0143', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('900', 'Complemento para la Culminación de la Empresa Procesadora de Granitos del estado Bolívar.', '1', '6', '6015000.00', '7', '1', '1', '100', '4', '2012', '2012-0117', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('901', 'Rehabilitación y Mejoras a Centros de Salud Misión Barrio Adentro II CDI - SRI, estado Bolívar. (II Etapa)', '12', '53', '5000000.00', '4', '1', '1', '100', '3', '2012', '2012-0118', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('902', 'Dotación y Adecuación de Mobiliarios, Equipos de Alojamiento y Equipos de Computación, para el Centro de Coordinación Policial Simón Bolívar, municipio Caroní, estado Bolívar. ', '2', '13', '550000.00', '9', '1', '2', '100', '4', '2010', '2010-0141', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('903', 'Fortalecimiento del Sistema de Transporte Bolivariano para adecuar las Unidades de TRANSBOLÍVAR del estado Bolívar.', '12', '53', '2000000.00', '6', '1', '1', '100', '23', '2012', '2012-0120', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('904', 'Construcción de Taller de Mantenimiento y Terminal de Pasajeros de Transporte Público (Transbolívar) en Puerto Ordaz, municipio Caroní, estado Bolívar. (II Etapa)', '2', '11', '10000000.00', '5', '1', '1', '100', '3', '2012', '2012-0121', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('905', 'Fortalecimiento del Hábitat Integral Comunitario para Fortalecer las Comunidades del municipio Heres, estado Bolívar.', '1', '53', '1400000.00', '8', '1', '1', '100', '4', '2012', '2012-0122', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('906', 'Fortalecimiento de Infraestructura Deportiva mediante la Adecuación de Equipos Electrónicos en el Complejo de Piscinas Olímpicas, municipio Heres, estado Bolívar.', '1', '1', '17000.00', '5', '1', '2', '100', '2', '2012', '2012-0144', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('907', 'Fortalecimiento de Infraestructura Deportiva mediante la Adecuación de Equipos Electrónicos en el Complejo de Piscinas Olímpicas, municipio Heres, estado Bolívar.', '1', '1', '92891.68', '5', '1', '3', '100', '2', '2012', '2012-0145', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('908', 'Rehabilitación y Mejoras de la Infraestructura de la  Escuela Rosa de Centeno, Ubicado en la Avenida España, parroquia La Sabanita, municipio Heres, estado Bolívar.', '1', '6', '240000.00', '3', '1', '1', '100', '2', '2012', '2012-0146', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('909', 'Complejo Cultural Manuel Piar, municipio Piar, estado Bolívar. (II Etapa)', '3', '54', '5000000.00', '5', '1', '1', '100', '17', '2012', '2012-0147', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('910', 'Recuperación del Centro Cultural Bolívar, parroquia Vista Hermosa, municipio Heres, estado Bolívar (II Etapa).', '1', '2', '4000000.00', '6', '1', '1', '100', '2', '2012', '2012-0148', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('911', 'Rehabilitación y Mejoras de las Instalaciones del SAIME, Ubicado en Ciudad Bolívar, municipio Heres, estado Bolívar.', '1', '2', '1600000.00', '5', '1', '1', '100', '2', '2012', '2012-0149', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('912', 'Fortalecimiento de Infraestructura de Salud mediante la Adecuación del Sistema de Climatización  del Centro de Diagnóstico Integral (CDI) de Guasipati, municipio Roscio, del estado Bolívar.', '8', '44', '387000.00', '5', '1', '1', '100', '2', '2012', '2012-0150', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('913', 'Fortalecimiento de Infraestructura Deportiva en el Polideportivo Vista al Sol, municipio Caroní, estado Bolívar.', '2', '17', '635000.00', '3', '1', '1', '100', '3', '2012', '2012-0151', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('914', 'Fortalecimiento de Infraestructura Deportiva en el Ambulatorio Vista al Sol, municipio Caroní, estado Bolívar.', '2', '17', '415000.00', '3', '1', '1', '100', '3', '2012', '2012-0152', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('915', 'Rehabilitación del Gimnasio Hermanas González, municipio Caroní, estado Bolívar. (II Etapa)', '2', '11', '500000.00', '3', '1', '1', '100', '3', '2012', '2012-0153', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('916', 'Plan de Asfaltado de la Vía El Pao - San Félix, municipio Caroní del estado Bolívar.', '2', '16', '2978854.00', '2', '1', '1', '100', '3', '2012', '2012-0154', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('917', 'Instalación de Tramo de Red de Tuberías de Aguas Servidas en la Avenida Dalla Costa, Barrio Guayana, municipio Caroní, estado Bolívar.', '2', '31', '300000.00', '1', '1', '1', '100', '1', '2012', '2012-0155', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('918', 'Remoción y Sustitución de Tramo de Tubería, sector Core 8, municipio Caroní del estado Bolívar.', '2', '10', '700000.00', '1', '1', '1', '100', '1', '2012', '2012-0156', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('919', 'Plan de Asfalto de la parroquia La Sabanita, municipio Heres, estado Bolívar.', '1', '6', '6000000.00', '2', '1', '4', '100', '2', '2012', '2012-0157', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('920', 'Construcción de Estación de Bombeo de Aguas Servidas en Parques del Sur, Ciudad Bolívar, municipio Heres, estado Bolívar.', '1', '6', '7010424.75', '1', '1', '5', '100', '3', '2012', '2012-0158', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('921', 'Suministro y Transporte de Válvulas de los Filtros en la Planta Angostura, Ciudad Bolívar, municipio Heres, estado Bolívar.', '1', '6', '2210279.89', '1', '1', '5', '100', '3', '2012', '2012-0159', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('922', 'Rehabilitación del Módulo B de la Planta de Tratamiento Los Alacranes en San Félix, municipio Caroní, estado Bolívar.', '2', '13', '3719469.96', '1', '1', '5', '100', '3', '2012', '2012-0160', '2017-01-09 16:48:37', '2017-01-09 16:48:37', null);
INSERT INTO `proyectos` VALUES ('923', 'Obras Civiles para la Planta de Agua Potable de 600 LPS del Acueducto Oeste de Ciudad Bolívar, edo. Bolívar.', '1', '6', '23223452.46', '1', '1', '5', '100', '3', '2012', '2012-0161', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('924', 'Obras Electromecánicas de la Captación y Matrices para la Planta de Agua Potable de 600 LPS del Acueducto Oeste de Ciudad Bolívar, municipio Heres, estado Bolívar.', '1', '6', '10788591.96', '1', '1', '5', '100', '3', '2012', '2012-0162', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('925', 'Rehabilitación de Tramo de Colector Principal en el sector Bolivariana 2000, municipio Heres, estado Bolívar.', '1', '6', '2798593.75', '1', '1', '5', '100', '3', '2012', '2012-0163', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('926', 'Construcción de Red de Aguas Servidas en el Sector Los Aceiticos Viejo, Ciudad Bolívar, municipio Heres, estado Bolívar.', '1', '1', '2810905.80', '1', '1', '5', '100', '3', '2012', '2012-0164', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('927', 'Centro Integral de Producción Agropecuaria Bolívar (CEPAIB), estado Bolívar.', '12', '53', '4937114.80', '7', '1', '5', '100', '16', '2012', '2012-0165', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('928', 'Centro Lácteo de Upata, parroquia Upata, municipio Piar, estado Bolívar.', '3', '54', '821920.86', '7', '1', '5', '100', '16', '2012', '2012-0166', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('929', 'Centro de Procesamiento de Producción Agroindustrial Bolívar - Modulo Socio Productivo de Hortalizas con 12 Casas de Cultivos Bajo Ambiente Controlado,  estado Bolívar.', '1', '8', '6485250.48', '7', '1', '5', '100', '16', '2012', '2012-0167', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('930', 'Construcción de Planta de Agua Potable de 600 Lps, Acueducto Oeste, Ciudad Bolívar, municipio Heres, estado Bolívar.', '1', '6', '16000000.00', '1', '1', '1', '100', '1', '2013', '2013-0001', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('931', 'Fortalecimiento de las Redes de Distribución de Agua Potable en Distintos sectores del estado Bolívar. ', '12', '53', '7000000.00', '1', '1', '1', '100\n\n', '1', '2013', '2013-0002', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('932', 'Perforación y Puesta en Marcha de Pozos Profundos en el estado Bolívar. ', '12', '53', '4000000.00', '1', '1', '1', '100', '1', '2013', '2013-0003', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('933', 'Fortalecimiento del Sistema de Agua Potable para Acueductos del estado Bolívar. ', '12', '53', '5000000.00', '1', '1', '1', '100\n', '1', '2013', '2013-0004', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('934', 'Rehabilitación de la Planta de Tratamiento Cupapuicito municipio Piar del estado Bolívar. ', '3', '54', '8000000.00', '1', '1', '1', '100', '1', '2013', '2013-0005', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('935', 'Fortalecimiento de las Redes de Aguas Servidas en Distintos sectores del estado Bolívar. ', '12', '53', '3000000.00', '1', '1', '1', '100\n', '1', '2013', '2013-0006', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('936', 'Rehabilitación Escuela la Fe en Dios, parroquia Vista Al Sol, municipio Caroní del estado Bolívar. ', '2', '17', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0009', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('937', 'Rehabilitación Escuela José Félix Ribas, parroquia Vista Al Sol, municipio Caroní del estado Bolívar. ', '2', '17', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0010', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('938', 'Rehabilitación Escuela Básica María Elena de Mora, parroquia Chirica, municipio Caroní del estado Bolívar.', '2', '12', '400000.00', '3', '1', '1', '100', '2', '2013', '2013-0011', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('939', 'Rehabilitación Escuela Bella Vista, parroquia 11 de Abril, municipio Caroní del estado Bolívar.', '2', '55', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0012', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('940', 'Rehabilitación Escuela Técnica Industrial 25 de Marzo, parroquia 11 de Abril, municipio Caroní del estado Bolívar.', '2', '55', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0013', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('941', 'Rehabilitación Escuela Luis Beltrán Prieto, parroquia Unare, municipio Caroní del estado Bolívar.', '2', '10', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0014', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('942', 'Rehabilitación Escuela Las Amazonas, parroquia Unare, municipio Caroní del estado Bolívar.', '2', '10', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0015', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('943', 'Rehabilitación Escuela José Félix Ribas, parroquia Simón Bolívar, municipio Caroní del estado Bolívar.', '2', '13', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0016', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('944', 'Rehabilitación Escuela Simón Rodríguez, parroquia Pozo Verde, municipio Caroní del estado Bolívar.', '2', '16', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0017', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('945', 'Rehabilitación Escuela Leoncio Martínez, parroquia Pozo Verde, municipio Caroní del estado Bolívar.', '2', '16', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0018', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('946', 'Rehabilitación Escuela Cambalache, sector Cambalache, municipio Caroní del estado Bolívar.', '2', '10', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0019', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('947', 'Rehabilitación U.E.E. Canaima, parroquia Dalla Costa, municipio Caroní del estado Bolívar.', '2', '31', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0020', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('948', 'Rehabilitación U.E.N. Brigada Forestal, parroquia Dalla Costa, municipio Caroní del estado Bolívar.', '2', '31', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0021', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('949', 'Rehabilitación Escuela Niños Pregoneros, parroquia Catedral, municipio Heres del estado Bolívar.', '1', '1', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0022', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('950', 'Rehabilitación Escuela Zea, parroquia Catedral, municipio Heres del estado Bolívar. ', '1', '1', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0023', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('951', 'Rehabilitación Escuela Marginal Jerusalén, parroquia la Sabanita, municipio Heres del estado Bolívar. ', '1', '6', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0024', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('952', 'Rehabilitación Escuela Mirador I, parroquia la Sabanita, municipio Heres del estado Bolívar. ', '1', '6', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0025', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('953', 'Rehabilitación Escuela 24 de Julio, parroquia Marhuanta, municipio Heres del estado Bolívar. ', '1', '4', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0026', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('954', 'Rehabilitación Escuela Antonio José de Sucre, parroquia José Antonio Páez, municipio Heres del estado Bolívar. ', '1', '3', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0027', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('955', 'Rehabilitación Escuelas de la parroquia Panapana, municipio Heres del estado Bolívar. ', '1', '8', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0028', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('956', 'Rehabilitación Escuelas de la parroquia Orinoco, municipio Heres del estado Bolívar. ', '1', '7', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0029', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('957', 'Escuela Concentrada Mixta las Flores, parroquia Agua Salada, municipio Heres del estado Bolívar.', '1', '5', '250000.00', '3', '1', '1', '100', '2', '2013', '2013-0030', '2017-01-09 16:48:38', '2017-01-09 16:48:38', null);
INSERT INTO `proyectos` VALUES ('958', 'Rehabilitación de la Escuela Bolivariana Manuel Yánez  parroquia Agua Salada, municipio Heres del estado Bolívar. ', '1', '5', '250000.00', '3', '1', '1', '100', '2', '2013', '2013-0031', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('959', 'Rehabilitación Escuelas de la parroquia Zea, municipio Heres del estado Bolívar. ', '1', '9', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0032', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('960', 'Rehabilitación de Techos, Pintura, Baños y Cierre de Aulas Escuela Moreno de Mendoza, el Manteco, municipio Piar del estado Bolívar. ', '3', '23', '780000.00', '3', '1', '1', '100', '2', '2013', '2013-0033', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('961', 'Remodelación y Ampliación en la Coordinación de Educación Especial Centro de Atención Integral a las personas con Autismo (Caipa), barrio Angostura, municipio Heres, estado Bolívar (II Etapa).', '1', '2', '522308.63', '3', '1', '1', '100', '3', '2013', '2013-0034', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('962', 'Rehabilitación del Centro de Educación Inicial Carlos Hernández Acosta, calle Machado Paseo Meneses parroquia Catedral, municipio Heres del estado Bolívar. ', '1', '1', '730000.00', '3', '1', '1', '100', '2', '2013', '2013-0035', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('963', 'Construcción de Cerca Perimetral Escuela Eduardo Oxford, Guasipati, municipio Roscio del estado Bolívar. ', '8', '44', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0036', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('964', 'Reparación y Mejoras de Baños en la Unidad Educativa Básica Nacional \"Siso Martínez\", Upata, municipio Piar, estado Bolívar. ', '3', '54', '300000.00', '3', '1', '1', '100', '2', '2013', '2013-0037', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('965', 'Reparación del Techo con Manto de la Escuela E.T.I. de Upata municipio Piar del estado Bolívar. ', '3', '54', '146184.19', '3', '1', '1', '100', '2', '2013', '2013-0038', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('966', 'Rehabilitación, Mantenimiento y Mejoras de la Unidad Educativa Vicente Marcano en el municipio Sifontes del estado Bolívar. ', '5', '52', '1673493.83', '3', '1', '1', '100', '2', '2013', '2013-0039', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('967', 'Reparación y Mejoras de la Escuela E.T.I. de San Miguel Km 67, municipio Sifontes del estado Bolívar. ', '5', '52', '629118.28', '3', '1', '1', '100', '2', '2013', '2013-0040', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('968', 'Rehabilitación de la Infraestructura de la Escuela Técnica Agropecuaria Robinsoniana Caicara del Orinoco, parroquia Capital Caicara, municipio General Manuel Cedeño, estado Bolívar.', '4', '30', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0041', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('969', 'Rehabilitación de la Infraestructura de la U.E.E. \"Imataca\", parroquia Barceloneta, municipio Bolivariano Angostura, estado Bolívar.', '6', '36', '500000.00', '3', '1', '1', '100', '2', '2013', '2013-0042', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('970', 'Construcción del Sistema de Distribución Aéreo de Energía Eléctrica del Sector 9 UD-328, Villa Loefling, parroquia Unare, municipio Caroní del estado Bolívar. ', '2', '10', '567240.00', '5', '1', '1', '100', '3', '2013', '2013-0043', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('971', 'Construcción de Acometida Instalaciones Eléctricas para Urbanismo Ubicado en sector 24 de Julio, parroquia Marhuanta, municipio Heres del estado Bolívar. ', '1', '4', '3932760.00', '5', '1', '1', '100', '8', '2013', '2013-0044', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('972', 'Construcción del Sistema de Distribución Aéreo de Energía Eléctrica del sector 1, UD-328, Villa del Sur, parroquia Unare, municipio Caroní del estado Bolívar. ', '2', '10', '2500000.00', '5', '1', '1', '100', '3', '2013', '2013-0045', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('973', 'Rehabilitación y Mejoras de Casas Patrimoniales en el Casco Histórico de Ciudad Bolívar, estado Bolívar. ', '1', '1', '4000000.00', '5', '1', '1', '100', '17', '2013', '2013-0048', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('974', 'Adecuación y Mejoras de la Planta de Bloques para la Gran Misión Vivienda Venezuela, estado Bolívar.', '12', '53', '200000.00', '5', '1', '1', '100', '8', '2013', '2013-0049', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('975', 'Construcción de Planta de Tratamiento para el Urbanismo Villa Informativa municipio Heres, estado Bolívar. ', '1', '4', '548000.00', '5', '1', '1', '100', '8', '2013', '2013-0050', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('976', 'Construcción de Muro de Contención para la Sede de Niños y Niñas con alguna Discapacidad Ubicada en la UD-245 del municipio Caroní del estado Bolívar. ', '2', '18', '446183.56', '5', '1', '1', '100', '3', '2013', '2013-0051', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('977', 'Fortalecimiento de Infraestructura para el Sistema de Deporte Comunitario, estado Bolívar (II Etapa). ', '12', '53', '14880337.31', '3', '1', '1', '100', '3', '2013', '2013-0052', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('978', 'Recuperación y Rehabilitación de Estadio de Futbol Cachamaycito, municipio Caroní, estado Bolívar.', '2', '11', '2900000.00', '3', '1', '1', '100', '2', '2013', '2013-0053', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('979', 'Reparación y Mejora de Cancha Deportiva, sector la Gasolina, municipio el Callao, estado Bolívar. ', '10', '49', '648431.88', '3', '1', '1', '100', '2', '2013', '2013-0054', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('980', 'Reparación y Mejora de Cancha Deportiva en Ciudad Dorada, Km 88 municipio Sifontes, estado Bolívar. ', '5', '34', '992153.52', '3', '1', '1', '100', '2', '2013', '2013-0055', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('981', 'Rehabilitación de Centros Diagnóstico Integral, Salas de Rehabilitación Integral y Centros de Alta Tecnología de la Misión Barrio Adentro en el estado Bolívar.', '12', '53', '6000000.00', '4', '1', '1', '100', '3', '2013', '2013-0057', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('982', 'Construcción de Modulo de Emergencia 171 y Dotación  de Ambulancia para Fortalecimiento de  las Comunidades, parroquia Guarataro, municipio Sucre, estado Bolívar.', '7', '40', '5000000.00', '4', '1', '1', '100', '3', '2013', '2013-0058', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('983', 'Fortalecimiento de la Infraestructura del Parque Metropolitano Leonardo Ruiz Pineda, parroquia Vista Hermosa, municipio Heres del estado Bolívar. ', '1', '2', '1000000.00', '5', '1', '1', '100', '22', '2013', '2013-0059', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('984', 'Fortalecimiento de Infraestructura del Parque Metropolitano La Llovizna, municipio Caroní del estado Bolívar. ', '2', '11', '1000000.00', '5', '1', '1', '100', '22', '2013', '2013-0060', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('985', 'Fortalecimiento Comunitario del Poder Popular a través del Equipamiento Urbano para todo el estado Bolívar. ', '12', '53', '3000000.00', '6', '1', '1', '100', '3', '2013', '2013-0061', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('986', 'Fortalecimiento de la Sede del Adulto Mayor municipio Caroní del estado Bolívar. ', '2', '18', '3500000.00', '6', '1', '1', '100', '3', '2013', '2013-0062', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('987', 'Adecuación de la Sede de Niños y Niñas con alguna Discapacidad ubicada en la UD-245 del municipio Caroní del estado Bolívar. ', '2', '18', '1400000.00', '6', '1', '1', '100', '3', '2013', '2013-0063', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('988', 'Rehabilitación de Casa de la Cultura en el municipio el Callao del estado Bolívar', '10', '49', '300000.00', '6', '1', '1', '100', '17', '2013', '2013-0064', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('989', 'Rehabilitación de Casa de la Cultura en Tumeremo municipio Sifontes del estado Bolívar. ', '5', '52', '300000.00', '6', '1', '1', '100', '17', '2013', '2013-0065', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('990', 'Construcción de Sede de la Orquesta Sinfónica de Ciudad Bolívar, municipio Heres, estado Bolívar. ', '1', '2', '4500000.00', '6', '1', '1', '100', '2', '2013', '2013-0067', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('991', 'Plan de Asfaltado de Vialidades municipio Caroní, estado Bolívar.  ', '2', '53', '26717979.56', '2', '1', '1', '100', '2', '2013', '2013-0068-1', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('992', 'Plan de Asfaltado de Vialidades municipio Caroní, estado Bolívar.  ', '2', '53', '3282020.44', '2', '1', '1', '100', '18', '2013', '2013-0068-2', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('993', 'Plan de Asfaltado de Vialidades municipio Caroní, estado Bolívar. (II Etapa).', '2', '53', '6500000.00', '2', '1', '1', '100', '3', '2013', '2013-0128', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('994', 'Plan de Asfaltado de Vialidades en Sectores Populares, municipio Caroní, estado Bolívar.  ', '2', '53', '7000000.00', '2', '1', '1', '100', '3', '2013', '2013-0069', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('995', 'Plan de Asfaltado de Vialidades municipio Heres, estado Bolívar. ', '1', '53', '39818215.34', '2', '1', '1', '100', '2', '2013', '2013-0070-1', '2017-01-09 16:48:39', '2017-01-09 16:48:39', null);
INSERT INTO `proyectos` VALUES ('996', 'Plan de Asfaltado de Vialidades municipio Heres, estado Bolívar. ', '1', '53', '7081784.66', '2', '1', '1', '100', '18', '2013', '2013-0070-2', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('997', 'Plan de Asfaltado de Vialidades municipio Heres, estado Bolívar (II Etapa).', '1', '53', '6500000.00', '2', '1', '1', '100', '2', '2013', '2013-0115', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('998', 'Plan de Asfaltado de Vialidades municipio Piar, estado Bolívar. ', '3', '53', '3000000.00', '2', '1', '1', '100', '2', '2013', '2013-0071', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('999', 'Plan de Asfaltado de Vialidades municipio Roscio, estado Bolívar. ', '8', '53', '3000000.00', '2', '1', '1', '100', '18', '2013', '2013-0072', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1000', 'Plan de Asfaltado de Vialidades municipio el Callao, estado Bolívar. ', '10', '49', '3000000.00', '2', '1', '1', '100', '2', '2013', '2013-0073', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1001', 'Plan de Asfaltado de Vialidades municipio Sifontes, estado Bolívar. ', '5', '53', '3000000.00', '2', '1', '1', '100', '2', '2013', '2013-0074', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1002', 'Plan de Asfaltado de Vialidades municipio Padre Pedro Chien, estado Bolívar. ', '11', '53', '3000000.00', '2', '1', '1', '100', '2', '2013', '2013-0075', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1003', 'Plan de Asfaltado de Vialidades municipio Bolivariano Angostura, estado Bolívar. ', '6', '53', '3000000.00', '2', '1', '1', '100', '2', '2013', '2013-0076', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1004', 'Plan de Asfaltado de Vialidades municipio Cedeño, estado Bolívar. ', '4', '53', '3000000.00', '2', '1', '1', '100', '2', '2013', '2013-0077', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1005', 'Plan de Asfaltado de Vialidades municipio Gran Sabana, estado Bolívar. ', '9', '53', '3000000.00', '2', '1', '1', '100', '2', '2013', '2013-0078', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1006', 'Plan de Asfaltado y Demarcación de Vialidades en el Retumbo, municipio Caroní del Estado Bolívar.', '2', '53', '2200000.00', '2', '1', '1', '100', '2', '2013', '2013-0079', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1007', 'Plan de Acondicionamiento de Vialidades Agrícolas en el estado Bolívar.', '12', '53', '2500000.00', '2', '1', '1', '100', '2', '2013', '2013-0080', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1008', 'Acondicionamiento de la Vialidad Rural de Taparote, parroquia Andrés Eloy Blanco, municipio Piar, estado Bolívar. ', '3', '22', '200000.00', '2', '1', '1', '100', '2', '2013', '2013-0081', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1009', 'Reparación y Mejoras de Vialidad de Upata - El Manteco (Construcción de Drenajes Transversales y Acondicionamiento de Calzada), municipio Piar, estado Bolívar ', '3', '53', '4188617.06', '2', '1', '1', '100', '2', '2013', '2013-0082', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1010', 'Plan de Asfaltado en el Sector San Juan y Áreas Adyacentes al Urbanismo el Paraíso, Vía hacia Minerven y San Miguel, El Callao, municipio El Callao del estado Bolívar. ', '10', '49', '3813657.28', '2', '1', '1', '100', '2', '2013', '2013-0083', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1011', 'Mejoras y Recuperación de Vialidad Engranzonada del Casco Central de la parroquia San Isidro, Km 85 al Km 88, municipio Sifontes, estado Bolívar (I Etapa).  ', '5', '34', '100000.00', '2', '1', '1', '100', '2', '2013', '2013-0084', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1012', 'II Etapa de Granitos Bolívar, municipio Heres del estado Bolívar. ', '1', '6', '3000000.00', '7', '1', '1', '100', '24', '2013', '2013-0087', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1013', 'Alfarería Bolívar en el municipio Heres del estado Bolívar. ', '1', '3', '9000000.00', '7', '1', '1', '100', '8', '2013', '2013-0089', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1014', 'Centro de Producción Bajo Ambiente Controlado Agrobolívar, parroquia Catedral, municipio Heres del estado Bolívar.  ', '1', '1', '500000.00', '7', '1', '1', '100', '16', '2013', '2013-0091', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1015', 'Centro Agrícola Integral Ezequiel Zamora, parroquia Panapana, municipio Heres del estado Bolívar.  ', '1', '8', '4000000.00', '7', '1', '1', '100', '16', '2013', '2013-0092', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1016', 'Centro Integral de Formación y Producción Maisanta, municipio Caroní del estado Bolívar.  ', '2', '14', '6500000.00', '7', '1', '1', '100', '16', '2013', '2013-0093', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1017', 'Alimentos Bolívar, Unidad Socialista Destinada a la Preservación, Comercialización a Precios Socialistas de Alimentos de Primera Calidad, municipio Heres, estado Bolívar. ', '1', '1', '6200000.00', '7', '1', '1', '100', '16', '2013', '2013-0094', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1018', 'Fortalecimiento de Granitos Bolívar a través de la Creación de Escuela de Graniteros, parroquia la Sabanita, municipio Heres del estado Bolívar. ', '1', '6', '2800000.00', '7', '1', '1', '100', '24', '2013', '2013-0095', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1019', 'Recuperación del Paredón Perimetral y Rehabilitación de Baños de Educación Inicial de la U.E.E. Teodora Méndez de Montes, parroquia Catedral del municipio Heres, estado Bolívar.', '1', '1', '1000000.00', '3', '1', '1', '100', '2', '2013', '2013-0126', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1020', 'Complemento para el Plan de Asfaltado de Vialidades municipio Heres, estado Bolívar.', '1', '53', '3500000.00', '2', '1', '1', '100', '2', '2013', '2013-0127', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1021', 'II Etapa de la Construcción de la Sede de la Orquesta Sinfónica de Ciudada Bolívar, municipio Heres, estado Bolívar.', '1', '2', '4523716.00', '6', '1', '1', '100', '2', '2013', '2013-0122', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1022', 'Construcción de Drenaje Transversal de una Batería de Tubería de Concreto D=42\" clase 4 en el sector Pastora vía Camature, Guasipati, municipio Roscio, estado Bolívar.', '8', '44', '1267633.05', '2', '1', '1', '100', '3', '2013', '2013-0123', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1023', 'Rehabilitación y Mejoras de la Casa de Niños Juan Germán Roscio en Guasipati, municipio Roscio estado Bolívar.', '8', '44', '208650.95', '3', '1', '1', '100', '3', '2013', '2013-0124', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1024', 'Intervención del Sistema de Drenaje para Alivio de la Cárcava Cañón del Diablo, parroquia Chirica, municipio Caroní del estado Bolívar. ', '2', '12', '4000000.00', '5', '1', '1', '100', '3', '2013', '2013-0100', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1025', 'Intervención del Sistema de Drenaje para Alivio de la Cárcava en la Victoria, parroquia Vista Al Sol, municipio Caroní del estado Bolívar. ', '2', '17', '4000000.00', '5', '1', '1', '100', '3', '2013', '2013-0101', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1026', 'Adquisición del Parque Automotor para el Fortalecimiento de la Seguridad Ciudadana del estado Bolívar.', '12', '53', '1552674.17', '9', '1', '1', '100', '4', '2011', '2012-0131', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1027', 'Fortalecimiento del Sistema de Seguridad Ciudadana y Comunitaria del estado Bolívar.', '12', '53', '5496844.00', '9', '1', '1', '100', '4', '2011', '2011-0110', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1028', 'Fortalecimiento de la Seguridad Ciudadana y Comunitaria de la Población del estado Bolívar.', '12', '53', '6286500.00', '9', '1', '1', '100', '4', '2011', '2011-0112', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1029', 'Fortalecimiento de la Seguridad Ciudadana y Comunitaria con Unidades de Alto Rendimiento, estado Bolívar.', '12', '53', '1874000.00', '9', '1', '1', '100', '4', '2011', '2011-0113', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1030', 'Fortalecimiento del Sistema de Seguridad Ciudadana Comunitaria, municipio Caroní, estado Bolívar.', '2', '53', '2700000.00', '9', '1', '1', '100', '20', '2012', '2012-0074', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1031', 'Construcción y Dotación de Estación de Bomberos para el municipio Roscio de estado Bolívar.', '8', '53', '1500000.00', '5', '1', '1', '100', '3', '2013', '2013-0111', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1032', 'Construcción de Drenaje Transversal simple colapsado en el Troncal 10 (Tramo El Callao - Tumeremo), municipio el Callao del estado Bolívar.', '10', '49', '1500000.00', '2', '1', '1', '100', '3', '2013', '2013-0125', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1033', 'Rehabilitación y Mejoras de la Avenida de los Trabajadores, municipio Caroní, estado Bolívar.', '2', '53', '2000000.00', '5', '1', '1', '100', '3', '2013', '2013-0113', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1034', 'Rehabilitación, Acondicionamiento y Dotación del Centro de Artes que Servirá como Sede del Comité Promotor de los Juegos Panamericanos Ciudad Bolívar 2019, parroquia Catedral, municipio Heres del estado Bolívar.', '1', '1', '17466090.28', '6', '1', '1', '100', '17', '2013', '2013-0116', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1035', 'Recuperación y Mejoras del Centro Cultural Bolívar, parroquia Vista Hermosa, municipio Heres, estado Bolívar (III Etapa).', '1', '2', '6741918.42', '6', '1', '1', '100', '2', '2013', '2013-0117', '2017-01-09 16:48:40', '2017-01-09 16:48:40', null);
INSERT INTO `proyectos` VALUES ('1036', 'Recuperación y Mejoras de Posadas Turísticas, municipio Heres, estado Bolívar.', '1', '1', '432664.76', '11', '1', '1', '100', '3', '2013', '2013-0119', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1037', 'Dotación de Kits de Trabajo para el Fortalecimiento de Economía Social Comunitaria en el estado Bolívar.', '12', '53', '5000000.00', '8', '1', '1', '100', '5', '2013', '2013-0120', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1038', 'Reparación y Mejora de Unidad Educativa, 19 de Abril Ner 211, ubicada en el municipio Caroní del estado Bolívar.', '2', '16', '134695.96', '3', '1', '1', '100', '2', '2013', '2013-0121', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1039', 'Fortalecimiento del Sistema de Emergencias 171 para el estado Bolívar.', '12', '53', '2000000.00', '6', '1', '1', '100', '20', '2013', '2013-0132', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1040', 'Complejo Cultural Manuel Piar, municipio Piar del estado Bolívar (III Etapa).', '3', '54', '6000000.00', '5', '1', '1', '100', '17', '2013', '2013-0133', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1041', 'Plan de Asfaltado de Vialidades municipio Heres, estado Bolívar (III Etapa).', '1', '53', '4591392.00', '2', '1', '1', '100', '2', '2013', '2013-0135', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1042', 'Rehabilitación de la U.E Juan Bautista González, municipio Heres del estado Bolívar.', '1', '6', '1000000.00', '3', '1', '1', '100', '2', '2013', '2013-0136', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1043', 'Construcción de Residencias Médicas de dos niveles para la Misión Barrio Adentro II, municipio El Callao estado Bolívar. ', '10', '49', '1200000.00', '4', '1', '1', '100', '3', '2013', '2013-0137', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1044', 'Asfaltado Sector Villa Polígono, parroquia Dalla Costa del municipio Caroní, estado Bolívar.', '2', '31', '1721867.94', '2', '1', '1', '100', '3', '2013', '2013-0138', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1045', 'Reparación de Tuberías de Aguas Servidas en la Cárcava ubicada en la Av. Las Américas cruce  con carrera Churun Meru, sector Alta Vista, municipio Caroní, estado Bolívar.', '2', '18', '290000.00', '5', '1', '1', '100', '3', '2013', '2013-0139', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1046', 'Reparación Eléctrica y Mejoras del Sistema de Iluminación de la Sede de Niños y Niñas con Diversidad Funcional ubicada en la Ud. 245 del municipio Caroní del estado Bolívar.', '2', '18', '63711.13', '5', '1', '1', '100', '3', '2013', '2013-0140', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1047', 'Construcción de Muro de Contención y Brocales en la UD-128, San Félix, municipio Caroní, estado Bolívar.', '2', '55', '79331.59', '5', '1', '1', '100', '3', '2013', '2013-0141', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1048', 'Complemento para la Culminación de Construcción de Centro de Salud en los Pijiguaos, municipio Cedeño, estado Bolívar.', '4', '56', '470137.88', '4', '1', '1', '100', '3', '2013', '2013-0142', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1049', 'Rehabilitación y Mejoras del Ambulatorio Rural María Isabel Farías, municipio Cedeño, estado Bolívar.', '4', '30', '460870.46', '4', '1', '1', '100', '3', '2013', '2013-0143', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1050', 'Reparación y Mejoras en la Escuela Hugo Chávez, municipio Caroní, estado Bolívar.', '2', '13', '173275.53', '3', '1', '1', '100', '3', '2013', '2013-0144', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1051', 'Plan de Asfaltados de Vialidades en Sectores Populares, municipio Caroní, estado Bolívar (II Etapa).', '2', '53', '9677492.00', '2', '1', '1', '100', '3', '2013', '2013-0145', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1052', 'Estabilización de Taludes Laterales y Reconstrucción Drenaje Transversal de Batería Cuádruple de 72” en el sector Mamonal de la vía en el sector Jualpa y Mamonal, carretera  Upata – El Palmar, estado Bolívar.', '3', '54', '6174213.97', '2', '1', '1', '100', '3', '2013', '2013-0146', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1053', 'Estabilización de Talud en carretera Troncal 10 sector Sierra de Lema, municipio Gran Sabana, estado Bolívar.', '9', '47', '1310193.68', '2', '1', '1', '100', '3', '2013', '2013-0147', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1054', 'Fortalecimiento Comunitario del Poder Popular a través del Equipamiento Urbano para todo el estado Bolívar (II Etapa).', '12', '53', '1500000.00', '8', '1', '1', '100', '4', '2013', '2013-0148', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1055', 'Construcción de Drenaje Transversal de una Línea de Tubería  (alcantarilla simple) y de Batería Doble (Alcantarilla doble) en la to-19 prog. 115+000, 95+000 y prog. 115 +800, municipio Sucre, estado Bolívar.', '7', '53', '2730253.20', '2', '1', '1', '100', '3', '2013', '2013-0149', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1056', 'Acondicionamiento, Demolición y Limpieza de Superficie de Apoyo en la Prolongación de la Avenida Caracas, sector Las Amazonas, municipio Caroní, estado Bolívar.', '2', '10', '400000.00', '5', '1', '1', '100', '3', '2013', '2013-0150', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1057', 'Construcción de dos (2) Drenajes Transversales con Tubería de Concreto d=36\" y 42\" y Terraceo para Elevar Cotas de Vialidad, en el sector Sabaneta, carretera Upata - El Pao, municipio Piar, estado Bolívar.', '3', '54', '3035215.28', '2', '1', '1', '100', '3', '2013', '2013-0151', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1058', 'Pavimentación en Urbanismo Salto Bolivariano, parroquia Unare, municipio Caroní, estado Bolívar (urbanismo para 64 viviendas).', '2', '10', '1013437.34', '5', '1', '1', '100', '3', '2013', '2013-0152', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1059', 'Rehabilitación de las calles de Rodaje ubicada en el Aeropuerto Internacional Manuel Carlos Piar de Puerto Ordaz, municipio Caroní, estado Bolívar.', '2', '10', '1438012.80', '5', '1', '1', '100', '14', '2013', '2013-0130', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1060', 'Rehabilitación del Tramo de Colector Principal en el sector Bolivariana 2000, municipio Heres, estado Bolívar (II Etapa).', '1', '6', '1078411.47', '1', '1', '6', '100', '1', '2013', '2013-0134', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1061', 'Obras Civiles de Captación y Matrices de la Planta de Agua Potable de 600 lps, Acueducto Oeste en el municipio Heres del estado Bolívar (II Etapa).', '1', '6', '9241798.04', '1', '1', '6', '100', '1', '2013', '2013-0153', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1062', 'Obras Electromecánicas de la Captación y Matrices para la Planta de Agua Potable de 600 lps del Acueducto Oeste en el municipio Heres, estado Bolívar (II E tapa). ', '1', '6', '3695610.68', '1', '1', '6', '100', '1', '2013', '2013-0154', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1063', 'Construcción de Estación de Bombeo de Aguas Servidas en Parques del Sur en el municipio Heres del estado Bolívar (II Etapa).', '1', '6', '2285347.22', '1', '1', '6', '100', '1', '2013', '2013-0155', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1064', 'Equipamiento, Dotación y Puesta en Marcha del Centro Lácteo de Upata. (Centro Lácteo de Upata, parroquia Upata, municipio Piar, estado Bolívar.)', '3', '54', '115444.62', '7', '1', '6', '100', '16', '2013', '2013-0156', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1065', 'Centro Agrícola Integral Ezequiel Zamora. (Centro de Procesamiento de Producción Agroindustrial Bolívar - Modulo Socio Productivo de Hortalizas con 12 Casas de Cultivos Bajo Ambiente Controlado,  estado Bolívar.)', '1', '8', '1896209.92', '7', '1', '6', '100', '16', '2013', '2013-0157', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1066', 'Financiamiento para Proyectos de Agua Potable a ejecutar a través de Hidrobolívar. (Punto de Cuenta 001-13)', '12', '53', '120243400.00', '1', '1', '7', '100\n', '1', '2013', '-', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1067', 'Financiamiento de Gastos Operativos 2013, de la Unidad de Quemados del Hospital Universitario Ruiz y Páez de Ciudad Bolívar. (Punto de Cuenta 002-13)', '1', '1', '99631388.00', '4', '1', '7', '100', '25', '2013', '-', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1068', 'Componente I: Recuperación de la Troncal 19, Caicara, municipio Cedeño, estado Bolívar.', '4', '30', '59999998.42', '2', '1', '7', '100', '3', '2013', '-', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1069', 'Componente II: Recuperación de la Troncal 16, municipio Angostura, estado Bolívar.', '6', '53', '29999952.38', '2', '1', '7', '100', '3', '2013', '-', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1070', 'Financiamiento de la Rehabilitación y Refacción del Hospital de Upata, Capital del municipio Autónomo Piar estado Bolívar. \"Gervasio Vera Custodio\". (Punto de Cuenta 004-13)', '3', '54', '80000000.00', '4', '1', '7', '100', '25', '2013', '-', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1071', 'Financiamiento de la Planta Eléctrica de 200 KVA, para la Maternidad Negra Hipólita, ubicada en San Félix, municipio Caroní. (Punto de Cuenta 006-13)', '2', '55', '815000.00', '4', '1', '7', '100', '4', '2013', '-', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1072', 'Construcción de un Ambulatorio Tipo III Gurí, municipio Bolivariano Angostura, estado Bolívar.', '6', '38', '8000000.00', '4', '1', '1', '100', '3', '2014', '2014-0001', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1073', 'Fortalecimiento del Sistema de Seguridad Ciudadana Mediante la Adquisición de Mobiliario y Equipos para Centros de Coordinación Policial, estado Bolívar. ', '12', '53', '1000000.00', '9', '1', '1', '100', '4', '2013', '2013-0105', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1074', 'Fortalecimiento del Sistema de Seguridad Ciudadana Comunitaria (II Etapa) - CIACS. ', '12', '53', '3000000.00', '9', '1', '1', '100', '20', '2013', '2013-0108', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1075', 'Adquisición de Chalecos Antibalas para Funcionarios de la Policía del estado Bolívar.', '12', '53', '6000000.00', '9', '1', '1', '100', '4', '2014', '2014-0003', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1076', 'Centro de Producción Genético de Ganado Caprino  y Ovino, “Ezequiel Zamora”, municipio Caroní, estado Bolívar (II Etapa).', '1', '8', '6000000.00', '7', '2', '1', '91', '16', '2014', '2014-0064', '2017-01-09 16:48:41', '2017-01-09 16:48:41', null);
INSERT INTO `proyectos` VALUES ('1077', 'Centro de Producción Genético de Ganado Caprino y Ovino, \"Ezequiel Zamora\", municipio Caroní, estado Bolívar.', '1', '8', '12000000.00', '7', '1', '1', '100', '16', '2014', '2014-0005', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1078', 'Suministro y Transporte de Válvulas de los Filtros en la Planta Angostura, Ciudad Bolívar, municipio Heres, estado Bolívar (II Etapa).', '1', '6', '4287528.10', '1', '1', '1', '100', '1', '2014', '2014-0006', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1079', 'Rehabilitación del Módulo B  de la Planta de Tratamiento los Alacranes en San Félix, municipio Caroní, estado Bolívar (II Etapa).', '2', '13', '5785717.93', '1', '1', '1', '100', '1', '2014', '2014-0007', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1080', 'Construcción de Red de Aguas Servidas en el Sector los Aceiticos Viejos, Ciudad Bolívar, municipio Heres, estado Bolívar (II Etapa).', '1', '1', '2775634.11', '1', '1', '1', '100', '3', '2014', '2014-0008', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1081', 'Consolidación del Servicio Eléctrico para el Sector Francisca Duarte, parroquia Chirica, municipio Caroní, estado Bolívar (II Etapa).', '2', '12', '3000000.00', '5', '1', '1', '100', '2', '2014', '2014-0009', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1082', 'Rehabilitación y Mejoras de CDI, SRI Y CAT en el estado Bolívar.', '12', '53', '8000000.00', '4', '1', '1', '100', '3', '2014', '2014-0010', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1083', 'Adquisición de Equipos de Radio Comunicación para Centros de Coordinación Policial del Estado Bolívar.', '12', '53', '5000000.00', '9', '1', '1', '100', '4', '2014', '2014-0004', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1084', 'Demarcación y Señalización de Vías de Comunicación Terrestre para el Desarrollo Económico del estado Bolívar.', '12', '53', '8000000.00', '2', '1', '1', '100', '8', '2014', '2014-0013', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1085', 'Culminación de la Sede del Adulto Mayor Ubicada en la UD-245, parroquia Universidad, municipio Caroní, estado Bolívar.', '2', '18', '16000000.00', '6', '1', '1', '100', '3', '2014', '2014-0014', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1086', 'Intervención de Cárcava en las Adyacencias del Hospital Uyapar en la UD-245, parroquia Universidad, municipio Caroní del Estado Bolívar.', '2', '18', '4000000.00', '5', '1', '1', '100', '3', '2014', '2014-0015', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1087', 'Rehabilitación de Casas Patrimoniales en el Casco Histórico de Ciudad Bolívar, municipio Heres del estado Bolívar.', '1', '1', '15000000.00', '5', '1', '1', '100', '17', '2014', '2014-0016', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1088', 'Mejoramiento de Circulación Vial en la Redoma Ruiz Pineda, municipio Heres, estado Bolívar.', '1', '2', '8000000.00', '2', '1', '1', '100', '3', '2014', '2014-0017', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1089', 'Rehabilitación de la Iglesia Nuestra Señora del Carmen, municipio el Callao, estado Bolívar.', '10', '49', '1435000.00', '6', '1', '1', '100', '2', '2014', '2014-0018', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1090', 'Plan de Asfaltado de Vialidades, municipio Heres, estado Bolívar.', '1', '53', '15055200.00', '2', '1', '1', '100', '2', '2014', '2014-0019', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1091', 'Aseguramiento de la Producción de Entretenimiento y Turismo Deportivo en el CTE Cachamay parroquia Cachamay, municipio Caroní del estado Bolívar.', '2', '11', '11745802.00', '5', '1', '1', '100', '2', '2014', '2014-0022', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1092', 'Mejoras de Alumbrado de la Cancha Deportiva Cachamaycito, parroquia Cachamay, municipio Caroní del estado Bolívar.', '2', '11', '2000000.00', '5', '1', '1', '100', '8', '2014', '2014-0024', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1093', 'Rehabilitación y Mejoras de Vialidades, municipio el Callao, estado Bolívar.', '10', '49', '3000000.00', '2', '1', '1', '100', '18', '2014', '2014-0025', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1094', 'Rehabilitación de Infraestructuras en Apoyo a la Gran Misión Vivienda Venezuela en el estado Bolívar.', '12', '53', '24141615.43', '5', '1', '1', '100', '3', '2014', '2014-0026', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1095', 'Complemento para la Adquisición e Instalación de la Planta Eléctrica de 200 Kva, para la Maternidad Negra Hipólita, ubicada en San Félix, municipio Caroní, estado Bolívar.', '2', '55', '2102045.78', '4', '1', '1', '100', '4', '2014', '2014-0027', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1096', 'Consolidación y Mejoras Estructurales de Servicios Autónomo Asfaltos Bolívar (SAAB).', '12', '53', '8000000.00', '2', '1', '1', '100', '18', '2014', '2014-0028', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1097', 'Plan de Asfaltado de Vialidades, municipio Caroní, estado Bolívar.', '2', '53', '5970400.00', '2', '1', '1', '100', '2', '2014', '2014-0029', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1098', 'Producción de Alevines para Fortalecer el Centro Piscícola del Orinoco, municipio Heres, estado Bolívar (II Etapa).', '1', '1', '1477784.10', '7', '1', '1', '100', '16', '2014', '2014-0088', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1099', 'Producción de Alevines para Fortalecer el Centro Piscícola del Orinoco, municipio Heres, estado Bolívar.', '1', '1', '1191416.01', '7', '1', '1', '100', '16', '2014', '2014-0030', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1100', 'Plan de Asfaltado de Vialidades, municipio Caroní, estado Bolívar.', '2', '53', '10029600.00', '2', '1', '1', '100', '18', '2014', '2014-0031', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1101', 'Plan de Asfaltado de Vialidades, municipio Heres, estado Bolívar.', '1', '53', '18944800.00', '2', '1', '1', '100', '18', '2014', '2014-0032', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1102', 'Rehabilitación y Mejoras de la Red Eléctrica del Sector Trapichito, parroquia Vista Al Sol, municipio Caroní, estado Bolívar.', '2', '17', '2300000.00', '5', '1', '1', '100', '2', '2014', '2014-0033', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1103', 'Fortalecimiento del Desarrollo Socio-Comunitario a través de la Adquisición de Vehículos en el estado Bolívar.', '12', '53', '12934839.27', '8', '1', '1', '100', '4', '2014', '2014-0035', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1104', 'Consolidación de la Vialidad Rural para el Transporte de Producción Agrícola y Pecuaria, municipio Heres, estado Bolívar.', '1', '53', '10000000.00', '2', '1', '1', '100', '2', '2014', '2014-0036', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1105', 'Consolidación de la Vialidad del municipio Gran Sabana, estado Bolívar.', '9', '53', '3000000.00', '2', '1', '1', '100', '2', '2014', '2014-0037', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1106', 'Consolidación de la Vialidad Rural para el Transporte de Producción Agrícola y Pecuaria, municipio Padre Pedro Chien, estado Bolívar.', '11', '53', '3500000.00', '2', '1', '1', '100', '2', '2014', '2014-0038', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1107', 'Consolidación de la Vialidad Rural para el Transporte de Producción Agrícola y Pecuaria en el Ramal Nº2 Vía Principal de acceso al municipio Padre Pedro Chien, estado Bolívar (II Etapa).', '11', '53', '1500000.00', '2', '1', '1', '100', '2', '2014', '2014-0106', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1108', 'Consolidación de la Vialidad Rural para el Transporte de Producción Agrícola y Pecuaria, en el Dorado Km 88, municipio Sifontes del estado Bolívar.', '5', '34', '3000000.00', '2', '1', '1', '100', '18', '2014', '2014-0039', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1109', 'Consolidación de la Vialidad Rural para el Transporte de Producción Agrícola y Pecuaria de Distintos Rubros en el estado Bolívar.', '12', '53', '2600000.00', '2', '1', '1', '100', '2', '2014', '2014-0040', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1110', 'Consolidación de la Vialidad Rural para el Transporte de Producción Agrícola y Pecuaria de Distintos Rubros en el estado Bolívar.', '1', '3', '3000000.00', '2', '1', '1', '100', '18', '2014', '2014-0061', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1111', 'Fabricación y Reparación de Mesas - Sillas para Fortalecer la Educación del estado Bolívar.', '12', '53', '6000000.00', '6', '1', '1', '100', '2', '2014', '2014-0041', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1112', 'Fortalecer la Economía Popular Comunitaria para Madres Trabajadoras en Situación de Vulnerabilidad Social del Estado Bolívar.', '12', '53', '5000000.00', '6', '1', '1', '100', '5', '2014', '2014-0042', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1113', 'Consolidación de la Vialidad Rural para el Transporte de Producción Agrícola y Pecuaria, municipio Sucre, estado Bolívar.', '7', '53', '1981876.70', '2', '1', '1', '100', '2', '2014', '2014-0044', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1114', 'Fortalecimiento Comunitario del Poder Popular a Través del Equipamiento Urbano para todo el estado Bolívar.', '12', '53', '7600345.84', '6', '1', '1', '100', '4', '2014', '2014-0045', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1115', 'Consolidación de la Vialidad Rural para el Transporte de Producción Agrícola y Pecuaria, municipio Caroní, estado Bolívar.', '2', '53', '11000000.00', '2', '1', '1', '100', '2', '2014', '2014-0046', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1116', 'Construcción de la Red de Aguas Servidas para el Sector Trapichito, parroquia Vista Al Sol, municipio Caroní, estado Bolívar.', '2', '17', '2000000.00', '5', '1', '1', '100', '2', '2014', '2014-0047', '2017-01-09 16:48:42', '2017-01-09 16:48:42', null);
INSERT INTO `proyectos` VALUES ('1117', 'Construcción de la Red de Agua Potable para el Sector Trapichito, parroquia Vista Al Sol, municipio Caroní, estado Bolívar.', '2', '17', '2000000.00', '5', '1', '1', '100', '2', '2014', '2014-0048', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1118', 'Instalación y Puesta en Marcha del Centro Piscícola Maisanta, municipio Caroní, estado Bolívar.', '2', '14', '2000000.00', '7', '1', '1', '100', '16', '2014', '2014-0090', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1119', 'Instalación y Puesta en Marcha del Centro de Producción Piscícola Intensiva en el estado Bolívar.', '1', '8', '3500000.00', '7', '1', '1', '100', '16', '2014', '2014-0049', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1120', 'Construcción de Losa de Piso en el Centro de Atención para el Adulto Mayor Jesús de la Divina Misericordia de las Amazonas, municipio Caroní, estado Bolívar.', '2', '10', '500000.00', '6', '1', '1', '100', '2', '2014', '2014-0050', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1121', 'Rehabilitación y Mejoras de las Infraestructuras Viales (Puentes, Defensas y Drenajes) en el Estado Bolívar.', '12', '53', '14100000.00', '2', '1', '1', '100', '3', '2014', '2014-0051', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1122', 'Remoción de Drenaje Transversal de una Batería de Tubería Metálica D=36\" Progresiva 8+900 de la Antigua Vía Upata, San Félix municipio Piar del estado Bolívar.', '3', '54', '650000.00', '2', '1', '1', '100', '3', '2014', '2014-0052', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1123', 'Plan de Asfaltados de Vialidades en Sectores Populares, municipio Caroní, estado Bolívar (III Etapa).', '2', '53', '8000000.00', '2', '1', '1', '100', '3', '2014', '2014-0053', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1124', 'Rehabilitación de la U.E.N. Creación la Victoria, parroquia Vista Al Sol, municipio Caroní, estado Bolívar.', '2', '17', '500000.00', '3', '1', '1', '100', '2', '2014', '2014-0054', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1125', 'Montaje y Desmontaje de Aire Acondicionados y Fabricación de Ducterias para el Pasillo de Galerías en el Aeropuerto Internacional \"Manuel Carlos Piar\", municipio Caroní, estado Bolívar.', '2', '10', '1092924.20', '5', '1', '1', '100', '14', '2014', '2014-0057', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1126', 'Construcción de Drenaje Transversal en el Km 30, sector San Juan, el Manteco, municipio Piar, estado Bolívar.', '3', '23', '1000000.00', '2', '1', '1', '100', '2', '2014', '2014-0058', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1127', 'Rehabilitación y Mejoras de Centro de Formación de Instructores Pedagógicos, ubicado en Gurí, municipio Bolivariano Angostura, estado Bolívar.', '6', '38', '825416.82', '5', '1', '1', '100', '3', '2014', '2014-0055', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1128', 'Montaje y Desmontaje de Aire Acondicionados y Fabricación de Ducterias en el Área de Hall Central del Aeropuerto Internacional \"Manuel Carlos Piar\", municipio Caroní, estado Bolívar.', '2', '10', '3020046.69', '5', '1', '1', '100', '14', '2014', '2014-0056', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1129', 'Complemento para el Fortalecimiento del Sistema de Emergencias 171 para el estado Bolívar.', '12', '53', '787029.11', '6', '1', '1', '100', '20', '2014', '2014-0059', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1130', 'Rehabilitación y Mejoras de la U.E. Horacio Cabrera en el municipio Sifontes, estado Bolívar.', '5', '52', '600000.00', '3', '1', '1', '100', '2', '2014', '2014-0060', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1131', 'Construcción y Puesta en Marcha del Sistema de Producción Agrícola Bajo Ambiente Controlado, parroquia Panapana, municipio Heres del estado Bolívar.', '1', '8', '698482.39', '7', '1', '1', '100', '16', '2014', '2014-0084', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1132', 'Construcción de la Sala de Beneficio Industrial Avícola (CEPAIB), municipio Heres, estado Bolívar (II Etapa).', '1', '8', '27300000.00', '7', '2', '1', '84', '16', '2014', '2014-0083', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1133', 'Sala de Beneficio Industrial Avícola (CEPAIB), municipio Heres, estado Bolívar.', '1', '8', '7000000.00', '7', '1', '1', '100', '16', '2014', '2014-0034', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1134', 'Centro de Producción de Conserva y Salsas en Centro Agrícola Integral Ezequiel Zamora, parroquia Panapana, municipio Heres del estado Bolívar.', '1', '8', '718657.10', '7', '2', '1', '90', '16', '2014', '2014-0085', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1135', 'Ampliación de Sistema de Producción Avícola, municipio Heres del estado Bolívar (II Etapa).', '1', '8', '19931086.00', '7', '2', '1', '85', '16', '2014', '2014-0086', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1136', 'Ampliación de Sistema de Producción Avícola, municipio Heres del estado Bolívar.', '1', '8', '7000000.00', '7', '1', '1', '100', '16', '2014', '2014-0023', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1137', 'Instalación de una Unidad Industrial de Alimentos Balanceados para Animales, ubicado en el Centro Maisanta, municipio Caroní en el estado Bolívar.', '2', '14', '2500000.00', '7', '1', '1', '100', '16', '2014', '2014-0087', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1138', 'Creación de Empresa para la Consolidación del Turismo en el estado Bolívar.', '12', '53', '1500000.00', '11', '1', '1', '100', '26', '2014', '2014-0067', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1139', 'Fortalecimiento de la Infraestructura del Parque Metropolitano Leonardo Ruiz Pineda, parroquia Vista Hermosa, municipio Heres del estado Bolívar (II Etapa).', '1', '2', '1000000.00', '5', '1', '1', '100', '8', '2014', '2014-0068', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1140', 'Centro de Procesamiento de Producción Agroindustrial Bolívar, Casas de Cultivos Bajo Ambiente Controlado en el municipio Gran Sabana, estado Bolívar.', '9', '47', '3000000.00', '7', '1', '1', '100', '16', '2014', '2014-0089', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1141', 'Instalación y Puesta en Marcha del Centro Lácteo de Upata, parroquia Upata, municipio Piar, estado Bolívar.', '3', '54', '3884598.48', '7', '1', '1', '100', '16', '2014', '2014-0091', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1142', 'Fortalecimiento de Infraestructura del Parque Metropolitano La Llovizna, municipio Caroní del estado Bolívar (II Etapa).', '2', '11', '1000000.00', '5', '1', '1', '100', '8', '2014', '2014-0069', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1143', 'Mejoras de Alumbrado de la Cancha Deportiva Cachamaycito, parroquia Cachamay, municipio Caroní del estado Bolívar (II Etapa).', '2', '11', '1000000.00', '5', '1', '1', '100', '8', '2014', '2014-0070', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1144', 'Ampliación de Vialidad en los Tramos Norte y Sur de Kaballape 1 y 2, Troncal 10, municipio Sifontes del estado Bolívar.', '5', '53', '900000.00', '2', '1', '1', '100', '3', '2014', '2014-0071', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1145', 'Consolidación de la Vialidad Rural para el Transporte de Producción Agrícola y Pecuaria en la Población de Guarataro, municipio Sucre, estado Bolívar (II Etapa).', '7', '40', '1018123.31', '2', '1', '1', '100', '2', '2014', '2014-0072', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1146', 'Construcción del Drenaje Transversal en el Km 30, sector San Juan, El Manteco, municipio Piar del estado Bolívar (II Etapa).', '3', '23', '500000.00', '2', '1', '1', '100', '2', '2014', '2014-0092', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1147', 'Complemento para la Construcción de Planta de Agua Potable de 600 LPS, Acueducto Oeste Ciudad Bolívar, municipio Heres del estado Bolívar.', '1', '6', '7000000.00', '1', '1', '1', '100', '1', '2014', '2014-0093', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1148', 'Rehabilitación de la Iglesia de Nuestra Señora del Carmen, municipio el Callao del estado Bolívar (II Etapa).', '10', '49', '3816517.60', '6', '1', '1', '100', '2', '2014', '2014-0094', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1149', 'Complemento para la Culminación de la Construcción de Sede de la Orquesta Sinfónica de Ciudad Bolívar, municipio Heres del estado Bolívar.', '1', '2', '2800000.00', '6', '1', '1', '100', '2', '2014', '2014-0095', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1150', 'Acondicionamiento y Mejora de Instalaciones Administrativas y Almacén ubicado en el Estadio de Béisbol La Ceiba, municipio Caroní del estado Bolívar.', '2', '13', '6000000.00', '5', '1', '1', '100', '2', '2014', '2014-0096', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1151', 'Mejoras en el Colector Principal del Sector Core 8 UD-337, parroquia Unare, municipio Caroní estado Bolívar.', '2', '10', '3000000.00', '1', '1', '1', '100', '1', '2014', '2014-0074', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1152', 'Rehabilitación de Cancha Deportiva en el Liceo Monseñor de Zabaleta, sector Zabaleta en  Tumeremo, municipio Sifontes del estado Bolívar.', '5', '52', '1500000.00', '3', '1', '1', '100', '2', '2014', '2014-0075', '2017-01-09 16:48:43', '2017-01-09 16:48:43', null);
INSERT INTO `proyectos` VALUES ('1153', 'Construcción de Centro de Educación Inicial, parroquia Unare, municipio Caroní, estado Bolívar.', '2', '10', '12000000.00', '3', '1', '1', '100', '3', '2014', '2014-0076', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1154', 'Rehabilitación del Centro de Educación Inicial Dr. José Manuel Agosto Méndez, ubicada en el paseo Moreno de Mendoza, parroquia Vista Hermosa, Ciudad Bolívar, municipio Heres, estado Bolívar.', '1', '2', '800000.00', '3', '1', '1', '100', '2', '2014', '2014-0077', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1155', 'Construcción de Canchas de Usos Múltiples en Terrazas del Caroní, Sector A, parroquia Unare, municipio Caroní, estado Bolívar (II Etapa).', '2', '10', '500000.00', '5', '1', '1', '100', '3', '2014', '2014-0078', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1156', 'Recuperación y Mejoras de Cancha Deportiva Casa de los Niños El Libertador, Upata, municipio Piar del estado Bolívar.', '3', '54', '3975918.39', '5', '1', '1', '100', '3', '2014', '2014-0079', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1157', 'Construcción de la Cancha Techada Los Corales (Área de Paz) UD-339, Eje Atlántico, parroquia Unare, municipio Caroní del estado Bolívar.', '2', '10', '4024081.61', '5', '1', '1', '100', '3', '2014', '2014-0080', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1158', 'Reconstrucción de Drenaje Transversal de dos baterías con tubería de concreto D=60\" en la progresiva 6+650 de la carretera vieja Upata - San Félix, municipio Caroní, estado Bolívar.', '2', '14', '1588757.79', '2', '1', '1', '100', '3', '2014', '2014-0081', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1159', 'Construcción de Cerca Perimetral Escuela Eduardo Oxford, Guasipati, municipio Roscio del estado Bolívar (II Etapa).', '8', '44', '848472.85', '3', '1', '1', '100', '2', '2014', '2014-0099', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1160', 'Construcción de Habitación Tipo Estudio en Área del Ambulatorio Brisas del Sur II \"Hugo Chávez Frías\", parroquia La Sabanita, municipio Heres del estado Bolívar.', '1', '6', '600000.00', '4', '1', '1', '100', '2', '2014', '2014-0100', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1161', 'Construcción de Alcantarilla Simple D=36\" en la progresiva 5+700 de la carretera vieja Upata - San Félix, en el sector La Recría, municipio Piar, estado Bolívar.', '3', '54', '1165345.48', '2', '1', '1', '100', '3', '2014', '2014-0082', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1162', 'Consolidación de la Vialidad Rural para el Transporte de Producción Agrícola y Pecuaria de Distintos Rubros en Sectores de la parroquia José Antonio Páez, Como Moja Casabe, Antonio José de Sucre y los Báez, municipio Heres, estado Bolívar (II Etapa).', '1', '3', '2400000.00', '2', '1', '1', '100', '2', '2014', '2014-0107', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1163', 'Ampliación Planta Potabilizadora y Estación de Rebombeo de Tumeremo, municipio Sifontes, estado Bolívar.', '5', '52', '55000000.00', '1', '2', '8', '72.38', '1', '2014', '-', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1164', 'Culminación Acueducto Este de San Félix UD-128, municipio Caroní, estado Bolívar.', '2', '55', '4000000.00', '1', '1', '8', '100', '1', '2014', '-', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1165', 'Aducción Avenida Norte Sur 6, Norte Sur 4, para Fortalecer el Sistema de Agua Potable del Sector Rio Sipapo y UD-339 y UD-340, municipio Caroní, estado Bolívar.', '2', '10', '17000000.00', '1', '2', '8', '61.71', '1', '2014', '-', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1166', 'Instalación de Tubería de Aducción Planta Potabilizadora Santa Rosa a Estanque Monserrat en Upata, municipio Piar, estado Bolívar. (II Etapa)', '3', '54', '12200000.00', '1', '2', '8', '85', '1', '2014', '-', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1167', 'Construcción de Planta Potabilizadora de 40 Lps Cupapuicito, Upata, municipio Piar, estado Bolívar.', '3', '54', '18000000.00', '1', '1', '8', '100', '1', '2014', '-', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1168', 'Fiesta del Asfalto en las Diferentes Áreas Municipales del estado Bolívar.', '12', '53', '50000000.00', '2', '1', '9', '100', '3', '2014', '-', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1169', 'Rehabilitación de la Avenida Leopoldo Sucre Figarella sentido San Félix – Puerto Ordaz, municipio Caroní del estado Bolívar. ', '2', '31', '40000000.00', '2', '1', '9', '100', '3', '2014', '-', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1170', 'Adquisición de 200 Km de Tuberías de Distintos Diámetros, Transformadores y Dosificadores de Cloro para Fortalecer las Redes de Agua Potable en el estado Bolívar.', '12', '53', '18000000.00', '1', '1', '10', '100', '1', '2014', '2014-0063', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1171', 'Construcción del Liceo Bolivariano en el municipio Sifontes del estado Bolívar.', '5', '52', '20000000.00', '3', '1', '10', '100', '3', '2014', '2014-0062', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1172', 'Rehabilitación, Acondicionamiento, Dotación de Equipos y Medicamentos para el Ambulatorio Rural en la Comunidad del Tauca, municipio Sucre del estado Bolívar.', '7', '53', '3800000.00', '4', '1', '11', '100', '3', '2014', '2014-0104', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1173', 'Reestructuración de la Escuela Doctor José Ángel Ruiz (Techos, Agua Potable, Baños, Ventiladores, Sillas y Paredes), sector El Roble, parroquia Simón Bolívar, municipio Caroní del estado Bolívar.', '2', '13', '1200000.00', '5', '1', '11', '100', '2', '2014', '2014-0102', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1174', 'Empresa Piscícola Comunitaria 24 de Julio, ubicada en el  Nuevo Barrio Tricolor del sector comunidad 24 de Julio, parroquia Marhuanta, municipio Heres del estado Bolívar.', '1', '4', '8500000.00', '7', '1', '11', '100', '16', '2014', '2014-0103', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1175', 'Rehabilitación y Mejoras de la Red de Distribución Eléctrica de Alta y Baja Tensión del sector Vista Al Sol, parroquia Vista Al Sol, municipio Caroní del estado Bolívar.', '2', '17', '1515302.76', '5', '1', '11', '100', '2', '2014', '2014-0105', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1176', 'Complemento para la Construcción de Planta de Agua Potable de 600 LPS, Acueducto Oeste, Ciudad Bolívar, municipio Heres, estado Bolívar (II Etapa).', '1', '6', '3033909.72', '1', '1', '1', '100', '1', '2014', '2014-0108', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1177', 'Culminación de la Sede del Adulto Mayor ubicada en la UD-245, parroquia Universidad, municipio Caroní, estado Bolívar (II Etapa).', '2', '18', '2000000.00', '6', '1', '1', '100', '3', '2014', '2014-0109', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1178', 'Culminación de la Sede del Adulto Mayor ubicada en la UD-245, parroquia Universidad, municipio Caroní, estado Bolívar (II Etapa).', '2', '18', '13252400.80', '6', '1', '1', '100', '3', '2014', '2014-0109', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1179', 'Reparación y Mejoras del Hospital Ruiz y Páez, municipio Heres, estado Bolívar.', '1', '1', '2069908.91', '4', '1', '1', '100', '2', '2014', '2014-0110', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1180', 'Impermeabilización de Azotea del Bloque 33 de la Urbanización Unare III, parroquia Unare, municipio Caroní del estado Bolívar.', '2', '10', '600000.00', '5', '1', '1', '100', '3', '2014', '2014-0111', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1181', 'Alimentos Bolívar, Unidad Socialista destinada a la Preservación, Comercialización a Precios Socialistas de Alimentos de Primera Calidad, municipio Heres, estado Bolívar (II Etapa).', '1', '1', '3500000.00', '7', '1', '1', '100', '27', '2014', '2014-0112', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1182', 'Construcción de Redes Eléctricas (Alta y Baja Tensión) de Urbanismo Villa Bolívar, sector Terrazas del Caroní, municipio Caroní del estado Bolívar.', '2', '10', '2000000.00', '5', '1', '1', '100', '3', '2014', '2014-0114', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1183', 'Construcción de Sistema de Recolección de Aguas Servidas para Urbanismo Villa Bolívar, sector Terrazas del Caroní, municipio Caroní del Estado Bolívar.', '2', '10', '1000000.00', '5', '1', '1', '100', '3', '2014', '2014-0115', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1184', 'Reparación Falla de Borde Km 74.5 Autopista sentido Puerto Ordaz – Ciudad Bolívar, estado Bolívar.', '12', '53', '277691.37', '2', '1', '1', '100', '3', '2014', '2014-0116', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1185', 'Rehabilitación y Mejoras de la Calle Neverí (tramo, carretera Suata – Materiales Reyfeca) carrera Guere (tramo, calle Neverí – calle Ipire), municipio Caroní del estado Bolívar.', '2', '10', '539012.42', '2', '1', '1', '100', '3', '2014', '2014-0117', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1186', 'Dotación del Centro Lácteo Upata, municipio Piar del estado Bolívar.', '3', '54', '1500000.00', '7', '1', '6', '100', '16', '2014', '2014-0118', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1187', 'Complemento para la Construcción de Galpón y Equipamiento para Procesadora de Hortalizas, municipio Heres del Estado Bolívar.', '1', '8', '1700000.00', '7', '1', '6', '100', '16', '2014', '2014-0119', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1188', 'Culminación para el Mejoramiento de Circulación Vial en la Redoma Ruíz Pineda, municipio Heres, estado Bolívar.', '1', '2', '3200000.00', '2', '1', '6', '100', '3', '2014', '2014-0120', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1189', 'Complemento para la Culminación del Acueducto Cupapuicito, municipio Piar del estado Bolívar.', '3', '54', '4000000.00', '1', '1', '6', '100', '1', '2014', '2014-0121', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1190', 'Intervención del Sistema de Drenaje para Alivio de la Cárcava de La Victoria, parroquia Vista Al Sol, municipio Caroní del estado Bolívar (II Etapa).', '2', '17', '5000000.00', '5', '1', '6', '100', '3', '2014', '2014-0122', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1191', 'Construcción de Aceras y Brocales en el Urbanismo Villa Bolívar, municipio Caroní del estado Bolívar.', '2', '10', '1000000.00', '5', '1', '6', '100', '3', '2014', '2014-0124', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1192', 'Culminación de la Sede del Adulto Mayor ubicada en la UD-245, parroquia Universidad, municipio Caroní, estado Bolívar ( II Etapa - Complemento).', '2', '18', '2000000.00', '6', '1', '2', '100', '3', '2014', '2014-0125', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1193', 'Adquisición de Equipos de Computación, Reproducción y Audiovisuales para Centros de Coordinación Policial del estado Bolívar.', '12', '53', '4000000.00', '9', '1', '1', '100', '4', '2014', '2014-0012', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1194', 'Adquisición de Mobiliarios y Equipos para la Policía del estado Bolívar. ', '12', '53', '4000000.00', '8', '1', '1', '100', '4', '2015', '2015-0003', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1195', 'Adquisición de Uniformes y Calzados para el Personal del Cuerpo Policial del estado Bolívar.', '12', '53', '43000000.00', '9', '1', '1', '100', '4', '2015', '2015-0002', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1196', 'Adquisición de Equipos de Radio Comunicación para Centros de Coordinación Policial del estado Bolívar.', '12', '53', '5000000.00', '9', '1', '1', '100', '4', '2015', '2015-0006', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1197', 'Rehabilitación y Mejoras de U.E.E. La Esmeralda, municipio Sucre del estado Bolívar.', '7', '41', '700000.00', '3', '1', '1', '100', '2', '2015', '2015-0007', '2017-01-09 16:48:44', '2017-01-09 16:48:44', null);
INSERT INTO `proyectos` VALUES ('1198', 'Rehabilitación y Mejoras de la U.E Las Tres Rosas, parroquia Tumeremo, municipio Sifontes del estado Bolívar.', '5', '52', '2000000.00', '3', '1', '1', '100', '2', '2015', '2015-0008', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1199', 'Rehabilitación y Mejoras de U.E.E. Simón Bolívar, parroquia Caicara del Orinoco, municipio Cedeño del estado Bolívar.', '4', '30', '1000000.00', '3', '1', '1', '100', '2', '2015', '2015-0009', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1200', 'Rehabilitación y Mejoras de U.E.E. Teresa de la Parra, parroquia Chirica, municipio Caroní, estado Bolívar.', '2', '12', '1100000.00', '3', '1', '1', '100', '2', '2015', '2015-0010', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1201', 'Rehabilitación y Mejoras de U.E.E. Josefita Osuna, municipio Padre Pedro Chien del estado Bolívar.', '11', '51', '1000000.00', '3', '1', '1', '100', '2', '2015', '2015-0011', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1202', 'Rehabilitación y Mejoras de U.E.B.E. Juan Bautista González, parroquia La Sabanita, municipio Heres del estado Bolívar.', '1', '6', '1000000.00', '3', '1', '1', '100', '2', '2015', '2015-0012', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1203', 'Intervención del Sistema de Drenaje para Alivio de la Cárcava de La Victoria, parroquia Vista Al Sol, municipio Caroní, estado Bolívar (III Etapa).', '2', '17', '10000000.00', '5', '1', '1', '100', '3', '2015', '2015-0013', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1204', 'Intervención de Cárcava en las Adyacencias del Hospital Uyapar, UD-245, parroquia Universidad, municipio Caroní del estado Bolívar  (II Etapa).', '2', '18', '5500000.00', '5', '1', '1', '100', '3', '2015', '2015-0014', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1205', 'Fortalecimiento de Infraestructura, Reparación y mejoras de Cancha Deportiva Casa de los Niños el Libertador, Upata, municipio Piar  del  estado Bolívar (II Etapa).', '3', '54', '703030.66', '6', '1', '1', '100', '3', '2015', '2015-0015', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1206', 'Rehabilitación y Bacheo de la Carretera San Félix - El Pao, parroquia Pozo Verde, municipio Caroní del estado Bolívar.', '2', '16', '10000000.00', '2', '1', '1', '100', '3', '2015', '2015-0016', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1207', 'Rehabilitación y Bacheo de la Carretera Upata - El Manteco, municipio Piar del estado Bolívar.', '3', '54', '10000000.00', '2', '1', '1', '100', '3', '2015', '2015-0017', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1208', 'Rehabilitación y Bacheo de la Carretera Upata - El Pao.  Progresiva 10+000 -28+650, municipio Piar del estado Bolívar.', '3', '54', '9875262.21', '2', '1', '1', '100', '3', '2015', '2015-0018', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1209', 'Complemento para la Instalación de Parque Infantil en la Urbanización Los Próceres, parroquia Agua Salada, municipio Heres del estado Bolívar.', '1', '5', '336850.80', '6', '1', '1', '100', '3', '2015', '2015-0019', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1210', 'Complemento para la Construcción de Cancha Techada, sector Maipure, parroquia Marhuanta, municipio Heres del estado Bolívar.', '1', '4', '976739.75', '3', '1', '1', '100', '3', '2015', '2015-0020', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1211', 'Remoción de Alcantarillados Colapsados con 02 Dos Baterías de Tubos Metálicos de 42\" sustituyéndolas por un Sistema    de    Alcantarillado   Adecuado   de   42”  sector La Casualidad, municipio Cedeño, estado Bolívar.', '4', '24', '2400000.00', '5', '1', '1', '100', '3', '2015', '2015-0021', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1212', 'Rehabilitación de la Electrificación Conjunto Residencial Bicentenario, UD-325, sector Paratepuy, parroquia Unare, municipio Caroní, estado Bolívar.', '2', '10', '1000000.00', '5', '1', '1', '100', '3', '2015', '2015-0022', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1213', 'Rehabilitación de la Electrificación del sector Valle Alto, parroquia 11 de Abril, municipio Caroní, estado Bolívar.', '2', '55', '2000000.00', '5', '1', '1', '100', '2', '2015', '2015-0023', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1214', 'Rehabilitación y Mejoras del Gimnasio Cubierto de la UNEXPO, municipio Caroní del estado Bolívar.', '2', '18', '2000000.00', '3', '1', '1', '100', '2', '2015', '2015-0024', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1215', 'Ampliación de Aulas en la Universidad Nacional Experimental de Guayana (UNEG), extensión Caicara, municipio Cedeño del estado Bolívar.', '4', '30', '6000000.00', '3', '1', '1', '100', '3', '2015', '2015-0025', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1216', 'Rehabilitación y Mejoras del C.E.I. Rayo de Luz, municipio Heres del estado Bolívar.', '1', '3', '463780.88', '3', '1', '1', '100', '3', '2015', '2015-0026', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1217', 'Construcción de Aulas en el Preescolar Nicolas Antonio Farreras, municipio El Callao del estado Bolívar.', '10', '49', '3600000.00', '3', '1', '1', '100', '3', '2015', '2015-0027', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1218', 'Rehabilitación y Mejoras de U.E.E. Narciso Fragachan, parroquia Catedral, municipio Heres del estado Bolívar.', '1', '1', '2000000.00', '3', '1', '1', '100', '2', '2015', '2015-0028', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1219', 'Rehabilitación y Mejoras de U.E.E. José Félix Ribas, barrio La Grua, parroquia Simón Bolívar, municipio Caroní del estado Bolívar.', '2', '13', '1200000.00', '3', '1', '1', '100', '2', '2015', '2015-0029', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1220', 'Rehabilitación y Mejoras de U.E.E. Carmita Rávago, municipio Roscio, estado Bolívar.', '8', '44', '1200000.00', '3', '1', '1', '100', '2', '2015', '2015-0030', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1221', 'Rehabilitación y Mejoras de U.E.E. Francisco Michelena y Rojas, municipio Gran Sabana, estado Bolívar.', '9', '57', '681198.32', '3', '1', '1', '100', '2', '2015', '2015-0031', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1222', 'Rehabilitación y Mejoras de E.B.E. El Cristo, municipio Bolivariano Angostura, estado Bolívar.', '6', '36', '1000000.00', '3', '1', '1', '100', '2', '2015', '2015-0032', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1223', 'Reparaciones de Maquinarias Pesadas para el estado Bolívar.', '12', '53', '18000000.00', '5', '1', '1', '100', '2', '2015', '2015-0033', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1224', 'Rehabilitación y mejoras de vías de accesos Avenida Nueva Granada, parroquia Vista Hermosa, municipio Heres del estado Bolívar.', '1', '2', '1447160.00', '2', '1', '1', '100', '2', '2015', '2015-0034', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1225', 'Rehabilitación y mejoras de la calle Páez, parroquia La Sabanita, municipio Heres del estado Bolívar.', '1', '6', '4341480.00', '2', '1', '1', '100', '2', '2015', '2015-0035', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1226', 'Culminación de la Sede del Adulto Mayor UD-245, parroquia Universidad, municipio Caroní del estado Bolívar (II Etapa).', '2', '18', '10416784.00', '6', '1', '1', '100', '28', '2015', '2015-0036', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1227', 'Culminación de la Sede del Adulto Mayor UD-245, parroquia Universidad, municipio Caroní del estado Bolívar (II Etapa).', '2', '18', '9752400.80', '6', '1', '1', '100', '3', '2015', '2015-0037', '2017-01-09 16:48:45', '2017-01-09 16:48:45', null);
INSERT INTO `proyectos` VALUES ('1228', 'Culminación de la Sede del Adulto Mayor UD-245, parroquia Universidad, municipio Caroní del estado Bolívar (II Etapa).', '2', '18', '3910081.89', '6', '1', '1', '100', '3', '2015', '2015-0037-1', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1229', 'Alimentos Bolívar, Unidad Socialista Destinada Para La Preservación, Comercialización a Precios Socialistas de Alimentos de Primera Calidad, municipio Heres, estado Bolívar. (Rehabilitación de Galpón y Adquisición de Equipos)', '1', '1', '16000000.00', '7', '1', '1', '100', '8', '2015', '2015-0038', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1230', 'Complemento para la Instalación de una Unidad Industrial de Alimentos Balanceados para Animales, ubicado en el Centro Maisanta, municipio Caroní, estado Bolívar.', '2', '14', '8000000.00', '7', '2', '1', '76', '16', '2015', '2015-0039', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1231', 'Adecuación y Mejoras para el Equipamiento del Centro Lácteo de Upata en el municipio Piar del estado Bolívar.', '3', '54', '8495120.80', '7', '1', '1', '100', '16', '2015', '2015-0041', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1232', 'Planta Procesadora de Pescado en el Centro Agrícola Ezequiel Zamora, municipio Heres del estado Bolívar.', '1', '8', '718195.69', '7', '1', '1', '100', '16', '2015', '2015-0042', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1233', 'Instalación y Puesta en Marcha del Centro de Producción Piscícola Intensiva en el Centro Agrícola Ezequiel Zamora, en el municipio Heres del estado Bolívar.', '1', '8', '20000000.00', '7', '2', '1', '35', '16', '2015', '2015-0043', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1234', 'Consolidación del Centro Agrícola Integral Ezequiel Zamora, municipio Heres del estado Bolívar. ', '1', '8', '12000000.00', '7', '1', '1', '100', '16', '2015', '2015-0045', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1235', 'Complemento para Sala de Beneficio Industrial Avícola (matadero) en el Centro Agrícola Ezequiel Zamora, municipio Heres del estado Bolívar.', '1', '8', '7000000.00', '7', '2', '1', '84', '16', '2015', '2015-0046', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1236', 'Complemento para Equipamiento de la Planta Procesadora de Hortalizas, municipio Heres del Estado Bolívar.', '1', '8', '7000000.00', '7', '1', '1', '100', '16', '2015', '2015-0047', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1237', 'Construcción de Instalaciones para Producción de Semillas Certificadas  y Mejoramiento de Práctica de Manejo de Cultivo, en el Centro Agrícola Ezequiel Zamora, municipio Heres, estado Bolívar (I Etapa).', '1', '8', '40000000.00', '7', '2', '1', '89', '16', '2015', '2015-0048', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1238', 'Fortalecimiento de Empresa Operadora Turística Bolívar en el municipio Heres del estado Bolívar.', '1', '1', '2000000.00', '11', '1', '1', '100', '26', '2015', '2015-0049', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1239', 'Ampliación de la Red Eléctrica de Media y Baja Tensión, Alumbrado Público del sector La Independencia, municipio Heres, estado Bolívar.', '1', '1', '2000000.00', '5', '1', '1', '100', '2', '2015', '2015-0051', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1240', 'Instalación de Tuberías en el sector Santa Rosa, municipio Piar del estado Bolívar.', '3', '23', '10000000.00', '1', '2', '1', '85', '1', '2015', '2015-0052', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1241', 'Aducción de Tubería desde la Avenida Norte Sur, municipio Caroní del estado Bolívar.', '2', '10', '8000000.00', '1', '1', '1', '100', '1', '2015', '2015-0053', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1242', 'Parque Urbano en el sector Los Mangos Villa Miranda, parroquia Universidad, municipio Caroní del estado Bolívar.', '2', '18', '590172.00', '5', '1', '1', '100', '3', '2015', '2015-0055', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1243', 'Ampliación y Mejoras de Parque Urbano en el Cerro el Gallo, parroquia Simón Bolívar, municipio Caroní del estado Bolívar.', '2', '13', '417000.00', '5', '1', '1', '100', '3', '2015', '2015-0056', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1244', 'Construcción de Parque Urbano Los Corales UD-339, municipio Caroní del estado Bolívar.', '2', '10', '690000.00', '5', '1', '1', '100', '3', '2015', '2015-0057', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1245', 'Complemento para la Instalación de Parque Infantil en Brisas del Orinoco, parroquia La Sabanita, municipio Heres del estado Bolívar.', '1', '6', '173089.87', '5', '1', '1', '100', '3', '2015', '2015-0058', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1246', 'Vía de Acceso, Áreas Exteriores y Ornato del Taller de Mantenimiento de Transporte, Transbolívar, municipio Caroní, estado Bolívar.', '2', '10', '10851225.38', '2', '1', '1', '100', '3', '2015', '2015-0059', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1247', 'Rehabilitación y mejoras de vías de accesos en la UD-137, parroquia Vista Al Sol, municipio Caroní del estado Bolívar.', '2', '17', '3851978.13', '2', '1', '1', '100', '2', '2015', '2015-0061', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1248', 'Rehabilitación y mejoras de vías de accesos en la parroquia Simón Bolívar, sector San Rafael y Colinas de Pinto Salinas,  municipio Caroní del estado Bolívar.', '2', '13', '3968402.15', '2', '1', '1', '100', '2', '2015', '2015-0062', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1249', 'Rehabilitación y mejoras de vías de accesos en la Parroquia Once de Abril, Sector 25 de Marzo y Villa El Eden, municipio Caroní del estado Bolívar.', '2', '55', '3851978.13', '2', '1', '1', '100', '2', '2015', '2015-0063', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1250', 'Rehabilitación y mejoras de vías de accesos en Chirica Vieja y Francisca Duarte,  parroquia Chirica, municipio Caroní del estado Bolívar.', '2', '12', '3555672.12', '2', '1', '1', '100', '2', '2015', '2015-0064', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1251', 'Rehabilitación y mejoras de vías de accesos en el sector Esperancita,  parroquia Universidad, municipio Caroní del estado Bolívar.', '2', '18', '677270.88', '2', '1', '1', '100', '2', '2015', '2015-0065', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1252', 'Rehabilitación y mejoras de vías de acceso en la parroquia Dalla Costa, sector Francisco de Miranda,   municipio Caroní del estado Bolívar.', '2', '31', '1481530.05', '2', '1', '1', '100', '2', '2015', '2015-0067', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1253', 'Rehabilitación y mejoras de vías de accesos a la calle Andrés Eloy Blanco, parroquia Vista Hermosa, municipio Heres del estado Bolívar.', '1', '2', '1085370.00', '2', '1', '1', '100', '2', '2015', '2015-0068', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1254', 'Rehabilitación y mejoras de vías de accesos en la av. República y paseo Gaspari, parroquia Catedral, municipio Heres del estado Bolívar.', '1', '1', '6584578.00', '2', '1', '1', '100', '2', '2015', '2015-0069', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1255', 'Rehabilitación y mejoras de vías de accesos en paseo Heres y calle Maracay, parroquia Catedral, municipio Heres del estado Bolívar.', '1', '1', '4329192.00', '2', '1', '1', '100', '2', '2015', '2015-0070', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1256', 'Rehabilitación y mejoras de vías de accesos en la Av. Germania y Upata, parroquia Catedral, municipio Heres del estado Bolívar.', '1', '1', '3979690.00', '2', '1', '1', '100', '2', '2015', '2015-0071', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1257', 'Rehabilitación y mejoras de vías de accesos a la redoma del psiquiatrico, parroquia Catedral, municipio Heres del estado Bolívar.', '1', '1', '723580.00', '2', '1', '1', '100', '2', '2015', '2015-0072', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1258', 'Rehabilitación y mejoras de vías de accesos en la calle principal de Cardozo, parroquia José Antonio Páez, municipio Heres del estado Bolívar.', '1', '3', '1808950.00', '2', '1', '1', '100', '2', '2015', '2015-0073', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1259', 'Consolidación y Mejoras de Vialidades Agrícola en el sector el Buey municipio Piar del estado Bolívar.', '3', '54', '8000000.00', '2', '1', '1', '100', '2', '2015', '2015-0074', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1260', 'Consolidación y Mejoras de Vialidades Agrícola en la parroquia Andrés Eloy Blanco (Las Cocuizas, Los Cantiles y La Morrocoya) municipio Piar del estado Bolívar.', '3', '22', '8000000.00', '2', '1', '1', '100', '2', '2015', '2015-0075', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1261', 'Construcción de drenaje transversales simple con tubería de concreto D=48\" en la progresiva 671+00 y 674+100 de la troncal 10, municipio Sifontes del estado Bolívar.', '5', '34', '20000000.00', '5', '1', '1', '100', '3', '2015', '2015-0076', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1262', 'Construcción de Liceo en Brisas del Este I, municipio Heres, estado Bolívar.', '1', '5', '20000000.00', '3', '1', '1', '100', '3', '2015', '2015-0077', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1263', 'Proyecto Recreacional y Deportivo en Villa Polígono, UD-149, parroquia Dalla Costa, municipio Caroní del estado Bolívar.', '2', '31', '3700000.00', '5', '1', '1', '100', '3', '2015', '2015-0078', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1264', 'Construcción de Parada Urbana y Gimnasio a Cielo Abierto en Villa Polígono, UD-149, parroquia Dalla Costa, municipio Caroní del estado Bolívar.', '2', '31', '1500000.00', '5', '1', '1', '100', '3', '2015', '2015-0079', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1265', 'Construcción de 2km de Canal de Drenaje en el sector Sueños de Bolívar, municipio Caroní, estado Bolívar.', '2', '10', '12000000.00', '1', '1', '1', '100', '1', '2015', '2015-0080', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1266', 'Fortalecimiento para la fabricación de mesas - sillas, incorporando la fabricación de perfiles metálicos, puertas y ventanas en apoyo a la Gran Misión Vivienda Venezuela en el estado Bolívar (II Etapa).', '12', '53', '5000000.00', '6', '1', '1', '100', '2', '2015', '2015-0081', '2017-01-09 16:48:46', '2017-01-09 16:48:46', null);
INSERT INTO `proyectos` VALUES ('1267', 'Fortalecimiento Comunitario del Poder Popular a través del Equipamiento Urbano en el estado Bolívar.', '12', '53', '924852.19', '6', '1', '3', '100', '4', '2015', '2015-0082', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1268', 'Fortalecimiento Comunitario del Poder Popular a través del Equipamiento Urbano en el estado Bolívar.', '12', '53', '1414845.41', '6', '1', '2', '100', '4', '2015', '2015-0083', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1269', 'Suministro de Materiales para el Mantenimiento de los Equipos de Bombeo de los diferentes Acueductos del estado Bolívar.', '12', '53', '60547200.00', '1', '1', '6', '100', '1', '2015', '2015-0084', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1270', 'Rehabilitación y Mejoras de la Iglesia Siervas del Santísimo Sacramento (Patrimonio Histórico), municipio Heres, estado Bolívar.', '1', '1', '3600000.00', '6', '1', '1', '100', '2', '2015', '2015-0085', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1271', 'Rehabilitación y Mejoras de la Casa de la Mujer en el municipio Heres del estado Bolívar.', '1', '1', '3000000.00', '6', '1', '1', '100', '2', '2015', '2015-0086', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1272', 'Adecuación y mejoras del Centro Agrícola Maisanta, municipio caroní del estado Bolívar', '2', '14', '21250000.00', '7', '2', '1', '76', '16', '2015', '2015-0087', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1273', 'Rehabilitación y mejoras de infraestructura de la producción de hortalizas en el Centro Agrícola Ezequiel Zamora, municipio Heres del estado Bolívar.', '1', '8', '10270000.00', '7', '1', '1', '100', '16', '2015', '2015-0088', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1274', 'Adecuación y mejoras del sistema de producción avícola en el centro agrícola Ezequiel Zamora, municipio Heres del estado Bolívar.', '1', '8', '30000000.00', '7', '2', '1', '85', '16', '2015', '2015-0089', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1275', 'Acondicionamiento y Mejoras de las Instalaciones Administrativas y Almacén ubicado en el Estadio de Béisbol La Ceiba, parroquia Simón Bolívar, San Félix, municipio Caroní del estado Bolívar. (II Etapa)', '2', '13', '3000000.00', '5', '1', '1', '100', '2', '2015', '2015-0090', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1276', 'Construcción de cancha en Liceo Bolivariano en el municipio Sifontes, estado Bolívar.', '5', '52', '6000000.00', '3', '1', '1', '100', '3', '2015', '2015-0093', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1277', 'Rehabilitación y  mejoras del Terminal de pasajero de Upata, municipio Piar, estado Bolívar.', '3', '54', '8000000.00', '5', '1', '1', '100', '3', '2015', '2015-0094', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1278', 'Adecuación y mejoras de el mercado municipal de El Callao, municipio El Callao, estado Bolívar.', '10', '49', '8000000.00', '5', '1', '1', '100', '3', '2015', '2015-0095', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1279', 'Rehabilitación y Mejoras de las redes eléctricas de la comunidad de Francisca Duarte, Parroquia Chirica, Municipio Caroní, Estado Bolívar.', '2', '12', '3000000.00', '5', '1', '1', '100', '2', '2015', '2015-0096', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1280', 'Consolidación y Mejoras de la Vialidad del Campo A1, Ciudad Piar, municipio Bolivariano Angostura, estado Bolívar.', '6', '36', '3000000.00', '2', '1', '1', '100', '2', '2015', '2015-0097', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1281', 'Rehabilitación y mejoras de la Avenida Libertador y calle Mario Valera, Municipio Cedeño, estado Bolívar.', '4', '30', '3000000.00', '2', '1', '1', '100', '2', '2015', '2015-0098', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1282', 'Rehabilitación y mejoras de vías de acceso a la carretera principal del Sector Manganeso, municipio Padre Pedro Chien, estado Bolívar.', '11', '51', '3000000.00', '2', '1', '1', '100', '2', '2015', '2015-0100', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1283', 'Rehabilitación y mejoras de vías de acceso de la calle principal de Lomas de Tumeremo y Moriche 2, municipio Sifontes, estado Bolívar.', '5', '52', '3000000.00', '2', '1', '1', '100', '2', '2015', '2015-0101', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1284', 'Rehabilitación y mejoras de vías de acceso a la calle Venezuela, sector Kewey, municipio Gran Sabana, estado Bolívar.', '9', '47', '3000000.00', '2', '1', '1', '100', '2', '2015', '2015-0102', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1285', 'Rehabilitación y mejoras de Ambulatorio Tipo III, Guri, municipio Bolivariano Angostura, estado Bolívar (II Etapa).', '6', '38', '6000000.00', '4', '1', '1', '100', '3', '2015', '2015-0103', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1286', 'Rehabilitación y mejoras de la Escuela de Música \"Mosensol\", municipio Caroní, estado Bolívar.', '2', '13', '3000000.00', '3', '1', '1', '100', '2', '2015', '2015-0104', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1287', 'Rehabilitación y mejoras del Centro Total de Entretenimiento Cachamay, municipio Caroní del estado Bolívar.', '2', '11', '5000000.00', '5', '1', '1', '100', '2', '2015', '2015-0105', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1288', 'Rehabilitación de Equipos mayores para el fortalecimiento de las plantas de Asfaltos ubicadas en los municipios Heres, Caroní y Piar del estado Bolívar.', '2', '53', '6000000.00', '8', '1', '1', '100', '18', '2015', '2015-0106', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1289', 'Construcción de Redes de Aguas Servidas en la calle Nueva Esperanza, Sector El Mereyal I, parroquia Catedral,  Municipio Heres, estado Bolívar. ', '1', '1', '3000000.00', '1', '2', '1', '30', '2', '2015', '2015-0107', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1290', 'Construcción de Centro de Educación Inicial en la parroquia Unare, municipio Caroní, estado Bolívar. (II Etapa)', '2', '10', '15000000.00', '3', '1', '1', '100', '3', '2015', '2015-0108', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1291', 'Construcción de Módulos de aulas para el Liceo de Agua Salada, Parroquia Agua Salada, Municipio Heres, estado Bolívar.', '1', '5', '8000000.00', '3', '1', '1', '100', '3', '2015', '2015-0109', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1292', 'Rehabilitación de la Unidad Educativa Estadal José Antonio Anzoategui, municipio Piar del estado Bolivar.', '3', '54', '1647295.42', '3', '1', '1', '100', '2', '2015', '2015-0110', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1293', 'Rehabilitación de Casa de los Niños María Gutterman, sector El Perú, parroquia Agua Salada, Municipio Heres, estado Bolívar.', '1', '5', '2564581.48', '3', '1', '1', '100', '3', '2015', '2015-0111', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1294', 'Rehabilitación del Centro de Educación Inicial Trapichito, sector Vista al Sol, parroquia Vista al Sol, municipio Caroní, estado Bolívar.', '2', '17', '3547211.77', '3', '1', '1', '100', '3', '2015', '2015-0112', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1295', 'Rehabilitación del Centro de Educación Inicial Roraima, UD-145, parroquia Dalla Costa, municipio Caroní, estado Bolívar', '2', '31', '1525117.04', '3', '1', '1', '100', '3', '2015', '2015-0113', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1296', 'Rehabilitación del Centro de Educación Inicial La Sapoarita, parroquia La Sabanita, municipio Heres, estado Bolívar.', '1', '6', '3809429.13', '3', '1', '1', '100', '3', '2015', '2015-0114', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1297', 'Mantenimiento y reparación de la toma en Chiripón, municipio Piar, estado Bolívar.', '3', '54', '10000000.00', '1', '1', '1', '100', '1', '2015', '2015-0115', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1298', 'Complemento para la Rehabilitación de la Planta de Tratamiento Cupapuicito, municipio Piar, estado Bolívar. ', '3', '54', '5000000.00', '1', '1', '1', '100', '1', '2015', '2015-0116', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1299', 'Rehabilitación de la Avenida Leopoldo Sucre Figarella, municipio Caroní, estado Bolívar.', '2', '18', '45000000.00', '2', '1', '1', '100', '3', '2015', '2015-0117', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1300', 'Rehabilitación y Mejoras del Parador Turístico Guayana, municipio Caroní, estado Bolívar.', '2', '10', '3000000.00', '7', '1', '1', '100', '2', '2015', '2015-0118', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1301', 'Rehabilitación y Mejoras de calle principal de Barrio Obrero y Mosquitón, municipio El Callao, estado Bolívar.', '10', '49', '3000000.00', '2', '1', '1', '100', '2', '2015', '2015-0119', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1302', 'Construcción de Drenajes Superficiales, para la Recolección de Agua Pluviales, en la vía El Pao con calle Bolívar de 23 de Enero, parroquia Chirica del municipio Caroní, estado Bolívar.', '2', '12', '5000000.00', '5', '1', '1', '100', '2', '2015', '2015-0120', '2017-01-09 16:48:47', '2017-01-09 16:48:47', null);
INSERT INTO `proyectos` VALUES ('1303', 'Rehabilitación de las vías de accesos y Opración a la Planta de Asfalto Piar, municipio Piar, estado Bolívar.', '3', '54', '6000000.00', '2', '1', '1', '100', '18', '2015', '2015-0121', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1304', 'Alimentos Bolívar, Unidad Socialista destinada a la preservación, comercialización a precios socialistas de alimentos de primera calidad, Municipio Heres, estado Bolívar (Rehabilitación de galpón y Adquisición de equipos)', '1', '1', '8932068.40', '7', '1', '1', '100', '8', '2015', '2015-0126', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1305', 'Consolidación del Centro Agrícola Integral Ezequiel Zamora, municipio Heres del estado Bolívar.', '1', '8', '25928846.80', '7', '1', '1', '100', '16', '2015', '2015-0127', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1306', 'Adecuación y Mejoras para el Equipamiento del Centro Lácteo de Upata en el municipio Piar del estado Bolívar.', '3', '54', '3700000.00', '7', '1', '1', '100', '16', '2015', '2015-0128', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1307', 'Culminación de la Sede del Adulto Mayor ubicada en la UD-245, parroquia Universidad, municipio Caroní, estado Bolívar. (II Etapa)', '2', '18', '1032011.39', '6', '1', '1', '100', '3', '2015', '2015-0129', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1308', 'Instalación de Tuberías en el sector Santa Rosa, municipio Piar, estado Bolívar.', '3', '23', '5000000.00', '1', '2', '1', '85', '1', '2015', '2015-0130', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1309', 'Rehabilitación de la Electrificación Conjunto Residencial Bicentenario UD-325, sector Paratepuy, parroquia Unare, municipio Caroní, estado Bolívar.', '2', '10', '2213361.00', '5', '1', '1', '100', '3', '2015', '2015-0131', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1310', 'Reparaciones de Maquinarias Pesadas para el estado Bolívar.', '12', '53', '6000000.00', '5', '1', '1', '100', '2', '2015', '2015-0132', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1311', 'Construcción de Aulas en el Preescolar Nicolas Antonio Farreras, municipio El Callao del estado Bolívar.', '10', '49', '1000000.00', '3', '1', '1', '100', '3', '2015', '2015-0133', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1312', 'Ampliación de Aulas en la Universidad Nacional Experimental de Guayana (UNEG), extensión Caicara, municipio Cedeño, estado Bolívar.', '4', '30', '6000000.00', '3', '1', '1', '100', '3', '2015', '2015-0134', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1313', 'Rehabilitación y Mejoras del Salón de Sesiones del Palacio Legislativo en el municipio Heres del estado Bolívar.', '1', '1', '2147295.42', '5', '1', '1', '100', '2', '2015', '2015-0135', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1314', 'Colocación de Tubería de 6\" de Agua Potable para el sector Cambalache, municipio Caroní, estado Bolívar.', '2', '10', '8000000.00', '1', '1', '1', '100', '1', '2015', '2015-0136', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1315', 'Fortalecimiento de infraestructura, Equipos y Transporte para adecuación de servicios del Instituto de la Vivienda, Obras y Servicios del estado Bolívar.', '12', '53', '1140160.55', '8', '1', '1', '100', '3', '2015', '2015-0137', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1316', 'Rehabilitación y Mejoras de vías de accesos Avenida Nueva Granada, parroquia Vista Hermosa, municipio Heres del estado Bolívar (II Etapa).', '1', '2', '5000000.00', '2', '1', '1', '100', '2', '2015', '2015-0138', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1317', 'Rehabilitación y Mejoras de la Cancha Deportiva ubicado en el Sector Los Coquitos, Parroquia Catedral, Municipio Heres del estado Bolívar.', '1', '1', '3000000.00', '5', '1', '1', '100', '2', '2015', '2015-0139', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1318', 'Instalación y Puesta en Marcha del Centro Piscícola 24 de Julio, municipio Heres del estado Bolívar.', '1', '4', '12000000.00', '7', '2', '1', '75', '16', '2016', '2016-0001', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1319', 'Adecuación del Sistema de Producción Agrícola para la Fabricación de Casas de Cultivo Bajo Ambiente Controlado, municipio Heres del estado Bolívar.', '1', '8', '5000000.00', '7', '2', '1', '75', '16', '2016', '2016-0002', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1320', 'Ampliación del Centro de Educación Inicial Las Teodokildas, parroquia Unare, municipio Caroní del estado Bolívar.', '2', '10', '20000000.00', '3', '1', '1', '100', '3', '2016', '2016-0003', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1321', 'Rehabilitación y Mejoras de Red Eléctrica de Cancha Techada de la UNEXPO, municipio Caroní del estado Bolívar.', '2', '18', '6500000.00', '5', '1', '1', '100', '2', '2016', '2016-0004', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1322', 'Rehabilitación de la Unidad Educativa Estadal José Antonio Anzoátegui, municipio Piar del estado Bolívar. (II Etapa)', '3', '54', '2000000.00', '3', '1', '1', '100', '2', '2016', '2016-0005', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1323', 'Rehabilitación y Mejoras de la Escuela de Música \"Mosensol\", municipio Caroní, estado Bolívar. (II Etapa)', '2', '13', '2500000.00', '3', '1', '1', '100', '2', '2016', '2016-0006', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1324', 'Rehabilitación y Mejoras de la U.E.E. Narciso Fragachán, parroquia Catedral, municipio Heres del estado Bolívar.', '1', '1', '2500000.00', '3', '1', '1', '100', '2', '2016', '2016-0007', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1325', 'Intervención del Sistema de Drenaje para el Alivio de la Cárcava de la Victoria, parroquia Vista Al Sol, municipio Caroní del estado Bolívar (IV Etapa).', '2', '17', '15000000.00', '5', '1', '1', '100', '3', '2016', '2016-0008', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1326', 'Sustitución de Colector de Aguas Servidas de la calle Paz de Once de Abril, San Félix, parroquia Once de Abril, municipio Caroní del estado Bolívar.', '2', '55', '8000000.00', '1', '1', '1', '100', '1', '2016', '2016-0011', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1327', 'Reparaciones de Maquinarias Pesadas para el estado Bolívar.', '12', '53', '74555107.24', '5', '2', '1', '60', '2', '2016', '2016-0012', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1328', 'Rehabilitación y Mejoras de Vía de Acceso a la Carretera Principal Manganeso - El Palmar, municipio Padre Pedro Chien del estado Bolívar.', '11', '51', '25000000.00', '2', '1', '1', '100', '2', '2016', '2016-0013', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1329', 'Rehabilitación de Acueductos Rurales en Maripa, municipio Sucre del estado Bolívar.', '7', '39', '10000000.00', '1', '1', '1', '100', '1', '2016', '2016-0014', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1330', 'Rehabilitación de Acueductos Rurales en Guarataro, municipio Sucre del Estado Bolívar.', '7', '40', '5000000.00', '1', '1', '1', '100', '1', '2016', '2016-0015', '2017-01-09 16:48:48', '2017-01-09 16:48:48', null);
INSERT INTO `proyectos` VALUES ('1331', 'Rehabilitación y Mejoras de Vías de Accesos de la calle Principal de la Esmeralda, municipio Sucre del estado Bolívar.', '7', '41', '3000000.00', '2', '1', '1', '100', '2', '2016', '2016-0020', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1332', 'Rehabilitación y Mejoras de Vías de Accesos de la calle Principal de la Tigrera, municipio Sucre del estado Bolívar.', '7', '40', '3000000.00', '2', '1', '1', '100', '2', '2016', '2016-0021', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1333', 'Rehabilitación y Mejoras de Vías de la Avenida Libertador y Avenida Carabobo, municipio Cedeño del estado Bolívar.', '4', '30', '5000000.00', '2', '1', '1', '100', '2', '2016', '2016-0022', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1334', 'Adecuación y Mejoras de la Planta para la Fabricación de Bloques de Concreto en el sector Cambalache, municipio Caroní del estado Bolívar.', '2', '10', '3000000.00', '5', '1', '1', '100', '8', '2016', '2016-0023', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1335', 'Rehabilitación y Mejoras de Vías de Accesos de la calle Caroní, Cumarebo y Guainiamo del Barrio Cruz Verde, municipio Cedeño del estado Bolívar.', '4', '30', '3100000.00', '2', '1', '1', '100', '2', '2016', '2016-0024', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1336', 'Rehabilitación y Mejoras de Vías de Accesos de la calle Río Bueno, Calle II y IV del Barrio Menca de Leoni, municipio Cedeño del estado Bolívar.', '4', '30', '3000000.00', '2', '1', '1', '100', '2', '2016', '2016-0025', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1337', 'Rehabilitación y Mejoras de Vías de Accesos de la calle Río 16, 18 y 20 del Barrio Caracas, municipio Cedeño del estado Bolívar.', '4', '30', '3000000.00', '2', '1', '1', '100', '2', '2016', '2016-0026', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1338', 'Rehabilitación y Mejoras de Vías de Accesos de calle La Coronel Rondón y El Pedregal del sector El Aeropuerto, municipio Cedeño del estado Bolívar.', '4', '30', '3000000.00', '2', '1', '1', '100', '2', '2016', '2016-0027', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1339', 'Rehabilitación y Mejoras de Vías de Accesos de calle La Plata del Barrio Guainiamo, municipio Cedeño del estado Bolívar.', '4', '26', '2900000.00', '2', '1', '1', '100', '2', '2016', '2016-0028', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1340', 'Rehabilitación del Gimnasio del Centro de Caicara del Orinoco, municipio Cedeño del estado Bolívar.', '4', '30', '10000000.00', '3', '1', '1', '100', '2', '2016', '2016-0029', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1341', 'Rehabilitación de Acueductos Rurales en Moitaco municipio Sucre del estado Bolívar.', '7', '41', '5000000.00', '1', '1', '1', '100', '1', '2016', '2016-0030', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1342', 'Bacheo en la Vía Principal Tramo La Quina, Ciudad Piar, municipio Bolivariano Angostura del estado Bolívar.', '6', '36', '12000000.00', '2', '1', '1', '100', '2', '2016', '2016-0031', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1343', 'Bacheo en la Vía Principal Ciudad Piar El Lindero, municipio Bolivariano Angostura del estado Bolívar.', '6', '36', '13000000.00', '2', '1', '1', '100', '2', '2016', '2016-0032', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1344', 'Rehabilitación y Mejoras en la Unidad Oncológica Virgen del  Valle, parroquia Catedral, municipio Heres del estado Bolívar.', '1', '1', '8000000.00', '4', '1', '1', '100', '2', '2016', '2016-0033', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1345', 'Ornamentación con Sistema de Riego en Sede de Adulto Mayor, parroquia Universidad, municipio Caroní del estado Bolívar.', '2', '18', '20000000.00', '6', '1', '1', '100', '28', '2016', '2016-0034', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1346', 'Drenaje de Lavado de Planta de Tratamiento del Acueducto Oeste de Ciudad Bolívar, parroquia La Sabanita, municipio Heres del estado Bolívar.', '1', '6', '16000000.00', '1', '1', '1', '100', '1', '2016', '2016-0035', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1347', 'Construcción de Cubierta de Techo para Cancha ubicada en el Core 8, parroquia Unare, municipio Caroní del estado Bolívar.', '2', '10', '11470429.30', '3', '1', '1', '100', '3', '2016', '2016-0036', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1348', 'Rehabilitación del Tanque 1 de la Red Acueducto Puerto Ordaz, ubicado en Golfo 9, Cerro Roberto, municipio Caroní del estado Bolívar.', '2', '10', '20000000.00', '1', '1', '1', '100', '1', '2016', '2016-0038', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1349', 'Rehabilitación de Vías de Acceso en la Intersección de la Avenida República con la Avenida Sucre, parroquia Vista Hermosa, municipio Heres del estado Bolívar.', '1', '2', '6804228.58', '2', '1', '1', '100', '2', '2016', '2016-0039', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1350', 'Bacheo en la Avenida Principal de la Urbanización El Perú, parroquia Agua Salada, municipio Heres del estado Bolívar.', '1', '5', '9670479.20', '2', '3', '1', '0', '2', '2016', '2016-0040', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1351', 'Rehabilitación y Mejoras de la Vialidad de la calle Meneses, parroquia José Antonio Páez, municipio Heres del estado Bolívar.', '1', '3', '8000760.35', '2', '1', '1', '100', '2', '2016', '2016-0041', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1352', 'Rehabilitación de Vialidad y Bacheo en la calle Páez, sector Construpatria, parroquia La Sabanita, municipio Heres del estado Bolívar.', '1', '6', '19419249.95', '2', '1', '1', '100', '2', '2016', '2016-0042', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1353', 'Rehabilitación y Mejoras de la Vialidad de la calle La Línea, parroquia José Antonio Páez, municipio Heres del estado Bolívar.', '1', '3', '17049374.75', '2', '1', '1', '100', '2', '2016', '2016-0043', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1354', 'Rehabilitación y Bacheo de las calles internas del Barrio Maipure II Sur, parroquia Marhuanta, municipio Heres del estado Bolívar.', '1', '4', '19419249.95', '2', '1', '1', '100', '2', '2016', '2016-0044', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1355', 'Rehabilitación a Vías de Acceso de la carretera Uchire, desde la Avenida Paseo Caroní hasta la Avenida Norte 3, parroquia Unare, municipio Caroní del estado Bolívar.', '2', '10', '8217825.00', '2', '1', '1', '100', '2', '2016', '2016-0045', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1356', 'Rehabilitación de Vías de Acceso de la carrera Querecure, desde la Avenida Paseo Caroní hasta la Avenida Norte 3, parroquia Unare, municipio Caroní del estado Bolívar.', '2', '10', '9861390.00', '2', '1', '1', '100', '2', '2016', '2016-0046', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1357', 'Rehabilitación a Vías de Acceso de la Avenida Carlos Narváez, UD-146, parroquia Dalla Costa, municipio Caroní del estado Bolívar.', '2', '31', '8217825.00', '2', '1', '1', '100', '2', '2016', '2016-0047', '2017-01-09 16:48:49', '2017-01-09 16:48:49', null);
INSERT INTO `proyectos` VALUES ('1358', 'Rehabilitación de Vialidades en el sector El Roble por Fuera, Vía Principal entre La Laja, parroquia Simón Bolívar, municipio Caroní del estado Bolívar.', '2', '13', '6275430.00', '2', '1', '1', '100', '2', '2016', '2016-0049', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1359', 'Rehabilitación de Vías de Acceso en el sector El Bolsillo, parroquia Chirica, municipio Caroní del estado Bolívar.', '2', '12', '18826290.00', '2', '1', '1', '100', '2', '2016', '2016-0050', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1360', 'Rehabilitación de Vialidad de calle Las Flores, desde la Avenida Manuel Piar a calle Yánez Pinzón, sector Once de Abril, municipio Caroní del estado Bolívar.', '2', '55', '13423363.20', '2', '1', '1', '100', '2', '2016', '2016-0051', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1361', 'Rehabilitación y Mejoras de la Vialidad de la calle Ricaurte, urbanización Kewey II, municipio Gran Sabana del estado Bolívar.', '9', '47', '13000000.00', '2', '3', '1', '0', '2', '2016', '2016-0053', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1362', 'Rehabilitación y Mejoras del Ambulatorio Vista Al Sol, parroquia Vista Al Sol, municipio Caroní del estado Bolívar.', '2', '17', '12000000.00', '4', '1', '1', '100', '2', '2016', '2016-0055', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1363', 'Instalación de Tubería de Aducción en Tumeremo, municipio Sifontes del estado Bolívar.', '5', '52', '20000000.00', '1', '1', '1', '100', '1', '2016', '2016-0056', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1364', 'Sede del Adulto Mayor en la UD-245, municipio Caroní del estado Bolívar.', '2', '18', '160069514.00', '6', '1', '6', '100', '3', '2016', '2016-0057', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1365', 'Mantenimiento y Adquisición de Bombas, Motores, Transformadores, Materiales Eléctricos, Materiales para Mantenimiento de Infraestructura para los Acueductos del estado Bolívar.', '12', '53', '300000000.00', '1', '1', '6', '100', '1', '2016', '2016-0058', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1366', 'Ampliación de Planta de Tratamiento para El Manteco, municipio Piar del estado Bolívar.', '3', '23', '40000000.00', '1', '2', '6', '80', '1', '2016', '2016-0059', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1367', 'Adquisición y Transporte de Sustancias Químicas para los Acueductos del estado Bolívar.', '12', '53', '150000000.00', '1', '1', '6', '100', '1', '2016', '2016-0060', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1368', 'Construcción de Embalse de Reserva en la Mina La Victoria para el Suministro de Agua al Acueducto Puente Blanco, municipio El Callao del estado Bolívar.', '10', '49', '32000000.00', '1', '1', '6', '100', '1', '2016', '2016-0061', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1369', 'Centro de Producción Genético de Ganado Caprino  y Ovino, “Ezequiel Zamora”, municipio Caroní, estado Bolívar (II Etapa).', '1', '8', '15500000.00', '7', '2', '1', '91', '16', '2016', '2016-0071', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1370', 'Rehabilitación y  mejoras del Terminal de pasajero de Upata, municipio Piar, estado Bolívar.', '3', '54', '15000000.00', '5', '2', '1', '95.2', '3', '2016', '2016-0073', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1371', 'Adecuación y mejoras de el mercado municipal de El Callao, municipio El Callao, estado Bolívar.', '10', '49', '12000000.00', '5', '2', '1', '98.2', '3', '2016', '2016-0074', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1372', 'Construcción de Módulos de aulas para el Liceo de Agua Salada, Parroquia Agua Salada, Municipio Heres, estado Bolívar.', '1', '5', '20000000.00', '3', '2', '1', '81.1', '3', '2016', '2016-0076', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1373', 'Adecuación y Mejoras para el Equipamiento del Centro Lácteo de Upata en el municipio Piar del estado Bolívar.', '3', '54', '10000000.00', '7', '1', '1', '100', '16', '2016', '2016-0077', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1374', 'Instalación y Puesta en Marcha del Centro de Producción Piscícola Intensiva en el Centro Agrícola Ezequiel Zamora, en el municipio Heres del estado Bolívar.', '1', '8', '20000000.00', '7', '2', '1', '32', '16', '2016', '2016-0078', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1375', 'Complemento para Sala de Beneficio Industrial Avícola (matadero) en el Centro Agrícola Ezequiel Zamora, municipio Heres del estado Bolívar.', '1', '8', '20000000.00', '7', '2', '1', '84', '16', '2016', '2016-0079', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1376', 'Construcción de Instalaciones para Producción de Semillas Certificadas  y Mejoramiento de Práctica de Manejo de Cultivo, en el Centro Agrícola Ezequiel Zamora, municipio Heres, estado Bolívar (I Etapa).', '1', '8', '80000000.00', '7', '2', '1', '89', '16', '2016', '2016-0080', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1377', 'Adecuación y mejoras del Centro Agrícola Maisanta, municipio caroní del estado Bolívar.', '2', '14', '20000000.00', '7', '2', '1', '63', '16', '2016', '2016-0081', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1378', 'Adecuación y mejoras del sistema de producción avícola en el centro agrícola Ezequiel Zamora, municipio Heres del estado Bolívar.', '1', '8', '40000000.00', '7', '2', '1', '32', '16', '2016', '2016-0082', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1379', 'Consolidación del Centro Agrícola Integral Ezequiel Zamora, municipio Heres del estado Bolívar.', '1', '8', '13000000.00', '7', '1', '1', '100', '16', '2016', '2016-0083', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1380', 'Rehabilitación y Mejoras de la U.E Las Tres Rosas, parroquia Tumeremo, municipio Sifontes del estado Bolívar.', '5', '52', '5000000.00', '3', '1', '1', '100', '2', '2016', '2016-0084', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1381', 'Rehabilitación y Mejoras de U.E.E. Francisco Michelena y Rojas, municipio Gran Sabana, estado Bolívar.', '9', '47', '1500000.00', '3', '1', '1', '100', '2', '2016', '2016-0085', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1382', 'Construcción de Redes de Aguas Servidas en la calle Nueva Esperanza, Sector El Mereyal I, parroquia Catedral,  Municipio Heres, estado Bolívar. ', '1', '1', '8000000.00', '1', '2', '1', '30', '2', '2016', '2016-0088', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1383', 'Rehabilitación y mejoras del Hospital Ruíz y Páez, municipio Heres del estado Bolívar.', '1', '1', '100000000.00', '4', '1', '1', '100', '25', '2016', '2016-0089', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1384', 'Construcción de Obras Electromecánicas del Acueducto Oeste de Ciudad Bolívar, municipio Heres del estado Bolívar.', '1', '6', '74000000.00', '1', '2', '1', '93', '1', '2016', '2016-0090', '2017-01-09 16:48:50', '2017-01-09 16:48:50', null);
INSERT INTO `proyectos` VALUES ('1385', 'Rehabilitación de la Infraestructura del Parque Metropolitano Leonardo Ruiz Pineda, parroquia Vista Hermosa, municipio Heres del estado Bolívar (III Etapa).', '1', '2', '3000000.00', '5', '1', '1', '100', '28', '2016', '2016-0092', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1386', 'Rehabilitación de Infraestructura del Parque Metropolitano La Llovizna, municipio Caroní del estado Bolívar (III Etapa).', '2', '11', '3000000.00', '5', '2', '1', '10', '28', '2016', '2016-0093', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1387', 'Proyecto de Relleno Sanitario Cañaveral, Parroquia Unare, municipio Caroní del estado Bolívar.', '2', '10', '2525730358.17', '5', '2', '6', '96.33', '3', '2016', '2016-0097', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1388', 'Culminación de la Construcción del Acueducto Oeste de Ciudad Bolívar, municipio Heres del estado Bolívar.', '1', '6', '500000000.00', '1', '1', '6', '100', '1', '2016', '2016-0098', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1389', 'Capital Semilla para el Plan de Abastecimiento Estadal según densidad poblacional del estado Bolívar.', '12', '53', '400000000.00', '7', '2', '6', '75', '27', '2016', '2016-0099', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1390', 'Capital Semilla para el Plan de Abastecimiento Estadal según densidad poblacional del estado Bolívar (II Fase).', '12', '53', '500000000.00', '7', '2', '6', '75', '27', '2016', '2016-0101', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1391', 'Culminación de la Construcción del Acueducto Oeste de Ciudad Bolívar, municipio Heres del estado Bolívar.', '1', '6', '249134612.42', '1', '1', '6', '100', '1', '2016', '2016-0102', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1392', 'Reparaciones de Maquinarias Pesadas para el estado Bolívar.', '12', '53', '50000000.00', '5', '2', '1', '20', '2', '2016', '2016-0104', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1393', 'Adecuación y mejoras del sistema de producción avícola en el centro agrícola Ezequiel Zamora, municipio Heres del estado Bolívar.', '1', '8', '10000000.00', '7', '2', '1', '32', '16', '2016', '2016-0106', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1394', 'Complemento para Sala de Beneficio Industrial Avícola (matadero) en el Centro Agrícola Ezequiel Zamora, municipio Heres del estado Bolívar.', '1', '8', '22000000.00', '7', '2', '1', '84', '16', '2016', '2016-0107', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1395', 'Instalación y Puesta en Marcha del Centro de Producción Piscícola Intensiva en el Centro Agrícola Ezequiel Zamora, en el municipio Heres del estado Bolívar.', '1', '8', '18000000.00', '7', '2', '1', '32', '16', '2016', '2016-0108', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1396', 'Adecuación y mejoras del Centro Agrícola Maisanta, municipio caroní del estado Bolívar.', '2', '14', '15000000.00', '7', '2', '1', '63', '16', '2016', '2016-0109', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1397', 'Construcción de Módulos de aulas para el Liceo de Agua Salada, Parroquia Agua Salada, Municipio Heres, estado Bolívar.', '1', '5', '15000000.00', '3', '2', '1', '81.1', '3', '2016', '2016-0110', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1398', 'Ampliación del Centro de Educación Inicial Las Teodokildas, parroquia Unare, municipio Caroní del estado Bolívar.', '2', '10', '15000000.00', '3', '1', '1', '100', '3', '2016', '2016-0111', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1399', 'Ornamentación con Sistema de Riego en Sede de Adulto Mayor, parroquia Universidad, municipio Caroní del estado Bolívar.', '2', '18', '10000000.00', '6', '1', '1', '100', '28', '2016', '2016-0112', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1400', 'Centro de Producción Genético de Ganado Caprino  y Ovino, “Ezequiel Zamora”, municipio Caroní, estado Bolívar (II Etapa).', '1', '8', '27600000.00', '7', '2', '1', '91', '16', '2016', '2016-0113', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1401', 'Rehabilitación y  mejoras del Terminal de pasajero de Upata, municipio Piar, estado Bolívar.', '3', '54', '10000000.00', '5', '2', '1', '95.2', '3', '2016', '2016-0115', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1402', 'Rehabilitación y mejoras de Ambulatorio Tipo III, Guri, municipio Bolivariano Angostura, estado Bolívar (II Etapa).', '6', '38', '20000000.00', '4', '2', '1', '66.25', '3', '2016', '2016-0116', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1403', 'Consolidación del Centro Agrícola Integral Ezequiel Zamora, municipio Heres del estado Bolívar.', '1', '8', '20000000.00', '7', '1', '1', '100', '16', '2016', '2016-0117', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1404', 'Adecuación y mejoras de el mercado municipal de El Callao, municipio El Callao, estado Bolívar.', '10', '49', '12136072.98', '5', '2', '1', '98.2', '3', '2016', '2016-0118', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1405', 'Instalación y Puesta en Marcha del Centro Piscícola 24 de Julio, municipio Heres del estado Bolívar.', '1', '4', '5000000.00', '7', '2', '1', '75', '16', '2016', '2016-0119', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1406', 'Sustitución de Colector de Aguas Servidas de la calle Paz de Once de Abril, San Félix, parroquia Once de Abril, municipio Caroní del estado Bolívar.', '2', '55', '10000000.00', '1', '2', '1', '10', '1', '2016', '2016-0120', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1407', 'Adecuación del Sistema de Producción Agrícola para la Fabricación de Casas de Cultivo Bajo Ambiente Controlado, municipio Heres del estado Bolívar.', '1', '8', '13000000.00', '7', '2', '1', '75', '16', '2016', '2016-0121', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1408', 'Instalación de Tubería de Aducción en Tumeremo, municipio Sifontes del estado Bolívar.', '5', '52', '12000000.00', '1', '1', '1', '100', '1', '2016', '2016-0122', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1409', 'Rehabilitación y Mejoras de la Vialidad de la calle Ricaurte, urbanización Kewey II, municipio Gran Sabana del estado Bolívar.', '9', '47', '12000000.00', '2', '3', '1', '0', '2', '2016', '2016-0123', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1410', 'Rehabilitación del Sistema de Drenaje para Alivio de la Cárcava Cañón del Diablo, parroquia Chirica, municipio Caroní, estado Bolívar.', '2', '12', '110000000.00', '5', '2', '6', '65', '3', '2016', '2016-0124', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1411', 'Rehabilitación y Mejoras de Edificaciones de Alimentos Bolívar C.A.', '12', '53', '2600000.00', '8', '1', '1', '100', '8', '2016', '2016-0125', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1412', 'Rehabilitación y Mejoras del Hospital Arnoldo Gabaldón, municipio Cedeño del estado Bolívar.', '4', '30', '32000000.00', '4', '1', '1', '100', '2', '2016', '2016-0126', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1413', 'Reparación de Ambulancias del Servicio de Emergencia 171 del estado Bolívar.', '12', '53', '25000000.00', '4', '2', '1', '10', '20', '2016', '2016-0128', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1414', 'Rehabilitación y Adecuación  del Terminal de Pasajeros del municipio Roscio, estado Bolívar.', '8', '44', '25000000.00', '5', '1', '1', '100', '3', '2016', '2016-0131', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1415', 'Rehabilitación de la Vía Principal hacia Guasipati Troncal 10, sector la Carata, municipio Roscio, estado Bolívar.', '8', '44', '24000000.00', '2', '1', '1', '100', '2', '2016', '2016-0133', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1416', 'Recuperación y Mejoras del Parque Ferial Agropecuario de Ciudad Bolívar, municipio Heres, estado Bolívar.', '1', '2', '11625563.62', '5', '1', '1', '100', '2', '2016', '2016-0135', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1417', 'Rehabilitación de Cancha Deportiva y Parque Infantil  en la Comunidad de los Hicoteos, parroquia Zea, municipio Heres, estado Bolívar.', '1', '9', '3525000.00', '5', '1', '1', '100', '2', '2016', '2016-0136', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1418', 'Rehabilitación de Cancha Deportiva en la Comunidad del sector la Paragua, parroquia Vista Hermosa, municipio Heres, estado Bolívar.', '1', '2', '3480000.00', '5', '2', '1', '15', '2', '2016', '2016-0137', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1419', 'Construcción y Dotación de la Sede del Adulto Mayor, municipio Caroní, estado Bolívar.', '2', '18', '40000000.00', '6', '1', '1', '100', '4', '2016', '2016-0138', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1420', 'Adecuación y Reparaciones Mayores de Mobiliario Escolar para el Liceo Fernando Peñalver, parroquia Catedral, municipio Heres, estado Bolívar.', '1', '1', '3400000.00', '3', '1', '1', '100', '2', '2016', '2016-0139', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1421', 'Rehabilitación y Mejoras de Módulo de Salud en el Sector Renni Otolina, San Félix, Vía Rio Claro, municipio Caroní, estado Bolívar.', '2', '14', '5000000.00', '4', '1', '1', '100', '3', '2016', '2016-0141', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1422', 'Adecuación y Reparaciones Mayores de Mobiliario Escolar para la U.E. Antonio José de Sucre, parroquia Chirica, municipio Caroní, estado Bolívar.', '2', '12', '3180000.00', '3', '1', '1', '100', '2', '2016', '2016-0142', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1423', 'Sede del Adulto Mayor en la UD-245, municipio Caroní del estado Bolívar.', '2', '18', '100188655.11', '6', '1', '6', '100', '3', '2016', '2016-0143', '2017-01-09 16:48:51', '2017-01-09 16:48:51', null);
INSERT INTO `proyectos` VALUES ('1424', 'Sede del Adulto Mayor en la UD-245, municipio Caroní del estado Bolívar.', '2', '18', '29067313.52', '6', '1', '6', '100', '2', '2016', '2016-0144', '2017-01-09 16:48:52', '2017-01-09 16:48:52', null);
INSERT INTO `proyectos` VALUES ('1425', 'Capital Semilla para el Plan de Abastecimiento Estadal según densidad poblacional del estado Bolívar.', '12', '53', '65000000.00', '7', '2', '6', '50', '29', '2016', '2016-0145', '2017-01-09 16:48:52', '2017-01-09 16:48:52', null);
INSERT INTO `proyectos` VALUES ('1426', 'Capital Semilla para el Plan de Abastecimiento Estadal según densidad poblacional del estado Bolívar.', '12', '53', '35000000.00', '7', '1', '6', '100', '30', '2016', '2016-0146', '2017-01-09 16:48:52', '2017-01-09 16:48:52', null);
INSERT INTO `proyectos` VALUES ('1427', 'Culminación de la Construcción de Drenajes de Aguas Servidas en Las Amazonas, parroquia Unare del estado Bolívar.', '2', '10', '5000000.00', '1', '1', '6', '100', '1', '2014', '2014-0123', '2017-01-09 16:48:52', '2017-01-09 16:48:52', null);
INSERT INTO `proyectos` VALUES ('1428', 'Rehabilitación y Mejoras del Tendido Eléctrico en el Sector La Jirafa, parroquia Catedral, municipio Heres del estado Bolívar.', '1', '1', '10000000.00', '5', '1', '1', '100', '2', '2016', '2016-0091', '2017-01-09 16:48:52', '2017-01-09 16:48:52', null);
INSERT INTO `proyectos` VALUES ('1429', 'Rehabilitación del Tendido Eléctrico del Sector El Platanal, parroquia Vista al Sol, municipio Caroní, estado Bolívar.', '2', '17', '10000000.00', '5', '1', '1', '100', '2', '2016', '2016-0095', '2017-01-09 16:48:52', '2017-01-09 16:48:52', null);
INSERT INTO `proyectos` VALUES ('1430', 'Rehabilitación de Vías de Acceso al Sector 5 de Julio, calle Carabobo, municipio Sifontes del estado Bolívar.', '5', '52', '15088164.00', '2', '1', '1', '100', '2', '2016', '2016-0129', '2017-01-09 16:48:52', '2017-01-09 16:48:52', null);
INSERT INTO `proyectos` VALUES ('1431', 'Rehabilitación y mejoras del Hospital Ruíz y Páez, municipio Heres del estado Bolívar.', '1', '1', '6000000.00', '4', '1', '1', '100', '2', '2016', '2016-0148', '2017-01-09 16:48:52', '2017-01-09 16:48:52', null);
INSERT INTO `proyectos` VALUES ('1432', 'Proyecto de Relleno Sanitario Cañaveral, Parroquia Unare, municipio Caroní del estado Bolívar.', '2', '10', '412900898.73', '5', '3', '6', '0', '3', '2016', '2016-0152', '2017-01-09 16:48:52', '2017-01-09 16:48:52', null);
INSERT INTO `proyectos` VALUES ('1433', 'Culminación de la construcción de Obras Civiles y Eléctromecánicas del Acueducto Oeste de Ciudad Colívar (Acueducto Bicentenario), Parroquia La Sabanita, municipio Heres del estado Bolívar.', '1', '6', '1725164772.65', '1', '3', '6', '0', '1', '2016', '2016-0153', '2017-01-09 16:48:52', '2017-01-09 16:48:52', null);
INSERT INTO `proyectos` VALUES ('1434', 'Rehabilitación y mejora de la U.E. Nelly de Sofía, parroquia Chirica, municipio Caroní del estado Bolívar.', '2', '12', '3536000.00', '3', '3', '1', '0', '2', '2016', '2016-0154', '2017-01-09 16:48:52', '2017-01-09 16:48:52', null);
INSERT INTO `proyectos` VALUES ('1435', 'Rehabilitación y Mejoras del Hospital Arnoldo Gabaldón, municipio Cedeño del estado Bolívar.', '4', '30', '464000.00', '4', '1', '1', '100', '2', '2016', '2016-0155', '2017-01-09 16:48:52', '2017-01-09 16:48:52', null);
INSERT INTO `proyectos` VALUES ('1436', 'Activación de Sistema de Rebombeo Chiripón, municipio Piar del estado Bolívar.', '3', '54', '35000000.00', '1', '1', '1', '100', '1', '2016', '2016-0105', '2017-01-09 16:48:52', '2017-01-09 16:48:52', null);
INSERT INTO `proyectos` VALUES ('1437', 'Rehabilitación y Mejoras de la Cancha Deportiva en la Comunidad de la Esmeralda, municipio Sucre del estado Bolívar.', '7', '41', '3389241.48', '5', '1', '1', '100', '2', '2016', '2016-0130', '2017-01-09 16:48:52', '2017-01-09 16:48:52', null);
INSERT INTO `proyectos` VALUES ('1438', 'Rehabilitación y Mejoras de la Cancha Deportiva en la Comunidad de la Caratica en Tumeremo, municipio Sifontes, estado Bolívar.', '5', '52', '3481932.01', '5', '1', '1', '100', '2', '2016', '2016-0132', '2017-01-09 16:48:52', '2017-01-09 16:48:52', null);
INSERT INTO `proyectos` VALUES ('1439', 'Rehabilitación de Cancha Deportiva en la Casa del Deporte, Tumeremo, municipio Sifontes del estado Bolívar.', '5', '52', '3123826.51', '5', '1', '1', '100', '2', '2016', '2016-0134', '2017-01-09 16:48:52', '2017-01-09 16:48:52', null);
INSERT INTO `proyectos` VALUES ('1440', 'Rehabilitación de la U.E.N. Trapichito II, San Félix, Parroquia Vista al Sol, Municipio Caroní, estado Bolívar.', '2', '17', '12500000.00', '3', '1', '1', '100', '8', '2016', '2016-0147', '2017-01-09 16:48:52', '2017-01-09 16:48:52', null);
INSERT INTO `proyectos` VALUES ('1441', 'Rehabilitación del Módulo B y C de la Planta de Tratamiento los Alacranes, municipio Caroní, estado Bolívar.', '2', '13', '23281804.31', '1', '1', '1', '100', '1', '2016', '2016-0127', '2017-01-09 16:48:52', '2017-01-09 16:48:52', null);
INSERT INTO `proyectos` VALUES ('1442', 'Rehabilitación y Mejoras de Vías de Accesos de la calle Principal de la Tigrera, municipio Sucre del estado Bolívar.', '7', '40', '3500000.00', '2', '3', '1', '0', '2', '2016', '2016-0149', '2017-01-09 16:48:52', '2017-01-09 16:48:52', null);
INSERT INTO `proyectos` VALUES ('1443', 'Rehabilitación y Mejoras de Vías de Accesos de la calle Principal de la Esmeralda, municipio Sucre del estado Bolívar.', '7', '41', '3500000.00', '2', '3', '1', '0', '2', '2016', '2016-0150', '2017-01-09 16:48:52', '2017-01-09 16:48:52', null);
INSERT INTO `proyectos` VALUES ('1444', 'Reparaciones de Maquinarias Pesadas para el estado Bolívar.', '12', '53', '3000000.00', '5', '3', '1', '0', '31', '2016', '2016-0151', '2017-01-09 16:48:52', '2017-01-09 16:48:52', null);

-- ----------------------------
-- Table structure for sector_primario
-- ----------------------------
DROP TABLE IF EXISTS `sector_primario`;
CREATE TABLE `sector_primario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sector_primario
-- ----------------------------
INSERT INTO `sector_primario` VALUES ('1', 'Agua', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `sector_primario` VALUES ('2', 'Infraestructura (Vial)', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `sector_primario` VALUES ('3', 'Educación', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `sector_primario` VALUES ('4', 'Salud', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `sector_primario` VALUES ('5', 'Infraestructura', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `sector_primario` VALUES ('6', 'Social', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `sector_primario` VALUES ('7', 'Agroindustria', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `sector_primario` VALUES ('8', 'Fortalecimiento Institucional', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `sector_primario` VALUES ('9', 'Seguridad', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `sector_primario` VALUES ('10', 'Minería', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);
INSERT INTO `sector_primario` VALUES ('11', 'Turismo', '2017-01-09 00:30:00', '2017-01-09 00:30:00', null);

-- ----------------------------
-- Table structure for sessions
-- ----------------------------
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sessions
-- ----------------------------
INSERT INTO `sessions` VALUES ('3HPomeKXp7EVZRezbOGbTgGBUcCeNB0tPUdcgm4h', '1', '::1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoiV2ZzeFpXTWhEVDE1WkpFT2g5Mm80TUkwNjNPcUo5Ym5idEVnSHBqUiI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czo0MToiaHR0cDovL2xvY2FsaG9zdC9wcm95ZWN0by9wcm95ZWN0b3MvY2FyZ2EiO31zOjk6Il9wcmV2aW91cyI7YToxOntzOjM6InVybCI7czo0MToiaHR0cDovL2xvY2FsaG9zdC9wcm95ZWN0by9wcm95ZWN0b3MvY2FyZ2EiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aToxO3M6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNDgzOTkyNjg2O3M6MToiYyI7aToxNDgzOTg2OTcwO3M6MToibCI7czoxOiIwIjt9fQ==', '1483992701');
INSERT INTO `sessions` VALUES ('HiqueE5rjDJYixY6XBGqC985NtJFxByLRicIVCaH', '1', '::1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoieTFYdXBvTGxRckdFbDNsc3NCeG5Pc3BjRWtuTHJzV2ZvY1dDU0JtRCI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czozMzoiaHR0cDovL2xvY2FsaG9zdC9wcm95ZWN0by9yZXBvcnRlIjt9czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzM6Imh0dHA6Ly9sb2NhbGhvc3QvcHJveWVjdG8vcmVwb3J0ZSI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6NTA6ImxvZ2luX3dlYl81OWJhMzZhZGRjMmIyZjk0MDE1ODBmMDE0YzdmNThlYTRlMzA5ODlkIjtpOjE7czo5OiJfc2YyX21ldGEiO2E6Mzp7czoxOiJ1IjtpOjE0ODM5OTU4NzA7czoxOiJjIjtpOjE0ODM5OTI3OTQ7czoxOiJsIjtzOjE6IjAiO319', '1483995877');

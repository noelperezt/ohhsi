@extends('backend::backend.default')

@section('title', 'Lista de Usuarios')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="red">
                            <h4 class="title">Lista de Usuarios</h4>
                        </div>
                        <div class="card-content table-responsive">
                            <table class="table">
                                <thead class="text-danger">
                                <th>ID</th>
                                <th>Estado</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Correo</th>
                                </thead>
                                <tbody>
                                 @foreach($users as $user)
                                    <tr>
                                        <td>{!! $user['id']!!}</td>
                                        <td>{!! $user['state']!!}</td>
                                        <td>{!! $user['name']!!}</td>
                                        <td>{!! $user['lastname']!!}</td>
                                        <td>{!! $user['email']!!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <a class="btn btn-primary pull-right" href="back/create" role="button">Crear Usuario</a>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

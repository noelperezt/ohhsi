<div class="sidebar" data-image="../assets/img/sidebar-1.jpg">
        <div class="logo">
          <a class="navbar-brand home" href="{{url('/')}}" data-animate-hover="bounce">
							<img src="{{asset('public/recursos/img/logo-small.png')}}" alt="Ohh SI logo" class="hidden-xs">
							<img src="{{asset('public/recursos/img/logo-small.png')}}" alt="Ohh SI logo" class="visible-xs"><span class="sr-only">Ohh SI ir a inicio</span>
					</a>
        </div>

        <div class="sidebar-wrapper">
            <ul class="nav">
                <li class="active">
                    <a href="#">
                        <i class="material-icons" style="color:#a9afbb">dashboard</i>
                        <p>Panel Administrativo</p>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Configuración
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                                <a href="#">
                                <i class="material-icons">person</i>
                                Usuarios
                            </a>
                        </li>
                        <li><a href="#">Catalogo Productos</a></li>
                        <li><a href="#">Categorías Blog</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Compras
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="compras/proveedores">
                                <i class="material-icons">person</i>
                                Proveedores
                            </a>
                        </li>
                        <li><a href="#">Orden de Compras</a></li>
                        <li><a href="#">Emisión de Pago a Proveedores</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Ventas
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Publicar Productos</a></li>
                        <li><a href="#">Productos en Venta</a></li>
                        <li><a href="#">Calificaciones de Productos</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Inventario
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Recepción de Mercancía</a></li>
                        <li><a href="#">Despacho</a></li>
                        <li><a href="#">Ajuste de Inventario</a></li>
                    </ul>
                </li>
                <li>
                  <a href="{{ url(Config::get('admin.prefix').'/login/salir') }}">
                    <i class="icon-logout"></i> Salir
                  </a>
                </li>
            </ul>
        </div>
    </div>

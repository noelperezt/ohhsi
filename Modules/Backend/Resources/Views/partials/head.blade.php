
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="{{asset('public/recursos/bootstrap/css/bootstrap.css')}}" rel="stylesheet"/>

    <!--  Material Dashboard CSS    -->
    <link href="{{asset('public/recursos/material/css/material-dashboard.css')}}" rel="stylesheet"/>

    <!-- Custom Style CSS     -->
    <link href="{{asset('public/recursos/css/estilo.css')}}" rel="stylesheet"/>

    <!--     Fonts and icons     -->
    <link href="{{asset('public/recursos/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>

<!--   Core JS Files   -->
<script src="{{asset('public/recursos/js/jquery-3.1.1.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/recursos/bootstrap/js/bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset('public/recursos/material/js/material.min.js')}}" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="../assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="../assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Material Dashboard javascript methods -->
<script src="{{asset('public/recursos/material/js/material-dashboard.js')}}"></script>

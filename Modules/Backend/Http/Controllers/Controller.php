<?php namespace Modules\Backend\Http\Controllers;

use Config;
use Storage;
use Illuminate\Filesystem\Filesystem;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController {
	protected $patch_js = [
		'public/js',
		'public/plugins',
		'Modules/Backend/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'Modules/Backend/Assets/css',
	];
}
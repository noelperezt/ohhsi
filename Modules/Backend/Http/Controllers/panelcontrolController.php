<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class panelcontrolController extends Controller
{
     public function index()
    {
        $users = [
            '0' => [
                'id'       => '0',
                'state'    => 'activo',
                'name'     => 'josefina',
                'lastname' => 'gonzalez',
                'email'    => 'fifina91@gmail.com',
                'password' => '1234'
            ],
            '1' => [
                'id'       => '1',
                'state'    => 'activo',
                'name'     => 'angel',
                'lastname' => 'bejarano',
                'email'    => 'abejarano@gmail.com',
                'password' => '1234'
            ],
            '2' => [
                'id'       => '2',
                'state'    => 'activo',
                'name'     => 'oscar',
                'lastname' => 'garcia',
                'email'    => 'okkg@gmail.com',
                'password' => '1234'
            ],
            '3' => [
                'id'       => '3',
                'state'    => 'activo',
                'name'     => 'moises',
                'lastname' => 'serrano',
                'email'    => 'moycs777@gmail.com',
                'password' => '1234'
            ]
        ];
        return view('backend::panelcontrol', compact('users'));
    }

    /**
     * Create a user
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        //return "crear";
        return view('backend::create');
    }

    /**
     * Store a user
     *
     * @param Request $request
     * @return array
     */
    public function store(Request $request)
    {
        User::create($request->all());
        return 'succes';
        return $request->all();
    }
}

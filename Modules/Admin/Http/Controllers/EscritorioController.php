<?php

namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Http\Controllers\Controller;

class EscritorioController extends Controller {
	public $autenticar = false;

	protected $titulo = 'Escritorio';

	public function __construct() {
		parent::__construct();

		$this->middleware('Authenticate');
	}

	public function getIndex() {
		if ($this->permisologia('ingresos/graficos') && $this->permisologia('proyectos/escritorio')) {
			return redirect()->route('panelControl');
		//	return $this->view('backend::panelcontrol');
		} elseif ($this->permisologia('ingresos/graficas/inventario')) {
			return redirect()->route('panelControl');

			//return Redirect('ingresos/graficas/inventario');
		} elseif ($this->permisologia('proyectos/escritorio')) {
			return redirect()->route('panelControl');
			
			//return Redirect('proyectos/escritorio');
		}
	}
}

@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.ubicacion', ['ubicacion' => ['Escritorio']])

	<div class="row" style="padding-top: 100px;">
		@if ($controller->permisologia('ingresos/graficas/inventario'))
		<div class="col-md-6 text-center">
			<a class="btn btn-primary btn-lg" href="{{ url('ingresos/graficas/inventario') }}" role="button">Inventario de Alimentos</a>
		</div>
		@endif
		@if ($controller->permisologia('proyectos/escritorio'))
		<div class="col-md-6 text-center">
			<a class="btn btn-primary btn-lg" href="{{ url('proyectos/escritorio') }}" role="button">Sistema de Proyecto</a>
		</div>
		@endif
	</div>
@endsection
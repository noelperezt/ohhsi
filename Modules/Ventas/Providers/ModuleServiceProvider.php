<?php

namespace Modules\Ventas\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'ventas');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'ventas');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'ventas');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}

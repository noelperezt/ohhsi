<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClienteDireccionEnvio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('cliente_direccion_envio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_contacto');
            $table->string('pais');
            $table->string('direccion1');
            $table->string('direccion2');
            $table->string('ciudad');
            $table->string('estado');
            $table->string('codigo_postal');
            $table->string('telefono');
            $table->char('principal');

            $table->integer('clientes_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('clientes_id')->references('id')->on('clientes')
                ->onDelete('cascade')
                ->onUpdate('cascade');
                
                       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

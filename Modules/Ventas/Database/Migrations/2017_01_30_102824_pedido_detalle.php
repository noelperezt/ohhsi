<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PedidoDetalle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido_detalle', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_pedido');
            $table->string('cantidad');
            $table->string('precio');
         

            $table->integer('clientes_id')->unsigned();
            $table->integer('productos_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('clientes_id')->references('id')->on('clientes')
                ->onDelete('cascade')
                ->onUpdate('cascade'); 

            $table->foreign('productos_id')->references('id')->on('productos')
                ->onDelete('cascade')
                ->onUpdate('cascade');
                
                       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pedido extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_pedido');
            $table->string('monto_total');
            $table->string('estado');//pendiente,abnulado,despachado
         

            $table->integer('clientes_id')->unsigned();
            $table->integer('cliente_direccion_envio_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('clientes_id')->references('id')->on('clientes')
                ->onDelete('cascade')
                ->onUpdate('cascade'); 
                
            $table->foreign('cliente_direccion_envio_id')->references('id')->on('cliente_direccion_envio')
                ->onDelete('cascade')
                ->onUpdate('cascade');
                
                       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

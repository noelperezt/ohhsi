var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		'limpiar' : function(){
			tabla.fnDraw();
		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [{"data":"nombre","name":"nombre"},{"data":"rut","name":"rut"},{"data":"direccion","name":"direccion"},{"data":"telefono","name":"telefono"},{"data":"correo","name":"correo"},{"data":"fecha registro","name":"fecha registro"},{"data":"activo","name":"activo"}]
	});
	
	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});
});
@extends(isset($layouts) ? $layouts : 'admin::layouts.default')

@section('content-top')
	@include('admin::partials.botonera')

	@include('admin::partials.ubicacion', ['ubicacion' => ['Proveedores'] ])
	<?php
		$columnas=[
			'Nombre' => '14.285714285714',
			'Rut' => '14.285714285714',
			'Direccion' => '14.2857142857s14',
			'Telefono' => '14.285714285714',
			'Correo' => '14.285714285714',
			'Fecha Registro' => '01/02/2017',
			'Activo' => '14.285714285714'
		];
	 ?>
		@include('admin::partials.modal-busqueda',$columnas)
@endsection

@section('content')
	<div class="row">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
			{!! $Proveedores->generate() !!}
		{!! Form::close() !!}
	</div>
@endsection

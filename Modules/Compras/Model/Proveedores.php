<?php
namespace Modules\Compras\Model;

use Modules\Admin\Model\Modelo;

class Proveedores extends modelo
{
	protected $table = 'proveedores';
    protected $fillable = ["nombre","rut","direccion","telefono","correo","fecha_registro","activo"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Proveedores',
        'required' => true
    ],
    'rut' => [
        'type' => 'text',
        'label' => 'Rut',
        'placeholder' => 'Rut del Proveedores',
        'required' => true
    ],
    'direccion' => [
        'type' => 'text',
        'label' => 'Direccion',
        'placeholder' => 'Direccion del Proveedores',
        'required' => true
    ],
    'telefono' => [
        'type' => 'text',
        'label' => 'Telefono',
        'placeholder' => 'Telefono del Proveedores',
        'required' => true
    ],
    'correo' => [
        'type' => 'text',
        'label' => 'Correo',
        'placeholder' => 'Correo del Proveedores',
        'required' => true
    ],
    'fecha_registro' => [
        'type' => 'date',
        'label' => 'Fecha_registro',
        'placeholder' => 'Fecha de registro del Proveedor',
        'required' => true
    ],
    'activo' => [
        'type' => 'checkbox',
        'label' => 'Activo',
        'placeholder' => 'Activo del Proveedores',
        'required' => true
    ]
];

    public function __construct(array $attributes = array())
	{
		parent::__construct($attributes);

	}
}

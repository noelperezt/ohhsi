<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecepcionCompraDetalle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('recepcion_compra_detalle', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cantidad_recibida');
            $table->string('monto');
            $table->string('precio_venta');

            $table->integer('producto_atributos_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('producto_atributos_id')->references('id')->on('producto_atributos')
                ->onDelete('cascade')
                ->onUpdate('cascade');
                
                       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

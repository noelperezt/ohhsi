<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CuentasPagar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('cuentas_pagar', function (Blueprint $table) {
            $table->increments('id');
            $table->string('concepto');
            $table->date('fecha_registro');
            $table->date('fecha_vencimiento');
            $table->string('monto');
            $table->string('saldo');
            $table->string('estado');

            $table->integer('proveedores_id')->unsigned();
            $table->integer('compras_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('proveedores_id')->references('id')->on('proveedores')
                ->onDelete('cascade')
                ->onUpdate('cascade');
                
            $table->foreign('compras_id')->references('id')->on('compras')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

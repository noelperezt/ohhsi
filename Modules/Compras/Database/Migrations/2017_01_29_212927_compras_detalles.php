<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComprasDetalles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compras_detalles', function (Blueprint $table) {
            //Se sujiere sin id

            $table->string('cantidad');
            $table->string('monto');

            $table->integer('compras_id')->unsigned();
            $table->integer('productos_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('compras_id')->references('id')->on('compras')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            
            $table->foreign('productos_id')->references('id')->on('productos')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Compras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compras', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_registro');
            $table->string('observaciones');
            $table->string('monto_flete')->default(0);
            $table->string('monto_total')->default(0);
            $table->string('modalidad_pago');

            $table->integer('proveedores_id')->unsigned();
            $table->integer('usuario_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('proveedores_id')->references('id')->on('proveedores')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('usuario_id')->references('id')->on('app_usuario')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

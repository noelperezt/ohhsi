<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Abonos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('abonos', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_pago');
            $table->string('monto');

            $table->integer('pagos_id')->unsigned();
            $table->integer('cuentas_pagar_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('pagos_id')->references('id')->on('pagos')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            
            $table->foreign('cuentas_pagar_id')->references('id')->on('cuentas_pagar')
                ->onDelete('cascade')
                ->onUpdate('cascade');
                       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

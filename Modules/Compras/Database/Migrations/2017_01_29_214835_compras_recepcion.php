<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComprasRecepcion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compras_recepcion', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_recepcion');
            $table->string('monto_flete');
            $table->string('monto_total');
            $table->string('porcentaje_ganancia');

            $table->integer('compras_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('compras_id')->references('id')->on('compras')
                ->onDelete('cascade')
                ->onUpdate('cascade');

                        

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'compras'], function () {
  Route::get('', function () {
      dd('compras module index page.!');
  });
    //dd('This is the Compras module index page. Build something great! g');
    Route::group(['prefix' => 'proveedores'], function () {
      Route::get('/', 'ProveedoresController@index' );
      Route::get('datatable', 'ProveedoresController@datatable' );
      //Rutas
      // Route::get('/', 				'ProveedoresController@index');
      // Route::get('buscar/{id}', 		'ProveedoresController@buscar');
      Route::post('guardar', 			'ProveedoresController@guardar');
      // Route::put('guardar/{id}', 		'ProveedoresController@guardar');
      // Route::delete('eliminar/{id}', 	'ProveedoresController@eliminar');
      // Route::post('restaurar/{id}', 	'ProveedoresController@restaurar');
      // Route::delete('destruir/{id}', 	'ProveedoresController@destruir');
      // Route::get('arbol', 			'ProveedoresController@arbol');
      // Route::get('datatable', 		'ProveedoresController@datatable');
    });

});

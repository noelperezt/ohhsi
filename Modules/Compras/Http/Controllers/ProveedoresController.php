<?php

namespace Modules\Compras\Http\Controllers;

//Controlador Padre
use Modules\Compras\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;

//Request
use Modules\Compras\Http\Requests\ProveedoresRequest;

//Modelos
use Modules\Compras\Model\Proveedores;

class ProveedoresController extends Controller {
	protected $titulo = 'Proveedores';

	public $librerias = [
		'alphanum',
		'maskedinput',
		'datatables',
	];
	public $js = ['Proveedores'];

	public function index() {
		return $this->view('compras::Proveedores', [
			'Proveedores' => new Proveedores()
		]);
	}

	public function nuevo() {
		$Proveedores = new Proveedores();
		return $this->view('compras::Proveedores', [
			'layouts' => 'admin::layouts.popup',
			'Proveedores' => $Proveedores
		]);
	}

	public function cambiar(Request $request, $id = 0) {
		$Proveedores = Proveedores::find($id);
		return $this->view('compras::Proveedores', [
			'layouts' => 'admin::layouts.popup',
			'Proveedores' => $Proveedores
		]);
	}

	public function buscar(Request $request, $id = 0){
		if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')){
			$Proveedores = Proveedores::withTrashed()->find($id);
		}else{
			$Proveedores = Proveedores::find($id);
		}

		if ($Proveedores){
			return array_merge($Proveedores->toArray(), [
				's' => 's',
				'msj' => trans('controller.buscar')
			]);
		}

		return trans('controller.nobuscar');
	}

	public function guardar(ProveedoresRequest $request, $id = 0){
		//dd($request->all());
		DB::beginTransaction();
		try{
			if ($id === 0){
				$Proveedores = Proveedores::create($request->all());
			}else{
				$Proveedores = Proveedores::find($id);
				$Proveedores->fill($request->all());
				$Proveedores->save();
			}
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return [
			'id' => $Proveedores->id,
			'texto' => $Proveedores->nombre,
			's' => 's',
			'msj' => trans('controller.incluir')
		];
	}

	public function eliminar(Request $request, $id = 0){
		try{
			Proveedores::destroy($id);
		}catch(Exception $e){
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

	public function restaurar(Request $request, $id = 0) {
		try {
			Proveedores::withTrashed()->find($id)->restore();
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.restaurar')];
	}

	public function destruir(Request $request, $id = 0) {
		try {
			Proveedores::withTrashed()->find($id)->forceDelete();
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.destruir')];
	}

	public function datatable(Request $request){
		$sql = Proveedores::select([
			'id', 'nombre', 'rut', 'direccion', 'telefono', 'correo', 'fecha_registro', 'activo', 'deleted_at'
		]);

		if ($request->verSoloEliminados == 'true'){
			$sql->onlyTrashed();
		}elseif ($request->verEliminados == 'true'){
			$sql->withTrashed();
		}

		return Datatables::of($sql)
			->setRowId('id')
			->setRowClass(function ($registro) {
				return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
			})
			->make(true);
	}
}

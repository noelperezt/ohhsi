<?php

namespace Modules\Compras\Http\Requests;

use App\Http\Requests\Request;

class ProveedoresRequest extends Request {
	protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:255'],
		'rut' => ['required', 'min:3', 'max:255'],
		'direccion' => ['required', 'min:3', 'max:255'],
		'telefono' => ['required', 'min:3', 'max:255'],
		'correo' => ['required', 'min:3', 'max:255'],
		'fecha_registro' => ['required', 'date_format:"d/m/Y"'],
		'activo' => ['required']
	];
}

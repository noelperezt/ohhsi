<?php namespace Modules\Pagina\Http\Controllers;

use Config;
use Storage;
use Illuminate\Filesystem\Filesystem;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController {
	protected $patch_js = [
		'public/js',
		'public/plugins',
		'Modules/Pagina/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'Modules/Pagina/Assets/css',
	];

	public $libreriasIniciales = [
		'OpenSans', 'font-awesome', 'simple-line-icons',
		'jquery-easing', 'jquery-migrate', 
		'animate', 'bootstrap', 'bootbox',
		'jquery-cookie',
		'pace', 'jquery-form', 'blockUI', 'jquery-shortcuts', 'pnotify'
	];

	
}
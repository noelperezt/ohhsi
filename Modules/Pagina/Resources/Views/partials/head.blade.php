<meta charset="UTF-8">
<meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="csrf-token" content="{{ csrf_token() }}"/>
<title>@yield('title', 'Ohhsi')</title>

<!-- link to costum styles-->
<link href="{{asset('public/recursos/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('public/recursos/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{asset('public/recursos/css/animate.min.css')}}" rel="styleshee">
<link href="{{asset('public/recursos/css/estilo.css')}}" rel="stylesheet">
<link href="{{asset('public/recursos/css/app.css')}}" rel="stylesheet">
<link href="{{asset('public/recursos/css/style.default.css')}}" rel="stylesheet">

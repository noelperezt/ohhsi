<!--  REDES SOCIALES -->
<div class="span-12" style="background-color: #e42a4d; padding-top: 10px;">
    <div class="img-redes" style="margin: 0 10%; text-align: center; padding-bottom: 10px;">
        <a href=""><img src="{{asset('public/recursos/img/iconfac.png')}}"  alt="facebook" class="icon-redes"></a>
        <a href=""><img src="{{asset('public/recursos/img/iconinsta.png')}}" alt="instagram" class="icon-redes"></a>
        <a href=""><img src="{{asset('public/recursos/img/icontwi.png')}}"   alt="twitter" class="icon-redes"></a>
    </div>
</div>

<div id="footer">
    <div class="container" style="color: #d9edf7">
        <div class="row">
            <div class="col-md-3">
                <h4>Sobre nosotros</h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aliquid dicta fugit harum, ipsam iste modi mollitia nam nemo nesciunt nihil nulla optio quis quos rem sunt tempore unde veritatis!
                </p>
            </div>
            <div class="col-md-3">
                <h4>Informacion</h4>
                <ul>
                    <li><a href="about">About us</a>
                    </li>
                    <li><a href="text.html">Terms and conditions</a>
                    </li>
                    <li><a href="faq.html">FAQ</a>
                    </li>
                    <li><a href="contact.html">Contact us</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <h4>Ultimos tweets</h4>
                <h5>Men</h5>
                <ul>
                    <li><a href="category.html">T-shirts</a>
                    </li>
                    <li><a href="category.html">Shirts</a>
                    </li>
                    <li><a href="category.html">Accessories</a>
                    </li>
                </ul>
                <hr class="hidden-md hidden-lg">
            </div>
            <div class="col-md-3">
                <h4>Contactanos</h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aut deleniti doloribus facilis itaque labore molestiae nemo numquam praesentium velit? Deserunt doloremque facilis maiores maxime molestias nobis optio qui, quidem.
                </p>
            </div>
            <!-- /.col-md-3 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /footer -->

<!DOCTYPE html>
<html>
<head>
	<title>	@yield('titulo')</title>
	@include('pagina::partials.head')
</head>
<body>
	@include('pagina::partials.menu')
	@yield('content')

	@include('pagina::partials.footer')
	@include('pagina::partials.footerBar')
</body>
</html>
